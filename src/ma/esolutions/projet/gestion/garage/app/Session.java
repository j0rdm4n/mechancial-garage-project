/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.app;

import ma.esolutions.projet.gestion.garage.model.User;

/**
 *
 * @author E-solutions
 */
public class Session {
    private static Session theSession = new Session();
    
    private User theCurrentUser;
    private boolean theLoginStatus;
    private boolean theFirstTimeRun = true;
    
    public boolean isTheFirstTimeRun() {
        return theFirstTimeRun;
    }

    public void setTheFirstTimeRun(boolean theFirstTimeRun) {
        this.theFirstTimeRun = theFirstTimeRun;
    }

    public void setTheCurrentUser(User user) {
        this.theCurrentUser = user;
    }
    public User getTheCurrentUser()
    {
        return theCurrentUser;
    }
    public void setTheLoginStatus(boolean theLoginStatus) {
        this.theLoginStatus = theLoginStatus;
    }

    public boolean isTheLoginStatus() {
        return theLoginStatus;
    }
    
    private Session(){
        
    }
    public static Session getSession()
    {
        return theSession;
    }
    
}
