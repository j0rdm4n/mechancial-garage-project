/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.app;

import com.alee.laf.WebLookAndFeel;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javax.swing.SwingUtilities;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;

import ma.esolutions.projet.gestion.garage.view.ApplicationView;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS16;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.IMAGES;
import ma.esolutions.projet.gestion.garage.view.SplashView;

/**
 *
 * @author Ahmed 
 */
public class Application extends ApplicationController {
    private static final String APPVERSION = "1.0";
    private static final String APPNAME = "Gestion De Garage"; 
    static private SplashView theSplashView;
   // static private ConnectionView theConnectionView;
    
    public Application() {
        super();
       
    }
    public static void main(String[] args)
    {
         theSplashView = new SplashView("Gestion Du Garage",ICONS16+"app.png",IMAGES+"splash.png");
         theSplashView.setVisible(true);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        
        SwingUtilities.invokeLater(()->{
            theAppController = new Application(); 
            theAppController.setSplashView(theSplashView);
            theAppController.run();
            
        });
    }
    public static String getAppName()
    {
        return APPNAME;
    }
    public static String getAppVersion()
    {
        return APPVERSION;
    }
            
    private static void runApp()
    {
        //Le Modéle 
        //ApplicationModel model = new ApplicationModel();
        //La vue :contient une réference vers le ApplicationModel
       // ApplicationView view  = new ApplicationView(model);
        //Le controlleur :contient une reference vers le modéle et une reference vers la vue
        
        //ApplicationController controller = new ApplicationController(model,view);
        // WebLookAndFeel.install ();
        
    }
    
}
