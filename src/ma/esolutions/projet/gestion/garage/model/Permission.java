/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 *
 * @author E-solutions
 */
@Entity
@Table(name="permissions")
public class Permission implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    
    private Long id;
    
    
    
    
    
    
    
   @Column(name="colThePermissionName")
   private String thePermissionName;
   
   @Column(name="colThePermissionCode")
   private int thePermissionCode;
 @ManyToMany(targetEntity=Role.class,fetch=FetchType.EAGER)
  @JoinColumn(name="colTheRoleID", nullable=true, insertable=true, updatable=true)
   private List<Role> theRelatedRoles = new ArrayList<Role>();   
    public int getThePermissionCode() {
        return thePermissionCode;
    }
   
    public void setThePermissionCode(int code)
    {
        this.thePermissionCode = code;
        
    }
  
    public void add(Permission p){
        
    }
    public void delete(){
        
    }
    public void assignPermission(Role role){
        
    }
    public void deletePermission(Role role)
    {
        
    }
    public Set<Permission> listAllPermissions()
    {
        return null;
    }
  

    public String getThePermissionName() {
        return thePermissionName;
    }

    

    public Permission setThePermissionName(String thePermisionName) {
        this.thePermissionName = thePermisionName;
        return this;
    }
 public static Set<Permission> all()
 {
   Set<Permission> permissions = new HashSet<Permission>();
   //Permission p = new Permission();
   //p.setThePermissionCode(PermissionsCodes.PERMISSION_CREATE_USERS);
   //permissions.add(p);
  /* permissions.add(new Permission().setThePermissionCode(PERMISSION_CREATE_USERS).setThePermissionName("Créer Un Utilisateur"));
   permissions.add(new Permission().setThePermissionCode(PERMISSION_DELETE_USERS ).setThePermissionName("Supprimer Un Utilisateur"));
   permissions.add(new Permission().setThePermissionCode(PERMISSION_ASSIGN_ROLES).setThePermissionName("Assigner Un Role"));
   permissions.add(new Permission().setThePermissionCode(PERMISSION_ADD_SALES).setThePermissionName("Ajouter Les Ordres De Ventes"));
   permissions.add(new Permission().setThePermissionCode(PERMISSION_ADD_PURCHASES).setThePermissionName("Ajouter Les Ordres d'Achats"));
   permissions.add(new Permission().setThePermissionCode(PERMISSION_ADD_FIXES).setThePermissionName("Ajouter Les Ordres De Réparations"));
   permissions.add(new Permission().setThePermissionCode(PERMISSION_MODIFY_SALES).setThePermissionName("Metter à Jour Les Ventes"));
   
   permissions.add(new Permission().setThePermissionCode(PERMISSION_MODIFY_PURCHASES ).setThePermissionName("Mettre à Jour Un Achat"));
   permissions.add(new Permission().setThePermissionCode(PERMISSION_MODIFY_FIXES).setThePermissionName("Mettre à Jour Une Réparation"));
   permissions.add(new Permission().setThePermissionCode(PERMISSION_LIST_SALES).setThePermissionName("Affciher Les Ventes"));
   permissions.add(new Permission().setThePermissionCode(PERMISSION_LIST_PURCHASES).setThePermissionName("Afficher Les Achats"));
   permissions.add(new Permission().setThePermissionCode(PERMISSION_LIST_FIXES).setThePermissionName("Afficher Les Réparations"));
   permissions.add(new Permission().setThePermissionCode(PERMISSION_ADD_VENDORS).setThePermissionName("Ajouter Les Fournisseurs"));

   permissions.add(new Permission().setThePermissionCode(PERMISSION_DELETE_VENDORS  ).setThePermissionName("Suprimmer Les Fournisseurs"));
   permissions.add(new Permission().setThePermissionCode(PERMISSION_LIST_VENDORS).setThePermissionName("Afficher Les Fournisseurs"));
   permissions.add(new Permission().setThePermissionCode(PERMISSION_ADD_INVENTORY).setThePermissionName("Ajouter Les Articles"));
   permissions.add(new Permission().setThePermissionCode(PERMISSION_MODIFY_INVENTORY).setThePermissionName("Mettre à Jour les Articles"));
   permissions.add(new Permission().setThePermissionCode(PERMISSION_LIST_INVENTORY).setThePermissionName("Afficher Les Articles"));
   permissions.add(new Permission().setThePermissionCode(PERMISSION_CLOSE_SALES).setThePermissionName("Fermer les Ordres De Ventes"));
  */
   return permissions;
 }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    @Override
    public String toString()
    {
        return this.thePermissionName;
    }
    
}