/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Query;
import javax.persistence.Tuple;
import ma.esolutions.projet.gestion.garage.model.common.AbstractDAO;

/**
 *
 * @author E-solutions
 */
public class PurchaseOrderDAO extends AbstractDAO<PurchaseOrder> {

    public PurchaseOrderDAO() {
        super(PurchaseOrder.class);
    }

    public Map<String, Number> getTotalSalesPerItem() {
        return null;
    }

    public Map<String, Number> getPurchasesPerDayForMonth(int theMonth) {
        String sql = "select DAY(o.theOrderDate) as d,"
                + "       SUM(o.theCost) as totale from PurchaseOrder o WHERE MONTH(o.theOrderDate) = :p_month GROUP BY  DAY(o.theOrderDate)  ";

        Query q = this.getTheEntityManager().createQuery(sql, Tuple.class);
        q.setParameter("p_month", theMonth);
        List<Tuple> result = q.getResultList();
        Map<String, Number> m = new HashMap<>();
        for (int i = 1; i <= 30; i++) {
            m.put("" + i, 0);
        }
        System.out.println("daily data ");
        //initMap(m);
        for (Tuple o : result) {
            int day = o.get(0, Integer.class);
            Number total = o.get(1, Number.class);
            m.put("" + day, total);

        }
        m.forEach((k, v) -> {

            System.out.println("day :  " + k + " total : " + v);
        });
        return m;

    }

    public Map getPurchasesPerYearForPast10Years() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Map getPurchasesPerMonthForYear(int theYear) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<PurchaseOrder> all() {
        String sql ="select p from PurchaseOrder p";
        
        
        return getTheEntityManager().createQuery(sql,PurchaseOrder.class).getResultList();
    }

}
