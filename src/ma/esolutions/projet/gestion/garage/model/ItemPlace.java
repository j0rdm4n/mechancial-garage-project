/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author E-solutions
 */
@Table(name="places")
@Entity
public class ItemPlace implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private Long id;
    
    @Column(name="colTheItemPlaceName")
    private String theItemPlaceName;
    
    public String getTheItemPlaceName() {
        return theItemPlaceName;
    }

    public void setTheItemPlaceName(String theItemPlaceName) {
        this.theItemPlaceName = theItemPlaceName;
    }
    
    @Override
    public String toString()
    {
        return theItemPlaceName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
}
