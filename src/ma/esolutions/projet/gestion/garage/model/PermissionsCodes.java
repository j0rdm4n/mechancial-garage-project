/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model;

/**
 *
 * @author born2code
 */
public abstract class PermissionsCodes extends BaseEntity {
    public static final int PERMISSION_ADD_SALES = 1 ;
    public static final int PERMISSION_ADD_PURCHASES =  2;
    public static final int PERMISSION_ADD_REPAIR =  3;
    public static final int PERMISSION_LIST_SALES =  4;
    public static final int PERMISSION_LIST_PURCHASES =  5;
    public static final int PERMISSION_LIST_REPAIRS = 6;
    public static final int PERMISSION_MODIFY_SALES = 7;
    public static final int PERMISSION_MODIFY_PURCHASES =  8;
    public static final int PERMISSION_MODIFY_FIXES =  9;
    public static final int PERMISSION_CREATE_USERS =  10;
    public static final int PERMISSION_DELETE_USERS =  11;
    public static final int PERMISSION_ASSIGN_ROLES =  12;
    public static final int PERMISSION_ADD_CUSTOMERS = 13;
    public static final int PERMISSION_DELETE_CUSTOMERS = 14;
    public static final int PERMISSION_LIST_CUSTOMERS =  15;
    public static final int PERMISSION_ADD_VENDORS =  16;
    public static final int PERMISSION_DELETE_VENDORS =  17;
    public static final int PERMISSION_LIST_VENDORS = 18;
    public static final int PERMISSION_ADD_INVENTORY = 19;
    public static final int PERMISSION_MODIFY_INVENTORY = 20;
    public static final int PERMISSION_LIST_INVENTORY =  21;
    public static int PERMISSION_CLOSE_SALES =  22;
    public static int PERMISSION_MODIFY_CUSTOMERS = 23;
    public static final int PERMISSION_ALL = 100;
    public static int PERMISSION_LIST_USERS = 24;
    public static int PERMISSION_ADD_ROLE =25;
    public static int PERMISSION_LIST_ROLES =  26;
    public static int PERMISSION_LIST_VEHICLES = 27;
    public static int PERMISSION_ADD_VEHICLE =  28;
    public static int PERMISSION_ADD_RETURN = 29;
    public static int PERMISSION_ADD_PLACE =  30;
    public static int PERMISSION_LIST_PLACES = 31;
    public static  int  PERMISSION_MODIFY_PLACES = 32;
    public static int PERMISSION_LIST_RETURNS =  33;
    public static int PERMISSION_LIST_PAYMENTS =  34;
    public static int PERMISSION_ADD_COMMERCIALPAPER =35;
    public static int PERMISSION_LIST_COMMERCIALPAPER = 36;
    
    
}
