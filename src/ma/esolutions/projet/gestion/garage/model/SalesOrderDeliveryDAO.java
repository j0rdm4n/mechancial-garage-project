/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model;

import javax.persistence.Query;
import ma.esolutions.projet.gestion.garage.model.common.AbstractDAO;

/**
 *
 * @author E-solutions
 */
public class SalesOrderDeliveryDAO extends AbstractDAO<SalesOrderDelivery>{

    public SalesOrderDeliveryDAO() {
        super(SalesOrderDelivery.class);
    }

    public SalesOrderDelivery getBySalesOrderId(Long id) {
        String sql = "select od from SalesOrderDelivery od where od.theSalesOrder.id = :id ";
        
        Query q =this.getTheEntityManager().createQuery(sql, SalesOrderDelivery.class );
        q.setParameter("id", id);
        try
        {
            SalesOrderDelivery d = (SalesOrderDelivery) q.getSingleResult();
            
            return d;
        }catch(Exception e)
        {
            return null;
        }
    
    }
    
}
