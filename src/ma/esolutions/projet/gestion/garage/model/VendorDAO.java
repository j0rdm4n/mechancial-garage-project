/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model;

import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.model.common.AbstractDAO;

/**
 *
 * @author E-solutions
 */
public class VendorDAO extends AbstractDAO<Vendor> {

    public VendorDAO() {
        super(Vendor.class);
    }

    

    public Long ifVendorExists(Vendor v) {
        Vendor r = null;
        TypedQuery q = this.getTheEntityManager().createQuery("select v from Vendor v where v.theVendorName = :name", Vendor.class);
        q.setParameter("name", v.getTheVendorName());
        try
        {
            r = (Vendor)q.getSingleResult();
        }
        catch(NoResultException e)
        {
            return (long)-1;
        }
        return r.getId();
    }

    public List<Vendor> all() {
        TypedQuery<Vendor> q = getTheEntityManager().createQuery("select v from Vendor v", Vendor.class);
        
        return q.getResultList();
    }
    
    
    
}
