/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.model;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.model.common.AbstractDAO;

/**
 *
 * @author E-solutions
 */
public class ItemDAO extends AbstractDAO<Item> {

    public ItemDAO() {
        super(Item.class);
    }

    public double calculateTheItemPrice(int method) {
        double price = 0.0;
        switch (method) {
            case StockMethods.METHOD_LIFO:
                price = getTheLastItemPrice();
                break;
            case StockMethods.METHOD_FIFO:
                price = getTheFirstItemPrice();
                break;
            case StockMethods.METHOD_AVERAGE:
                price = getTheAveragedPrice();
                break;
        }

        return price;
    }

    private double getTheLastItemPrice() {

        return 0.0;
    }

    private double getTheFirstItemPrice() {
        return 0.0;
    }

    private double getTheAveragedPrice() {
        return 0.0;
    }

    public List<Item> all() {
        String sql = "select e from Item e ";
        TypedQuery<Item> query = (TypedQuery<Item>) getTheEntityManager().createQuery(sql);
        System.out.println("the reuslt set " + query.getResultList());
        return query.getResultList();
    }

    public boolean isItemAvailable(Item item, int quantity) {

        return true;

    }

    public void returnItem(Item ee) {
        String sql = "select e from Item e where e.theItemName = :name ";
        TypedQuery<Item> query = (TypedQuery<Item>) getTheEntityManager().createQuery(sql);
        this.getTheEntityManager().getMetamodel().getEntities();
        query.setParameter("name", ee.getTheItemName());
        List<Item> items = query.getResultList();
        if (items.size() == 1) {
            Item ii = items.get(0);
            System.out.println("returned " + ii);
            try {
                ii.setTheItemQuantity(ii.getTheItemQuantity() + ee.getTheItemQuantity());
            } catch (NegativeStockException ex) {
                Logger.getLogger(ItemDAO.class.getName()).log(Level.SEVERE, null, ex);
            } catch (MinimalStockException ex) {
                Logger.getLogger(ItemDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("persisit item with id " + ii.getId() + " q : " + ii.getTheItemQuantity());
            this.update(ii);
        }
    }

    public Map<String, Number> getStockPercentPerItem() {

        Map<String, Number> m = new HashMap();
        // String sql = "select o.theOrderedItem as item ,sum(o.theQuantity * o.theOrderedItem.theUnitPrice) from SalesOrderLine o GROUP BY o.theOrderedItem";
        Query q
                = getTheEntityManager().createQuery("select it1.theItemName ,it1.theItemQuantity from Item it1", Tuple.class);

        List<Tuple> l = q.getResultList();
        double totals = 0.0;
        // System.out.println("the sum of stock " + it1.get());
        for (Tuple o : l) {
            String itemName = o.get(0, String.class);
            Number qq;
            qq = o.get(1, Number.class);

           // Number total = o.get(2,Number.class);
            // double percent = (double)((double)total * 100)/totals;
            DecimalFormat df = new DecimalFormat();
            df.setMaximumFractionDigits(1);
            System.out.println("item name " + itemName + " q " + qq);
            totals += (long) (o.get(1, Number.class));
        }
        System.out.println("total items " + totals);
        for (Tuple o : l) {

            m.put(o.get(0, String.class), (long) (o.get(1, Number.class)) / totals);

        }
        return m;

    }

    public Map<String, Number> getStockQuantityPerItem() {
        Map<String, Number> m = new HashMap();
        Query q
                = getTheEntityManager().createQuery("select it1.theItemName ,it1.theItemQuantity from Item it1", Tuple.class);

        List<Tuple> l = q.getResultList();
        double totals = 0.0;
        // System.out.println("the sum of stock " + it1.get());
        for (Tuple o : l) {
            String itemName = o.get(0, String.class);
            Number qq;
            qq = o.get(1, Number.class);
            m.put(o.get(0, String.class), (long) (o.get(1, Number.class)));
           // System.out.println(o.get(0));
        
        }
     
        return m;

    }

    public boolean isItemWithSameNameAlreadyExists(String name) {
        
        try
        {
        Query q = getTheEntityManager().createQuery("select it from Item it where it.theItemName = :iname ",Item.class);
        q.getSingleResult();
        }catch(Exception e)
        {
            System.out.println("item with this name already exists ");
            return true;
        }
        
        return false;
    }

}
