/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.model.common.AbstractDAO;

/**
 *
 * @author born2code
 */
public class UserDAO extends AbstractDAO<User>{
    private RoleDAO roleDAO;
    private PermissionDAO permissionDAO;

    public UserDAO() {
        super(User.class);
    }
    public User superUser(){
        return null;
    }
    public  User findByUsernameAndPassword(String username,String password){
    Query q = getTheEntityManager().createQuery("select u from User u where u.theUsername=:username and u.thePassword = :password");
        q.setParameter("username", username);
        q.setParameter("password",password);
        try
        {
            User user = (User)q.getSingleResult();
            System.out.println(" find by username and password" + user.getTheUsername() + " " + user.getThePassword());
    
            if(username.equalsIgnoreCase(user.getTheUsername()) && 
               password.equalsIgnoreCase(user.getThePassword()))
            {
                return user; 
            }
        }catch(NoResultException e)
        {
            return null;
        }
        return null;
       
        
        
        
    } 
    
    public void createAdminAdmin()
    {
        Role adminRole = new Role();
        
        adminRole.setTheRoleName("Role Admin");
        List<Permission> ps = new ArrayList();
         
        
        permissionDAO = new PermissionDAO();
        ps.add(permissionDAO.getPermissionByCode(PermissionsCodes.PERMISSION_ADD_ROLE));
        ps.add(permissionDAO.getPermissionByCode(PermissionsCodes.PERMISSION_LIST_ROLES));
        ps.add(permissionDAO.getPermissionByCode(PermissionsCodes.PERMISSION_CREATE_USERS));
        ps.add(permissionDAO.getPermissionByCode(PermissionsCodes.PERMISSION_LIST_USERS));
        adminRole.setTheRolePermissions(ps);
        System.out.println("role of admin admin " + adminRole.getTheRolePermissions());
        
        
        
        
        roleDAO = new RoleDAO();
        roleDAO.create(adminRole);
        User admin = new User();
        admin.setTheUsername("admin");
        admin.setThePassword("admin");
        admin.AddRole(adminRole);
        create(admin);
        
    }
    
    public User checkIfUserExists(String username,String password) {
        User user = findByUsernameAndPassword(username,password);
        return user;
    }

    public void makeSuperUser(User user) {
            //Set<Role> roles = user.getTheAttributedRoles();
           // 
        Set<Role> roles = new HashSet<Role>();
        //Role r = Role.createRole(Role.SUPER);
        //RoleDAO roleDAO = new RoleDAO();
      //roleDAO.create(r);
      //  roles.add(r);
       // user.setTheAttributedRoles(roles);
    
    }

  

    public List<User> all() {
        TypedQuery q = this.getTheEntityManager().createQuery("select u from User u", User.class);
        
        return q.getResultList();
    }
    
    
}
