/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author E-solutions
 */
@Entity
@Table(name="repair_payments")
public class RepairInvoicePayment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private long id;
    
    @Column(name="colTheCashingDate")
    private Date theCashingDate;
    
    @Column(name="colTheAmount")
    private double theAmount;
    
    @OneToOne(targetEntity=RepairInvoice.class)
    @JoinColumn(name="colTheInvoiceID", nullable=true, insertable=true, updatable=true)
    private RepairInvoice theRepairInvoice; 

    public RepairInvoice getTheRepairInvoice() {
        return theRepairInvoice;
    }

    public void setTheRepairInvoice(RepairInvoice theRepairInvoice) {
        this.theRepairInvoice = theRepairInvoice;
    }
    
    public long getId()
    {
        return id;
    }
    public void setTheCashingDate(Date date)
    {
        this.theCashingDate = date;
    }
    public void setTheAmount(double amount)
    {
        this.theAmount = amount;
    }
    public Date getTheCashingDate()
    {
        return this.theCashingDate;
    }
    public double getTheAmount()
    {
        return theAmount;
    }

}
