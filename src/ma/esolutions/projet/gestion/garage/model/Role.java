/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 *
 * @author E-solutions
 */

@Entity
@Table(name="roles")
public class Role implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private Long id;

    
    
    @Column(name="colTheRoleName")
    private String theRoleName;
    
    @ManyToMany(targetEntity=Permission.class)
    @JoinColumn(name="colThePermissionID", nullable=true, insertable=true, updatable=true)
    private List<Permission> theRolePermissions;
    
    @ManyToMany(targetEntity=User.class)
    @JoinColumn(name="colTheuserID", nullable=true, insertable=true, updatable=true)
    private List<User> theUsers = new ArrayList<>();
    
    
    public List<User> getTheUsers() {
        return theUsers;
    }

    public void setTheUsers(List<User> theUsers) {
        this.theUsers = theUsers;
    }

    
    
    public Role(){
        
    }

   

    public String getTheRoleName() {
        return theRoleName;
    }

    public List<Permission> getTheRolePermissions() {
        return theRolePermissions;
    }

    public void setTheRoleName(String theRoleName) {
        this.theRoleName = theRoleName;
    }

    public void setTheRolePermissions(List<Permission> theRolePermissions) {
        this.theRolePermissions = theRolePermissions;
    }
    
    @Override
    public String toString()
    {
        return theRoleName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
