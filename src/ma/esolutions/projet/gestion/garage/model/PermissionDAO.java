/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.model.common.AbstractDAO;

/**
 *
 * @author E-solutions
 */
public class PermissionDAO extends AbstractDAO<Permission> {

    public static List<Permission> createPermissions() {
    List<Permission> permissions = new ArrayList<Permission>();
                
                PermissionDAO pDAO = new PermissionDAO();
               
                Permission p = new Permission();
                p.setThePermissionCode(PermissionsCodes.PERMISSION_ADD_CUSTOMERS);
                p.setThePermissionName("ajouter les clients");
                pDAO.create(p);
                permissions.add(p); 
                 p = new Permission();
                p.setThePermissionCode(PermissionsCodes.PERMISSION_ADD_REPAIR);
                p.setThePermissionName("ajouter les réparations");
                pDAO.create(p);
                permissions.add(p); 
                   p = new Permission();
                p.setThePermissionCode(PermissionsCodes.PERMISSION_ADD_INVENTORY);
                p.setThePermissionName("ajouter les articles");
                pDAO.create(p);
                permissions.add(p); 
                   p = new Permission();
                p.setThePermissionCode(PermissionsCodes.PERMISSION_ADD_PURCHASES);
                p.setThePermissionName("ajouter les achats");
                pDAO.create(p);
                    permissions.add(p); 
                
                   p = new Permission();
                p.setThePermissionCode(PermissionsCodes.PERMISSION_ADD_SALES);
                p.setThePermissionName("ajouter les ventes");
                pDAO.create(p);
                    permissions.add(p); 
                
                   p = new Permission();
                p.setThePermissionCode(PermissionsCodes.PERMISSION_ADD_VENDORS);
                p.setThePermissionName("ajouter les fournisseurs");
                pDAO.create(p);
                    permissions.add(p); 
                    
                   p = new Permission();
                p.setThePermissionCode(PermissionsCodes.PERMISSION_ASSIGN_ROLES);
                p.setThePermissionName("assigner les roles");
                pDAO.create(p);
                    permissions.add(p); 
                
                   p = new Permission();
                p.setThePermissionCode(PermissionsCodes.PERMISSION_CLOSE_SALES);
                p.setThePermissionName("fermer les ordres de ventes");
                pDAO.create(p);
                    permissions.add(p); 
                    
                   p = new Permission();
                p.setThePermissionCode(PermissionsCodes.PERMISSION_CREATE_USERS);
                p.setThePermissionName("créer les utilisateurs");
                pDAO.create(p);
                    permissions.add(p); 
                    
                   p = new Permission();
                p.setThePermissionCode(PermissionsCodes.PERMISSION_DELETE_CUSTOMERS);
                p.setThePermissionName("supprimer les clients");
                pDAO.create(p);
                    permissions.add(p); 
                    
                    
                   p = new Permission();
                p.setThePermissionCode(PermissionsCodes.PERMISSION_DELETE_VENDORS);
                p.setThePermissionName("supprimer les fournisseurs");
                pDAO.create(p);
                    permissions.add(p); 
                    
                   p = new Permission();
                p.setThePermissionCode(PermissionsCodes.PERMISSION_LIST_CUSTOMERS);
                p.setThePermissionName("afficher les clients");
                pDAO.create(p);
                    permissions.add(p); 
                    
                   p = new Permission();
                p.setThePermissionCode(PermissionsCodes.PERMISSION_LIST_REPAIRS);
                p.setThePermissionName("afficher les réparations");
                pDAO.create(p);
                    permissions.add(p); 
                    
                   p = new Permission();
                p.setThePermissionCode(PermissionsCodes.PERMISSION_LIST_INVENTORY);
                p.setThePermissionName("afficher les stocks");
                pDAO.create(p);
                    permissions.add(p); 
                    
                    
                   p = new Permission();
                p.setThePermissionCode(PermissionsCodes.PERMISSION_LIST_PURCHASES);
                p.setThePermissionName("afficher les achats");
                pDAO.create(p);
                    permissions.add(p); 
                    
                    
                   p = new Permission();
                p.setThePermissionCode(PermissionsCodes.PERMISSION_LIST_SALES);
                p.setThePermissionName("afficher les ventes");
                pDAO.create(p);
                    permissions.add(p); 
                    
                    
                   p = new Permission();
                p.setThePermissionCode(PermissionsCodes.PERMISSION_LIST_VENDORS);
                p.setThePermissionName("afficher les fournisseurs");
                pDAO.create(p);
                    permissions.add(p); 
                    
                   p = new Permission();
                p.setThePermissionCode(PermissionsCodes.PERMISSION_MODIFY_FIXES);
                p.setThePermissionName("mettre à jour les réparations");
                pDAO.create(p);
                    permissions.add(p); 
                    
                    
                   p = new Permission();
                p.setThePermissionCode(PermissionsCodes.PERMISSION_MODIFY_INVENTORY);
                p.setThePermissionName("mettre à jour les stocks");
                pDAO.create(p);
                    permissions.add(p); 
                    
                   p = new Permission();
                p.setThePermissionCode(PermissionsCodes.PERMISSION_MODIFY_PURCHASES);
                p.setThePermissionName("mettre à jour les achats");
                pDAO.create(p);
                    permissions.add(p); 
                    
                   p = new Permission();
                p.setThePermissionCode(PermissionsCodes.PERMISSION_MODIFY_SALES);
                p.setThePermissionName("mettre à jour les ventes");
                pDAO.create(p);
                    permissions.add(p); 
                 p = new Permission();
                 p.setThePermissionCode(PermissionsCodes.PERMISSION_ADD_COMMERCIALPAPER);
                 p.setThePermissionName("ajouter des effets");
                 pDAO.create(p);
                 p = new Permission();
                 p.setThePermissionCode(PermissionsCodes.PERMISSION_LIST_COMMERCIALPAPER);
                 p.setThePermissionName("afficher les effets");
                 pDAO.create(p);
                 
                 p = new Permission();
                 p.setThePermissionCode(PermissionsCodes.PERMISSION_LIST_PLACES);
                 p.setThePermissionName("afficher les emplacements");
                 pDAO.create(p);
                
                 
                 p = new Permission();
                 p.setThePermissionCode(PermissionsCodes.PERMISSION_ADD_PLACE);
                 p.setThePermissionName("ajouter les emplacements");
                 pDAO.create(p);
                
                 p = new Permission();
                 p.setThePermissionCode(PermissionsCodes.PERMISSION_LIST_PAYMENTS);
                 p.setThePermissionName("afficher les payements");
                  pDAO.create(p);
                
                 p = new Permission();
                 p.setThePermissionCode(PermissionsCodes.PERMISSION_MODIFY_PLACES);
                 p.setThePermissionName("modifer les emplacements");
                  pDAO.create(p);
                
                 p=new Permission();
                 p.setThePermissionCode(PermissionsCodes.PERMISSION_LIST_VEHICLES);
                 p.setThePermissionName("afficher les véhicules ");
                  pDAO.create(p);
                
                 p = new Permission();
                 p.setThePermissionCode(PermissionsCodes.PERMISSION_LIST_USERS);
                 p.setThePermissionName("afficher les utilisateurs ");
                  pDAO.create(p);
                
                 p = new Permission();
                 p.setThePermissionCode(PermissionsCodes.PERMISSION_ADD_ROLE);
                 p.setThePermissionName("ajouter les roles");
                 
                  pDAO.create(p);
                
                 p = new Permission();
                 p.setThePermissionCode(PermissionsCodes.PERMISSION_LIST_ROLES);
                 p.setThePermissionName("afficher les roles");
                 pDAO.create(p);
                
                  
                  return permissions;
    }

    public PermissionDAO() {
        super(Permission.class);
    }
    public List<Permission> all() {

        TypedQuery q = this.getTheEntityManager().createQuery("select p from Permission p",Permission.class);
        System.out.println("getting all permissions " + q.getResultList());
        return q.getResultList();
    }
    public Permission getPermissionByCode(int code)
    {
        TypedQuery q = this.getTheEntityManager().createQuery("select p from Permission p where p.thePermissionCode = :code ",Permission.class);
        q.setParameter("code", code);
        List<Permission> l = q.getResultList();
        if(l.size() >= 1)
            return l.get(0);
        else
            return null;
           
    }
    
}
