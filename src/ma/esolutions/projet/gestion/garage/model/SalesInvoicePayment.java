/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
/**
 *
 * @author E-solutions
 */
@Entity
@Table(name="sales_payments")
public class SalesInvoicePayment implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private long id;
    
    @Column(name="colTheCashingDate")
    private Date theCashingDate;

 
    
    @Column(name="colTheTTCAmount")
    private double theTTCAmount;
    
    @Column(name="colTheHTAmount")
    private double theHTAmount ;

   
    
    @OneToOne(targetEntity=SalesInvoice.class)
    @JoinColumn(name="colTheInvoiceID", nullable=true, insertable=true, updatable=true)
    private SalesInvoice theSalesInvoice= new SalesInvoice();
    
    
    public void setId(long id) {
        this.id = id;
    }
 public double getTheHTAmount() {
        return theHTAmount;
    }

    public void setTheHTAmount(double theHTAmount) {
        this.theHTAmount = theHTAmount;
    }
    public SalesInvoice getTheSalesInvoice() {
        return theSalesInvoice;
    }

    public void setTheSalesInvoice(SalesInvoice theSalesInvoice) {
        this.theSalesInvoice = theSalesInvoice;
    }
    
    
    public long getId()
    {
        return this.id;
    }
    public void setThePaymentDate(Date date)
    {
        this.theCashingDate = date;
    }
    public void setTheTTCAmount(double amount)
    {
        this.theTTCAmount = amount;
    }
    public Date getTheCashingDate()
    {
        return this.theCashingDate;
    }
    public double getTheTTCAmount()
    {
        return theTTCAmount;
    }
}
