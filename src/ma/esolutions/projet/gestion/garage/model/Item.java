/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.model;

import java.io.Serializable;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Persistence;
import javax.persistence.Table;
import javax.swing.JOptionPane;
import ma.esolutions.projet.gestion.garage.view.ApplicationView;

/**
 *
 * @author E-solutions
 */
@Table(name = "stocks")
@Entity
public class Item implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private Long id;

    @Column(name = "colTheName", nullable = false, unique = true)
    private String theItemName;

    @Column(name = "colTheItemQuantity", nullable = false)
    private long theItemQuantity;

    @Column(name = "colTheUnitPrice", nullable = false)
    private double theUnitPrice;

    @Column(name = "colTheGain", nullable = false)
    private double theGain;

    @Column(name = "colTheStocMin")
    private int stockMin;

    public int getStockMin() {
        return stockMin;
    }

    public void setStockMin(int stockMin) {
        this.stockMin = stockMin;
    }

    public double getTheGain() {
        return theGain;
    }

    public void setTheGain(double theGain) {
        this.theGain = theGain;
    }

    @Column(name = "colTheSalesPriceTTC")
    private double theSalesPriceTTC;

    @Column(name = "colTheOldUnitPrice")
    private double theOldUnitPrice;

    public double getTheSalesPriceTTC() {
        return theSalesPriceTTC;
    }

    public void setTheSalesPriceTTC(double theSalesPriceTTC) {
        this.theSalesPriceTTC = theSalesPriceTTC;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "colThePlaceID", nullable = false, insertable = true, updatable = true)
    private ItemPlace theItemPlace;

    @Column(name = "colTheItemAddedDate", nullable = false)
    private Date theItemAddedDate;
    @Column(name = "colTheItemTVA")
    private int theTVA;

    @OneToOne(fetch = FetchType.EAGER)
    private Vendor theVendor;

    public double getTheOldUnitPrice() {
        return theOldUnitPrice;
    }

    public void setTheOldUnitPrice(double theOldUnitPrice) {
        this.theOldUnitPrice = theOldUnitPrice;
    }

    public Vendor getTheVendor() {
        return theVendor;
    }

    public void setTheVendor(Vendor theVendor) {
        this.theVendor = theVendor;
    }

    public int getTheTVA() {
        return theTVA;
    }

    public void setTheTVA(int theTVA) {
        this.theTVA = theTVA;
    }

    public Date getTheItemAddedDate() {
        return theItemAddedDate;
    }

    public void setTheItemAddedDate(Date theItemAddedDate) {
        this.theItemAddedDate = theItemAddedDate;
    }

    public void setTheItemName(String name) {
        theItemName = name;
    }

    public String getTheItemName() {
        return theItemName;
    }

    public long getTheItemQuantity() {
        return theItemQuantity;
    }

    public void setTheItemQuantity(long theItemQuantity) throws NegativeStockException, MinimalStockException {
        if (theItemQuantity < this.theItemQuantity) {
            System.out.println("stock out");
            
        this.theItemQuantity = theItemQuantity;
        System.out.println("the new item quantity " + this.theItemQuantity);
            if ((this.theItemQuantity - theItemQuantity) < this.stockMin) {
                if (stockMin == 0) {
                  //  throw new NegativeStockException();
                    
                }
                else
                {
                    System.out.println("stockmin != 0");
                      //  int showAskYesNo = ApplicationView.showAskYesNo("Cette opération va dépasser le stock minimum,continuer ? ");
                      //  if(showAskYesNo == JOptionPane.NO_OPTION)
                    //        throw new MinimalStockException();
                        
                            
                }
            }
        }
        if (theItemQuantity > this.theItemQuantity) {
            System.out.println("stock in");

        }

    }

    public ItemPlace getTheItemPlace() {
        return theItemPlace;
    }

    public void setTheItemPlace(ItemPlace theItemPlace) {
        this.theItemPlace = theItemPlace;
    }

    public double getTheUnitPrice() {
        return theUnitPrice;
    }

    public void setTheUnitPrice(double price) {
        //avant de determiner le prix final d un item ,utiliser une 
        //methode pour le calculer
        theUnitPrice = price;
    }

    @Override
    public String toString() {
        return this.theItemName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void persist(Object object) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("GestGaragePU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            em.persist(object);
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }
}
