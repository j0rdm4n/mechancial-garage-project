/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author E-solutions
 */
@Table(name="reparations")
@Entity
public class RepairOrder implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private Long id;
    
    @Column(name="colTheStartDate")
    private Date theStartDate;
    
    @Column(name="colTheClosedDate")
    private Date theClosedDate;
    
    @Column(name="colTheStatus")
    private String theStatus = "ouverte";
    
    @ManyToOne(fetch=FetchType.EAGER)
   // @JoinColumn(name="colTheCustomerID", nullable=true, insertable=true, updatable=true)
    private Customer theCustomer;
    
    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="colTheVehicleID", nullable=true, insertable=true, updatable=true)
    private Vehicle theVehicle;
    
    @OneToMany(cascade = {CascadeType.ALL})
    private Set<SalesOrderLine> theOrderLines = new HashSet<SalesOrderLine>();
     
    @Column(name="colThePaymentType")
    private String thePaymentType ="espéces" ;
    
    @Column(name="colTheLaborCost")
    private Long theLaborCost ;
    
    @Column(name="colTheTotal")
    private float theTotal;

    @Column(name="coltheInvoicedStatus")
    private String theInvoicedStatus = "non";
    
        @Column(name="colTheDeliveredStatus")
    private String theDeliveredStatus = "non";

    public String getTheDeliveredStatus() {
        return theDeliveredStatus;
    }

    public void setTheDeliveredStatus(String theDeliveredStatus) {
        this.theDeliveredStatus = theDeliveredStatus;
    }
    
 @Column(name="colThePaymentStatus")
    private String thePaymentStatus = "non";
      public String getThePaymentStatus() {
        return thePaymentStatus;
    }

    public void setThePaymentStatus(String thePaymentStatus) {
        this.thePaymentStatus = thePaymentStatus;
    }
    
    public String getTheInvoicedStatus() {
        return theInvoicedStatus;
    }

    public void setTheInvoicedStatus(String theInvoicedStatus) {
        this.theInvoicedStatus = theInvoicedStatus;
    }
    public float getTheTotalTTC() {
        return theTotal;
    }

    public void setTheTotal(float theTotal) {
        this.theTotal = theTotal;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getTheStartDate() {
        return theStartDate;
    }

    public void setTheStartDate(Date theStartDate) {
        this.theStartDate = theStartDate;
    }

    public Date getTheClosedDate() {
        return theClosedDate;
    }

    public Vehicle getTheVehicle() {
        return theVehicle;
    }

    public void setTheVehicle(Vehicle theVehicle) {
        this.theVehicle = theVehicle;
    }

    public void setTheClosedDate(Date theClosedDate) {
        this.theClosedDate = theClosedDate;
    }

    public String getTheStatus() {
        return theStatus;
    }

    public void setTheStatus(String theStatus) {
        this.theStatus = theStatus;
    }

    public Customer getTheCustomer() {
        return theCustomer;
    }

    public void setTheCustomer(Customer theCustomer) {
        this.theCustomer = theCustomer;
    }

    public Set<SalesOrderLine> getTheOrderLines() {
        return theOrderLines;
    }

    public void setTheOrderLines(Set<SalesOrderLine> theOrderLines) {
        this.theOrderLines = theOrderLines;
    }
    public void addSalesOrderLine(SalesOrderLine s)
    {
        theOrderLines.add(s);
    }
    public String getThePaymentType() {
        return thePaymentType;
    }

    public void setThePaymentType(String thePaymentType) {
        this.thePaymentType = thePaymentType;
    }

    public Long getTheLaborCost() {
        return theLaborCost;
    }

    public void setTheLaborCost(Long theLaborCost) {
        this.theLaborCost = theLaborCost;
    }
    
     
    
}
