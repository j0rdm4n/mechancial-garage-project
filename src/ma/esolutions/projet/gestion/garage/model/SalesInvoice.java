/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author E-solutions
 */
@Entity
@Table(name = "sales_invoices")
public class SalesInvoice implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private long id;
    @OneToOne(targetEntity = Customer.class)
    @JoinColumn(name = "colTheCustomerID", nullable = true, insertable = true, updatable = true)
    private Customer theCustomer;

    public Customer getTheCustomer() {
        return theCustomer;
    }

    public void setTheCustomer(Customer theCustomer) {
        this.theCustomer = theCustomer;
    }

    @Column(name = "colThePayementType")
    private String thePaymentType;

    @OneToOne(targetEntity = SalesOrder.class)
    @JoinColumn(name = "colTheOrderID", nullable = true, insertable = true, updatable = true)
    private SalesOrder theSalesOrder = new SalesOrder();

    @Column(name = "colTheInvoiceDate")
    private Date theInvoiceDate;

    @Column(name = "colThePayementDate")
    private Date thePayementDate;

    @Column(name="colTheTotal")
    private double theTotal;

    public double getTheTotal() {
        return theTotal;
    }

    public void setTheTotal(double theTotal) {
        this.theTotal = theTotal;
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getThePaymentType() {
        return thePaymentType;
    }

    public void setThePaymentType(String thePaymentType) {
        this.thePaymentType = thePaymentType;
    }

    public SalesOrder getTheSalesOrder() {
        return theSalesOrder;
    }

    public void setTheSalesOrder(SalesOrder theSalesOrder) {
        this.theSalesOrder = theSalesOrder;
    }

    public Date getTheInvoiceDate() {
        return theInvoiceDate;
    }

    public void setTheInvoiceDate(Date theInvoiceDate) {
        this.theInvoiceDate = theInvoiceDate;
    }

    public Date getThePayementDate() {
        return thePayementDate;
    }

    public void setThePayementDate(Date thePayementDate) {
        this.thePayementDate = thePayementDate;
    }

}
