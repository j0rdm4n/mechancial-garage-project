package ma.esolutions.projet.gestion.garage.model.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;

public class JpaUtil {

    private static EntityManagerFactory emf = null;
    private static String dbName = "";
    private static String dbUser = "";
    private static String dbPassword = "";

    public JpaUtil() {
    }

    private static void createDB(String dbuser, String dbname, String dbpassword) throws SQLException {
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/?user=" + dbuser, dbuser, dbpassword);
        System.out.println("connection with server " + con);
        Statement stmt = con.createStatement();
        stmt.executeUpdate("create database if not exists " + dbname);
        dbName = dbname;
        dbUser = dbuser;
        dbPassword = dbpassword;
    }

    public static EntityManagerFactory getEntityManagerFactory() {

        if (emf == null) {

           
            Map<String,String> s = ApplicationController.getAppController().getSettingsMap();
           System.out.println("dbname is " + s.get("database"));
            Map<String, String> persistenceMap = new HashMap<>();
            StringBuilder url = new StringBuilder();
            url.append("jdbc:mysql://");url.append(s.get("host"));url.append(":");url.append(s.get("port"));url.append("/");
            dbName =s.get("database");
            url.append(dbName);
            url.append("?");
            url.append("zeroDateTimeBehavior=convertToNull");
            dbUser = s.get("user");
            dbPassword = s.get("password");
            System.out.println("constrcuted url is " + url.toString());
            persistenceMap.put("javax.persistence.jdbc.url", url.toString());
            persistenceMap.put("javax.persistence.jdbc.user", dbUser);
            persistenceMap.put("javax.persistence.jdbc.password", dbPassword);
            persistenceMap.put("javax.persistence.jdbc.driver", "com.mysql.jdbc.Driver");

            emf = Persistence.createEntityManagerFactory("GestGaragePU", persistenceMap);
        }
        return emf;
    }

    public static void closeEntityManagerFactory() {
        if (emf != null) {
            emf.close();
        }
    }

    public static EntityManager getEntityManager() {
        return getEntityManagerFactory().createEntityManager();
    }

}
