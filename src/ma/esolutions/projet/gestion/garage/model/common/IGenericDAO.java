/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model.common;

import ma.esolutions.projet.gestion.garage.model.BaseEntity;

/**
 *
 * @author born2code
 */
public interface IGenericDAO<T> {

    /**
     *
     *
     * @param  entity model.
     * @return created entity
     */
    T create(T entity);

    /**
     * Find entity.
     *
     * @param id entity id
     * @return entity
     */
    T find(Long id);

    /**
     * Remove entity.
     *
     * @param entity entity model
     */
    void remove(T entity);

    /**
     * Update entity.
     *
     * @param entity entity model
     * @return updated entity
     */
    T update(T entity);
    
}
