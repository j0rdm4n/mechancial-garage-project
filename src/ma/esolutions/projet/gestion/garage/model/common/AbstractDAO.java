
package ma.esolutions.projet.gestion.garage.model.common;

import ma.esolutions.projet.gestion.garage.model.BaseEntity;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import ma.esolutions.projet.gestion.garage.model.common.JpaUtil;


public abstract class AbstractDAO<T> implements IGenericDAO<T> {

    protected EntityManager theEntityManager;
    private final Class<T> theEntityClass;

    public AbstractDAO(Class<T> entityClass) {
        this.theEntityClass = entityClass;
    }

    
    public EntityManager getTheEntityManager() {
        if (theEntityManager == null) {
            theEntityManager = JpaUtil.getEntityManager();
        }

        return theEntityManager;
    }

    public Class<T> getTheEntityClass() {
        return theEntityClass;
    }

  
    @Override
    public T create(T entity) {
        getTheEntityManager().getTransaction().begin();
        getTheEntityManager().persist(entity);
       
        getTheEntityManager().getTransaction().commit();

        return entity;
    }

    @Override
    public T update(T entity) {
        getTheEntityManager().getTransaction().begin();
        entity = getTheEntityManager().merge(entity);
        
        getTheEntityManager().getTransaction().commit();

        return entity;
    }

    @Override
    public void remove(T entity) {
        getTheEntityManager().getTransaction().begin();
        getTheEntityManager().remove(getTheEntityManager().merge(entity));
        getTheEntityManager().flush();
        getTheEntityManager().getTransaction().commit();
    }

    
    @Override
    public T find(Long id) {
        return getTheEntityManager().find(theEntityClass, id);
    }

    /**
     *
     * @param namedQueryName named query name from entity
     * @return result list
     */
    @SuppressWarnings("unchecked")
    public List<T> getListWithNamedQuery(String namedQueryName) {
        return getTheEntityManager().createNamedQuery(namedQueryName).getResultList();
    }

   
    @SuppressWarnings("unchecked")
    public List<T> getListWithNamedQuery(String namedQueryName, int resultLimit) {
        return getTheEntityManager().createNamedQuery(namedQueryName).
                setMaxResults(resultLimit).getResultList();
    }

    public List<T> getListWithNamedQuery(String namedQueryName,
            Map<String, Object> parameters) {

        return getListWithNamedQuery(namedQueryName, parameters, 0);
    }

    
    @SuppressWarnings("unchecked")
    public List<T> getListWithNamedQuery(String namedQueryName,
            Map<String, Object> parameters, int resultLimit) {

        Set<Entry<String, Object>> params = parameters.entrySet();
        Query query = getTheEntityManager().createNamedQuery(namedQueryName);

        if (resultLimit > 0) {
            query.setMaxResults(resultLimit);
        }

        for (Entry<String, Object> entry : params) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<T> getListWithNamedQuery(String namedQueryName,
            Map<String, Object> parameters, int start, int end) {

        Set<Entry<String, Object>> params = parameters.entrySet();
        Query query = getTheEntityManager().createNamedQuery(namedQueryName);

        for (Entry<String, Object> entry : params) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        query.setMaxResults(end - start);
        query.setFirstResult(start);

        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<T> getListWithNamedQuery(String namedQueryName, int start, int end) {
        Query query = getTheEntityManager().createNamedQuery(namedQueryName);

        query.setMaxResults(end - start);
        query.setFirstResult(start);

        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<T> getListByNativeQuery(String sql) {
        return getTheEntityManager().createNativeQuery(sql, theEntityClass).getResultList();
    }

   
    @SuppressWarnings("unchecked")
    public List<T> getListByNativeQuery(String sql, int resultLimit) {
        return getTheEntityManager().createNativeQuery(sql, theEntityClass)
                .setMaxResults(resultLimit).getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<T> getListByNativeQuery(String sql, int start, int end) {
        Query query = getTheEntityManager().createNativeQuery(sql, theEntityClass);
        query.setMaxResults(end - start);
        query.setFirstResult(start);

        return query.getResultList();
    }

}
