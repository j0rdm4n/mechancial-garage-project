/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author E-solutions
 */
@Entity
@Table(name="payments")
public class Payment implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private long id;
    
    @Column(name="colTheCashingDate")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date theCashingDate;

 
    @Column(name="colTheReversedSate")
    private String theReversedState="non";

    public String getTheReversedState() {
        return theReversedState;
    }

    public void setTheReversedState(String theReversedState) {
        this.theReversedState = theReversedState;
    }
    
    @Column(name="colTheTTCAmount")
    private double theTTCAmount;
    
    @Column(name="colTheHTAmount")
    private double theHTAmount ;
    
    @Column(name="colThePaymentType")
    private String thePaymentType;

    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="colTheCustomerID", nullable=true, insertable=true, updatable=true)
    private Customer theCustomer ;

    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="colTheVendorID", nullable=true, insertable=true, updatable=true)
    private Vendor theVendor ;

    public Vendor getTheVendor() {
        return theVendor;
    }

    public void setTheVendor(Vendor theVendor) {
        this.theVendor = theVendor;
        this.setTheBy(theVendor.getTheVendorName());
        this.setThePartnerType("fournisseur");
    }
    
    @Column(name="colThePayer")
    private String theBy = "responsable";

    public String getTheBy() {
        return theBy;
    }

    public void setTheBy(String theBy) {
        this.theBy = theBy;
    }
    @Column(name="colThePartnerType")
    private String thePartnerType = "client";

    public String getThePartnerType() {
        return thePartnerType;
    }

    public void setThePartnerType(String thePartnerType) {
        this.thePartnerType = thePartnerType;
    }
    
    public Customer getTheCustomer() {
        return theCustomer;
    }

    public void setTheCustomer(Customer theCustomer) {
        this.theCustomer = theCustomer;
        this.setTheBy(theCustomer.getTheFirstname() + " " + theCustomer.getTheLastname());
        this.setThePartnerType("client");
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getTheCashingDate() {
        return theCashingDate;
    }

    public void setTheCashingDate(Date theCashingDate) {
        this.theCashingDate = theCashingDate;
    }

    public double getTheTTCAmount() {
        return theTTCAmount;
    }

    public void setTheHTaxAmount(double theTTCAmount) {
        this.theTTCAmount = theTTCAmount;
    }

    public double getTheAmountTTC() {
        return theHTAmount;
    }

    public void setTheTTCAmount(double theHTAmount) {
        this.theHTAmount = theHTAmount;
    }

    public String getThePaymentType() {
        return thePaymentType;
    }

    public void setThePaymentType(String thePaymentType) {
        this.thePaymentType = thePaymentType;
    }
    
}
