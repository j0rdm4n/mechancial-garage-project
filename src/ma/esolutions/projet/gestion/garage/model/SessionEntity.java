/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author E-solutions
 */
@Entity
@Table(name="session")
public class SessionEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    
    private Long id;
    
    @Column(name="colTheFirstTimeRun")
    private boolean theFirstTimeRun;

    public boolean isTheFirstTimeRun() {
        return theFirstTimeRun;
    }

    public void setTheFirstTimeRun(boolean theFirstTimeRun) {
        this.theFirstTimeRun = theFirstTimeRun;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
}
