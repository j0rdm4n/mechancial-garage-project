package ma.esolutions.projet.gestion.garage.model;

import java.util.List;
import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.model.common.AbstractDAO;

/**
 * Customer DAO
 *
 * @author Ahmed
 */
public class CustomerDAO extends AbstractDAO<Customer> {

    public CustomerDAO() {
        super(Customer.class);
    }
//tous les clients actifs

    public List<Customer> all() {
        TypedQuery<Customer> q = this.getTheEntityManager().createQuery("select c from Customer c where c.theActiveStatus = true", Customer.class);

        return q.getResultList();
    }

    public List<Customer> getTopNCustomers(int n) {

        String sql = "select cl  from Customer cl order by cl.theTotal desc";

        Query q = this.getTheEntityManager().createQuery(sql, Customer.class);
        q.setFirstResult(0);
        q.setMaxResults(n);

        return q.getResultList();

    }

}
