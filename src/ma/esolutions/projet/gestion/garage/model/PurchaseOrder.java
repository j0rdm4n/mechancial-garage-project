/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author E-solutions
 */
@Entity
@Table(name="purchases")
public class PurchaseOrder implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private Long id;
    
    
    @Column(name="colTheOrderDate")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date theOrderDate;
    @Column(name="colTheEffetDueDate")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date theEffetDueDate;
    
    
    public Date getTheOrderDate() {
        return theOrderDate;
    }

    public Date getTheEffetDueDate() {
        return theEffetDueDate;
    }

    public void setTheEffetDueDate(Date theEffetDueDate) {
        this.theEffetDueDate = theEffetDueDate;
    }

    public void setTheOrderDate(Date theOrderDate) {
        this.theOrderDate = theOrderDate;
    }

    public Vendor getTheVendor() {
        return theVendor;
    }

    public void setTheVendor(Vendor theVendor) {
        this.theVendor = theVendor;
    }

    /**
     *
     * @return
     */
    public List<PurchaseOrderLine> getTheOrderedItems() {
        return theOrderedItems;
    }

    public void setTheOrderedItems(List<PurchaseOrderLine> theOrderedItems) {
        this.theOrderedItems = theOrderedItems;
    }
    public void addTheOrderedItem(PurchaseOrderLine ol)
    {
        this.theOrderedItems.add(ol);
    }
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="colTheVendorID", nullable=true, insertable=true, updatable=true)
    private Vendor theVendor;
    
    @Column(name = "colReceived", nullable = false)
    private boolean Received = false;
    @Column(name="colCost",nullable= true)
    private double theCost ;

    @Column(name="colDelivered")
    private String theDeliveredStatus="non";
    
    @Column(name="colInvoiced")
    private String theInvoicedStatus ="non"; 
    
    @Column(name="colPaymentType")
    private String thePaymentType ="espéces";

    public String getThePaymentType() {
        return thePaymentType;
    }

    public void setThePaymentType(String thePaymentType) {
        this.thePaymentType = thePaymentType;
    }
    

    public String getTheDeliveredStatus() {
        return theDeliveredStatus;
    }

    public void setTheDeliveredStatus(String theDeliveredStatus) {
        this.theDeliveredStatus = theDeliveredStatus;
    }

    public String getTheInvoicedStatus() {
        return theInvoicedStatus;
    }

    public void setTheInvoicedStatus(String theInvoicedStatus) {
        this.theInvoicedStatus = theInvoicedStatus;
    }
    
    
    public double getTheCost() {
        return theCost;
    }

    public void setTheCost(double theCost) {
        this.theCost = theCost;
    }
    
    public boolean isReceived() {
        return Received;
    }

    public void setReceived(boolean Received) {
        this.Received = Received;
        if(Received)
        {
            setTheDeliveredStatus("oui");
        }
    }
    
    @OneToMany(cascade = {CascadeType.ALL})
    private  List<PurchaseOrderLine> theOrderedItems = new ArrayList<>();
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    
    
}
