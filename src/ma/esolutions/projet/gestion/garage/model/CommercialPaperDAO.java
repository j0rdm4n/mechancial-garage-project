/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model;

import java.util.Date;
import java.util.List;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.model.common.AbstractDAO;

/**
 *
 * @author E-solutions
 */
public class CommercialPaperDAO extends AbstractDAO<CommercialPaper>{

    public CommercialPaperDAO() {
        super(CommercialPaper.class);
    }
    public List<CommercialPaper> all()
    {
        String sql = "select c from CommercialPaper c ";
        
        TypedQuery q =  getTheEntityManager().createQuery(sql,CommercialPaper.class);
        return q.getResultList();
        
    }
    public List<CommercialPaper> findByDueDate(Date duedate)
    {
        String sql = "select c from CommercialPaper c where c.dueDate = :date ";
        
        TypedQuery q =  getTheEntityManager().createQuery(sql,CommercialPaper.class);
        q.setParameter("date", duedate);
        return q.getResultList();
    }
    
    public List<CommercialPaper> findByVendorName(String name)
    {
      String sql = "select c from CommercialPaper c where c.vendor.theVendorName like :name ";
        
        TypedQuery q =  getTheEntityManager().createQuery(sql,CommercialPaper.class);
        q.setParameter("name", "%"+name+"%");
        
        return q.getResultList();
    }
}
