/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author E-solutions
 */
@Entity
@Table(name="vehicles")
public class Vehicle implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Long id;

    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="colTheOwnerID", nullable=true, insertable=true, updatable=true)
    private Customer theVehicleOwner=new Customer();
    private String theVehicleIdentificationNumber;

    public Customer getTheVehicleOwner() {
        return theVehicleOwner;
    }

    public void setTheVehicleOwner(Customer theVehicleOwner) {
        this.theVehicleOwner = theVehicleOwner;
    }

    public String getTheVehicleIdentificationNumber() {
        return theVehicleIdentificationNumber;
    }

    public void setTheVehicleIdentificationNumber(String theVehicleIdentificationNumber) {
        this.theVehicleIdentificationNumber = theVehicleIdentificationNumber;
    }
    
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
  public String toString()
  {
      return theVehicleIdentificationNumber;
  }
}
