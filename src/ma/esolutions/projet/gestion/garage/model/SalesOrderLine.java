/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model;

import java.io.Serializable;
import java.util.HashSet;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author E-solutions
 */
@Table(name="sales_lines")
@Entity
public class SalesOrderLine implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
   
    private Long id;
    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="colTheOrderedItemID", nullable=true, insertable=true, updatable=true)
    //@Column(name="coltTheOrderedItemID")
    private Item theOrderedItem;
    
    @Column(name="colTheQuantity")
    private int theQuantity;
    
    
    @Column(name="colTheUnitPrice")
    private double theUnitPrice;

    public Item getTheOrderedItem() {
        return theOrderedItem;
    }

    public void setTheOrderedItem(Item theOrderedItem) {
        this.theOrderedItem = theOrderedItem;
    }

    public int getTheQuantity() {
        return theQuantity;
    }

    public void setTheQuantity(int theQuantity) {
        this.theQuantity = theQuantity;
    }

    public double getTheUnitPrice() {
        return theUnitPrice;
    }

    public void setTheUnitPrice(double theUnitPrice) {
        this.theUnitPrice = theUnitPrice;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    
}
