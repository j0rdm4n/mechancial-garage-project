/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author E-solutions
 */
@Entity
@Table(name="sales")
public class SalesOrder implements Serializable{
   
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private Long id;
    
    @Column(name="colTheStartDate")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date theStartDate;
    
    @Column(name="colTheDeliveredStatus")
    private String theDeliveredStatus = "non";

    public String getTheDeliveredStatus() {
        return theDeliveredStatus;
    }

    public void setTheDeliveredStatus(String theDeliveredStatus) {
        this.theDeliveredStatus = theDeliveredStatus;
    }
    
    @Column(name="colTheClosedDate")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date theClosedDate;
    
    @Column(name="colTheStatus")
    private int theStatus = OrdersStatus.OPEN;
    
    @Column(name="colTheInvoicedStatus")
    private String theInvoicedStatus = "non";

    @Column(name="colThePaymentStatus")
    private String thePaymentStatus = "non";
      public String getThePaymentStatus() {
        return thePaymentStatus;
    }

    public void setThePaymentStatus(String thePaymentStatus) {
        this.thePaymentStatus = thePaymentStatus;
    }
    
    public String getTheInvoicedStatus() {
        return theInvoicedStatus;
    }

  

    public void setTheInvoicedStatus(String theInvoicedStatus) {
        this.theInvoicedStatus = theInvoicedStatus;
    }
    
   // @OneToOne(fetch=FetchType.EAGER)
    //@JoinColumn(name="colTheCustomerID", nullable=true, insertable=true, updatable=true)
    @ManyToOne
    private Customer theCustomer;
    
    @Column(name="colTheReturnedStatus")
    private String returnedStatus = "non";

    public String getReturnedStatus() {
        return returnedStatus;
    }

    public void setReturnedStatus(String returnedStatus) {
        this.returnedStatus = returnedStatus;
    }
    
    @OneToMany(cascade = {CascadeType.ALL})
    private Set<SalesOrderLine> theOrderLines = new HashSet<>();
    
    @Column(name="colTheTotalTTCPrice")
    private Double theTotalTTCPrice;
   
    @Column(name="colTheTotalHTPrice")
    private Double theTotalHTPrice;
    
    
    public Double getTheTotalHTPrice() {
        return theTotalHTPrice;
    }
    
    public void setTheTotalHTPrice(Double theTotalHTPrice) {
        this.theTotalHTPrice = theTotalHTPrice;
    }
    
    @Column(name="colThePaymentType")
    private String thePaymentType = "espéces";
   
    public Double getTheTotalTTCPrice() {
        return theTotalTTCPrice;
    }

    public void setTheTotalTTCPrice(Double theTotalPrice) {
        this.theTotalTTCPrice = theTotalPrice;
    }
    
     
     
    public String getThePaymentType() {
        return thePaymentType;
    }

    public void setThePaymentType(String thePaymentType) {
        this.thePaymentType = thePaymentType;
    }
            
   // private long theOrderedAmount;
  
    //private HashSet<Item> theOrderedItems;
    //@Column(name="colTheOrderLineID")
 
    public Set<SalesOrderLine> getTheOrderLines() {
        return theOrderLines;
    }
    public void addSalesOrderLine(SalesOrderLine s)
    {
        theOrderLines.add(s);
    }
    public void setTheOrderLines(HashSet<SalesOrderLine> theOrderLines) {
        this.theOrderLines = theOrderLines;
    }


    
   
    public void setTheStartDate(Date start)
    {
        theStartDate = start;
    }
    public Date geTheStartDate()
    {
        return theStartDate;
    }
    public void setTheClosedDate(Date date)
    {
        theClosedDate = date;
    }
    public Date getTheClosedDate()
    {
        return theClosedDate;
    }
    
    public void setTheCustomer(Customer c)
    {
        theCustomer = c;
    }
    public Customer getTheCustomer()
    {
        
        return theCustomer;
    }
    public void setTheStatus(int status)
    {
        theStatus = status;
    }
    public int getTheStatus()
    {
        return theStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    
  
    
  
     
}
