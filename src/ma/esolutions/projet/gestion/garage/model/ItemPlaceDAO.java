/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model;

import java.util.Iterator;
import java.util.List;
import javax.persistence.Query;
import ma.esolutions.projet.gestion.garage.model.common.AbstractDAO;

/**
 *
 * @author E-solutions
 */
public class ItemPlaceDAO extends AbstractDAO<ItemPlace>{

    public ItemPlaceDAO() {
        super(ItemPlace.class);
    }

    public boolean checkIfPlaceExistsWithName(String text) {
       String sql = "select e from ItemPlace e where e.theItemPlaceName = :name";
        Query q = getTheEntityManager().createQuery(sql,ItemPlace.class);
        q.setParameter("name", text);
        
        List<ItemPlace> l = q.getResultList();
        Iterator<ItemPlace> ie= l.iterator();
        while(ie.hasNext())
        {
             if(ie.next().getTheItemPlaceName().equalsIgnoreCase(text))
             {
                 return true;
             }
        }
        return false;
      
    }

    public List<ItemPlace> all() {
        String sql = "select e from ItemPlace e";
        Query q = getTheEntityManager().createQuery(sql,ItemPlace.class);
        List<ItemPlace> l = q.getResultList();
        
        return l;
    }
    
}
