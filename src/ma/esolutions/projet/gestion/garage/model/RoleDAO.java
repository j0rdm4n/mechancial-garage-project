/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model;

import java.util.List;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.model.common.AbstractDAO;

/**
 *
 * @author E-solutions
 */
public class RoleDAO extends AbstractDAO<Role> {

    public RoleDAO() {
        super(Role.class);
    }

    public List<Role> all() {

        TypedQuery q = this.getTheEntityManager().createQuery("select r from Role r", Role.class);
        return q.getResultList();
    }
    
}
