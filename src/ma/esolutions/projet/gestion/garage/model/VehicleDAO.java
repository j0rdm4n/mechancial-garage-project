/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model;

import java.util.List;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.model.common.AbstractDAO;

/**
 *
 * @author E-solutions
 */
public class VehicleDAO extends AbstractDAO<Vehicle>{

    public VehicleDAO() {
        super(Vehicle.class);
    }

    public List<Vehicle> getVehiclesByCustomer(Customer c) {
        TypedQuery q = this.getTheEntityManager().createQuery("select v from Vehicle v where v.theVehicleOwner =:owner", Vehicle.class);
        q.setParameter("owner", c);
        
        return q.getResultList();
    }

    public List<Vehicle> all() {
        TypedQuery q = this.getTheEntityManager().createQuery("select v from Vehicle v", Vehicle.class);
        
        return q.getResultList();
    }
    
}
