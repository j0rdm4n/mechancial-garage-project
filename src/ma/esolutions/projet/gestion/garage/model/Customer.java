
package ma.esolutions.projet.gestion.garage.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
//import ma.esolutions.projet.gestion.garage.model.common.BaseEntity2;

/**
 * Customer entity
 *
 * @author Ahmed
 */
@Entity
@Table(name = "customers")



public class Customer implements Serializable {

   


    @Column(name = "colFirstname", length = 50)
    private String theFirstname;

    @Column(name = "colLastName", length = 50)
    private String theLastname;

    @Lob
    @Column(name = "colAddress", length = 65535)
    private String theAddress;

    @Column(name = "colCity", length = 50)
    private String theCity;


    @Column(name = "colPostalecode", length = 50)
    private String thePostalcode;
    
    
    @Column(name="colTheTotal")
    private double theTotal = 0 ; 

    public double getTheTotal() {
        return theTotal;
    }

    public void setTheTotal(double theTotal) {
        this.theTotal += theTotal;
    }
    
    @Column(name = "colPhone", length = 50)
    private String thePhone;

    @Column(name = "colFax", length = 50)
    private String theFax;

    @Column(name = "colMobile", length = 50)
    private String theMobile;

    @Column(name = "colEmail", length = 50)
    private String theEmail;
    
    @Column(name="colCredit")
    private double theCredit;
    
    @OneToMany
    private List<SalesOrder> theSalesOrders ;
    
    @OneToMany
    private List<RepairOrder> theRepairOrders ;

    public List<RepairOrder> getTheRepairOrders() {
        return theRepairOrders;
    }

    public void addTheRepairOrder(RepairOrder theRepairOrder) {
        this.theRepairOrders.add(theRepairOrder);
        double sum = theRepairOrder.getTheTotalTTC() + theRepairOrder.getTheLaborCost();
        this.setTheTotal(sum);
    }
    
    public List<SalesOrder> getTheSalesOrders() {
        return theSalesOrders;
    }
    
    public void addTheSalesOrder(SalesOrder theSalesOrder) {
        theSalesOrders.add(theSalesOrder);
        this.setTheTotal(theSalesOrder.getTheTotalHTPrice());
        System.out.println("total sales for client " + this.theLastname + " : " + this.getTheTotal());
    }
    
    
    
    public double getTheCredit() {
        return theCredit;
    }

    public void setTheCredit(double theCredit) {
        this.theCredit = theCredit;
    }
    
    public String getTheAddress() {
        return theAddress;
    }

    public void setTheAddress(String theAddress) {
        this.theAddress = theAddress;
    }

    public String getTheCity() {
        return theCity;
    }

    public void setTheCity(String theCity) {
        this.theCity = theCity;
    }

    public String getThePostalcode() {
        return thePostalcode;
    }

    public void setThePostalcode(String thePostalcode) {
        this.thePostalcode = thePostalcode;
    }

    public String getTheEmail() {
        return theEmail;
    }

    public void setTheEmail(String theEmail) {
        this.theEmail = theEmail;
    }

    public Vehicle getTheVehicle() {
        return theVehicle;
    }

    public void setTheVehicle(Vehicle theVehicle) {
        this.theVehicle = theVehicle;
    }
    
    @OneToOne
    @JoinColumn(name="colTheVehicleID", nullable=true, insertable=true, updatable=true)
    private Vehicle theVehicle;

    @Basic(optional = false)
    @Column(name = "colActiveStatus", nullable = false)
    private boolean theActiveStatus;

    @Lob
    @Column(name = "colNotes", length = 65535)
    private String theNotes;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    
    private Long id;


    public Customer() {
    }


    public String getTheFirstname() {
        return theFirstname;
    }

    public void setTheFirstname(String firstname) {
        this.theFirstname = firstname;
    }

    public String getTheLastname() {
        return theLastname;
    }

    public void setTheLastname(String lastname) {
        this.theLastname = lastname;
    }

    public String getAddress() {
        return theAddress;
    }

    public void setAddress(String address) {
        this.theAddress = address;
    }

    public String getCity() {
        return theCity;
    }

    public void setCity(String city) {
        this.theCity = city;
    }


    public String getPostalcode() {
        return thePostalcode;
    }

    public void setPostalcode(String postalCode) {
        this.thePostalcode = postalCode;
    }

    public String getThePhone() {
        return thePhone;
    }

    public void setThePhone(String thePhone) {
        this.thePhone = thePhone;
    }

    public String getTheFax() {
        return theFax;
    }

    public void setTheFax(String theFax) {
        this.theFax = theFax;
    }

    public String getTheMobile() {
        return theMobile;
    }

    public void setTheMobile(String mobile) {
        this.theMobile = mobile;
    }

    public String getEmail() {
        return theEmail;
    }

    public void setEmail(String email) {
        this.theEmail = email;
    }


    public boolean getTheActiveStatus() {
        return theActiveStatus;
    }

    public void setTheActiveStatus(boolean status) {
        this.theActiveStatus = status;
    }

    public String getTheNotes() {
        return theNotes;
    }

    public void setTheNotes(String notes) {
        this.theNotes = notes;
    }

    @Override
    public String toString()
    {
        return this.getTheFirstname() + " " + this.getTheLastname() + "-" + "CC"+this.getId() ;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }




}
