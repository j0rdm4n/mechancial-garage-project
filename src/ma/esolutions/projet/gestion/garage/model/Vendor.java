/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author E-solutions
 */
@Entity
@Table(name="vendors")
public class Vendor implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private Long id;
    @Column(name="colTheVendorName")
    private String theVendorName;
    @Column(name="colTheVendorPhone")
    private String theVendorPhone;
    @Column(name="colTheVendorEmail")
    private String theVendorEmail;
    @Column(name="colTheVendorAddress")
    private String theVendorAddress;
    
    @Column(name="colTheVendorPostalCode")
    private Long theVendorPostalCode;
    @Column(name="colTheVendorCity")
    private String theVendorCity;
    @Column(name="colTheVendorSinceDate")
    private Date theVendorSinceDate;
    
    @OneToMany(mappedBy = "theVendor")
    private List<PurchaseOrder> thePurchaseOrders;

    public List<PurchaseOrder> getThePurchaseOrders() {
        return thePurchaseOrders;
    }

    public void addThePurchaseOrder(PurchaseOrder thePurchaseOrder) {
        thePurchaseOrders.add(thePurchaseOrder);
    }

    public Long getTheVendorPostalCode() {
        return theVendorPostalCode;
    }

    public void setTheVendorPostalCode(Long theVendorPostalCode) {
        this.theVendorPostalCode = theVendorPostalCode;
    }

    public String getTheVendorCity() {
        return theVendorCity;
    }

    public void setTheVendorCity(String theVendorCity) {
        this.theVendorCity = theVendorCity;
    }
     

    public String getTheVendorName() {
        return theVendorName;
    }

    public void setTheVendorName(String theVendorName) {
        this.theVendorName = theVendorName;
    }

    public String getTheVendorPhone() {
        return theVendorPhone;
    }

    public void setTheVendorPhone(String theVendorPhone) {
        this.theVendorPhone = theVendorPhone;
    }

    public String getTheVendorEmail() {
        return theVendorEmail;
    }

    public void setTheVendorEmail(String theVendorEmail) {
        this.theVendorEmail = theVendorEmail;
    }

    public String getTheVendorAddress() {
        return theVendorAddress;
    }

    public void setTheVendorAddress(String theVendorAddress) {
        this.theVendorAddress = theVendorAddress;
    }

    public Date getTheVendorSinceDate() {
        return theVendorSinceDate;
    }

    public void setTheVendorSinceDate(Date theVendorSinceDate) {
        this.theVendorSinceDate = theVendorSinceDate;
    }
   
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    @Override
    public String toString()
    {
        if(getId() == null)
            return this.theVendorName ;
        return  this.theVendorName;
    }
    
}
