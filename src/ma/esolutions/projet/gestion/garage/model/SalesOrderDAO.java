/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.model.common.AbstractDAO;

/**
 *
 * @author E-solutions
 */
public class SalesOrderDAO extends AbstractDAO<SalesOrder> {

    public SalesOrderDAO() {
        super(SalesOrder.class);
    }

    public List<Object[]> getAvgSalesPerDate() {
        Query q = this.getTheEntityManager().createQuery("select avg(s.theTotalPrice) from SalesOrder s group by s.theClosedDate ", SalesOrder.class);

        return q.getResultList();

    }

    public Map<String, Number> getSalesPerMonthForYear(int year) {
        String sql = "select YEAR(o.theClosedDate) as y,"
                + "       MONTH(o.theClosedDate) as m,"
                + "       SUM(o.theTotalTTCPrice) as totale from SalesOrder o WHERE  o.thePaymentStatus ='oui' and YEAR(o.theClosedDate) = :p_year GROUP BY  YEAR(o.theClosedDate),MONTH(o.theClosedDate)  ";

        Query q = this.getTheEntityManager().createQuery(sql, Tuple.class);
        q.setParameter("p_year", year);
        List<Tuple> result = q.getResultList();
        Map<String, Number> m = new HashMap<>();
        initMap(m);
        for (Tuple o : result) {
            int yearr = o.get(0, Integer.class);
            int month = o.get(1, Integer.class);
            Number total = o.get(2, Number.class);
            setMapData(month, m, total);

        }
        m.forEach((k, v) -> {

            System.out.println("month :  " + k + " total : " + v);
        });
        return m;
    }

    private void setMapData(int month, Map<String, Number> m, Number total) {
        // m.put(month,total);
        switch (month) {
            case 1:
                m.put("Janvier", total);
                break;
            case 2:
                m.put("Février", total);
                break;
            case 3:
                m.put("Mars", total);
                break;
            case 4:
                m.put("Avril", total);
                break;
            case 5:
                m.put("Mai", total);
                break;
            case 6:
                m.put("Juin", total);
                break;
            case 7:
                m.put("Juillet", total);
                break;
            case 8:
                m.put("Aout", total);
                break;
            case 9:
                m.put("Septembre", total);
                break;
            case 10:
                m.put("Octobre", total);
                break;
            case 11:
                m.put("Novembre", total);
                break;
            case 12:
                m.put("Décembre", total);
                break;

        }
    }

    private void initMap(Map<String, Number> m) {
        m.put("Janvier", 0);
        m.put("Février", 0);
        m.put("Mars", 0);
        m.put("Avril", 0);
        m.put("Mai", 0);
        m.put("Juin", 0);
        m.put("Juillet", 0);
        m.put("Aout", 0);
        m.put("Septembre", 0);
        m.put("Octobre", 0);
        m.put("Novembre", 0);
        m.put("Décembre", 0);
    }

    public Map getSalesPerDayForMonth(int month) {
        String sql = "select DAY(o.theClosedDate) as d,"
                + "       SUM(o.theTotalTTCPrice) as totale from SalesOrder o WHERE MONTH(o.theClosedDate) = :p_month GROUP BY  DAY(o.theClosedDate)  ";

        Query q = this.getTheEntityManager().createQuery(sql, Tuple.class);
        q.setParameter("p_month", month);
        List<Tuple> result = q.getResultList();
        Map<String, Number> m = new HashMap<>();
        for (int i = 1; i <= 30; i++) {
            m.put("" + i, 0);
        }
        System.out.println("daily data ");
        //initMap(m);
        for (Tuple o : result) {
            int day = o.get(0, Integer.class);
            Number total = o.get(1, Number.class);
            m.put("" + day, total);

        }
        m.forEach((k, v) -> {

            System.out.println("day :  " + k + " total : " + v);
        });
        return m;

    }

    public Map<String, Number> getSalesPerYears() {
        String sql = "select YEAR(o.theClosedDate) as m,"
                + "SUM(o.theTotalPrice) as totale from SalesOrder o  GROUP BY  YEAR(o.theClosedDate) ";

        Query q = this.getTheEntityManager().createQuery(sql, Tuple.class);

        List<Tuple> result = q.getResultList();
        Map<String, Number> m = new HashMap<>();
        for (int i = 0; i < result.size(); i++) {
            System.out.println(" element " + ((Tuple) result.get(i)).get("m", Integer.class));
        }
        System.out.println("years " + result.size());

        for (Tuple o : result) {
            System.out.println(o);
            if (o != null) {

                int year = o.get("m", Integer.class);
                Number total = o.get("totale", Number.class);

                m.put("" + year, total);
            }
        }
        return m;

    }

    public SalesOrder getCorrespondingSalesOrder(Return e) {
        String sql = "select o from SalesOrder o where o.theCustomer = :customer and :item member of  o.theOrderLines.theOrderedItem";
        Query q = this.getTheEntityManager().createQuery(sql, SalesOrder.class);
        q.setParameter("customer", e.getTheCustomer());
        q.setParameter("item", e.getTheItem());
        System.out.println(q.getResultList());
        return null;

    }

    public Map getSalesPerCustomer(int theYear, int theMonth, int theDay) {
        String sql = "select o.theCustomer,SUM(o.theTotalTTCPrice) from SalesOrder o where  o.thePaymentStatus ='oui' and YEAR(o.theStartDate) = YEAR(CURDATE()) and MONTH(o.theStartDate) = MONTH(CURDATE()) and DAY(o.theStartDate) = DAY(CURDATE()) GROUP BY o.theCustomer ";
        Query q = this.getTheEntityManager().createQuery(sql, Tuple.class);
       // q.setParameter("pyear", theYear);
      //  q.setParameter("pmonth", theMonth);
      //  q.setParameter("pday", theDay);
        System.out.println(q.getResultList());
        Map<String, Number> m = new HashMap<>();
        List<Tuple> r = q.getResultList();
        for (Tuple o : r) {
            Customer customer = o.get(0, Customer.class);
            double total = o.get(1, Double.class);
            m.put(customer.toString(), total);
        }
        return m;

    }

    public List<SalesOrder> all() {

        TypedQuery<SalesOrder> q = this.getTheEntityManager().createQuery("select s from SalesOrder s ", SalesOrder.class);
        return q.getResultList();
    }

    public double getTotalSalesForYear(int year) {

        String s = "select sum(o.theTotalTTCPrice) as total from SalesOrder o where YEAR(o.theStartDate) = :pyear and o.thePaymentStatus ='oui'";
        Query query = this.getTheEntityManager().createQuery(s, Tuple.class);
        query.setParameter("pyear", year);
        List<Tuple> rl = query.getResultList();

        System.out.println("total size " + rl.size());
        double total = 0;
        for (Tuple o : rl) {
            total = o.get("total", double.class);
        }

        return total;
    }

    public long getCountOrdersPerYear(int year) {
    
        String s = "select count(distinct o) as total from SalesOrder o where YEAR(o.theStartDate) = :pyear and o.thePaymentStatus ='oui'";
        Query query = this.getTheEntityManager().createQuery(s, Tuple.class);
        query.setParameter("pyear", year);
        List<Tuple> rl = query.getResultList();

        System.out.println("total size " + rl.size());
        long total = 0;
        for (Tuple o : rl) {
            total = o.get("total", Long.class);
        }

        return total;
    
    }
    
    public long getCountCustomersPerYear(int year) {
    
        String s = "select count(distinct o.theCustomer) as total from SalesOrder o where YEAR(o.theStartDate) = :pyear and o.thePaymentStatus ='oui'";
        Query query = this.getTheEntityManager().createQuery(s, Tuple.class);
        query.setParameter("pyear", year);
        List<Tuple> rl = query.getResultList();

        System.out.println("total size " + rl.size());
        long total = 0;
        for (Tuple o : rl) {
            total = o.get("total", long.class);
        }

        return total;
    
    }
    
    public long getCountItemsPerYear(int year) {
        
    return 0;
        /*String s = "select count(distinct o.theOrderedItem) from SalesOrderLine o where YEAR(o..theStartDate) = :pyear and o.thePaymentStatus ='oui'";
        Query query = this.getTheEntityManager().createQuery(s, Tuple.class);
        query.setParameter("pyear", year);
        List<Tuple> rl = query.getResultList();

        System.out.println("total size " + rl.size());
        long total = 0;
        for (Tuple o : rl) {
            total = o.get("total", long.class);
        }

        return total;*/
    
    }



}
