/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model;

import java.util.List;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.model.common.AbstractDAO;

/**
 *
 * @author E-solutions
 */
public class RepairOrderDAO extends AbstractDAO<RepairOrder>{

    public RepairOrderDAO() {
        super(RepairOrder.class);
    }

    public List<RepairOrder> all() {
        TypedQuery q = this.getTheEntityManager().createQuery("select r from RepairOrder r", RepairOrder.class);
        return q.getResultList();
    }
    
}
