/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;
import javax.persistence.Query;
import javax.persistence.Tuple;
import ma.esolutions.projet.gestion.garage.model.common.AbstractDAO;
import ma.esolutions.projet.gestion.garage.view.ApplicationView;

/**
 *
 * @author E-solutions
 */
public class PaymentDAO extends AbstractDAO<Payment> {

    public PaymentDAO() {
        super(Payment.class);
    }

    public double getTodayTotal() {
        String sql = "select SUM(o.theTTCAmount) as totale from Payment o WHERE Date(o.theCashingDate) = CURDATE()";

        Query q = this.getTheEntityManager().createQuery(sql, Tuple.class);
        // q.setParameter("timestamp", new Date());
        List<Tuple> result = q.getResultList();
        System.out.println("total caisse today " + result.get(0).get("totale", Double.class));
        return result.get(0).get("totale", Double.class);

    }

    public List<Payment> all() {

        return this.theEntityManager.createQuery("select p from Payment p ", Payment.class).getResultList();
    }

    public Map<Integer, Double> getCashOutAmmountPerMonth() {
        String sql = "select SUM(o.theHTAmount) as total,MONTH(o.theCashingDate) as m from Payment o where o.thePartnerType = 'fournisseur' group by MONTH(o.theCashingDate),YEAR(o.theCashingDate)";
        Query q = this.getTheEntityManager().createQuery(sql, Tuple.class);
        List<Tuple> rs = q.getResultList();
        Map<Integer, Double> m = new HashMap<>();

        IntStream.rangeClosed(1, 12).forEachOrdered((e) -> {
            m.put(e, 0.0);

        });

        rs.forEach((Tuple t) -> {

            m.put(Integer.parseInt(t.get("m").toString()), Double.parseDouble(t.get("total").toString()));

        });
        return m;
    }

    public Map<Integer, Double> getCashInAmmountPerMonth() {

        String sql = "select SUM(o.theHTAmount) as total ,MONTH(o.theCashingDate) as m from Payment o where o.thePartnerType = 'client' group by MONTH(o.theCashingDate),YEAR(o.theCashingDate)";
        Query q = this.getTheEntityManager().createQuery(sql, Tuple.class);
        List<Tuple> rs = q.getResultList();
        Map<Integer, Double> m = new HashMap<>();

        IntStream.rangeClosed(1, 12).forEachOrdered((e) -> {
            m.put(e, 0.0);

        });
        if (rs != null) {
            try {

                rs.forEach((Tuple t) -> {

                    m.put(Integer.parseInt(t.get("m").toString()), Double.parseDouble(t.get("total").toString()));

                });
            } catch (Exception e) {
                ApplicationView.showErrorMsg("Une erreur s'est produite !");
            }
        }
        return m;
    }

    public double getDailyInTotal() {
        String sql = "select SUM(o.theHTAmount) as total ,DAY(o.theCashingDate) as m from Payment o where o.thePartnerType ='client'  and DAY(o.theCashingDate) = DAY(CURDATE())";
        Query q = getTheEntityManager().createQuery(sql, Tuple.class);
        List<Tuple> rs = q.getResultList();
        System.out.println("caisse daily in " + rs.get(0).get("total"));
        if (rs != null) {
            if (rs.get(0) != null) {
                if (rs.get(0).get("total") != null) {
                    return Double.parseDouble(rs.get(0).get("total").toString());
                }
            }
        }
        return 0.0;

    }

    public double getDailyOutTotal() {
        String sql = "select SUM(o.theHTAmount) as total ,DAY(o.theCashingDate) as m from Payment o where o.thePartnerType ='fournisseur'   and  DAY(o.theCashingDate) = DAY(CURDATE())";
        Query q = this.getTheEntityManager().createQuery(sql, Tuple.class);
        List<Tuple> rs = q.getResultList();
        if (rs != null) {
            if (rs.get(0) != null) {
                if (rs.get(0).get("total") != null) {
                    return Double.parseDouble(rs.get(0).get("total").toString());
                }
            }
        }
        return 0.0;

    }

    public double getDailyTotal() {
        String sql = "select SUM(o.theHTAmount) as total ,DAY(o.theCashingDate) as m from Payment o where DAY(o.theCashingDate) = DAY(CURDATE())";
        Query q = this.getTheEntityManager().createQuery(sql, Tuple.class);
        List<Tuple> rs = q.getResultList();
        if (rs != null) {
            if (rs.get(0) != null) {
                if (rs.get(0).get("total") != null) {
                    return Double.parseDouble(rs.get(0).get("total").toString());
                }
            }
        }
        return 0.0;

    }

    public double getDailyAlimentationTotal() {
        String sql = "select SUM(o.theHTAmount) as total ,DAY(o.theCashingDate) as m from Payment o where o.thePaymentType ='alimentation'   and  DAY(o.theCashingDate) = DAY(CURDATE())";
        Query q = this.getTheEntityManager().createQuery(sql, Tuple.class);
        List<Tuple> rs = q.getResultList();
        if (rs != null) {
            if (rs.get(0) != null) {
                if (rs.get(0).get("total") != null) {
                    return Double.parseDouble(rs.get(0).get("total").toString());
                }
            }
        }
        return 0.0;
    }

    public double getDailyDepensesTotal() {
        String sql = "select SUM(o.theHTAmount) as total ,DAY(o.theCashingDate) as m from Payment o where o.thePaymentType ='depense'   and  DAY(o.theCashingDate) = DAY(CURDATE())";
        Query q = this.getTheEntityManager().createQuery(sql, Tuple.class);
        List<Tuple> rs = q.getResultList();
        if (rs != null) {
            if (rs.get(0) != null) {
                if (rs.get(0).get("total") != null) {
                    return Double.parseDouble(rs.get(0).get("total").toString());
                }
            }
        }
        return 0.0;
    }

    public List<Payment> getNonTaxedPayments() {

        String sql = "select p from Payment p where p.theReversedState = 'non' ";
    
        Query q = getTheEntityManager().createQuery(sql, Payment.class);
        System.out.println(q.getResultList());
        return q.getResultList();
    }

}
