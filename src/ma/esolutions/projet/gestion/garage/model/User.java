/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author E-solutions
 */
@Entity
@Table(name="users")
public class User  implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
   
    private Long id;

  
   @Column(name="colTheFirstname")
   private String theFirstname;
   
   @Column(name="colTheLastname")
   private String theLastname;
   
   @Column(name="colTheUsername")
   private String theUsername;
   
   @Column(name="colThePassword")
   private String thePassword;
   
   @Column(name="colTheEmail")
   private String theEmail;
   
   @Column(name="colTheAddress")
   private String theAddress;
   
   @Column(name="colThePhone")
   private String thePhone;
   
  @ManyToMany(targetEntity=Role.class,fetch=FetchType.EAGER)
  @JoinColumn(name="colTheRoleID", nullable=true, insertable=true, updatable=true)
   private List<Role> theAttributedRoles = new ArrayList<Role>();   
   
   public void AddRole(Role role)
   {
       getTheAttributedRoles().add(role);
   }
    public void setTheAttributedRoles(List<Role> theAttributedRoles) {
        this.theAttributedRoles = theAttributedRoles;
    }
    
    public List<Role> getTheAttributedRoles() {
        return theAttributedRoles;
    }
   
   
   public User(){
       
   }

    public String getTheFirstname() {
        return theFirstname;
    }

    public String getTheLastname() {
        return theLastname;
    }

    public String getTheUsername() {
        return theUsername;
    }

    public String getThePassword() {
        return thePassword;
    }

    public String getTheEmail() {
        return theEmail;
    }

    public String getTheAddress() {
        return theAddress;
    }

    public String getThePhone() {
        return thePhone;
    }

    public void setTheFirstname(String theFirstname) {
        this.theFirstname = theFirstname;
    }

    public void setTheLastname(String theLastname) {
        this.theLastname = theLastname;
    }

    public void setTheUsername(String theUsername) {
        this.theUsername = theUsername;
    }

    public void setThePassword(String thePassword) {
        this.thePassword = thePassword;
    }

    public void setTheEmail(String theEmail) {
        this.theEmail = theEmail;
    }

    public void setTheAddress(String theAddress) {
        this.theAddress = theAddress;
    }

    public void setThePhone(String thePhone) {
        this.thePhone = thePhone;
    }

    public void superUser() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
       
        return  theUsername ;
    }
   
    
   
   
    
}