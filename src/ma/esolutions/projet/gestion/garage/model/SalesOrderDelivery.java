/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author E-solutions
 */
@Entity
@Table(name="deliveries")
public class SalesOrderDelivery implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private Long id;
    
    @OneToOne(targetEntity=SalesOrder.class)
    @JoinColumn(name="colTheOrderID", nullable=true, insertable=true, updatable=true)
    SalesOrder theSalesOrder = new SalesOrder(); 
    
    @Column(name="colThePayementDate")
    private Date theDeliveryDate;

    public SalesOrder getTheSalesOrder() {
        return theSalesOrder;
    }

    public void setTheSalesOrder(SalesOrder theSalesOrder) {
        this.theSalesOrder = theSalesOrder;
    }

    public Date getTheDeliveryDate() {
        return theDeliveryDate;
    }

    public void setTheDeliveryDate(Date theDeliveryDate) {
        this.theDeliveryDate = theDeliveryDate;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    
}
