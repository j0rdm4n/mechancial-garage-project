/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author E-solutions
 */
@Entity
@Table(name="returns")
public class Return implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private Long id;
    
    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="colTheCustomerID", nullable=true, insertable=true, updatable=true)
    private Customer theCustomer;
     
    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="colTheItemID", nullable=true, insertable=true, updatable=true)
    private Item theItem;
    
    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="colTheDeliveryID",nullable=true,insertable=true,updatable=true)
    private SalesOrderDelivery theDelivery ;

    public SalesOrderDelivery getTheDelivery() {
        return theDelivery;
    }

    public void setTheDelivery(SalesOrderDelivery theDelivery) {
        this.theDelivery = theDelivery;
    }
    
    
    
    @Column(name="colTheItemReturnDate")
    private Date theItemReturnDate ; 
    
    @Column(name="colTheItemQuantity")
    private long theItemQuantity;
    
    @Column(name="colTheTotalPrice")
    private double theTotalPrice;

    public Customer getTheCustomer() {
        return theCustomer;
    }

    public void setTheCustomer(Customer theCustomer) {
        this.theCustomer = theCustomer;
    }

    public Item getTheItem() {
        return theItem;
    }

    public void setTheItem(Item theItem) {
        this.theItem = theItem;
    }

    public Date getTheItemReturnDate() {
        return theItemReturnDate;
    }

    public void setTheItemReturnDate(Date theItemReturnDate) {
        this.theItemReturnDate = theItemReturnDate;
    }

    public long getTheItemQuantity() {
        return theItemQuantity;
    }

    public void setTheItemQuantity(long theItemQuantity) {
        this.theItemQuantity = theItemQuantity;
    }

    public double getTheTotalPrice() {
        return theTotalPrice;
    }

    public void setTheTotalPrice(double theTotalPrice) {
        this.theTotalPrice = theTotalPrice;
    }
    
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
}
