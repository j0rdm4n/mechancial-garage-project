/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.model;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.model.common.AbstractDAO;

/**
 *
 * @author E-solutions
 */
public class SalesOrderLineDAO extends AbstractDAO<SalesOrderLine> {

    public SalesOrderLineDAO() {
        super(SalesOrderLine.class);
    }

    public double getTotalPrice() {
        return 0.0;
    }

    public double getUnitPriceByItemAndCustomer(Customer c, Item e) {
        TypedQuery q1 = this.getTheEntityManager().createQuery("select l from SalesOrderLine l  where l.theOrderedItem = :item ", SalesOrderLine.class);
        q1.setParameter("item", e);
        TypedQuery q2 = this.getTheEntityManager().createQuery("select o from SalesOrder o where o.theCustomer = :customer ", SalesOrder.class);
        q2.setParameter("customer", c);
        List<SalesOrder> orders = q2.getResultList();
        List<SalesOrderLine> lines = q1.getResultList();
        orders.forEach((o) -> {
            lines.retainAll(o.getTheOrderLines());
        });
        if (lines.size() == 1) {
            return lines.get(0).getTheUnitPrice();
        }
        return 0.0;

    }

    public Map<String, Number> getTotalSalesPerItem() {
        Map<String, Number> m = new HashMap();
        String sql = "select o.theOrderedItem as item ,sum(o.theQuantity * o.theOrderedItem.theUnitPrice) from SalesOrderLine o GROUP BY o.theOrderedItem";
        Query q = this.getTheEntityManager().createQuery(sql, Tuple.class);

        List<Tuple> l = q.getResultList();
        double totals = 0.0;
        for(Tuple o : l)
        {
            totals=totals +  (double)o.get(1,Number.class);
            
        }
        
       
        for(Tuple o : l)
        {
            Item item = o.get(0, Item.class);
            Number total = o.get(1, Number.class);
            double percent = (double)((double)total * 100)/totals;
            DecimalFormat df = new DecimalFormat();
            df.setMaximumFractionDigits(1);
            System.out.println("item " + item + " total " +total );
            m.put(item.toString() +" -"+total+"- ("+ df.format(percent)+"%" +")", total);
        }
        
        return m;

    }
}
