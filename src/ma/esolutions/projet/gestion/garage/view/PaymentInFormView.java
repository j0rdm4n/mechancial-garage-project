/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.awt.Component;
import java.util.Date;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import ma.esolutions.projet.gestion.garage.app.Session;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.model.Payment;
import ma.esolutions.projet.gestion.garage.model.PaymentDAO;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS16;
import ma.esolutions.projet.gestion.garage.view.common.AbstractFormView;
import ma.esolutions.projet.gestion.garage.view.common.JTextFieldExtended;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author E-solutions
 */
public class PaymentInFormView extends AbstractFormView<Payment> {

    private boolean isNewModel;
    private  Payment thePayment;
    private JTextFieldExtended amountTxt;

    public PaymentInFormView(Payment p) {
        super(ApplicationController.getAppView());

        thePayment = p;

    }

    @Override
    public void buildUI() {

        initComponents();
        // add pages
        addPageToForm("Informations sur le client",
                buildPage());

        popFields();
        pack();
    }

    private JPanel buildPage() {
        JPanel p = new JPanel(new MigLayout());

        p.add(new JLabel("Montant à entrer à la caisse "), "gaptop 60,split,left,width 300");
        amountTxt = new JTextFieldExtended(90);

        p.add(amountTxt, "right,width 300,wrap");

        return p;

    }

    @Override
    public boolean Save() {
        pushFields();
        if (thePayment != null) {
            (new PaymentDAO()).create(thePayment);
        }
        this.clearFields();
        this.thePayment = new Payment();
        return false;
    }

    @Override
    public Payment getEntity() {
        return thePayment;
    }

    @Override
    public String getFormIconPath() {
        return ICONS16 + "caisse.png";
    }

    @Override
    public String getFormTitle() {
        return "Alimentation de caisse";
    }

    @Override
    public void onHelp() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void popFields() {
    }

    @Override
    public Payment pushFields() {
        thePayment.setTheBy(Session.getSession().getTheCurrentUser().getTheFirstname() + " " + Session.getSession().getTheCurrentUser().getTheLastname());
        thePayment.setTheCashingDate(new Date());
        thePayment.setThePartnerType("utilisateur");
        thePayment.setThePaymentType("alimentation");
        try {
            thePayment.setTheTTCAmount(Double.parseDouble(amountTxt.getText()));
            thePayment.setTheHTaxAmount(thePayment.getTheAmountTTC());
        } catch (NumberFormatException e) {
            thePayment.setTheTTCAmount(0);
        }
        return thePayment;
    }

    @Override
    public void clearFields() {
        this.amountTxt.setText("0");
    }

    @Override
    public String getIconPath() {
        return ICONS16 + "caisse.png";

    }

    @Override
    public Component asComponent() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
