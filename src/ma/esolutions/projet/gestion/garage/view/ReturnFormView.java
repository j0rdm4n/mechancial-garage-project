/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.awt.Component;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.controller.commands.ItemReturnCommand;
import ma.esolutions.projet.gestion.garage.model.Customer;
import ma.esolutions.projet.gestion.garage.model.CustomerDAO;
import ma.esolutions.projet.gestion.garage.model.Item;
import ma.esolutions.projet.gestion.garage.model.ItemDAO;
import ma.esolutions.projet.gestion.garage.model.MinimalStockException;
import ma.esolutions.projet.gestion.garage.model.NegativeStockException;
import ma.esolutions.projet.gestion.garage.model.Return;
import ma.esolutions.projet.gestion.garage.model.SalesOrder;
import ma.esolutions.projet.gestion.garage.model.SalesOrderDelivery;
import ma.esolutions.projet.gestion.garage.model.SalesOrderDeliveryDAO;
import ma.esolutions.projet.gestion.garage.model.SalesOrderLine;
import ma.esolutions.projet.gestion.garage.model.SalesOrderLineDAO;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS22;
import ma.esolutions.projet.gestion.garage.view.common.AbstractFormView;
import ma.esolutions.projet.gestion.garage.view.common.JTextFieldExtended;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author E-solutions
 */
public class ReturnFormView extends AbstractFormView<Return> {

    private Return theReturn;
    private boolean isNewModel;
    private CustomerDAO theCustomerDAO;
    private List<Customer> theCustomers;
    private List<Item> theExistingItems;
    private JLabel theItemNameLabel;
    private ItemDAO theItemDAO;
    private DefaultComboBoxModel theItemNameModel;
    private JComboBox theItemNameComboBox;
    private JLabel theItemQuantiteLabel;
    private JTextFieldExtended theItemQuantiteTextfield;
    private JLabel theReturnDateLabel;
    private JTextFieldExtended theReturnDateTextfield;
    private Date theDate;
    private JLabel theTotalPriceLabel;
    private JTextFieldExtended theTotalPriceTextfield;
    private JTextFieldExtended theCodeTextField;
    private DefaultComboBoxModel theCustomerNameModel;
    private JComboBox theCustomerNameComboBox;
    private JLabel theCustomerLabel;
    private JButton calcPriceBtn;
    private SalesOrderLineDAO theSalesOrderLineDAO;
    private SalesOrderDeliveryDAO theSalesOrderDelivery;
    private JLabel theCodeLabel;

    public ReturnFormView(Return ret) {
        super(ApplicationController.getAppView());
        if (ret != null) {
            if (ret.getTheDelivery() != null) {
                if (ret.getTheDelivery().getId() == null) {
                    isNewModel = true;
                } else {
                    isNewModel = false;
                }
            }
        }
        theReturn = ret;
        theItemDAO = new ItemDAO();
        theExistingItems = theItemDAO.all();
        if (isNewModel) {
            theExistingItems = theItemDAO.all();
        } else {
            if (ret.getTheDelivery() != null) {
                theExistingItems = ret.getTheDelivery().getTheSalesOrder().getTheOrderLines().stream().map(l -> l.getTheOrderedItem()).collect(Collectors.toList());
            }
        }
        theCustomerDAO = new CustomerDAO();
        theCustomers = theCustomerDAO.all();
        theSalesOrderLineDAO = new SalesOrderLineDAO();

    }

    @Override
    public void buildUI() {
        initComponents();
        addPageToForm("Articles à retourner",
                buildPage());
        if (!isNewModel) {
            popFields();
        }
        pack();
    }

    private JPanel buildPage() {
        JPanel panel = new JPanel(new MigLayout());

        theItemNameLabel = new JLabel("Nom de l'article");

        theItemNameModel = new DefaultComboBoxModel(theExistingItems.toArray());
        theItemNameComboBox = new JComboBox(theItemNameModel);
        theItemNameComboBox.setEditable(true);
        theItemQuantiteLabel = new JLabel("Quantité");
        theItemQuantiteTextfield = new JTextFieldExtended(50);
        theItemQuantiteTextfield.setText("0");
        theReturnDateLabel = new JLabel("Date ");
        theReturnDateTextfield = new JTextFieldExtended(50);
        theDate = new Date();
        theReturnDateTextfield.setText(theDate.toString());
        theReturnDateTextfield.setEnabled(false);
        theTotalPriceLabel = new JLabel("Prix Total");
        theTotalPriceTextfield = new JTextFieldExtended(50);
        theTotalPriceTextfield.setEditable(false);
        theCustomerLabel = new JLabel("Client");
        theCustomerNameModel = new DefaultComboBoxModel(theCustomers.toArray());
        theCustomerNameComboBox = new JComboBox(theCustomerNameModel);
        theCustomerNameComboBox.setEditable(true);
        theCodeTextField = new JTextFieldExtended(50);

        calcPriceBtn = new JButton("Ajouter ");
        theSalesOrderDelivery = new SalesOrderDeliveryDAO();
        calcPriceBtn.addActionListener((ee) -> {
            setTotalPrice(theSalesOrderLineDAO);
            if (!theCodeTextField.getText().equals("")) {
                SalesOrderDelivery d = theSalesOrderDelivery.find(Long.parseLong(theCodeTextField.getText()));

                System.out.println(d);
                Item selected = (Item) theItemNameComboBox.getSelectedItem();
                SalesOrder o = d.getTheSalesOrder();
                double pr = 0.0;
                //  o.getTheOrderLines().stream().filter(l->l.getTheOrderedItem().getTheItemName().equalsIgnoreCase(selected.getTheItemName()));

                for (SalesOrderLine ol : o.getTheOrderLines()) {
                    if (ol.getTheOrderedItem().getTheItemName().equalsIgnoreCase(selected.getTheItemName())) {
                        pr = ol.getTheOrderedItem().getTheUnitPrice() * (1 + (ol.getTheOrderedItem().getTheTVA()) / 100);
                        //  System.out.println("returned item price " + pr);
                        theTotalPriceTextfield.setText(String.valueOf(pr * Integer.parseInt(theItemQuantiteTextfield.getText())));
                        Item it = new Item();
                        it.setId(ol.getTheOrderedItem().getId());
                        try {
                            it.setTheItemQuantity(Integer.parseInt(theItemQuantiteTextfield.getText()));
                        } catch (NegativeStockException ex) {
                            Logger.getLogger(ReturnFormView.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (MinimalStockException ex) {
                            Logger.getLogger(ReturnFormView.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        it.setTheUnitPrice(ol.getTheOrderedItem().getTheUnitPrice());
                        it.setTheSalesPriceTTC(ol.getTheOrderedItem().getTheUnitPrice() * (1 + ol.getTheOrderedItem().getTheTVA() / 100.0));
                        System.out.println("returned item" + it.getTheItemQuantity());
                        ((ItemReturnCommand) theControllerCommand).addToReturnList(it);

                    }
                }
            }
        });
        theCustomerNameComboBox.addItemListener((e) -> {
            this.theTotalPriceTextfield.setText("0");
            String s = String.valueOf(getUnitPriceByItemAndCustomer(theSalesOrderLineDAO));

            if (theCustomerNameComboBox.getSelectedIndex() != -1) {
                if (theItemNameComboBox.getSelectedIndex() != -1) {
                    setTotalPrice(theSalesOrderLineDAO);
                }
            }
        });
        theItemNameComboBox.addItemListener((e) -> {
            this.theTotalPriceTextfield.setText("0");

            String s;
            if (theCustomerNameComboBox.getSelectedIndex() != -1) {
                if (theItemNameComboBox.getSelectedIndex() != -1) {
                    //setTotalPrice(theSalesOrderLineDAO);
                    if (theReturn.getTheDelivery() != null) {
                        theReturn.setTheDelivery(this.theSalesOrderDelivery.find(Long.parseLong(theCodeTextField.getText())));
                    } else {
                        theReturn.getTheDelivery().getTheSalesOrder().getTheOrderLines().forEach(ol -> {
                            if (Objects.equals(ol.getTheOrderedItem().getId(), ((Item) theItemNameComboBox.getSelectedItem()).getId())) {
                                theItemQuantiteTextfield.setText(String.valueOf(ol.getTheQuantity()));

                            }

                        });
                    }
                }
            }
        });
        theCodeLabel = new JLabel("Code de livraison");
        panel.add(theCustomerLabel, "gaptop 100,split,left,width 300");
        panel.add(theCustomerNameComboBox, "gaptop 100,right,width 300,wrap");
        panel.add(theCodeLabel, "split ,width 300,left");
        panel.add(theCodeTextField, "right,width 300,wrap");
        panel.add(theReturnDateLabel, "split,width 300,left");

        panel.add(theReturnDateTextfield, "right,width 300,wrap");

        panel.add(theItemNameLabel, "split,width 300,left");

        panel.add(theItemNameComboBox, "right,width 300,wrap");
        panel.add(theItemQuantiteLabel, "split,width 300,left");

        panel.add(theItemQuantiteTextfield, "right,width 300,wrap");
        if (theItemNameComboBox.getSelectedIndex() != -1) {
            if (theReturn != null) {
                if (theReturn.getTheDelivery() != null) {
                    theReturn.getTheDelivery().getTheSalesOrder().getTheOrderLines().forEach(ol -> {
                        if (Objects.equals(ol.getTheOrderedItem().getId(), ((Item) theItemNameComboBox.getSelectedItem()).getId())) {
                            theItemQuantiteTextfield.setText(String.valueOf(ol.getTheQuantity()));

                        }

                    });
                }
            }
        }

        panel.add(theTotalPriceLabel, "split,width 300,left");

        panel.add(theTotalPriceTextfield, "right,width 300,wrap");
        panel.add(calcPriceBtn, "split,right,width 300,wrap");
        return panel;
    }

    private void setTotalPrice(SalesOrderLineDAO theSalesOrderLineDAO) {
        String s;
        try {
            Long q = Long.parseLong(theItemQuantiteTextfield.getText());
            s = String.valueOf(q * getUnitPriceByItemAndCustomer(theSalesOrderLineDAO));

        } catch (NumberFormatException ee) {
            s = "0.0";
        }
        this.theTotalPriceTextfield.setText(s);
    }

    private double getUnitPriceByItemAndCustomer(SalesOrderLineDAO theSalesOrderLineDAO) {
        return theSalesOrderLineDAO.getUnitPriceByItemAndCustomer((Customer) theCustomerNameComboBox.getSelectedItem(),
                (Item) theItemNameComboBox.getSelectedItem());
    }

    @Override
    public Return getEntity() {
        return theReturn;
    }

    @Override
    public String getFormIconPath() {
        return ICONS22 + "return.png";
    }

    @Override
    public String getFormTitle() {
        if (isNewModel) {
            return "Retourner un article";
        } else {
            return "Mettre à jour ...";
        }
    }

    @Override
    public void onHelp() {
        return;
    }

    @Override
    public void popFields() {
        if (theReturn.getTheDelivery() != null) {
            this.theCodeTextField.setText(String.valueOf(theReturn.getTheDelivery().getId()));
        }
    }

    @Override
    public Return pushFields() {
        Return ret = new Return();
        if (this.theCustomerNameComboBox.getSelectedIndex() != -1) {
            ret.setTheCustomer((Customer) this.theCustomerNameComboBox.getSelectedItem());
        }
        if (this.theItemNameComboBox.getSelectedIndex() != -1) {
            ret.setTheItem((Item) this.theItemNameComboBox.getSelectedItem());
        }
        ret.setTheItemQuantity(Long.parseLong(this.theItemQuantiteTextfield.getText()));
        ret.setTheItemReturnDate(theDate);
        if (theCustomerNameComboBox.getSelectedIndex() != -1 && theItemNameComboBox.getSelectedIndex() != -1) {
            ret.setTheTotalPrice(Double.parseDouble(theTotalPriceTextfield.getText()));
        }
        System.out.println("price " + ret.getTheTotalPrice());
        this.theReturn = ret;
        return ret;
    }

    @Override
    public String getIconPath() {
        return ICONS22 + "return.png";
    }

    @Override
    public Component asComponent() {
        return null;
    }

}
