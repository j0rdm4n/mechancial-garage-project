/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.controller.commands.PlaceAddCommand;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.Item;
import ma.esolutions.projet.gestion.garage.model.ItemPlace;
import ma.esolutions.projet.gestion.garage.model.ItemPlaceDAO;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS16;
import ma.esolutions.projet.gestion.garage.view.common.AbstractListView;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSearchPanel;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSidePanel;
import ma.esolutions.projet.gestion.garage.view.common.EntityTableColumn;

/**
 *
 * @author E-solutions
 */
public class PlaceListView extends AbstractListView<ItemPlace> {
    private PlaceSidePanel thePlaceSidePanel;

    public PlaceListView( List<ItemPlace> data) {
        super(null, data);
    }

    @Override
    public void addTableColumns() {
         getTableModel().addColumn(new EntityTableColumn(
                "Nom d'emplacement", "theItemPlaceName", String.class, 300));
    }

    @Override
    public void showEntitySummary() {
    }

    @Override
    public void showEntityActions() {
    }

    @Override
    public void fireUpdateEvent(MouseEvent e) {
        System.out.println("updating   plll place");
        
        ItemPlace place;
        place = this.getSelectedModel();
        System.out.println("selected model " + place);
        if(place != null)
        {
            ApplicationController.getAppView().firePlacesUpdateEvent(place);
        }
    }

    @Override
    public AbstractSidePanel getSidePanel() {
        if(thePlaceSidePanel == null)
            thePlaceSidePanel = new PlaceSidePanel();
         this.setTheRowObserver(thePlaceSidePanel);
        return thePlaceSidePanel;
    }

    @Override
    public List<ItemPlace> getTheTableData() {
        return new ArrayList();
    }

    @Override
    public String getTitle() {
        return "Emplacements";
    }

    @Override
    public String getIconPath() {
            return ApplicationView.ICONS22+"list-place-icon.png";
    }
    @Override
     public AbstractSearchPanel getSearchPanel() {

        if (theSearchPanel == null) {
            theSearchPanel = new PlaceSearchPanel();
            theSearchPanel.setParentListView(this);
        }
        return theSearchPanel;
    }

    @Override
    public ItemPlace getModel() {
        return new ItemPlace();
    }

    @Override
    public void delete(ItemPlace e) {
        (new ItemPlaceDAO()).remove(e);
    }

    @Override
    public SecureCommand<ItemPlace> getRelatedFormCommand(ItemPlace e) {
        return new PlaceAddCommand(e);
    }
}
