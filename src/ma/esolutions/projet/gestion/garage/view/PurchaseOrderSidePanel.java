/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import javafx.scene.Node;
import javafx.scene.layout.VBox;
import ma.esolutions.projet.gestion.garage.model.PurchaseOrder;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSidePanel;

/**
 *
 * @author E-solutions
 */
class PurchaseOrderSidePanel extends AbstractSidePanel<PurchaseOrder> {

    public PurchaseOrderSidePanel() {
    }

    @Override
    public Node createCenterUI() 
    {
        return new VBox();
    }

    @Override
    public void handleRowSelected() {
        
    }

    @Override
    public void getFieldsValues() {
    }

    @Override
    public String getTitle() {
        
        return "Détails Commande achat";
    }
    
}
