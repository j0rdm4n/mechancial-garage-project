/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import java.util.Map;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.Chart;
import javafx.scene.chart.XYChart;
import ma.esolutions.projet.gestion.garage.controller.MainController;

/**
 *
 * @author born2code
 */
public class QuantityPerItemBarChart extends AbstractStockXYChart {

    public QuantityPerItemBarChart(MainController controller) {
        super(controller);
    }

 

   



    @Override
    protected void notifyChart() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Chart createChart() {
          BarChart bar = new BarChart<>(this.getXAxis(),this.getYAxis());
        bar.setTitle(getTitle());
        bar.setBarGap(1.0);
        bar.setHorizontalZeroLineVisible(true);
        
        getData();
                theSeries.setName(getSeriesLabel());

        
        bar.getData().addAll(theSeries);
        return bar;
    }

    @Override
    public String getReports() {
        return "";
    }
    
}
