/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import ma.esolutions.projet.gestion.garage.controller.commands.CommercialPaperAddCommand;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.CommercialPaper;
import ma.esolutions.projet.gestion.garage.model.CommercialPaperDAO;
import ma.esolutions.projet.gestion.garage.model.Payment;
import ma.esolutions.projet.gestion.garage.model.PaymentDAO;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS16;
import ma.esolutions.projet.gestion.garage.view.common.AbstractListView;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSearchPanel;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSidePanel;
import ma.esolutions.projet.gestion.garage.view.common.EntityTableColumn;

/**
 *
 * @author E-solutions
 */
public class CommercialPaperListView extends AbstractListView<CommercialPaper> {

    private CommercialPaperSidePanel theCommercialPaperSidePanel;

    public CommercialPaperListView() {
        super(null, new ArrayList());
    }

    @Override
    public AbstractSearchPanel getSearchPanel() {
        if (theSearchPanel == null) {
            theSearchPanel = new CommercialPaperSearchPanel();
            theSearchPanel.setParentListView(this);
        }
        return theSearchPanel;
    }

    @Override
    public void addTableColumns() {
        getTableModel().addColumn(new EntityTableColumn(
                "Nom de fournisseur", "vendor", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "Montant", "ammount", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "Date d'entrée", "date", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "Date de paiement", "dueDate", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "Etat", "state", String.class, 300));

    }

    @Override
    public void showEntitySummary() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void showEntityActions() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void fireUpdateEvent(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AbstractSidePanel getSidePanel() {
        if (theCommercialPaperSidePanel == null) {
            theCommercialPaperSidePanel = new CommercialPaperSidePanel();
            setTheRowObserver(theCommercialPaperSidePanel);
        }

        return theCommercialPaperSidePanel;
    }

    @Override
    public String getTitle() {
        return "Gestion des effets ";
    }

    @Override
    public String getIconPath() {
        return ICONS16 + "inventory.png";
    }

    /*@Override
     public SecureCommand<CommercialPaper> getRelatedFormCommand() {
       
     CommercialPaper cp = getSelectedModel();
     return new CommercialPaperAddCommand(new CommercialPaper());
     }*/
    @Override
    public SecureCommand<CommercialPaper> getRelatedFormCommand(CommercialPaper e) {

        return new CommercialPaperAddCommand(e);

    }

    @Override
    public CommercialPaper getModel() {
        return new CommercialPaper();
    }

    @Override
    public void delete(CommercialPaper e) {

        (new CommercialPaperDAO()).remove(e);
    }

    @Override
    public boolean hasCash() {
        return true;
    }

    @Override
    public void onPayment() {
        System.out.println("payment effet ! ");
        CommercialPaper cp = getSelectedModel();
        if (cp == null) {
            ApplicationView.showErrorMsg("Selectionnez un effet pour le payer !");
            return;
        }
        if (cp.getState().equalsIgnoreCase("payé")) {
            ApplicationView.showErrorMsg("Cet effet est déja payé !");
            return;
        }
        int tva = 20;
        Payment p = new Payment();
        p.setTheVendor(cp.getVendor());
        p.setThePartnerType("fournisseur");
        p.setThePaymentType("achat");
        if (!cp.getPurchaseOrder().getTheOrderedItems().isEmpty()) {
            if (cp.getPurchaseOrder().getTheOrderedItems().get(0) != null) {
                tva = cp.getPurchaseOrder().getTheOrderedItems().get(0).getTheOrderedItem().getTheTVA();
            }
        }
        p.setTheHTaxAmount((-1) * cp.getPurchaseOrder().getTheCost() / (1 + tva / 100.0));
        p.setTheTTCAmount((-1) * cp.getPurchaseOrder().getTheCost());
        p.setTheCashingDate(cp.getDueDate());

        (new PaymentDAO()).create(p);
        cp.setState("payé");

        (new CommercialPaperDAO()).update(cp);
        ApplicationView.showErrorMsg("Le payment de cet effet est fait avec succés ! ");
    }

}
