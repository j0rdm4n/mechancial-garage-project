/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import com.itextpdf.awt.DefaultFontMapper;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import static javax.swing.Spring.width;
import javax.swing.table.DefaultTableModel;
import ma.esolutions.projet.gestion.garage.controller.commands.ItemReturnCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.SalesOrderOpenCommand;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.Customer;
import ma.esolutions.projet.gestion.garage.model.CustomerDAO;
import ma.esolutions.projet.gestion.garage.model.Item;
import ma.esolutions.projet.gestion.garage.model.ItemDAO;
import ma.esolutions.projet.gestion.garage.model.MinimalStockException;
import ma.esolutions.projet.gestion.garage.model.NegativeStockException;
import ma.esolutions.projet.gestion.garage.model.Payment;
import ma.esolutions.projet.gestion.garage.model.PaymentDAO;
import ma.esolutions.projet.gestion.garage.model.Return;
import ma.esolutions.projet.gestion.garage.model.ReturnDAO;
import ma.esolutions.projet.gestion.garage.model.SalesInvoice;
import ma.esolutions.projet.gestion.garage.model.SalesInvoiceDAO;
import ma.esolutions.projet.gestion.garage.model.SalesOrder;
import ma.esolutions.projet.gestion.garage.model.SalesOrderDAO;
import ma.esolutions.projet.gestion.garage.model.SalesOrderDelivery;
import ma.esolutions.projet.gestion.garage.model.SalesOrderDeliveryDAO;
import ma.esolutions.projet.gestion.garage.model.SalesOrderLine;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS16;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS22;
import ma.esolutions.projet.gestion.garage.view.common.AbstractListView;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSearchPanel;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSidePanel;
import ma.esolutions.projet.gestion.garage.view.common.EntityTableColumn;
import ma.esolutions.projet.gestion.garage.view.common.IRowObserver;
import net.miginfocom.swing.MigLayout;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author E-solutions
 */
public class SalesOrderListView extends AbstractListView<SalesOrder> {

    private SalesOrderSidePanel theSalesOrderSidePanel;
    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
            Font.BOLD);
    private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.NORMAL, BaseColor.RED);
    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16,
            Font.BOLD);
    private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.BOLD);
    //  private static String PDF = "D:\\ProjetsJava\\gestgarage\\src\\ma\\esolutions\\projet\\gestion\\garage\\app\\resources\\";
    private SalesInvoice theSalesInvoice;

    public SalesOrderListView(List<SalesOrder> data) {
        super(null, data);
    }

    @Override
    public void addTableColumns() {
        getTableModel().addColumn(new EntityTableColumn(
                "Date", "theStartDate", String.class, 300));
        /*   getTableModel().addColumn(new EntityTableColumn(
         "Date de fin", "theClosedDate", String.class, 300));*/
        getTableModel().addColumn(new EntityTableColumn(
                "Client", "theCustomer", String.class, 300));

        getTableModel().addColumn(new EntityTableColumn(
                "Payement", "thePaymentType", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn("Facturé ?", "theInvoicedStatus", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn("Livré ?", "theDeliveredStatus", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn("Encaissement de facture", "thePaymentStatus", String.class, 300));
    }

    @Override
    public void showEntitySummary() {
    }

    @Override
    public void showEntityActions() {
    }

    @Override
    public void fireUpdateEvent(MouseEvent e) {
    }

    @Override
    public AbstractSidePanel getSidePanel() {
        if (theSalesOrderSidePanel == null) {
            theSalesOrderSidePanel = new SalesOrderSidePanel();
        }

        this.setTheRowObserver(theSalesOrderSidePanel);
        return theSalesOrderSidePanel;
    }

    @Override
    public AbstractSearchPanel getSearchPanel() {
        if (theSearchPanel == null) {
            theSearchPanel = new SalesOrderSearchPanel();
            theSearchPanel.setParentListView(this);
        }

        return theSearchPanel;
    }

    @Override
    public List<SalesOrder> getTheTableData() {
        return null;
    }

    @Override
    public String getTitle() {
        return "Gestion des ventes";
    }

    @Override
    public String getIconPath() {

        return ICONS22 + "list-sell-icon.png";
    }

    @Override
    public void setTheRowObserver(IRowObserver theRowObserver) {
        this.theRowObserver = theRowObserver;
    }

    @Override
    public SalesOrder getModel() {
        return new SalesOrder();
    }

    @Override
    public void delete(SalesOrder e) {
        (new SalesOrderDAO()).remove(e);
    }

    @Override
    public SecureCommand<SalesOrder> getRelatedFormCommand(SalesOrder e) {
        return new SalesOrderOpenCommand(e);
    }

    @Override
    protected boolean hasInvoice() {
        return true;
    }

    @Override
    protected boolean hasReturn() {
        return true;
    }

    private int getCurrentYear() {
        Calendar r = Calendar.getInstance();
        return r.get(Calendar.YEAR);
    }

    protected boolean hasLineChart() {
        return true;
    }

    @Override
    protected JFreeChart getLineChart() {

        SalesOrderDAO salesOrderDAO = new SalesOrderDAO();
        DefaultTableModel dtm = new DefaultTableModel();
        dtm.addColumn("Mois");
        dtm.addColumn("Ventes");
        JTable table = new JTable();
        table.setModel(dtm);
        Map<String, Number> m = salesOrderDAO.getSalesPerMonthForYear(getCurrentYear());

        DefaultCategoryDataset salesData = new DefaultCategoryDataset();

        m.forEach((k, v) -> {
            salesData.setValue(v, "Q", k);
            Object[] rowData = new String[2];
            rowData[0] = k;
            rowData[1] = String.valueOf(v);
            dtm.addRow(rowData);
        });
        JFreeChart objChartLine = ChartFactory.createLineChart("Ventes pour année courante", "Mois", "Ventes", salesData, PlotOrientation.VERTICAL, true, true, false);
        lineChart = objChartLine;
        return objChartLine;
    }

    @Override
    protected boolean hasBarChart() {
        return true;
    }

    @Override
    protected JFreeChart getBarChart() {
        SalesOrderDAO salesOrderDAO = new SalesOrderDAO();
        DefaultTableModel dtm = new DefaultTableModel();
        dtm.addColumn("Mois");
        dtm.addColumn("Ventes");
        JTable table = new JTable();
        table.setModel(dtm);
        Map<String, Number> m = salesOrderDAO.getSalesPerMonthForYear(getCurrentYear());

        DefaultCategoryDataset salesData = new DefaultCategoryDataset();

        m.forEach((k, v) -> {
            salesData.setValue(v, "Q", k);
            Object[] rowData = new String[2];
            rowData[0] = k;
            rowData[1] = String.valueOf(v);
            dtm.addRow(rowData);
        });
        JFreeChart objChartBar = ChartFactory.createBarChart("Ventes pour année courante", "Mois", "Ventes", salesData, PlotOrientation.VERTICAL, true, true, false);
        barChart = objChartBar;
        return objChartBar;
    }

    @Override
    protected void onReport() {

        SalesOrderDAO salesOrderDAO = new SalesOrderDAO();
        Map<String, Number> m = salesOrderDAO.getSalesPerMonthForYear(getCurrentYear());
        DefaultTableModel dtm = new DefaultTableModel();
        dtm.addColumn("Mois");
        dtm.addColumn("Ventes");
        JTable table = new JTable();
        table.setModel(dtm);
        double totalAmmount = salesOrderDAO.getTotalSalesForYear(getCurrentYear());
        long totalOrders = salesOrderDAO.getCountOrdersPerYear(getCurrentYear());
        // long totalItems = salesOrderDAO.getCountItemsPerYear(getCurrentYear());
        long totalCustomers = salesOrderDAO.getCountCustomersPerYear(getCurrentYear());

        DefaultCategoryDataset salesData = new DefaultCategoryDataset();

        m.forEach((k, v) -> {
            salesData.setValue(v, "Ventes", k);
            Object[] rowData = new String[2];
            rowData[0] = k;
            rowData[1] = String.valueOf(v);
            dtm.addRow(rowData);
        });

        JFrame container = new JFrame("Les rapports de ventes... ");
        container.setForeground(Color.blue);
        container.setLayout(new BorderLayout());
        container.add(new JLabel("Rapport des ventes pour l'année " + getCurrentYear()), BorderLayout.NORTH);
        JPanel panel = new JPanel(new MigLayout());
        JFreeChart objChartBar = ChartFactory.createBarChart("Ventes pour année courante", "Mois", "Ventes", salesData, PlotOrientation.VERTICAL, true, true, false);
        JFreeChart objChartLine = ChartFactory.createLineChart("Ventes pour année courante", "Mois", "Ventes", salesData, PlotOrientation.VERTICAL, true, true, false);
        JPanel chartbar = new ChartPanel(objChartBar);
        JPanel chartline = new ChartPanel(objChartLine);
        JPanel summary = new JPanel(new MigLayout());
        chartbar.setPreferredSize(new Dimension(600, 300));
        chartline.setPreferredSize(new Dimension(600, 300));
        table.setPreferredSize(new Dimension(600, 300));

        summary.add(new JLabel("Montant total des ventes "), "split ,width 100,left ");
        summary.add(new JLabel("" + totalAmmount), "width 100,right,wrap");

        summary.add(new JLabel("Nombre total des commandes "), "split,width 100,left");
        summary.add(new JLabel("" + totalOrders), "width 100,right,wrap");

        summary.add(new JLabel("Nombre total des clients "), "split,width 100,left");
        summary.add(new JLabel("" + totalCustomers), "width 100,right,wrap");

        // summary.add(new JLabel("Nombre total des produits "),"split,width 100,left");
        //  summary.add(new JLabel(""+totalItems),"width 100,right,wrap");
        panel.add(new JScrollPane(table), "split,left");
        panel.add(summary, "right,wrap");
        panel.add(new JScrollPane(chartbar), "wrap");

        panel.add(new JScrollPane(chartline), "wrap");

        JButton pdfBtn = new JButton("Générer le rapport en format pdf");

        String filename = "ventes" + String.valueOf(year()) + String.valueOf(month()) + String.valueOf(day()) + ".pdf";
        pdfBtn.addActionListener((e) -> {
            Document doc = new Document(PageSize.A4.rotate());

            try {
                try {
                    //graphics2dl.dispose();
                    //graphics2db.dispose();
                    createPdf(filename);
                    int yesno = ApplicationView.showAskYesNo("Voulez vous visualiser le rapport des ventes en format pdf ?");
                    if (yesno == JOptionPane.YES_OPTION) {
                        viewPdf(filename);
                    }

                } catch (DocumentException ex) {
                    Logger.getLogger(SalesOrderListView.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (IOException ex) {
                Logger.getLogger(SalesOrderListView.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        JScrollPane pane = new JScrollPane(panel);
        pane.setPreferredSize(new Dimension(800, 600));

        container.getContentPane().add(pane, BorderLayout.CENTER);
        container.add(pdfBtn, BorderLayout.SOUTH);
        container.pack();
        container.setVisible(true);

    }

    @Override
    protected void onReturn() {
        SalesOrder so = getSelectedModel();
        if (so == null) {
            ApplicationView.showErrorMsg("Selectionnez une commande ! ");
            return;

        }
        if (so.getReturnedStatus().equalsIgnoreCase("oui")) {
            ApplicationView.showErrorMsg("Impossible d'effectuer cette opération ! la commande est déja retournée !");

            return;
        }
        if (so.getTheDeliveredStatus().equalsIgnoreCase("non")) {
            ApplicationView.showErrorMsg("Selectionnez une commande déja livrée pour le retour !");
            return;

        }
        if (so.getTheDeliveredStatus().equalsIgnoreCase("oui")) {

            SalesOrderDeliveryDAO dao = new SalesOrderDeliveryDAO();
            SalesOrderDelivery sod = dao.getBySalesOrderId(so.getId());
            if (sod != null) {

                int noyes = ApplicationView.showAskYesNo("Voulez vous retourner toute la commande ? ");
                if (noyes == JOptionPane.NO_OPTION) {
                    Return ret = new Return();
                    ret.setTheDelivery(sod);
                    ItemReturnCommand cmd = new ItemReturnCommand(ret);
                    cmd.execute();
                    List<Item> returned = cmd.getReturnedItems();
                    if (Objects.equals(returned.size(), 0)) {
                        ApplicationView.showErrorMsg("Rien n'est retourné !");
                        return;
                    }
                    double retTTC;
                    double retHT;
                    retTTC = returned.stream().mapToDouble(ee -> ee.getTheSalesPriceTTC() * ee.getTheItemQuantity()).sum();
                    retHT = returned.stream().mapToDouble(ee -> ee.getTheUnitPrice() * ee.getTheItemQuantity()).sum();
                    if (so.getThePaymentStatus().equalsIgnoreCase("oui")) {
                        if (so.getThePaymentType().equalsIgnoreCase("crédit")) {
                            Customer co = so.getTheCustomer();
                            double crd = co.getTheCredit() - retTTC;
                            if (crd >= 0) {
                                co.setTheCredit(crd);
                                (new CustomerDAO()).update(co);
                            } else {
                                ApplicationView.showErrorMsg("Erreur : une valeur négative pour le crédit,opération annullée ! ");
                                return;
                            }
                        }
                        if (so.getThePaymentType().equalsIgnoreCase("espéces")) {
                            Payment e = new Payment();
                            e.setThePaymentType("retour");
                            e.setTheCustomer(so.getTheCustomer());
                            e.setTheTTCAmount((-1) * retTTC);
                            e.setTheHTaxAmount((-1) * retHT);
                            System.out.println("the returned ammount is " + retTTC);
                            e.setTheCashingDate(new Date());
                            (new PaymentDAO()).create(e);
                        }

                    }

                    so.setReturnedStatus("oui");
                    (new SalesOrderDAO()).update(so);

                    ApplicationView.showErrorMsg("la commande a été retournée avec succés");
                    return;
                }

                Set<SalesOrderLine> lines = sod.getTheSalesOrder().getTheOrderLines();
                ItemDAO itemDAO = new ItemDAO();
                ReturnDAO returnDAO = new ReturnDAO();
                double retAmt = 0;
                double retAmtHT = 0;
                for (SalesOrderLine l : lines) {
                    Item it = l.getTheOrderedItem();
                    try {
                        // Item pItem= itemDAO.find(it.getId());
                        it.setTheItemQuantity(it.getTheItemQuantity() + l.getTheQuantity());
                    } catch (NegativeStockException ex) {
                        Logger.getLogger(SalesOrderListView.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (MinimalStockException ex) {
                        Logger.getLogger(SalesOrderListView.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    itemDAO.update(it);
                    retAmt += l.getTheQuantity() * l.getTheOrderedItem().getTheSalesPriceTTC();
                    retAmtHT += l.getTheQuantity() * (l.getTheOrderedItem().getTheUnitPrice());
                    System.out.println("loop sales order lines" + l);
                    Return ret = new Return();
                    ret.setTheCustomer(so.getTheCustomer());
                    ret.setTheItemReturnDate(new Date());
                    ret.setTheItem(it);
                    ret.setTheItemQuantity(l.getTheQuantity());
                    ret.setTheDelivery(sod);
                    returnDAO.create(ret);

                }
                if (so.getThePaymentStatus().equalsIgnoreCase("oui")) {

                    if (so.getThePaymentType().equalsIgnoreCase("crédit")) {
                        Customer co = so.getTheCustomer();
                        double crd = co.getTheCredit() - retAmt;
                        if (crd >= 0) {
                            co.setTheCredit(crd);
                            (new CustomerDAO()).update(co);
                        } else {
                            ApplicationView.showErrorMsg("Erreur : une valeur négative pour le crédit,opération annullée ! ");
                            return;
                        }
                    }
                    if (so.getThePaymentType().equalsIgnoreCase("espéces")) {
                        Payment e = new Payment();
                        e.setThePaymentType("retour");
                        e.setTheCustomer(so.getTheCustomer());
                        e.setTheTTCAmount((-1) * retAmt);
                        e.setTheHTaxAmount((-1) * retAmtHT);
                        System.out.println("the returned ammount is " + retAmt);
                        e.setTheCashingDate(new Date());
                        (new PaymentDAO()).create(e);
                    }
                }
                so.setReturnedStatus("oui");
                (new SalesOrderDAO()).update(so);
                ApplicationView.showErrorMsg("la commande a été retournée avec succés");

            }
        }

    }

    @Override
    protected boolean hasInvoicePrint() {
        return true;
    }

    @Override
    protected boolean hasCash() {
        return true;
    }
   
    protected void onPayment() {
        SalesOrder so = getSelectedModel();
        if (so == null) {
            ApplicationView.showErrorMsg("Selectionnez une commande facturée pour l'encaisser ! ");
            return;
        }
        if (so.getTheInvoicedStatus().equalsIgnoreCase("non")) {
            int yesno = ApplicationView.showAskYesNo("Cette commande n'est pas encore facturée.Voulez vous la facturer ?");
            if (yesno == JOptionPane.NO_OPTION) {
                return;
            }
            generateInvoice(so);

        }
        if (so.getTheInvoicedStatus().equalsIgnoreCase("oui")) {
            if (so.getThePaymentStatus().equalsIgnoreCase("non")) {
                if (so.getThePaymentType().equalsIgnoreCase("espéces")) {
                    notifyPayment(so);

                }
                if (so.getThePaymentType().equalsIgnoreCase("crédit")) {
                    System.out.println("updating credit !!!! " + so.getTheTotalTTCPrice());
                    notifyCredit(so);
                }
                so.setThePaymentStatus("oui");
                (new SalesOrderDAO()).update(so);
                ApplicationView.showErrorMsg("Encaissement effectué avec succés !");
            } else {
                ApplicationView.showErrorMsg("Vous avez déja encaissé la facture de cette commande !");

            }
        }
    }

    protected void onPrintInvoice() {
        super.onPrintInvoice();

        SalesOrder so = getSelectedModel();

        if (so == null) {
            ApplicationView.showErrorMsg("Selectionnez une commande pour imprimer sa facture !");
            return;
        }
        if (so.getTheInvoicedStatus().equalsIgnoreCase("non")) {
            ApplicationView.showAskYesNo("Cette commande n'as pas de facture !");
            return;
        }
        if (so.getTheInvoicedStatus().equalsIgnoreCase("oui")) {
            if (theSalesInvoice != null) {
                System.out.println("printing invoice");
                printInvoice(theSalesInvoice);
            }
        }

    }

    protected void onInvoice() {
        SalesOrder e = getSelectedModel();
        if (e == null) {
            ApplicationView.showErrorMsg("Selectionnez une commande pour la facturer !");
        } else {
            if (e.getTheInvoicedStatus().equalsIgnoreCase("oui")) {
                ApplicationView.showErrorMsg("Cette commande est déja facturé");
            } else {
                generateInvoice(e);
            }

        }
    }

    @Override
    protected boolean hasBilling() {
        return true;
    }

    @Override
    protected void onBilling() {

        SalesOrder so = getSelectedModel();
        if (so == null) {
            ApplicationView.showErrorMsg("Selectionnez une commande  pour procéder à la livraison ! ");
            return;
        }
        if (so.getTheDeliveredStatus().equalsIgnoreCase("oui")) {
            int noyes = ApplicationView.showAskYesNo("Voulez vous imprimer le bon");
            if (noyes == JOptionPane.YES_OPTION) {
                printBill(so);
            }

            return;
        }
        if (so.getTheInvoicedStatus().equalsIgnoreCase("non")) {
            int yesno = ApplicationView.showAskYesNo("Cette commande n'est pas encore facturée.Voulez vous la facturer ?");
            if (yesno == JOptionPane.NO_OPTION) {
                return;
            }
            generateInvoice(so);
            if (so.getThePaymentStatus().equalsIgnoreCase("non")) {
                int yesno2 = ApplicationView.showAskYesNo("La facture de cette commande n'est pas encore encaissé.Voulez vous l'encaisser ?");
                if (yesno2 == JOptionPane.YES_OPTION) {
                    notifyPayment(so);
                }
            }

        }
        if (so.getTheInvoicedStatus().equalsIgnoreCase("oui")) {
            //if(ro.getEncaissementStatus())

            //printBill();
           

            try {
                notifyStock(so);
                createBill(so);
                 so.setTheDeliveredStatus("oui");
                int noyes = ApplicationView.showAskYesNo("Voulez vous imprimer ce bon ? ");
                if (noyes == JOptionPane.YES_OPTION) {
                  //  printBill(so);
                    ApplicationView.showErrorMsg("Cette fonctionalité n'est pas disponible dans cette version !");
                    return;
                }
            } catch (NegativeStockException ex ) {
                Logger.getLogger(SalesOrderListView.class.getName()).log(Level.SEVERE, null, ex);
               // ApplicationView.showErrorMsg("Commande annullée car le stock est indisponible");
                  int askYesNo = ApplicationView.showAskYesNo("Voulez vous créer une notification de commande d'achat ?");
                    if(askYesNo == JOptionPane.YES_OPTION)
                    {
                        ApplicationView.createAndShowPurchaseNotification();
                        
                    }
            } catch (MinimalStockException ex) {
                Logger.getLogger(SalesOrderListView.class.getName()).log(Level.SEVERE, null, ex);
              //  ApplicationView.showErrorMsg("commande annullée car le stock est minimum !");
                  int askYesNo = ApplicationView.showAskYesNo("Voulez vous créer une notification de commande d'achat ?");
                    if(askYesNo == JOptionPane.YES_OPTION)
                    {
                        ApplicationView.createAndShowPurchaseNotification();
                        
                    }
            }

        }

    }

    private void printBill(SalesOrder so) {

    }

    private void createBill(SalesOrder so) {
        SalesOrderDelivery sod = new SalesOrderDelivery();

        sod.setTheDeliveryDate(new Date());
        sod.setTheSalesOrder(so);
        SalesOrderDeliveryDAO deliveryDAO = new SalesOrderDeliveryDAO();

        if (deliveryDAO.getBySalesOrderId(so.getId()) == null) {

            (new SalesOrderDeliveryDAO()).create(sod);
            ApplicationView.showErrorMsg("Le bon de livraison a été crée avec succés !");
        }

    }

    private void generateInvoice(SalesOrder e) {
        if (e.getTheInvoicedStatus().equalsIgnoreCase("non")) {
            SalesInvoice oi = new SalesInvoice();
            oi.setTheCustomer(e.getTheCustomer());
            oi.setTheInvoiceDate(new Date());
            oi.setTheSalesOrder(e);
            oi.setThePaymentType(e.getThePaymentType());
            oi.setTheTotal(e.getTheTotalTTCPrice());

            (new SalesInvoiceDAO()).create(oi);
            e.setTheInvoicedStatus("oui");
            (new SalesOrderDAO()).update(e);
            theSalesInvoice = oi;
            System.out.println("the sales invoice " + theSalesInvoice);
            //refreshData();
            int YesNo = ApplicationView.showAskYesNo("Facture generée avec succés ,voullez vous l'imprimer ? ");
            if (YesNo == JOptionPane.YES_OPTION) {

                printInvoice(oi);
            }

        }
    }

    private void printInvoice(SalesInvoice oi) {

        setupPdf(oi.getId(), "Facture", "Facturation", "facture", oi.getTheCustomer().getTheFirstname() + " " + oi.getTheCustomer().getTheLastname(), (float) oi.getTheTotal(), (float) ((float) oi.getTheTotal() / (1 + 20.0 / 100)), oi.getThePaymentType());
        String path = System.getProperty("user.dir");
        System.out.println("app path " + path);
        if (Desktop.isDesktopSupported()) {
            File f = new File(path + "\\facture" + oi.getId() + ".pdf");
            try {
                Desktop.getDesktop().open(f);
            } catch (IOException ex) {
                Logger.getLogger(SalesOrderFormView.class.getName()).log(Level.SEVERE, null, ex);
                ApplicationView.showErrorMsg("Le fichier de facture n'existe pas !");
            }
        }

    }

    private void setupPdf(long id, String title, String subject, String name, String customer, float ttc, float ht, String paymenttype) {

        Document document = new Document();
        String path = System.getProperty("user.dir");
        System.out.println(path);
        try {
            try {
                PdfWriter.getInstance(document, new FileOutputStream(name + id + ".pdf"));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(SalesOrderFormView.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (DocumentException ex) {
            Logger.getLogger(SalesOrderFormView.class.getName()).log(Level.SEVERE, null, ex);
        }
        document.open();
        addMetaData(document, title, subject);
        try {
            addTitlePage(document, title, id, customer, ttc, ht, paymenttype);
        } catch (DocumentException ex) {
            Logger.getLogger(SalesOrderFormView.class.getName()).log(Level.SEVERE, null, ex);
        }

        document.close();
    }

    private void addTitlePage(Document document, String title, long id, String customerName, float totalTTC, float totalHT, String paymentType)
            throws DocumentException {
        Paragraph preface = new Paragraph();
        addEmptyLine(preface, 1);

        preface.add(new Paragraph(title + id, catFont));
        addEmptyLine(preface, 1);

        addEmptyLine(preface, 3);
        preface.add(new Paragraph("Nom du client :" + customerName,
                smallBold));
        preface.add(new Paragraph("Prix total TTC à payer :" + totalTTC + " DH",
                smallBold));
        preface.add(new Paragraph("Prix total HT à payer :" + totalHT + " DH",
                smallBold));
        preface.add(new Paragraph("Payement par  :" + paymentType,
                smallBold));

        addEmptyLine(preface, 4);
        preface.add(new Paragraph("", redFont));
        createTableInvoice(theSalesInvoice, preface);

        document.add(preface);
        //  document.newPage();
    }

    private void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

    private void createTableInvoice(SalesInvoice oi, Paragraph subCatPart)
            throws BadElementException {
        System.out.println("nbr of cells" + oi.getTheSalesOrder().getTheOrderLines().size());
        PdfPTable table = new PdfPTable(3);

        PdfPCell c1 = new PdfPCell(new Phrase("Article"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Prix unitaire"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Quantité"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);
        table.setHeaderRows(1);
        Set<SalesOrderLine> ol = oi.getTheSalesOrder().getTheOrderLines();
        ol.forEach((o) -> {

            table.addCell(o.getTheOrderedItem().toString());
            table.addCell("" + o.getTheOrderedItem().getTheUnitPrice() + " DH");
            table.addCell("" + o.getTheQuantity());
        });

        // table.addCell("2.1");
        // table.addCell("2.2");
        // table.addCell("2.3");
        subCatPart.add(table);

    }

    private void createTableVoucher(SalesOrderDelivery oi, Paragraph subCatPart)
            throws BadElementException {
        System.out.println("nbr of cells" + oi.getTheSalesOrder().getTheOrderLines().size());
        PdfPTable table = new PdfPTable(2);

        PdfPCell c1 = new PdfPCell(new Phrase("Article"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Quantité"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);
        table.setHeaderRows(1);
        Set<SalesOrderLine> ol = oi.getTheSalesOrder().getTheOrderLines();
        ol.forEach((o) -> {

            table.addCell(o.getTheOrderedItem().toString());
            table.addCell("" + o.getTheOrderedItem().getTheUnitPrice() + " DH");
            table.addCell("" + o.getTheQuantity());
        });
        subCatPart.add(table);

    }

    private void addMetaData(Document document, String title, String subject) {
        document.addTitle(title);
        document.addSubject(subject);
        document.addAuthor("Bouchefra Ahmed");
        document.addCreator("Bouchefra Ahmed");
    }

    public void notifyCredit(SalesOrder so) {
        Customer c = so.getTheCustomer();
        c.setTheCredit(c.getTheCredit() + so.getTheTotalTTCPrice());
        System.out.println("setting the client credit " + c.getTheCredit());
        (new CustomerDAO()).update(c);
    }

    public void notifyStock(SalesOrder so) throws NegativeStockException, MinimalStockException {
        Item item;
        for (SalesOrderLine ol : so.getTheOrderLines()) {
            item = ol.getTheOrderedItem();
            if (item != null) {
                if (item.getTheItemQuantity() > 0) {

                    item.setTheItemQuantity(item.getTheItemQuantity() - ol.getTheQuantity());
                    (new ItemDAO()).update(item);

                }

            }
        }
    }

    public void notifyPayment(SalesOrder so) {
        System.out.println("notifying the payment");
        Item item = null;
        double quantity = 0;
        double price = 0;
        double pricettc = 0;
        double sumht = 0.0;
        double sumttc = 0.0;
        for (SalesOrderLine ol : so.getTheOrderLines()) {
            item = ol.getTheOrderedItem();
            quantity = ol.getTheQuantity();
            price = item.getTheUnitPrice();
            pricettc = item.getTheSalesPriceTTC();
            System.out.println("tva" + item.getTheTVA() + " price ttc " + pricettc);
            System.out.println("the total ttc quantity " + quantity * price);
            sumht += price * quantity;
            sumttc += quantity * pricettc;
        }
        if (so.getThePaymentType().equalsIgnoreCase("crédit")) {
            Customer co = so.getTheCustomer();
            co.setTheCredit(co.getTheCredit() + sumttc);
            (new CustomerDAO()).update(co);
            return;
        }
        Payment p = new Payment();
        p.setThePaymentType("vente");
        p.setTheHTaxAmount(sumht);
        p.setTheTTCAmount(sumttc);
        p.setTheCashingDate(new Date());
        System.out.println("customer " + so.getTheCustomer());
        p.setTheCustomer(so.getTheCustomer());

        (new PaymentDAO()).create(p);
        System.out.println("notifying payment " + p);
        // (new PaymentDAO()).getTodayTotal();
    }

}
