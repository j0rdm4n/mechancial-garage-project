/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.awt.BorderLayout;
import java.util.List;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javax.swing.JButton;
import javax.swing.JPanel;
import ma.esolutions.projet.gestion.garage.model.Customer;
import ma.esolutions.projet.gestion.garage.model.CustomerDAO;
import ma.esolutions.projet.gestion.garage.model.Vendor;
import ma.esolutions.projet.gestion.garage.model.VendorDAO;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSidePanel;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author E-solutions
 */
final class VendorSidePanel extends AbstractSidePanel<Vendor> {

    public VendorSidePanel() {
        super();

        Node center = createCenterUI();

        Platform.runLater(() -> {
            //getRoot().setTop(createDynamicBar());
            // if (this.theEntity != null) {
            getRoot().setCenter(center);
            //}
        });
    }

    public void update() {
        Node center = createCenterUI();

        Platform.runLater(() -> {
            //getRoot().setTop(createDynamicBar());
            // if (this.theEntity != null) {
            getRoot().setCenter(center);
            //}
        });
    }

    
    @Override
    public Node createCenterUI() {
        CategoryAxis xAxis = new CategoryAxis();
        xAxis.setAutoRanging(true);
         int nbr = 10;
        xAxis.setTickLength(1);
        NumberAxis yAxis = new NumberAxis();
        yAxis.setTickLength(1);
        BarChart<String, Number> bc
                = new BarChart<>(xAxis, yAxis);
       
        xAxis.setLabel("Fournisseurs");
        yAxis.setLabel("total ventes");
        bc.setBarGap(3);
        bc.setCategoryGap(5);
        xAxis.setTickLabelRotation(90);
        XYChart.Series series1 = new XYChart.Series();
       
        VendorDAO vendorDAO = new VendorDAO();
        List<Vendor> l = vendorDAO.all();
        l.forEach(v->{  
            float tt = (float) v.getThePurchaseOrders().stream().mapToDouble(p->{ return p.getTheCost(); }).sum();
            series1.getData().add(new XYChart.Data(v.toString(), Math.abs(tt)));
        });
        
       
        
        bc.getData().add(series1);
        GridPane gp = new GridPane();
        int row = 0;int col = 0;
        
        gp.add(new Label("Fournisseurs"),col,row);
        row++;
        gp.add(bc,col,row);
        return gp;
        
    }
    @Override
    public void getFieldsValues() {
    }

    @Override
    public String getTitle() {
        return "Détails de fournisseur";
    }


    @Override
    public void handleRowSelected() {
    }

}
