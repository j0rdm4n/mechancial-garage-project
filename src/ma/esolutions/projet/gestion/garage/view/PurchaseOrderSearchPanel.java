/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSearchPanel;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author E-solutions
 */
public class PurchaseOrderSearchPanel extends AbstractSearchPanel {
    private JTextField vendorTxt;

    @Override
    public JPanel buildSearchForm() {
        JPanel searchPanel = new JPanel(new MigLayout());
        
        vendorTxt = new JTextField();
        
        
        searchPanel.add(new JLabel("Fournisseur "),"split,width 300,left");
        searchPanel.add(vendorTxt,"right , width 300,wrap");
        
        return searchPanel;
    }

    @Override
    public String buildSearchFilter() {
        StringBuilder filter = new StringBuilder();
        filter.append("theVendor");
        filter.append(":like:");
        filter.append(vendorTxt.getText());
        filter.append(";");
         return filter.toString();
    }
    
}
