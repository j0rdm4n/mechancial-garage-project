/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import java.awt.event.MouseEvent;
import java.util.List;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.controller.commands.UserAddCommand;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.User;
import ma.esolutions.projet.gestion.garage.model.UserDAO;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS22;
import ma.esolutions.projet.gestion.garage.view.common.AbstractListView;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSearchPanel;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSidePanel;
import ma.esolutions.projet.gestion.garage.view.common.EntityTableColumn;
import ma.esolutions.projet.gestion.garage.view.common.IRowObserver;

/**
 *
 * @author E-solutions
 */
public class UserListView extends AbstractListView<User> {
    private UserSidePanel theUserSidePanel;

    public UserListView(List<User> data) {
        super(null, data);
    }
    public AbstractSearchPanel getSearchPanel()
    {
            if(theSearchPanel == null)
            {
                theSearchPanel =new UserSearchPanel();
                theSearchPanel.setParentListView(this);
            }
            return theSearchPanel;
    }
    @Override
    public void addTableColumns() {
        getTableModel().addColumn(new EntityTableColumn(
                "Prénom", "theFirstname", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "Nom", "theLastname", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "Username", "theUsername", String.class, 300));
    
        getTableModel().addColumn(new EntityTableColumn(
                "Mot de passe", "thePassword", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "Email", "theEmail", String.class, 300));
       /* getTableModel().addColumn(new EntityTableColumn(
                "Addresse", "theAddress", String.class, 300));*/
        
        getTableModel().addColumn(new EntityTableColumn(
                "Télephone", "thePhone", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "Roles", "theAttributedRoles", String.class, 300));
        
    }

    @Override
    public void showEntitySummary() {
    }

    @Override
    public void showEntityActions() {
    }

    @Override
    public void fireUpdateEvent(MouseEvent e) {
          User u = getSelectedModel();
        if (u != null) {
            ApplicationController.getAppView().fireUserUpdateEvent(u);
        }
    }

    @Override
    public AbstractSidePanel getSidePanel() {
        if(theUserSidePanel == null)
        theUserSidePanel = new UserSidePanel();
       
        this.setTheRowObserver(theUserSidePanel);
        return theUserSidePanel;
    }

    @Override
    public List<User> getTheTableData() {
        return null;
    }

    @Override
    public String getTitle() {
        return "Gestion des utilisateurs";
    }

    @Override
    public String getIconPath() {
            return ICONS22 + "user-icon.png";
    }

    @Override
    public void setTheRowObserver(IRowObserver theRowObserver) {
        this.theRowObserver = theRowObserver;
    }

    @Override
    public User getModel() {
        return new User();
    }

    @Override
    public void delete(User e) {
            (new UserDAO()).remove(e);    }

    @Override
    public SecureCommand<User> getRelatedFormCommand(User e) {
        return new UserAddCommand(e);
    }
    
}
