/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSearchPanel;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author E-solutions
 */
public class UserSearchPanel extends AbstractSearchPanel {
    private JTextField nameTxt;
    private JTextField emailTxt;

    @Override
    public JPanel buildSearchForm() {
        JPanel searchPanel =new JPanel(new MigLayout());
        nameTxt = new JTextField();
        emailTxt = new JTextField();
        searchPanel.add(new JLabel("nom d'utilisateur "),"split ,left,width 300");
        searchPanel.add(nameTxt,"right,width 300,wrap");
        
        searchPanel.add(new JLabel("email"),"split,left,width 300");
        searchPanel.add(emailTxt,"right,width 300,wrap");
        
        return searchPanel;
        
    }

    @Override
    public String buildSearchFilter() {
        
        StringBuilder filter = new StringBuilder();
        filter.append("theName");
        filter.append(":like:");
        filter.append(nameTxt.getText());
        filter.append(";");
        filter.append("theEmail");
        filter.append(":like:");
        filter.append(emailTxt.getText());
        filter.append(";");
        return filter.toString();
    }
    
}
