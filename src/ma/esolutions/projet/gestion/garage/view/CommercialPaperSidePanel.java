/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import ma.esolutions.projet.gestion.garage.model.CommercialPaper;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.STYLES;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSidePanel;

/**
 *
 * @author E-solutions
 */
public class CommercialPaperSidePanel extends AbstractSidePanel<CommercialPaper> {

    @Override
    public Node createCenterUI() {
        return new GridPane();
    }

    @Override
    public void handleRowSelected() {
        //   System.out.println(this.theEntity);
//        Class entityClass = theEntity.getClass();
        //   for(Field f:entityClass.getFields())
        //   System.out.println("filed name " + f.getName());

        Node center = createUI();

        Platform.runLater(() -> {
            //getRoot().setTop(createDynamicBar());
            if (this.theEntity != null) {
                getRoot().setCenter(center);
            }
        });
    }

    public Node createUI() {
        GridPane grid = new GridPane();
        int row = 1;
        int col = 2;

        if (theEntity != null) {
            grid.setStyle(STYLES);
            grid.setAlignment(Pos.CENTER);
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(25, 25, 25, 25));
            Label vendorName = new Label("Fournisseur");
            grid.add(vendorName, 0, row);
            Label vendorNamev = new Label(theEntity.getVendor().toString());
            grid.add(vendorNamev, col, row++);

            Label ammName = new Label("Montant");
            grid.add(ammName, 0, row);
            Label ammNamev = new Label(String.valueOf(theEntity.getAmmount()));

            grid.add(ammNamev, col, row++);

        }
        return grid;
    }

    @Override
    public void getFieldsValues() {
    }

    @Override
    public String getTitle() {
        return "Détails effet ";
    }

}
