/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import javafx.scene.chart.Chart;
import javafx.scene.chart.LineChart;
import ma.esolutions.projet.gestion.garage.controller.MainController;

/**
 *
 * @author E-solutions
 */
public class MonthlySalesLineChart extends MonthlySalesXYChart{

    public MonthlySalesLineChart(MainController controller) {
        super(controller);
    }

 

    @Override
    protected Chart createChart() {
        
         LineChart lineChart = new LineChart(xAxis,yAxis);
         theSeries.setName(getSeriesLabel());
         getData();
         lineChart.getData().addAll(theSeries);
         return lineChart;
    }

    @Override
    public String getReports() {
        return "Mois avec le maximum des ventes \n"+
               "Mois avec le minimum des ventes \n"+
               "Le moyen arithmétique des ventes\n";
    }
    
}
