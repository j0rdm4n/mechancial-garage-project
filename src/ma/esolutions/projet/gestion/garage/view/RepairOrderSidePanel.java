/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.awt.BorderLayout;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javax.swing.JButton;
import javax.swing.JPanel;
import ma.esolutions.projet.gestion.garage.model.RepairOrder;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.STYLES;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSidePanel;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author E-solutions
 */
public class RepairOrderSidePanel extends AbstractSidePanel<RepairOrder> {

    public RepairOrderSidePanel() {
        super();
    }

    @Override
    public void getFieldsValues() {
    }

    @Override
    public String getTitle() {

        return "Détails de réparation";
    }

    @Override
    public Node createCenterUI() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Node createUI() {
        GridPane grid = new GridPane();
        int row = 1;
        int col = 2;

        if (theEntity != null) {
            grid.setStyle(STYLES);
            grid.setAlignment(Pos.CENTER);
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(25, 25, 25, 25));
            Label itemName = new Label("Nom de client");
            grid.add(itemName, 0, row);
            Label itemNamev = new Label(this.theEntity.getTheCustomer().toString());
            grid.add(itemNamev, col, row++);

            Label place = new Label("Date de début ");
            grid.add(place, 0, row);

            Label placev = new Label(theEntity.getTheStartDate().toString());
            grid.add(placev, col, row++);

            Label q = new Label("Date de fin ");
            grid.add(q, 0, row);

            Label qv = new Label("" + theEntity.getTheClosedDate().toString());
            grid.add(qv, col, row++);

            Label prix = new Label("Prix de main d'oeuvre ");
            grid.add(prix, 0, row);
            Label prixv = new Label("" + theEntity.getTheLaborCost());
            grid.add(prixv, col, row++);

        }
        return grid;
    }

    @Override
    public void handleRowSelected() {
        Node center = createUI();

        Platform.runLater(() -> {
            //getRoot().setTop(createDynamicBar());
            if (this.theEntity != null) {
                getRoot().setCenter(center);
            }
        });
    }

}
