/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.YES_OPTION;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.model.Customer;
import ma.esolutions.projet.gestion.garage.model.CustomerDAO;
import ma.esolutions.projet.gestion.garage.model.Item;
import ma.esolutions.projet.gestion.garage.model.ItemDAO;
import ma.esolutions.projet.gestion.garage.model.MinimalStockException;
import ma.esolutions.projet.gestion.garage.model.NegativeStockException;
import ma.esolutions.projet.gestion.garage.model.Payment;
import ma.esolutions.projet.gestion.garage.model.PaymentDAO;
import ma.esolutions.projet.gestion.garage.model.RepairOrder;
import ma.esolutions.projet.gestion.garage.model.RepairOrderDAO;
import ma.esolutions.projet.gestion.garage.model.SalesOrderLine;
import ma.esolutions.projet.gestion.garage.model.Vehicle;
import ma.esolutions.projet.gestion.garage.model.VehicleDAO;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS22;
import ma.esolutions.projet.gestion.garage.view.common.AbstractFormView;
import ma.esolutions.projet.gestion.garage.view.common.JTextFieldExtended;
import net.miginfocom.swing.MigLayout;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

/**
 *
 * @author E-solutions
 */
public class RepairOrderFormView extends AbstractFormView<RepairOrder> {

    private RepairOrder theRepairOrder;
    private final ArrayList<Item> theItems;
    private boolean isNewOrder;
    private JPanel thePanel;
    private JLabel theStartDateLabel;
    private JLabel theCloseDateLabel;
    private JDatePickerImpl theStartDatePicker;
    private JDatePanelImpl theStartDate;
    private JDatePanelImpl theCloseDate;
    private JDatePickerImpl theCloseDatePicker;
    private JLabel theStatusLabel;
    private JCheckBox theStatusCheckBox;
    private JLabel theCustomerLabel;
    private JComboBox theCustomerCombobox;
    private JLabel theOrderedItemsLabel;
    private JTextFieldExtended theItemsCountTextfield;
    private JLabel theItemsLabel;
    private JLabel theItemsQuantityLabel;
    private JComboBox theItemsCombobox;
    private JPanel theItemsPanel;
    private JLabel thePayementLabel;
    private String[] thePaymentTypes;
    private JComboBox thePayementCombo;
    private JButton theAddItemButton;
    private JButton theCloseButton;
    private JButton theSaveButton;
    private JScrollPane theItemsTableScrollPane;
    private DefaultTableModel theItemsTableModel;
    private JLabel theLaborCostLabel;
    private JTextFieldExtended theLaborCostTextfield;
    private ComboBoxModel theCustomersModel;
    private CustomerDAO theCustomerDAO;
    private JComboBox theVehicleCombobox;
    private ArrayList<Vehicle> theVehicles;
    private JLabel theVehicleLabel;
    private VehicleDAO theVehicleDAO;
    private final PaymentDAO thePaymentDAO;
    private final ItemDAO theItemDAO;

    public RepairOrderFormView(RepairOrder order, List<Item> items) {
        super(ApplicationController.getAppView());

        theRepairOrder = order;
        theItems = (ArrayList<Item>) items;
        theVehicleDAO = new VehicleDAO();
        theVehicles = new ArrayList();
        thePaymentDAO = new PaymentDAO();
        theItemDAO = new ItemDAO();
        if (order.getId() == null) {
            isNewOrder = true;
            theCustomerDAO = new CustomerDAO();
        } else {
            isNewOrder = false;
        }

    }

    @Override
    public void buildUI() {
        initComponents();
        addPageToForm("Commande de réparation ", buildPage());
        popFields();
        pack();

    }

    private JPanel buildPage() {
        JPanel panel = new JPanel(new MigLayout());
        theStartDateLabel = new JLabel("Date de début ");
        theCloseDateLabel = new JLabel("Date de fin");
        SimpleDateFormat theDateFormatter = new SimpleDateFormat();
        UtilDateModel mm = new UtilDateModel();
        mm.setValue(new Date());
        mm.setSelected(true);

        theStartDate = new JDatePanelImpl(mm);
        theStartDatePicker = new JDatePickerImpl((JDatePanelImpl) theStartDate, new JFormattedTextField.AbstractFormatter() {
            private String theDatePattern = "dd-MM-yyyy";
            private SimpleDateFormat theSimpleDateFormatter = new SimpleDateFormat(theDatePattern);

            @Override
            public Object stringToValue(String text) throws ParseException {
                return theSimpleDateFormatter.parseObject(text);
            }

            @Override
            public String valueToString(Object value) throws ParseException {
                if (value != null) {
                    Calendar cal = (Calendar) value;
                    return theSimpleDateFormatter.format(cal.getTime());

                }

                return "";
            }
        });
        Date d = new Date();
        UtilDateModel m = new UtilDateModel();

        m.setValue(new Date());
        m.setSelected(true);
        theCloseDate = new JDatePanelImpl(m);
        theCloseDatePicker = new JDatePickerImpl(theCloseDate, new JFormattedTextField.AbstractFormatter() {
            private String theDatePattern = "dd-MM-yyyy";
            private SimpleDateFormat theSimpleDateFormatter = new SimpleDateFormat(theDatePattern);

            @Override
            public Object stringToValue(String text) throws ParseException {

                return theSimpleDateFormatter.parseObject(text);
            }

            @Override
            public String valueToString(Object value) throws ParseException {
                if (value != null) {
                    Calendar cal = (Calendar) value;
                    return theSimpleDateFormatter.format(cal.getTime());

                }

                return "";

            }
        });

        theLaborCostLabel = new JLabel("Prix d'Oeuvre");
        theLaborCostTextfield = new JTextFieldExtended(90);
        theStatusLabel = new JLabel("Fermé ");
        theStatusCheckBox = new JCheckBox();
        theCustomerLabel = new JLabel("Client");
        if (theCustomerDAO != null) {
            theCustomersModel = new DefaultComboBoxModel(theCustomerDAO.all().toArray());
        } else {
            theCustomersModel = new DefaultComboBoxModel();

        }
        theCustomerCombobox = new JComboBox(theCustomersModel);
        theCustomerCombobox.setEditable(true);
        theOrderedItemsLabel = new JLabel("Articles demandés ");
        theItemsCountTextfield = new JTextFieldExtended(50);

        theItemsLabel = new JLabel("Articles");
        theItemsQuantityLabel = new JLabel("Quantité ");
        System.out.println("comb items " + theItems);
        theItemsCombobox = new JComboBox(theItems.toArray());
        theItemsPanel = new JPanel(new MigLayout());
        thePayementLabel = new JLabel("Méthode de Payement ");
        thePaymentTypes = new String[]{"Espéces", "Crédit"};
        thePayementCombo = new JComboBox(thePaymentTypes);
        JTable theItemsTable = buildItemsTable();
        theItemsCombobox.setEditable(true);
        theAddItemButton = new JButton("Ajouter un Article ");
        theCloseButton = new JButton("Fermer ");
        theSaveButton = new JButton("Enregistrer ");
        theVehicleLabel = new JLabel("Véhicule");

        repaint();
        theCustomerCombobox.addActionListener((e) -> {
            if (theCustomerCombobox.getSelectedIndex() != -1) {
                Customer c = (Customer) theCustomerCombobox.getSelectedItem();

                theVehicles = (ArrayList<Vehicle>) theVehicleDAO.getVehiclesByCustomer(c);
                System.out.println(theVehicles);
                theVehicleCombobox.setModel(new DefaultComboBoxModel(theVehicles.toArray()));
                theVehicleCombobox.repaint();
                theVehicleCombobox.invalidate();
                repaint();

            }
        });
        theVehicleCombobox = new JComboBox(new DefaultComboBoxModel(theVehicles.toArray()));
        Customer cc = (Customer) theCustomerCombobox.getSelectedItem();

        theVehicles = (ArrayList<Vehicle>) theVehicleDAO.getVehiclesByCustomer(cc);
        System.out.println(theVehicles);
        theVehicleCombobox.setModel(new DefaultComboBoxModel(theVehicles.toArray()));
        theVehicleCombobox.repaint();
        theVehicleCombobox.invalidate();

        theSaveButton.addActionListener(new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                theRepairOrder = pushFields();
            }
        });

        theAddItemButton.addActionListener(new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                int quantity = 0;
                try {
                    quantity = Integer.parseInt(theItemsCountTextfield.getText());

                } catch (NumberFormatException ex) {
                    ApplicationView.showErrorMsg("Entrez une valeur valide pour la quantité");
                }
                Item item = (Item) theItemsCombobox.getSelectedItem();

                if (isItemAvailable(item, quantity)) {
                    String price = String.valueOf((item).getTheSalesPriceTTC());
                    /*theItemsTableModel.addRow(new String[]{item.toString(),
                        theItemsCountTextfield.getText(), price, String.valueOf(item.getTheTVA())});*/
                    
                         if (isItemMinimal(item, quantity)) {

                        int askYesNo = ApplicationView.showAskYesNo("La réalisation de cette réparation va rendre la quantité de cet article  minimal,continuer ?");
                        if (askYesNo == JOptionPane.YES_OPTION) {
                            theItemsTableModel.addRow(new String[]{item.toString(),
                                theItemsCountTextfield.getText(), price, String.valueOf(item.getTheTVA())});
                                
                        }
                    } else {
                        theItemsTableModel.addRow(new String[]{item.toString(),
                            theItemsCountTextfield.getText(), price, String.valueOf(item.getTheTVA())});

                    }
                 /*   int askYesNo = ApplicationView.showAskYesNo("Voulez vous créer une notification de commande d'achat ?");
                    if(askYesNo == JOptionPane.YES_OPTION)
                    {
                        ApplicationView.createAndShowPurchaseNotification();
                        
                    }*/
                } else {
                    // setStatusbarMsg("Alerte : la quantitée demandée n'est pas disponible en stock");
                    int response = ApplicationView.showAskYesNo("la quantité demandée n'est pas disponible en stock");


                }
                theItemsCountTextfield.setText("");
            }

        });
        theCloseButton.addActionListener(new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                dispose();
            }
        });
        panel.add(theCustomerLabel, "gaptop 100,split,left,width 300");
        panel.add(theCustomerCombobox, "gaptop 100,right,width 300,wrap");
        panel.add(theVehicleLabel, "split,left,width 300");
        panel.add(theVehicleCombobox, "right,width 300,wrap");

        panel.add(theStartDateLabel, "split,width 300,left");

        panel.add(theStartDatePicker, "right,width 300,wrap");
    //    panel.add(theCloseDateLabel, "split,width 300,left");
     //   panel.add(theCloseDatePicker, "width 300,wrap");
        //   panel.add(theStatusLabel, "split,left,width 300");
        //  panel.add(theStatusCheckBox, "right,width 300,wrap");

        panel.add(theItemsLabel, "split,left,width 300");
        panel.add(theItemsCombobox, "right,width 300,wrap");
        panel.add(theOrderedItemsLabel, "split,left,width 300,wrap");
        panel.add(theItemsQuantityLabel, "split ,left,width 300");
        panel.add(theItemsCountTextfield, "right,width 300,wrap");
        panel.add(theAddItemButton, "split,right,width 300,height 20,wrap");
        panel.add(thePayementLabel, "split,left,width 300");
        panel.add(thePayementCombo, "right,width 300,wrap");
        panel.add(theOrderedItemsLabel, "split,left,width 300,left,wrap");
        panel.add(theItemsPanel, "right,width 300,wrap");
        theItemsTableScrollPane = new JScrollPane(theItemsTable);
        // table.setPreferredSize(new Dimension(400,200));
        theItemsTableScrollPane.setSize(new Dimension(300, 100));
        JScrollBar sb = new JScrollBar(JScrollBar.VERTICAL);
        sb.setAutoscrolls(true);
        theItemsTableScrollPane.setVerticalScrollBar(sb);
        theItemsTableScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

        theItemsTableScrollPane.setVisible(true);
        theItemsTableScrollPane.revalidate();
        panel.add(theItemsTableScrollPane, "right,height 60,width 300,wrap");
        panel.add(theLaborCostLabel, "split,left,width 300,height 20");
        panel.add(theLaborCostTextfield, "right,width 300,height 20,wrap");

        // panel.add(theSaveButton, "split,left,width 300,height 20,gaptop 30");
        //panel.add(theCloseButton, "right,width 300,height 20,gaptop 30,wrap");
        return panel;
    }

    private boolean isItemAvailable(Item item, int quantity) {
        if (item.getTheItemQuantity() < quantity) {
            return false;
        }
        return true;
    }
    private boolean isItemMinimal(Item item, int quantity) {
        if (Math.abs(item.getTheItemQuantity() - quantity) > item.getStockMin()) {
            return false;
        }
        return true;
    }
    /* private JTable buildItemsTable() {
     String[] columnsNames = {"Article", "Quantité"};
     theItemsTableModel = new DefaultTableModel(columnsNames, 0);

     //   theItemsTableModel.setColumnIdentifiers(new String[]{"Article","Quatité"});
     //tableModel.setRowCount(4);
     JTable table = new JTable(theItemsTableModel);

     //  table.addColumn(new TableColumn());
     return table;
     }*/

    private JTable buildItemsTable() {
        String[] columnsNames = {"Article", "Quantité", "Prix de vente TTC", "Pourcentage TVA"};
        theItemsTableModel = new DefaultTableModel(columnsNames, 0);

        //   theItemsTableModel.setColumnIdentifiers(new String[]{"Article","Quatité"});
        //tableModel.setRowCount(4);
        JTable table = new JTable(theItemsTableModel);

        //  table.addColumn(new TableColumn());
        return table;
    }

    @Override
    public RepairOrder getEntity() {
        return theRepairOrder;
    }

    @Override
    public String getFormIconPath() {
        return ICONS22 + "repair.png";
    }

    @Override
    public String getFormTitle() {
        return "Créer une commande de réparation ";
    }

    @Override
    public void onHelp() {
    }

    @Override
    public void popFields() {
    }
 
    public void notifyStock() {
        Item item = new Item();
        for (SalesOrderLine ol : theRepairOrder.getTheOrderLines()) {
            item = ol.getTheOrderedItem();

            try {
                item.setTheItemQuantity(item.getTheItemQuantity() - ol.getTheQuantity());
            } catch (NegativeStockException ex) {
                Logger.getLogger(RepairOrderFormView.class.getName()).log(Level.SEVERE, null, ex);
            } catch (MinimalStockException ex) {
                Logger.getLogger(RepairOrderFormView.class.getName()).log(Level.SEVERE, null, ex);
            }
            theItemDAO.update(item);
        }
    }

    public void notifyPayment() {
        System.out.println("notifying the payment");
        Item item = null;
        double quantity = 0;
        double price = 0;
        double sum = 0.0;
        double sumht = 0.0;
        for (SalesOrderLine ol : theRepairOrder.getTheOrderLines()) {
            item = ol.getTheOrderedItem();
            quantity = ol.getTheQuantity();
            price = item.getTheSalesPriceTTC();
            System.out.println("the total ttc quantity " + quantity * price);
            sum += price * quantity;
        }
        sum += theRepairOrder.getTheLaborCost();
        theRepairOrder.setTheTotal((float)sum);
        
        System.out.println("the repair order sum price " + sum);
        Payment p = new Payment();
        p.setThePaymentType("réparation et vente");
        p.setTheHTaxAmount(sum);
        p.setTheTTCAmount(sum);
        p.setTheCashingDate(new Date());
        System.out.println("customer " + theRepairOrder.getTheCustomer());
        p.setTheCustomer(theRepairOrder.getTheCustomer());
        thePaymentDAO.create(p);
    }

    @Override
    public RepairOrder pushFields() {
        RepairOrder o = new RepairOrder();
        o.setTheClosedDate((Date) this.theCloseDate.getModel().getValue());
        o.setTheStartDate((Date) this.theStartDate.getModel().getValue());
        if (this.theStatusCheckBox.isSelected()) {
            o.setTheStatus("ouverte");

        }
        if (theCustomerCombobox.getSelectedIndex() != -1) {
            o.setTheCustomer((Customer) theCustomerCombobox.getSelectedItem());
        }
        Set<SalesOrderLine> orderLines = new HashSet<SalesOrderLine>();
        theItemsTableModel.getDataVector().forEach((Object e) -> {
            SalesOrderLine s = new SalesOrderLine();

            Vector r = (Vector) e;
            System.out.println(r.get(0) + " " + r.get(1));
            Item myItem;
            s.setTheQuantity(Integer.parseInt((String) r.get(1)));
            theItems.forEach((Item it) -> {
                if (it.getTheItemName() == r.get(0)) {
                    s.setTheOrderedItem(it);
                }
            });
            orderLines.add(s);
        });
        System.out.println(orderLines);
        o.setTheOrderLines((HashSet<SalesOrderLine>) orderLines);
        try
        {
            o.setTheLaborCost(Long.parseLong(this.theLaborCostTextfield.getText()));
        }catch(NumberFormatException e)
        {
            o.setTheLaborCost((long)0);
        }
        if (theVehicleCombobox.getSelectedIndex() != -1) {
            o.setTheVehicle((Vehicle) theVehicleCombobox.getSelectedItem());
        }
        /*  if (theVehicleCombobox.getSelectedIndex() != -1) {
         o.setTheVehicle((Vehicle) theVehicleCombobox.getSelectedItem());
         } else {
         ApplicationView.showErrorMsg("Selectionnez le véhicule à réparer ");
         }*/
        System.out.println(o);
        this.theRepairOrder = o;
        return o;
    }

    @Override
    public String getIconPath() {
        return ICONS22 + "repair.png";
    }

    @Override
    public Component asComponent() {
        return null;
    }

    private void openPurchaseCommand() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
