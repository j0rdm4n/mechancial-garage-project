/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.util.Map;
import java.util.Set;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.Chart;
import javafx.scene.chart.XYChart;
import ma.esolutions.projet.gestion.garage.controller.MainController;

/**
 *
 * @author E-solutions
 */
public class SalesPerCustomerBarChart extends SalesPerCustomerXYChart {

    public SalesPerCustomerBarChart(MainController controller) {
        super(controller);
    }

    @Override
    protected void notifyChart() {
    }
 @Override
    protected XYChart.Series getData() {
        Map mm = theController.getSalesPerCustomer(theYear,theMonth,theDay);
        theSeries = new XYChart.Series();

      /*  for (String m : months) {
            theSeries.getData().add(new XYChart.Data(m, mm.get(m)));
        }*/
        Set<String> customers = mm.keySet();
        for(String s : customers)
        {
            theSeries.getData().add(new XYChart.Data(s, mm.get(s) ));
        }
        return theSeries;
    }
    @Override
    protected Chart createChart() {
        BarChart bar = new BarChart<>(xAxis, yAxis);
        bar.setTitle(getTitle());
        bar.setBarGap(1.0);
        bar.setHorizontalZeroLineVisible(true);

        getData();
        theSeries.setName(getSeriesLabel());

        System.out.println("theSeries is null ?" + theSeries);

        bar.getData().addAll(theSeries);
        return bar;

    }
   

}
