/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

/**
 *
 * @author E-solutions
 */
abstract public class DailyCaisseXYChart extends AbstractCaisseXYChart {

    private int theMonth;
    private int theYear;
    @Override
    protected String getXAxisLabel() {
        return "Encaissements journaliers";
    }

    public int getTheMonth() {
        return theMonth;
    }

    public void setTheMonth(int theMonth) {
        this.theMonth = theMonth;
    }

    public int getTheYear() {
        return theYear;
    }

    public void setTheYear(int theYear) {
        this.theYear = theYear;
    }
   
    @Override
    protected String getSeriesLabel() {
        return "Etat de caisse pour chaque jour pour moi: " + getTheMonth() +" année " + getTheYear();
    }

    
}
