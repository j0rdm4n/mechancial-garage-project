/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import ma.esolutions.projet.gestion.garage.app.Session;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.controller.LoginController;
import ma.esolutions.projet.gestion.garage.controller.MainController;
import ma.esolutions.projet.gestion.garage.controller.UserController;
import ma.esolutions.projet.gestion.garage.controller.commands.CommercialPaperAddCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.CommercialPaperListCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.CustomerAddCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.CustomerUpdateCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.CustomersListCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.InventoryAddCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.InventoryListCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.InventoryUpdateCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.ItemReturnCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.ItemReturnListCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.ListPaymentsCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.PlaceAddCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.PlaceListCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.PlaceUpdateCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.PurchaseOrderListCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.PurchaseOrderOpenCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.RepairOrderListCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.RepairOrderOpenCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.RoleAddCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.RoleListCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.SalesOrderListCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.SalesOrderOpenCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.SwitchAccountCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.UserAddCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.UserListCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.VehicleAddCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.VehicleListCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.VendorAddCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.VendorListCommand;
import ma.esolutions.projet.gestion.garage.model.CommercialPaper;
import ma.esolutions.projet.gestion.garage.model.Customer;
import ma.esolutions.projet.gestion.garage.model.Item;
import ma.esolutions.projet.gestion.garage.model.ItemPlace;
import ma.esolutions.projet.gestion.garage.model.PurchaseOrder;
import ma.esolutions.projet.gestion.garage.model.RepairOrder;
import ma.esolutions.projet.gestion.garage.model.Return;
import ma.esolutions.projet.gestion.garage.model.Role;
import ma.esolutions.projet.gestion.garage.model.SalesOrder;
import ma.esolutions.projet.gestion.garage.model.User;
import ma.esolutions.projet.gestion.garage.model.Vehicle;
import ma.esolutions.projet.gestion.garage.model.Vendor;
import ma.esolutions.projet.gestion.garage.view.common.IListView;
import ma.esolutions.projet.gestion.garage.view.common.IView;
import org.jdesktop.swingx.JXStatusBar;
import org.jvnet.flamingo.common.JCommandButton;
import org.jvnet.flamingo.common.icon.EmptyResizableIcon;
import org.jvnet.flamingo.common.icon.ResizableIcon;
import org.jvnet.flamingo.ribbon.JRibbonBand;
import org.jvnet.flamingo.ribbon.JRibbonFrame;
import org.jvnet.flamingo.ribbon.RibbonApplicationMenu;
import org.jvnet.flamingo.ribbon.RibbonApplicationMenuEntryPrimary;
import org.jvnet.flamingo.ribbon.RibbonApplicationMenuEntrySecondary;
import org.jvnet.flamingo.ribbon.RibbonElementPriority;
import org.jvnet.flamingo.ribbon.RibbonTask;
import org.jvnet.flamingo.ribbon.resize.CoreRibbonResizePolicies;
import org.jvnet.lafwidget.LafWidget;
import org.jvnet.substance.SubstanceLookAndFeel;
import org.jvnet.substance.api.ColorSchemeAssociationKind;
import org.jvnet.substance.api.ComponentState;
import org.jvnet.substance.api.SubstanceColorScheme;
import org.jvnet.substance.api.SubstanceConstants;
import org.jvnet.substance.skin.SubstanceOfficeSilver2007LookAndFeel;
import org.jvnet.substance.utils.SubstanceColorSchemeUtilities;

/**
 *
 * @author Ahmed
 */
public class ApplicationView extends JRibbonFrame implements ActionListener {

    public static final String MESSAGE_ASK_TITLE = "Quitter";
    public static final String MESSAGE_EXIT = "Voulez vous quitter ?";
    private static final String LABEL_ADMIN = "Administrateur ";
    public static final int OK_OPTION = JOptionPane.OK_OPTION;
    public static final int YES_OPTION = JOptionPane.YES_OPTION;
    public static final int NO_OPTION = JOptionPane.NO_OPTION;
    public static final int CANCEL_OPTION = JOptionPane.CANCEL_OPTION;
    public static final int CLOSED_OPTION = JOptionPane.CLOSED_OPTION;
    public static final String ICONS12 = "/ma/esolutions/projet/gestion/garage/app/resources/icons12/";
    public static final String ICONS16 = "/ma/esolutions/projet/gestion/garage/app/resources/icons16/";
    public static final String ICONS22 = "/ma/esolutions/projet/gestion/garage/app/resources/icons22/";
    public static final String IMAGES = "/ma/esolutions/projet/gestion/garage/app/resources/images/";
    private static final String ERROR_GUI = "Une erreur s'est produite lors de la construction de l'interface utilistaeur";
    public static final String ICONS48 = "/ma/esolutions/projet/gestion/garage/app/resources/icons48/";
    public static final String STYLES = "/ma/esolutions/projet/gestion/garage/app/resources/styles/styles.css";
    public static final String CHART_STYLE = "/ma/esolutions/projet/gestion/garage/app/resources/styles/charts.css";
    private static JTabbedPane theTabs;
    static String APPVERSION="1.0 beta";
    static String COPYRIGHT="Copyright esolutions 2014";
    static String APPDESCRIPTION = "eGestion pour la gestion commerciale simplifiée ";
    static String APPNAME="eGestion";

    public static void showConnectionPage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
  public static void browseUrl(String url) {
        try {
            Desktop.getDesktop().browse(URI.create(url));
        } catch (IOException ex) {
            ApplicationView.showErrorMsg("Impossible d'ouvrir ce lien !");
        }
    }

    static void createAndShowPurchaseNotification() {
        ApplicationView.showErrorMsg("Cette fonctionalité n'est pas disponible dans cette version !");
        throw new UnsupportedOperationException("Create purchase notification"); //To change body of generated methods, choose Tools | Templates.
        
    }


    //private ApplicationModel theAppModel = null;
    private CreateUserListener theLoginListener;
    private JXStatusBar theStatusBar;
    private static JPanel theMainPanel;

    private String theAppTitle;
    private boolean showExitDialog;

    private UserController theUserController;
    private LoginController theLoginController;
    private MainController theMainController;
    private IView theCurrentView;
    private boolean isListView;
    private CustomerListView theCustomersListView;
    private JLabel theUserLabel;
    private JLabel theStatusLabel;
    private InventoryListView theInventoryListView;
    private PlaceListView thePlaceListView;
    private PurchaseOrderOpenCommand thePurchaseOrderOpenCommand;
    private PurchaseOrderListCommand thePurchaseOrderListCommand;
    private CommercialPaperAddCommand theCommerciaPaperAddCommand;
    private CommercialPaperListCommand theCommercialPaperListCommand;

    public CustomerListView getTheCustomersListView() {
        return theCustomersListView;
    }

    public void setTheCustomersListView(CustomerListView theCustomersListView) {
        this.theCustomersListView = theCustomersListView;
    }

    /**
     *
     * @return
     */
    public void setLoginListener(CreateUserListener loginListener) {
        this.theLoginListener = loginListener;
    }

    public void fireLoginEvent(CreateUserEvent event) {
        if (theLoginListener != null) {
            theLoginListener.userCreated(event);
        }
    }

    private void initGUI() throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {

        JFrame.setDefaultLookAndFeelDecorated(true);
        JDialog.setDefaultLookAndFeelDecorated(true);
        try {
            UIManager.setLookAndFeel(new SubstanceOfficeSilver2007LookAndFeel());
            //UIManager.setLookAndFeel("org.pushingpixels.substance.api.skin.SubstanceGraphiteLookAndFeel");
            UIManager.put(SubstanceLookAndFeel.TABBED_PANE_CONTENT_BORDER_KIND,
                    SubstanceConstants.TabContentPaneBorderKind.DOUBLE_PLACEMENT);
            UIManager.put(LafWidget.TEXT_EDIT_CONTEXT_MENU, true);

        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(ApplicationController.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.setIconImages(Arrays.asList(
                new ImageIcon(getClass().getResource(
                                IMAGES + "app-26.png")).getImage(),
                new ImageIcon(getClass().getResource(
                                IMAGES + "app-32.png")).getImage())
        );
        addWindowListener(new WindowAdapter() {

            @Override
            public void windowOpened(WindowEvent e) {
                try {
                    //DatabaseMapper.getInstance().connect();
                } catch (Exception ee) {
                    //JOptionPane.showMessageDialAppViewi.this, "Impossible de se connecter à la base de données","Erreur",JOptionPane.WARNING_MESSAGE);
                }

            }

            @Override
            public void windowClosed(WindowEvent e) {
                // DatabaseMapper.getInstance().disconnect();
                exit(e);
            }
        });
    }

    private void setUpGUI(int width, int height) {
        try {
            initGUI();
            setSize(width, height);
            setResizable(true);
            setVisible(true);
            // this.setDefaultCloseOperation();
            initComponents();
            setLocationRelativeTo(null);

        } catch (ClassNotFoundException | UnsupportedLookAndFeelException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(ApplicationView.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void setUpTaskbar() {

        JCommandButton cbtnDashboard = new JCommandButton("",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "home-icon.png"))));
        cbtnDashboard.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Platform.runLater(() -> {
                    System.out.println("from javafx thread ");
                });
                onOpenHome();
            }

        });
        JCommandButton cbtnLogout = new JCommandButton("",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS16 + "exit.png"))));
        cbtnLogout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onExit();

            }

        });
        this.getRibbon().addTaskbarComponent(cbtnDashboard);
        this.getRibbon().addTaskbarComponent(cbtnLogout);
    }

    private void setUpApplicationMenu() {
        System.out.println("menu");
        RibbonApplicationMenuEntryPrimary amEntryDashboard = new RibbonApplicationMenuEntryPrimary(
                createResizableIcon(
                        new ImageIcon(getClass().getResource(ICONS48 + "home-icon.png"))),
                "Page de bienvenue              ", (ActionEvent e) -> {
                            onOpenHome();
                        }, JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_ACTION);
        amEntryDashboard.setActionKeyTip("D");

        RibbonApplicationMenuEntryPrimary amEntryExit = new RibbonApplicationMenuEntryPrimary(
                createResizableIcon(
                        new ImageIcon(getClass().getResource(ICONS48 + "exit-icon.png"))),
                "Quitter                  ", (ActionEvent e) -> {
                            onExit();
                        }, JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_ACTION);
        amEntryDashboard.setActionKeyTip("D");

        RibbonApplicationMenu applicationMenu = new RibbonApplicationMenu();
        applicationMenu.addMenuEntry(amEntryDashboard);
        applicationMenu.addMenuEntry(amEntryExit);

        this.getRibbon().setApplicationMenu(applicationMenu);

    }

    private JRibbonBand createApplicationBand() {
        JRibbonBand fichierBand = new JRibbonBand(
                "Application",
                new EmptyResizableIcon(22));
        fichierBand.setResizePolicies(
                CoreRibbonResizePolicies.getCorePoliciesRestrictive(fichierBand));

        JCommandButton cbtnAppExit = new JCommandButton(
                "Quitter",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "exitt.png"))));
        cbtnAppExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onExit();
            }
        });
        JCommandButton cbtnAppSettings = new JCommandButton(
                "Paramétres",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "settings.png"))));
        cbtnAppSettings.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onSettings();
            }
        });
        cbtnAppSettings.setActionKeyTip("S");
        JCommandButton cbtnAppLogin = new JCommandButton(
                "Se connecter ...",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "change-user-icon.png"))));
        cbtnAppLogin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onReconnect();
            }
        });
        cbtnAppLogin.setActionKeyTip("C");

        /*JCommandButton cbtnAppDisconnect = new JCommandButton(
         "Déconnexion",
         createResizableIcon(new ImageIcon(getClass().getResource(
         ICONS22 + "disconnect.png"))));
         cbtnAppDisconnect.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
         onLogout();
         }
         });
         cbtnAppDisconnect.setActionKeyTip("D");
         */
        fichierBand.addCommandButton(cbtnAppLogin, RibbonElementPriority.TOP);
        //  fichierBand.addCommandButton(cbtnAppDisconnect, RibbonElementPriority.TOP);
        //  fichierBand.addCommandButton(cbtnAppSettings, RibbonElementPriority.TOP);

        fichierBand.addCommandButton(cbtnAppExit, RibbonElementPriority.TOP);

        return fichierBand;
    }

    private JRibbonBand createUsersBand() {
        JRibbonBand fichierBand = new JRibbonBand(
                "Gestion des Utilisateurs",
                new EmptyResizableIcon(22));
        fichierBand.setResizePolicies(
                CoreRibbonResizePolicies.getCorePoliciesRestrictive(fichierBand));

        JCommandButton cbtnCreateUser = new JCommandButton(
                "Créer des utilisateurs",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "user-icon.png"))));
        cbtnCreateUser.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onCreateUser();
            }
        });
        cbtnCreateUser.setActionKeyTip("C");
        fichierBand.addCommandButton(cbtnCreateUser, RibbonElementPriority.TOP);

        JCommandButton cbtnDeleteUser = new JCommandButton(
                "Créer des roles",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "users-icon.png"))));
        cbtnDeleteUser.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onAddRole();
            }
        });
        cbtnDeleteUser.setActionKeyTip("S");
        fichierBand.addCommandButton(cbtnDeleteUser, RibbonElementPriority.TOP);

        JCommandButton cbtnAssignRole = new JCommandButton(
                "Afficher les roles",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "list-users-icon.png"))));
        cbtnAssignRole.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onShowRole();
            }

        });
        cbtnAssignRole.setActionKeyTip("A");
        fichierBand.addCommandButton(cbtnAssignRole, RibbonElementPriority.TOP);

        JCommandButton cbtnShowUsers = new JCommandButton(
                "Afficher les Utilisateurs",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "list-user-icon.png"))));
        cbtnShowUsers.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onShowUsers();
            }
        });
        cbtnShowUsers.setActionKeyTip("S");
        fichierBand.addCommandButton(cbtnShowUsers, RibbonElementPriority.TOP);

        return fichierBand;
    }

    private JRibbonBand createClientsBand() {
        JRibbonBand clientsBand = new JRibbonBand(
                "Gestion de Clients",
                new EmptyResizableIcon(22));
        clientsBand.setResizePolicies(
                CoreRibbonResizePolicies.getCorePoliciesRestrictive(clientsBand));
        JCommandButton cbtnAddCustomer = new JCommandButton(
                "Créer un fiche client",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "customer-icon.png"))));
        cbtnAddCustomer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onAddCustomer();
            }
        });
        cbtnAddCustomer.setActionKeyTip("A");
        clientsBand.addCommandButton(cbtnAddCustomer, RibbonElementPriority.TOP);
        JCommandButton cbtnAddVehicle = new JCommandButton(
                "Céer un fiche véhicule",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "vehicle-icon.png"))));

        cbtnAddVehicle.addActionListener((e) -> {
            onAddVehicle();
        });
        clientsBand.addCommandButton(cbtnAddVehicle, RibbonElementPriority.TOP);

        JCommandButton cbtnModifyCustomer = new JCommandButton(
                "Afficher les véhicules",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "list-vehicle-icon.png"))));
        cbtnModifyCustomer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onListVehicles();
            }

        });
        cbtnModifyCustomer.setActionKeyTip("A");
        clientsBand.addCommandButton(cbtnModifyCustomer, RibbonElementPriority.TOP);

        JCommandButton cbtnShowCustomers = new JCommandButton(
                "Afficher les Clients",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "list-customer-icon.png"))));
        cbtnShowCustomers.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onShowCustomers();
            }
        });
        cbtnShowCustomers.setActionKeyTip("A");
        clientsBand.addCommandButton(cbtnShowCustomers, RibbonElementPriority.TOP);

        return clientsBand;
    }

    private JRibbonBand createPlaceBand() {
        JRibbonBand placeBand = new JRibbonBand("Gestion des emplacements", new EmptyResizableIcon(22));
        placeBand.setResizePolicies(
                CoreRibbonResizePolicies.getCorePoliciesRestrictive(placeBand));
        JCommandButton cbtnNewPlace = new JCommandButton("Ajouter nouvel emplacement",
                createResizableIcon(new ImageIcon(getClass().getResource(ICONS22 + "place-icon.png"))));
        cbtnNewPlace.addActionListener((e) -> {
            onAddPlace();
        });
        JCommandButton cbtnListPlace = new JCommandButton("Afficher les emplacements ",
                createResizableIcon(new ImageIcon(getClass().getResource(ICONS22 + "list-place-icon.png"))));
        cbtnListPlace.addActionListener((e) -> {
            onShowPlaces();
        });

        placeBand.addCommandButton(cbtnNewPlace, RibbonElementPriority.TOP);
        placeBand.addCommandButton(cbtnListPlace, RibbonElementPriority.TOP);

        return placeBand;
    }

    private JRibbonBand createStockBand() {

        JRibbonBand stockBand = new JRibbonBand(
                "Gestion du stock",
                new EmptyResizableIcon(22));
        stockBand.setResizePolicies(
                CoreRibbonResizePolicies.getCorePoliciesRestrictive(stockBand));
        JCommandButton cbtnAppShow = new JCommandButton("Afficher le Stock",
                createResizableIcon(new ImageIcon(getClass().getResource(ICONS22 + "list-stock-icon.png"))));
        cbtnAppShow.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                onShowStock();
            }

        });
        JCommandButton cbtnAppAdd = new JCommandButton(
                "Alimenter le stock",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "stock-icon.png"))));
        cbtnAppAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onAddStock();
            }

        });
        // cbtnRapports.setActionKeyTip("A");
        stockBand.addCommandButton(cbtnAppAdd, RibbonElementPriority.TOP);
        stockBand.addCommandButton(cbtnAppShow, RibbonElementPriority.TOP);
        return stockBand;
    }

    private JRibbonBand createPurchasesBand() {
        JRibbonBand purchasesBand = new JRibbonBand(
                "Gestion des Achats",
                new EmptyResizableIcon(22));

        purchasesBand.setResizePolicies(
                CoreRibbonResizePolicies.getCorePoliciesRestrictive(purchasesBand));
        JCommandButton cbtnNewPurchase = new JCommandButton("Créer une commande achat",
                createResizableIcon(new ImageIcon(getClass().getResource(ICONS22 + "buy-icon.png"))));

        cbtnNewPurchase.addActionListener((e) -> {
            onAddPurchases();
        });
        purchasesBand.addCommandButton(cbtnNewPurchase, RibbonElementPriority.TOP);

        JCommandButton cbtnListPurchase = new JCommandButton("Afficher les commandes des achats",
                createResizableIcon(new ImageIcon(getClass().getResource(ICONS22 + "list-buyer-icon.png"))));

        cbtnListPurchase.addActionListener((e) -> {
            onShowPurchases();

        });
        purchasesBand.addCommandButton(cbtnListPurchase, RibbonElementPriority.TOP);

        return purchasesBand;
    }

    private JRibbonBand createSalesBand() {
        JRibbonBand salesBand = new JRibbonBand(
                "Gestion des Ventes",
                new EmptyResizableIcon(22));
        salesBand.setResizePolicies(
                CoreRibbonResizePolicies.getCorePoliciesRestrictive(salesBand));

        JCommandButton cbtnAppOuvrir = new JCommandButton(
                "Créer une commande de vente",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "sell-icon.png"))));
        cbtnAppOuvrir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onOpenSalesOrder();
            }

        });
        cbtnAppOuvrir.setActionKeyTip("O");
        salesBand.addCommandButton(cbtnAppOuvrir, RibbonElementPriority.TOP);
        JCommandButton cbtnAppAfficher = new JCommandButton(
                "Afficher les commandes de ventes",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "list-sell-icon.png"))));
        cbtnAppAfficher.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onShowSalesOrders();
            }

        });
        cbtnAppAfficher.setActionKeyTip("O");
        salesBand.addCommandButton(cbtnAppAfficher, RibbonElementPriority.TOP);
        return salesBand;
    }

    /*  private JRibbonBand createCaisseBand() {
     JRibbonBand retBand = new JRibbonBand(
     "Gestion de caisse",
     new EmptyResizableIcon(22));
     retBand.setResizePolicies(
     CoreRibbonResizePolicies.getCorePoliciesRestrictive(retBand));
        
     JCommandButton cbtnSales = new JCommandButton(
     "Encaissements des ventes",
     createResizableIcon(new ImageIcon(getClass().getResource(
     ICONS22 + "list_sales.png"))));
        
     cbtnSales.addActionListener(new ActionListener() {
     @Override
     public void actionPerformed(ActionEvent e) {
     onShowSalesEnc();

     }

     });
        
        
     JCommandButton cbtnPurchases = new JCommandButton(
     "Encaissements des achats",
     createResizableIcon(new ImageIcon(getClass().getResource(
     ICONS22 + "return.png"))));
       
     cbtnPurchases.addActionListener(new ActionListener() {
     @Override
     public void actionPerformed(ActionEvent e) {
     onShowPurchasesEnc();

     }

     });

     JCommandButton cbtnEnc = new JCommandButton(
     "A Encaisser",
     createResizableIcon(new ImageIcon(getClass().getResource(
     ICONS22 + "return.png"))));
       
     cbtnEnc.addActionListener(new ActionListener() {
     @Override
     public void actionPerformed(ActionEvent e) {
     onAEncaisser();
     }

     });
     retBand.addCommandButton(cbtnEnc, RibbonElementPriority.TOP);
     retBand.addCommandButton(cbtnPurchases, RibbonElementPriority.TOP);
     retBand.addCommandButton(cbtnSales, RibbonElementPriority.TOP);
     return retBand;
     }*/
    private JRibbonBand createReturnBand() {
        JRibbonBand retBand = new JRibbonBand(
                "Gestion de retour",
                new EmptyResizableIcon(22));
        retBand.setResizePolicies(
                CoreRibbonResizePolicies.getCorePoliciesRestrictive(retBand));

        JCommandButton cbtnAdd = new JCommandButton(
                "Retourner un article",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "return-icon.png"))));
        cbtnAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onAddReturn();
            }

        });
        cbtnAdd.setActionKeyTip("R");
        retBand.addCommandButton(cbtnAdd, RibbonElementPriority.TOP);

        JCommandButton cbtnList = new JCommandButton(
                "Afficher les retours",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "list-return-icon.png"))));
        cbtnList.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("list returns");
                onShowReturns();
            }

        });
        cbtnList.setActionKeyTip("L");
        retBand.addCommandButton(cbtnList, RibbonElementPriority.TOP);
        return retBand;
    }

    private JRibbonBand createRepairBand() {
        JRibbonBand reparationsBand = new JRibbonBand(
                "Gestion des Réparations",
                new EmptyResizableIcon(22));
        reparationsBand.setResizePolicies(
                CoreRibbonResizePolicies.getCorePoliciesRestrictive(reparationsBand));
        JCommandButton cbtnAppOuvrir = new JCommandButton(
                "Ouvrir une commande de réparation",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "repair-icon.png"))));
        cbtnAppOuvrir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onOpenRepairOrder();
            }

        });
        cbtnAppOuvrir.setActionKeyTip("O");

        reparationsBand.addCommandButton(cbtnAppOuvrir, RibbonElementPriority.TOP);
        /*JCommandButton cbtnAppCloturer = new JCommandButton(
         "Cloturer un ordre de réparation",
         createResizableIcon(new ImageIcon(getClass().getResource(
         ICONS22 + "close_repair.png"))));
         cbtnAppCloturer.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
         onCloseRepairOrder();
         }

         });*/
        //cbtnAppCloturer.setActionKeyTip("O");

       // reparationsBand.addCommandButton(cbtnAppCloturer, RibbonElementPriority.TOP);
        JCommandButton cbtnAppAfficher = new JCommandButton(
                "Afficher les commandes de réparations",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "list-repair-icon.png"))));
        cbtnAppAfficher.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onShowRepairOrders();
            }

        });
        cbtnAppAfficher.setActionKeyTip("O");

        reparationsBand.addCommandButton(cbtnAppAfficher, RibbonElementPriority.TOP);

        return reparationsBand;
    }

    private JRibbonBand createVendorsBand() {
        JRibbonBand fournisseursBand = new JRibbonBand(
                "",
                new EmptyResizableIcon(22));
        fournisseursBand.setResizePolicies(
                CoreRibbonResizePolicies.getCorePoliciesRestrictive(fournisseursBand));

        JCommandButton cbtnAddProvider = new JCommandButton(
                "Ajouter un fournisseur",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "new-seller-icon.png"))));
        cbtnAddProvider.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onAddVendor();
            }
        });
        cbtnAddProvider.setActionKeyTip("A");
        fournisseursBand.addCommandButton(cbtnAddProvider, RibbonElementPriority.TOP);

        JCommandButton cbtnShowProviders = new JCommandButton(
                "Afficher les fournisseurs",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "seller-icon.png"))));
        cbtnShowProviders.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onShowVendors();
            }
        });
        cbtnShowProviders.setActionKeyTip("A");
        fournisseursBand.addCommandButton(cbtnShowProviders, RibbonElementPriority.TOP);

        return fournisseursBand;
    }

    private JRibbonBand createAboutBand() {
        JRibbonBand parametresBand = new JRibbonBand(
                "Commun",
                new EmptyResizableIcon(22));
        parametresBand.setResizePolicies(
                CoreRibbonResizePolicies.getCorePoliciesRestrictive(parametresBand));

        JCommandButton cbtnParametre = new JCommandButton(
                "Informations",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "about-icon.png"))));
        cbtnParametre.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onAbout();
            }

        });
        cbtnParametre.setActionKeyTip("P");

        parametresBand.addCommandButton(cbtnParametre, RibbonElementPriority.TOP);
        return parametresBand;
    }

    private JRibbonBand createCaisseBand() {
        JRibbonBand caisseBand = new JRibbonBand(
                "Caisse",
                new EmptyResizableIcon(22));
        caisseBand.setResizePolicies(
                CoreRibbonResizePolicies.getCorePoliciesRestrictive(caisseBand));

        JCommandButton cbtnAlimenter = new JCommandButton(
                "Alimenter",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "exit.png"))));
        cbtnAlimenter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //acceded au parametrage 
            }
        });
        cbtnAlimenter.setActionKeyTip("A");

      //  caisseBand.addCommandButton(cbtnAlimenter, RibbonElementPriority.TOP);
        JCommandButton cbtnDepenser = new JCommandButton(
                "Dépenser",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "caisse-icon.png"))));
        cbtnDepenser.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //acceded au parametrage 
            }
        });
        cbtnDepenser.setActionKeyTip("E");
        //  caisseBand.addCommandButton(cbtnDepenser, RibbonElementPriority.TOP);

        JCommandButton cbtnAfficher = new JCommandButton(
                "Afficher état de caisse",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "caisse-icon.png"))));
        cbtnAfficher.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //acceded au parametrage 
                onShowCaisseStatus();
            }

        });
        cbtnAfficher.setActionKeyTip("E");
        caisseBand.addCommandButton(cbtnAfficher, RibbonElementPriority.TOP);
        JCommandButton cbtnEncaiss = new JCommandButton(
                "A Encaisser",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "exit.png"))));
        cbtnDepenser.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //acceded au parametrage 
                onAEncaisser();
            }
        });
        cbtnEncaiss.setActionKeyTip("E");
        // caisseBand.addCommandButton(cbtnEncaiss, RibbonElementPriority.TOP);

        return caisseBand;
    }

    private JRibbonBand createRapportsBand() {
        JRibbonBand outilsBand = new JRibbonBand(
                "Gestion des Rapports",
                new EmptyResizableIcon(22));
        outilsBand.setResizePolicies(
                CoreRibbonResizePolicies.getCorePoliciesRestrictive(outilsBand));

        JCommandButton cbtnRapports = new JCommandButton(
                "Rapports des Ventes",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "exit.png"))));
        cbtnRapports.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //acceded au parametrage 
            }
        });
        cbtnRapports.setActionKeyTip("R");

        outilsBand.addCommandButton(cbtnRapports, RibbonElementPriority.TOP);

        JCommandButton cbtnRapportsAchat = new JCommandButton(
                "Rapports des Achats",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "exit.png"))));
        cbtnRapportsAchat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //acceded au parametrage 
            }
        });
        cbtnRapportsAchat.setActionKeyTip("E");
        outilsBand.addCommandButton(cbtnRapportsAchat, RibbonElementPriority.TOP);
        JCommandButton cbtnRapportsReparations = new JCommandButton("Rapports des réparations", createResizableIcon(new ImageIcon(getClass().getResource(
                ICONS22 + "exit.png"))));
        cbtnRapportsReparations.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //acceded au parametrage 
            }
        });
        outilsBand.addCommandButton(cbtnRapportsReparations, RibbonElementPriority.TOP);

        return outilsBand;
    }

    private JRibbonBand createEffetsBand() {
        JRibbonBand effetsBand = new JRibbonBand(
                "Gestion des Effets",
                new EmptyResizableIcon(22));
        effetsBand.setResizePolicies(
                CoreRibbonResizePolicies.getCorePoliciesRestrictive(effetsBand));

        JCommandButton cbtnAjouter = new JCommandButton(
                "Entrer les effets ",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "check-icon.png"))));
        cbtnAjouter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onCommercialPaperAdd();
            }

        });
        cbtnAjouter.setActionKeyTip("R");

        effetsBand.addCommandButton(cbtnAjouter, RibbonElementPriority.TOP);

        JCommandButton cbtnEncaisser = new JCommandButton(
                "Encaisser",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "exit.png"))));
        cbtnEncaisser.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //acceded au parametrage 
            }
        });
        cbtnEncaisser.setActionKeyTip("E");
        // effetsBand.addCommandButton(cbtnEncaisser, RibbonElementPriority.TOP);

        JCommandButton cbtnAfficher = new JCommandButton(
                "Afficher les effets",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "list-checks-icon.png"))));
        cbtnAfficher.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //acceded au parametrage 
                onCommercialPaperList();
            }
        });
        cbtnAfficher.setActionKeyTip("E");
        effetsBand.addCommandButton(cbtnAfficher, RibbonElementPriority.TOP);

        return effetsBand;
    }

    private JRibbonBand createEditBand() {
        JRibbonBand editBand = new JRibbonBand(
                "",
                new EmptyResizableIcon(22));
        editBand.setResizePolicies(
                CoreRibbonResizePolicies.getCorePoliciesRestrictive(editBand));

        JCommandButton cbtnAppCancel = new JCommandButton(
                "Annuller",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "exit.png"))));
        cbtnAppCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //acceded au parametrage 
            }
        });
        JCommandButton cbtnAppCopy = new JCommandButton(
                "Copier",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "exit.png"))));
        cbtnAppCopy.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //acceded au parametrage 
            }
        });
        JCommandButton cbtnAppPaste = new JCommandButton(
                "Coller",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "exit.png"))));
        cbtnAppPaste.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //acceded au parametrage 
            }
        });
        //cbtnAppAdd.setActionKeyTip("A");
        editBand.addCommandButton(cbtnAppCancel, RibbonElementPriority.TOP);
        editBand.addCommandButton(cbtnAppCopy, RibbonElementPriority.TOP);
        editBand.addCommandButton(cbtnAppPaste, RibbonElementPriority.TOP);
        return editBand;
    }

    private void setUpRibbonMenu() {
               // file task
        /*RibbonTask fileTask = new RibbonTask(
         "Fichier",
         createActionsBand(),
         createAppExitBand()
         );*/
        // fileTask.setKeyTip("F");

        RibbonTask fichierTask = new RibbonTask("Fichier", createApplicationBand(), createUsersBand());
        // RibbonTask editionTask = new RibbonTask("Edition", createEditBand());
        RibbonTask clientsTask = new RibbonTask("Clients", createClientsBand());
        RibbonTask fournisseursTask = new RibbonTask("Fournisseurs", createVendorsBand());
        RibbonTask salesTask = new RibbonTask("Achats et ventes", createSalesBand(), createPurchasesBand(), createReturnBand());
        RibbonTask reparationsTask = new RibbonTask("Réparations", createRepairBand());
        RibbonTask stockTask = new RibbonTask("Stock", createStockBand(), createPlaceBand());
        RibbonTask outilsTask = new RibbonTask("Outils", createEffetsBand(), createCaisseBand(), createAboutBand());

        // extras task
      /*  RibbonTask extrasTask = new RibbonTask(
         "Extra",
         getViewBand(),
         getExtrasBand());
         extrasTask.setKeyTip("E");

         RibbonTask helpTask = new RibbonTask(
         "Aide",
         getHelpBand());
         helpTask.setKeyTip("H");*/
        this.getRibbon().addTask(fichierTask);
        //this.getRibbon().addTask(editionTask);
        this.getRibbon().addTask(clientsTask);
        this.getRibbon().addTask(fournisseursTask);
        this.getRibbon().addTask(salesTask);
        this.getRibbon().addTask(reparationsTask);
        this.getRibbon().addTask(stockTask);
        this.getRibbon().addTask(outilsTask);

        // this.getRibbon().addTask(extrasTask);
        //this.getRibbon().addTask(helpTask);
        setUpTaskbar();
        setUpApplicationMenu();

        // help button to left side
       /* this.getRibbon().configureHelp(createResizableIcon(
                new ImageIcon(getClass().getResource(ICONS16 + "help.png"))),
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        //onHelp();
                    }
                }
        );*/
        

    }

    private JRibbonBand createAppExitBand() {
        JRibbonBand appExitBand = new JRibbonBand(
                "Quitter",
                new EmptyResizableIcon(22));
        appExitBand.setResizePolicies(
                CoreRibbonResizePolicies.getCorePoliciesRestrictive(appExitBand));

        JCommandButton cbtnAppExit = new JCommandButton(
                "Quitter",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "exit.png"))));
        cbtnAppExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                exit(null);
            }
        });
        cbtnAppExit.setActionKeyTip("X");
        appExitBand.addCommandButton(cbtnAppExit, RibbonElementPriority.TOP);

        return appExitBand;

    }

    private JRibbonBand createActionsBand() {
        JRibbonBand actionsBand = new JRibbonBand(
                "Actions",
                new EmptyResizableIcon(22));
        actionsBand.setResizePolicies(
                CoreRibbonResizePolicies.getCorePoliciesRestrictive(actionsBand));

        JCommandButton cbtnDashboard = new JCommandButton(
                "Home",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "home.png"))));
        cbtnDashboard.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //onOpenDashboard();
            }
        });
        cbtnDashboard.setActionKeyTip("H");
        actionsBand.addCommandButton(cbtnDashboard, RibbonElementPriority.TOP);

        JCommandButton cbtnClients = new JCommandButton(
                "Clients",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "home.png"))));
        cbtnClients.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //onOpenDashboard();
            }
        });
        cbtnClients.setActionKeyTip("C");
        actionsBand.addCommandButton(cbtnClients, RibbonElementPriority.TOP);
        JCommandButton cbtnReparations = new JCommandButton(
                "Réparations",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "home.png"))));
        cbtnReparations.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //onOpenDashboard();
            }
        });
        cbtnReparations.setActionKeyTip("R");
        actionsBand.addCommandButton(cbtnReparations, RibbonElementPriority.TOP);

        JCommandButton cbtnVentes = new JCommandButton(
                "Ventes",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "home.png"))));
        cbtnVentes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //onOpenDashboard();
            }
        });
        cbtnVentes.setActionKeyTip("R");
        actionsBand.addCommandButton(cbtnVentes, RibbonElementPriority.TOP);

        JCommandButton cbtnFournisseurs = new JCommandButton(
                "Fournissuers",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "home.png"))));
        cbtnFournisseurs.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //onOpenDashboard();
            }
        });
        cbtnFournisseurs.setActionKeyTip("F");
        actionsBand.addCommandButton(cbtnFournisseurs, RibbonElementPriority.TOP);
        JCommandButton cbtnCommandes = new JCommandButton(
                "Commandes",
                createResizableIcon(new ImageIcon(getClass().getResource(
                                        ICONS22 + "home.png"))));
        cbtnCommandes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //onOpenDashboard();
            }
        });
        cbtnCommandes.setActionKeyTip("F");
        actionsBand.addCommandButton(cbtnCommandes, RibbonElementPriority.TOP);

        return actionsBand;

    }

    private Color getSubstanceComponentBorderColor(Component component) {
        SubstanceColorScheme borderColorScheme = SubstanceColorSchemeUtilities
                .getColorScheme(component, ColorSchemeAssociationKind.BORDER,
                        ComponentState.DEFAULT);

        return borderColorScheme.getDarkColor();
    }

    public void notifyStatusBar(ImageIcon icon) {
        theStatusLabel.setIcon(icon);
        theStatusLabel.repaint();
        theStatusBar.repaint();
    }

    public void notifyStatusBar(ImageIcon user, ImageIcon status) {

        theUserLabel.setIcon(user);
        theUserLabel.repaint();

        theStatusLabel.setIcon(status);
        theStatusLabel.repaint();
        theStatusBar.repaint();
    }

    public void notifyStatusBar(String status, String user) {
        theStatusLabel.setText(status);
        if (user != "") {
            theUserLabel.setText("Utilisateur : " + user);
        } else {
            theUserLabel.setText("");
        }

        theStatusLabel.repaint();
        theUserLabel.repaint();
        theStatusBar.repaint();
    }

    private void setUpStatusBar() {
        theStatusBar = new JXStatusBar();
        theStatusBar.setPreferredSize(new Dimension(15, 34));

        theUserLabel = new JLabel();
        theStatusLabel = new JLabel();
        theStatusLabel.setIcon(new ImageIcon(getClass().getResource(ICONS22 + "connected.png")));
        theStatusLabel.setText("Connecté");
        theUserLabel.setText(LABEL_ADMIN);
        theUserLabel.setIcon(new ImageIcon(getClass().getResource(ICONS22 + "luser.png")));
        theUserLabel.setForeground(Color.blue);
        theStatusLabel.setForeground(Color.blue);
        theStatusBar.add(theUserLabel, new JXStatusBar.Constraint(600));

        SimpleDateFormat sdf = new SimpleDateFormat();
        sdf.applyPattern("EEEE', 'dd. MMMM yyyy");
        JLabel dateLabel = new JLabel(" " + sdf.format(new Date()).toString());
        dateLabel.setForeground(Color.blue);
        dateLabel.setIcon(new ImageIcon(getClass().getResource(ICONS22 + "date.png")));
        theStatusBar.add(dateLabel, new JXStatusBar.Constraint(280));
        theStatusBar.add(theStatusLabel, new JXStatusBar.Constraint(120));

    }

    public void resumeUILoading() {
        setUpStatusBar();
        if (theStatusBar != null) {
            getContentPane().add(theStatusBar, BorderLayout.SOUTH);
            notifyStatusBar("Connecté", Session.getSession().getTheCurrentUser().toString());

        } else {
            Logger.getLogger(ApplicationView.class.getName()).log(Level.SEVERE, ERROR_GUI);
            showExitDialog = false;
            exit(null);

        }
        //setUpMenus();
        setUpRibbonMenu();
        //showDefaultView();
        this.repaint();
    }

    private void initComponents() {

        showLoginView();
        if (theMainPanel != null) {
            getContentPane().add(theMainPanel, BorderLayout.CENTER);
        } else {
            Logger.getLogger(ApplicationView.class.getName()).log(Level.SEVERE, ERROR_GUI);
            showExitDialog = false;
            exit(null);
        }

        pack();

    }

    public static int showAskYesNo(String message) {
        return JOptionPane.showConfirmDialog(new JFrame(),
                "<html><body>" + message + "</body></html>",
                MESSAGE_ASK_TITLE,
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);
    }

    public static int showErrorMsg(String message) {
        /* return JOptionPane.showConfirmDialog(new JFrame(),
         "<html><body>" + message + "</body></html>",
         MESSAGE_ASK_TITLE,
         JOptionPane.OK_OPTION,
         JOptionPane.QUESTION_MESSAGE);*/
        JOptionPane.showMessageDialog(null, message);
        return 0;
    }

    private void exit(WindowEvent evt) {
        if (showExitDialog) {

            if (showAskYesNo(MESSAGE_EXIT) == NO_OPTION) {
                return;
            }
        }

        ApplicationController.get().exit();
    }

    public ApplicationView(String title) {
        System.out.println("the ApplicationView Constructor ");

        this.theAppTitle = title;
        setUpGUI(1024, 600);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //fire events 
    }

    private void showFXLoginView() {
        JFXPanel panel = new JFXPanel();
        theMainPanel = new JPanel(new BorderLayout());
        theMainPanel.setBorder(BorderFactory.createMatteBorder(0, -1, 0, -1,
                getSubstanceComponentBorderColor(theMainPanel)));
        theMainPanel.add(panel, BorderLayout.CENTER);

    }

    private void showLoginView() {
        LoginController loginController = new LoginController();
        theMainPanel = new JPanel(new BorderLayout());
        theMainPanel.setBorder(BorderFactory.createMatteBorder(0, -1, 0, -1,
                getSubstanceComponentBorderColor(theMainPanel)));
        theMainPanel.add(loginController.getPageView().asComponent(), BorderLayout.CENTER);

    }

    private void showDefaultView() {
        System.out.println("shoing default view");

        MainController mainController = new MainController();
        theMainPanel = new JPanel(new BorderLayout());
        theMainPanel.setBorder(BorderFactory.createMatteBorder(0, -1, 0, -1,
                getSubstanceComponentBorderColor(theMainPanel)));
        MainView mv = (MainView) mainController.getPageView();

        // mv.buildCenterPanel();
        theMainPanel.add(mv.asComponent(), BorderLayout.CENTER);
        mv.init(mainController);
        //getContentPane().add(theMainPanel, BorderLayout.CENTER);

    }

    private void showCreateUserDlg() {
        theUserController = new UserController();
        //theUserController.getPageView().asComponent()
        theMainPanel = new JPanel(new BorderLayout());
        theMainPanel.setBorder(BorderFactory.createMatteBorder(0, -1, 0, -1,
                getSubstanceComponentBorderColor(theMainPanel)));
        theMainPanel.add(theUserController.getPageView().asComponent(), BorderLayout.CENTER);

    }

    public static ResizableIcon createResizableIcon(final Icon icon) {
        ResizableIcon resizableIcon = new ResizableIcon() {
            int width = icon.getIconWidth();
            int height = icon.getIconHeight();

            @Override
            public int getIconHeight() {
                return this.height;
            }

            @Override
            public int getIconWidth() {
                return this.width;
            }

            @Override
            public void paintIcon(Component c, Graphics g, int x, int y) {
                icon.paintIcon(c, g, x, y);
            }

            @Override
            public void setDimension(Dimension newDimension) {
                this.width = newDimension.width;
                this.height = newDimension.height;
            }
        };

        return resizableIcon;
    }

    public static void showPage(IView page) {

        theMainPanel.removeAll();
        theMainPanel.add(page.asComponent());
        theMainPanel.revalidate();
        theMainPanel.repaint();

        page.asComponent().requestFocus();
    }

    private void onOpenHome() {
        System.out.println("onopenhome");
        theMainController = new MainController();
        MainView mv = (MainView) theMainController.getPageView();

        showListView(mv);
        JFXPanel jf = new JFXPanel();

    }

    private void onAddVehicle() {
        VehicleAddCommand theVehicleAddCommand = new VehicleAddCommand(new Vehicle());
        theVehicleAddCommand.execute();
    }

    private void onCommercialPaperAdd() {

        theCommerciaPaperAddCommand = new CommercialPaperAddCommand(new CommercialPaper());
        theCommerciaPaperAddCommand.execute();
    }

    private void onCommercialPaperList() {
        theCommercialPaperListCommand = new CommercialPaperListCommand();
        theCommercialPaperListCommand.execute();
    }

    private void onListVehicles() {
        VehicleListCommand theVehicleListCommand = new VehicleListCommand();
        theVehicleListCommand.execute();

    }

    private void onLogout() {
        Session.getSession().setTheLoginStatus(false);
        Session.getSession().setTheCurrentUser(new User());
        notifyStatusBar("déconnecté", Session.getSession().getTheCurrentUser().toString());
        notifyStatusBar(new ImageIcon(getClass().getResource(ICONS22 + "disconnected.png")));
        notifyStatusBar(new ImageIcon(getClass().getResource(ICONS22 + "nuser.png")), new ImageIcon(getClass().getResource(ICONS22 + "disconnected.png")));
        ReLoginView view = new ReLoginView();
        view.init();
        showPage(view);

    }

    private void onExit() {
        exit(null);
    }

    private void onReconnect() {
        SwitchAccountCommand theSwitchAccountCommand = new SwitchAccountCommand();
        theSwitchAccountCommand.execute();
    }

    private void onCreateUser() {

        UserAddCommand theUserAddCommand = new UserAddCommand(new User());

        theUserAddCommand.execute();

    }

    private void onDeleteUser() {

    }

    private void onAddRole() {
        RoleAddCommand theAddRoleCommand = new RoleAddCommand(new Role());
        theAddRoleCommand.execute();
    }

    private void onShowUsers() {
        System.out.println("show users");
        UserListCommand theUserListCommand = new UserListCommand();

        theUserListCommand.execute();

    }

    private void onAddReturn() {
        ItemReturnCommand theItemReturnCommand = new ItemReturnCommand(new Return());
        theItemReturnCommand.execute();
    }

    private void onShowReturns() {
        System.out.println("show returns");
        ItemReturnListCommand theReturnListCommand = new ItemReturnListCommand();
        theReturnListCommand.execute();
    }

    private void onSettings() {

    }

    private void onAddCustomer() {
        System.out.println("onAddCustomer()");
     //CommandAddCustomer
        //Customer customer = new Customer();

        //CustomerAddCommand customerAddCommand = new CustomerAddCommand(customer);
        //customerAddCommand.execute();
        //CustomerFormView customerView = new CustomerFormView(new Customer() );
        //customerView.setVisible(true);
        //customerView.showDialog();
        /*   SwitchAccountFormView accountView = new SwitchAccountFormView(new User());
         accountView.setVisible(true);
         accountView.showDialog();*/
        CustomerAddCommand theCustomerAddCommand = new CustomerAddCommand(new Customer());
        theCustomerAddCommand.execute();
        System.out.println("current user roles " + Session.getSession().getTheCurrentUser().getTheAttributedRoles());

    }

    private void onModifyCustomer() {

        fireCustomerUpdateEvent(new Customer());

    }

    private void onShowRole() {
        RoleListCommand theRoleListCommand = new RoleListCommand();
        theRoleListCommand.execute();
    }

    private void onShowCustomers() {
        System.out.println("show customers");
        CustomersListCommand theCommandCustomersList = new CustomersListCommand(new Customer());
        theCommandCustomersList.execute();
        theCustomersListView = (CustomerListView) theCommandCustomersList.getTheCustomersListView();
        //theCustomersListView.refreshData();
    }

    private void onAddStock() {
        InventoryAddCommand theInventoryAddCommand = new InventoryAddCommand(new Item());
        theInventoryAddCommand.execute();
    }

    private void onShowStock() {
        InventoryListCommand theInventoyListCommand = new InventoryListCommand(new Item());
        theInventoyListCommand.execute();
        theInventoryListView = theInventoyListCommand.getTheInventoryListView();

    }

    private void onAddVendor() {

        VendorAddCommand theVendorAddCommand = new VendorAddCommand(new Vendor());

        theVendorAddCommand.execute();

    }

    private void onModifyProvider() {

    }

    private void onShowVendors() {

        VendorListCommand theVendorListCommand = new VendorListCommand();
        theVendorListCommand.execute();
    }

    private void onOpenRepairOrder() {
        RepairOrderOpenCommand theRepairOrderOpenCommand = new RepairOrderOpenCommand(new RepairOrder());
        theRepairOrderOpenCommand.execute();
    }

    private void onCloseRepairOrder() {
    }

    private void onShowRepairOrders() {
        System.out.println("show repair orders");
        RepairOrderListCommand theRepairOrderListCommand = new RepairOrderListCommand(new RepairOrder());
        theRepairOrderListCommand.execute();
    }

    private void onOpenSalesOrder() {

        SalesOrder o = new SalesOrder();

        SalesOrderOpenCommand theSalesOrderOpenCommand = new SalesOrderOpenCommand(o);
        theSalesOrderOpenCommand.execute();
    }

    private void onShowSalesOrders() {
        SalesOrderListCommand theSalesOrderListCommand = new SalesOrderListCommand();
        theSalesOrderListCommand.execute();
    }

    public void setTheMainPanel(JPanel panel) {
        theMainPanel = panel;
    }

    public JPanel getTheMainPanel() {
        return theMainPanel;
    }

    static public Boolean doesTabExist(JTabbedPane tabs, String tabName) {
        for (int i = 0; i < tabs.getTabCount(); i++) {
            if (tabs.getTitleAt(i).equals(tabName)) {
                return true;
            }
        }

        return false;
    }

    static boolean ifComponentExists(JTabbedPane tabs, Component comp) {
        int count = tabs.getTabCount();
        System.out.println("count : " + count);
        for (int i = 1; i < count; i++) {
            if (tabs.getTabComponentAt(i).equals(comp)) {
                return true;
            }
        }
        return false;
    }

    static public void closeTab(String name) {
        System.out.println(" closing tab " + theTabs.indexOfTab(name));
        theTabs.remove(theTabs.indexOfTab(name));
        theTabs.repaint();
    }

    static public void showListView(IView myPage) {
        //theCurrentView = myPage;
        // isListView = true;
        if (theTabs == null) {
            theTabs = new JTabbedPane();
            theMainPanel.removeAll();
            theMainPanel.add(theTabs);
            theMainPanel.revalidate();
            theMainPanel.repaint();

        }
        //theMainPanel.removeAll();
        //theMainPanel.add(myPage.asComponent());
        // theTabs.addTab(IMAGES, null, theTabs, IMAGES);
        if (!doesTabExist(theTabs, myPage.getTitle())) {
            theTabs.addTab(myPage.getTitle(), new ImageIcon(ApplicationView.class.getResource(myPage.getIconPath())), myPage.asComponent(), "");
            theTabs.setSelectedIndex(theTabs.indexOfTab(myPage.getTitle()));
        } //   theTabs.add(myPage.getTitle(),myPage.asComponent());
        else {
            theTabs.remove(theTabs.indexOfTab(myPage.getTitle()));

            theTabs.addTab(myPage.getTitle(), new ImageIcon(ApplicationView.class.getResource(myPage.getIconPath())), myPage.asComponent(), "");
            theTabs.setSelectedIndex(theTabs.indexOfTab(myPage.getTitle()));
            //theTabs.setSelectedIndex(theTabs.indexOfTab(myPage.getTitle()));
        }
        myPage.asComponent().requestFocus();

    }

    public void refreshCurrentView() {

        if (isListView) {

            IListView view = (IListView) getTheCurrentView();
            //  view.refreshData(data);

        }

    }

    public IView getTheCurrentView() {
        return theCurrentView;
    }

    private void onShowPurchases() {
        thePurchaseOrderListCommand = new PurchaseOrderListCommand();
        thePurchaseOrderListCommand.execute();
    }

    private void onAddPurchases() {
        thePurchaseOrderOpenCommand = new PurchaseOrderOpenCommand(new PurchaseOrder());
        thePurchaseOrderOpenCommand.execute();
    }

    private void onShowPlaces() {
        PlaceListCommand thePlaceListCommand = new PlaceListCommand();
        thePlaceListCommand.execute();
        this.thePlaceListView = thePlaceListCommand.getThePlaceListView();
    }

    private void onShowCaisseStatus() {

        System.out.println("show caisse status");

        ListPaymentsCommand theListPaymentsCommand = new ListPaymentsCommand();
        theListPaymentsCommand.execute();
    }

    private void onAEncaisser() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void onShowPurchasesEnc() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void onShowSalesEnc() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void onAddPlace() {
        PlaceAddCommand thePlaceAddCommand = new PlaceAddCommand(new ItemPlace());
        thePlaceAddCommand.execute();
    }

    private void onAbout() {
        
        AboutView.showDialog();
    }

    public void fireCustomerUpdateEvent(Customer customer) {
        System.out.println(" upadting the customer with id" + customer.getId());
        CustomerUpdateCommand theCustomerUpdateCommand = new CustomerUpdateCommand(customer);
        theCustomerUpdateCommand.execute();

    }

    public void fireInventoryUpdateEvent(Item e) {

        System.out.println("updating the item with it" + e.getId());
        InventoryUpdateCommand theInventoryUpdateCommand = new InventoryUpdateCommand(e);
        theInventoryUpdateCommand.execute();
    }

    public void firePlaceUpdateEvent(ItemPlace place) {

        PlaceUpdateCommand thePlaceUpdateCommand = new PlaceUpdateCommand(place);
        thePlaceUpdateCommand.execute();

    }

    public void fireVendorUpdateEvent(Vendor v) {
        VendorAddCommand theVendorAddCommand = new VendorAddCommand(v);
        theVendorAddCommand.execute();
    }

    public InventoryListView getInventoryListView() {
        return this.theInventoryListView;
    }

    public PlaceListView getThePlaceListView() {
        return thePlaceListView;
    }

    void firePlacesUpdateEvent(ItemPlace place) {
        System.out.println("fires update event" + place.getId());
        PlaceUpdateCommand thePlaceUpdateCommand = new PlaceUpdateCommand(place);
        thePlaceUpdateCommand.execute();
    }

    void fireRoleUpdateEvent(Role r) {

        RoleAddCommand theRoleAddCommand = new RoleAddCommand(r);
        theRoleAddCommand.execute();

    }

    void fireVehicleUpdateEvent(Vehicle v) {
        VehicleAddCommand theVehicleAddCommand = new VehicleAddCommand(v);
        theVehicleAddCommand.execute();
    }

    void fireUserUpdateEvent(User u) {
        UserAddCommand theUserAddCommand = new UserAddCommand(u);
        theUserAddCommand.execute();
    }

    void fireReturnUpdateEvent(Return ret) {
        ItemReturnCommand theReturnCommand = new ItemReturnCommand(ret);
        theReturnCommand.execute();
    }

    void firePurchaseOrderUpdateEvent(PurchaseOrder po) {
        PurchaseOrderOpenCommand thePurchaseOrderOpenCommand = new PurchaseOrderOpenCommand(po);
        thePurchaseOrderOpenCommand.execute();

    }

}
