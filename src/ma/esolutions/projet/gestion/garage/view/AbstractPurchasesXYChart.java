/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import ma.esolutions.projet.gestion.garage.controller.MainController;
import ma.esolutions.projet.gestion.garage.view.common.AbstractXYChart;

/**
 *
 * @author born2code
 */
abstract public class AbstractPurchasesXYChart extends AbstractXYChart{

    public AbstractPurchasesXYChart(MainController controller) {
        super(controller);
    }

    @Override
    protected String getYAxisLabel() {
        return "Achats";
    }

    @Override
    public String getTitle() {
        return "Achats";
    }

}
