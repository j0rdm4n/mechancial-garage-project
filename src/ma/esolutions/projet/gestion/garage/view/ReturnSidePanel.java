/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import ma.esolutions.projet.gestion.garage.model.Return;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.STYLES;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSidePanel;

/**
 *
 * @author E-solutions
 */
class ReturnSidePanel extends AbstractSidePanel<Return>{

    public ReturnSidePanel() {
    }

    @Override
    public Node createCenterUI() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void handleRowSelected() {
            Node center = createUI();

        Platform.runLater(() -> {
            //getRoot().setTop(createDynamicBar());
            if (this.theEntity != null) {
                getRoot().setCenter(center);
            }
        });
    }
public Node createUI() {
        GridPane grid = new GridPane();
        int row = 1;
        int col = 2;

        if (theEntity != null) {
            grid.setStyle(STYLES);
            grid.setAlignment(Pos.CENTER);
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(25, 25, 25, 25));
            Label cusName = new Label("Client");
            grid.add(cusName, 0, row);
            Label cusNamev = new Label(this.theEntity.getTheCustomer().toString());
            grid.add(cusNamev, col, row++);

            Label artName = new Label("Article ");
            grid.add(artName, 0, row);

            Label artNamev = new Label(theEntity.getTheItem().toString());
            grid.add(artNamev, col, row++);

            Label q = new Label("Quantité ");
            grid.add(q, 0, row);

            Label qv = new Label(""+theEntity.getTheItemQuantity());
            grid.add(qv, col, row++);

            Label total = new Label("Prix total TTC");
            grid.add(total, 0, row);
            Label totalv = new Label(theEntity.getTheTotalPrice()+"");
            grid.add(totalv, col, row++);

            Label date = new Label("Date de retour");
            grid.add(date, 0, row);
            Label datev = new Label(theEntity.getTheItemReturnDate().toString());
            grid.add(datev, col, row++);
        }
        return grid;
    }
    @Override
    public void getFieldsValues() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getTitle() {
        return "Détails de retour";
    }
    
}
