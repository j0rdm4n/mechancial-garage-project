/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.awt.event.MouseEvent;
import java.util.List;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.controller.commands.VendorAddCommand;
import ma.esolutions.projet.gestion.garage.controller.common.IDataPageController;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.Vendor;
import ma.esolutions.projet.gestion.garage.model.VendorDAO;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS22;
import ma.esolutions.projet.gestion.garage.view.common.AbstractListView;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSearchPanel;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSidePanel;
import ma.esolutions.projet.gestion.garage.view.common.EntityTableColumn;

/**
 *
 * @author E-solutions
 */
public class VendorListView extends AbstractListView<Vendor> {

    private VendorSidePanel theVendorSidePanel;

    public VendorListView(List<Vendor> data) {
        super(null, data);
    }

    @Override
    public AbstractSearchPanel getSearchPanel() {
        if (theSearchPanel == null) {
            theSearchPanel = new VendorSearchPanel();
            theSearchPanel.setParentListView(this);
        }
        return theSearchPanel;
    }

    @Override
    public void addTableColumns() {
        getTableModel().addColumn(new EntityTableColumn(
                "Nom", "theVendorName", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "Tél", "theVendorPhone", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "Email", "theVendorEmail", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "Addresse", "theVendorAddress", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "Code Postal", "theVendorPostalCode", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "Ville", "theVendorCity", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "Date d'ajout", "theVendorSinceDate", String.class, 300));

    }

    @Override
    protected void onReport() {
        ApplicationView.showAskYesNo("Voulez vous visualiser le rapport des fourniisuers en format pdf ?");
        throw new UnsupportedOperationException("implement vendor list pdf report");
    }

    @Override
    public void showEntitySummary() {
    }

    @Override
    public void showEntityActions() {
    }

    @Override
    public void fireUpdateEvent(MouseEvent e) {

        Vendor v = getSelectedModel();
        System.out.println("selceted model : ");
        System.out.println(v);
        if (v != null) {
            ApplicationController.getAppView().fireVendorUpdateEvent(v);
        }
    }

    @Override
    public AbstractSidePanel getSidePanel() {

        if (theVendorSidePanel == null) {
            theVendorSidePanel = new VendorSidePanel();
        }
        this.setTheRowObserver(theVendorSidePanel);
        return theVendorSidePanel;
    }

    @Override
    public List<Vendor> getTheTableData() {
        return null;
    }

    @Override
    public String getTitle() {
        return "Gestion des fournisseurs";
    }

    @Override
    public String getIconPath() {
        return ICONS22 + "seller-icon.png";
    }

    @Override
    public Vendor getModel() {
        return new Vendor();
    }

    @Override
    public void delete(Vendor e) {
        (new VendorDAO()).remove(e);
    }

    @Override
    public SecureCommand<Vendor> getRelatedFormCommand(Vendor e) {
        return new VendorAddCommand(e);
    }

    @Override
    protected boolean hasReport() {
        return false;
    }

    @Override
    protected void onNew() {
        super.onNew(); //To change body of generated methods, choose Tools | Templates.
        if (theVendorSidePanel != null) {
            theVendorSidePanel.update();
        }
    }

    @Override
    protected void onDelete() {
        super.onDelete(); //To change body of generated methods, choose Tools | Templates.
          if (theVendorSidePanel != null) {
            theVendorSidePanel.update();
        }
        
    }

}
