
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

//~--- non-JDK imports --------------------------------------------------------
import java.awt.event.MouseEvent;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.controller.commands.PurchaseOrderOpenCommand;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.CommercialPaper;
import ma.esolutions.projet.gestion.garage.model.CommercialPaperDAO;
import ma.esolutions.projet.gestion.garage.model.Customer;
import ma.esolutions.projet.gestion.garage.model.Item;
import ma.esolutions.projet.gestion.garage.model.ItemDAO;
import ma.esolutions.projet.gestion.garage.model.MinimalStockException;
import ma.esolutions.projet.gestion.garage.model.NegativeStockException;
import ma.esolutions.projet.gestion.garage.model.Payment;
import ma.esolutions.projet.gestion.garage.model.PaymentDAO;
import ma.esolutions.projet.gestion.garage.model.PurchaseOrder;
import ma.esolutions.projet.gestion.garage.model.PurchaseOrderDAO;
import ma.esolutions.projet.gestion.garage.model.PurchaseOrderLine;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS16;
import ma.esolutions.projet.gestion.garage.view.common.AbstractListView;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSearchPanel;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSidePanel;
import ma.esolutions.projet.gestion.garage.view.common.EntityTableColumn;

/**
 *
 * @author E-solutions
 */
public class PurchaseOrderListView extends AbstractListView<PurchaseOrder> {

    private PurchaseOrderSidePanel thePurchaseOrderSidePanel;
    private final ItemDAO itemDAO;

    public PurchaseOrderListView(List<PurchaseOrder> data) {
        super(null, data);
        itemDAO = new ItemDAO();
    }

    @Override
    public AbstractSearchPanel getSearchPanel() {
        if (theSearchPanel == null) {
            theSearchPanel = new PurchaseOrderSearchPanel();
            theSearchPanel.setParentListView(this);
        }

        return theSearchPanel;
    }

    @Override
    public void addTableColumns() {
        getTableModel().addColumn(new EntityTableColumn("Date de Commande", "theOrderDate", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn("Fournisseur", "theVendor", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn("Prix total", "theCost", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn("Reçue ", "theDeliveredStatus", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn("Payé", "theInvoicedStatus", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn("Paiement par", "thePaymentType", String.class, 300));
    }

    @Override
    protected void onReport() {
        ApplicationView.showAskYesNo("Voulez vous visualiser la rapport des achats en format pdf ?");

        throw new UnsupportedOperationException("implement purchases pdf report ");
    }

    @Override
    public void showEntitySummary() {
    }

    @Override
    public void showEntityActions() {
    }

    @Override
    public void fireUpdateEvent(MouseEvent e) {
        PurchaseOrder po;

        po = this.getSelectedModel();

        if (po != null) {
            ApplicationController.getAppView().firePurchaseOrderUpdateEvent(po);
        }
    }

    @Override
    public AbstractSidePanel getSidePanel() {
        if (thePurchaseOrderSidePanel == null) {
            thePurchaseOrderSidePanel = new PurchaseOrderSidePanel();
        }

        this.setTheRowObserver(thePurchaseOrderSidePanel);

        return thePurchaseOrderSidePanel;
    }

    @Override
    public List<PurchaseOrder> getTheTableData() {
        return null;
    }

    @Override
    public String getTitle() {
        return "Gestion de commandes d'achat";
    }

    @Override
    public String getIconPath() {
        return ApplicationView.ICONS22 + "list-buyer-icon.png";
    }

    @Override
    public PurchaseOrder getModel() {
        return new PurchaseOrder();
    }

    @Override
    public void delete(PurchaseOrder e) {
        (new PurchaseOrderDAO()).remove(e);
    }

    @Override
    public SecureCommand<PurchaseOrder> getRelatedFormCommand(PurchaseOrder e) {
        return new PurchaseOrderOpenCommand(e);
    }

    @Override
    protected boolean hasValidateReception() {
        return true;
    }

    protected void onValidateReception() {
        PurchaseOrder po = getSelectedModel();

        if (po == null) {
            ApplicationView.showErrorMsg("Pour valider la réception,selectionnez une commande");

            return;
        }

        if (po.getTheDeliveredStatus().equalsIgnoreCase("oui")) {
            ApplicationView.showErrorMsg("Cette commande est déja reçue ! ");

            return;
        }

        int yesno = ApplicationView.showAskYesNo("Vous étes sur de vouloir valider la réception de cette commande ? ");

        if (yesno == JOptionPane.YES_OPTION) {
            List<PurchaseOrderLine> lines = po.getTheOrderedItems();

            System.out.println(" nbr de lignes en commande d'achat " + lines.size());

            // ItemDAO itemDAO = new ItemDAO();
            double cost = 0.0;
            for (PurchaseOrderLine l : lines) {
                Item it = l.getTheOrderedItem();
                int c = l.getTheQuantity();

                cost += l.getTheQuantity() * (l.getTheUnitPrice() + l.getTheOrderedItem().getTheGain()) * (1 + (l.getTheOrderedItem().getTheTVA() / 100.0));

                long oldq = it.getTheItemQuantity();
                double oldp = it.getTheUnitPrice();
                long newq = l.getTheQuantity();
                double newp = l.getTheUnitPrice();
                float moyen = (float) (((oldq * oldp) + (newq * newp)) / oldq + newq);

                System.out.println(" oldq " + oldq + " oldp " + oldp + "newq " + newq + "newp " + newp + " = " + moyen);

                /*  int yn1 = ApplicationView.showAskYesNo("La valeur moyen des prix est " + String.valueOf(moyen)
                 + ",modifier le prix ?");

                 if (yn1 == JOptionPane.YES_OPTION) {
                 it.setTheUnitPrice(moyen);
                 (new ItemDAO()).update(it);
                 }*/
                System.out.println(" new quantity of item " + l.getTheOrderedItem() + "is " + l.getTheQuantity());
                try {
                    it.setTheItemQuantity(it.getTheItemQuantity() + l.getTheQuantity());
                } catch (NegativeStockException ex) {
                    Logger.getLogger(PurchaseOrderListView.class.getName()).log(Level.SEVERE, null, ex);
                } catch (MinimalStockException ex) {
                    Logger.getLogger(PurchaseOrderListView.class.getName()).log(Level.SEVERE, null, ex);
                }
                it.setTheUnitPrice(l.getTheUnitPrice());
                it.setTheSalesPriceTTC((l.getTheUnitPrice() + l.getTheOrderedItem().getTheGain())
                        * (1 + l.getTheOrderedItem().getTheTVA() / 100.0));
                itemDAO.update(it);
                System.out.println(" saved " + it + " with q : " + it.getTheItemQuantity());
            }

            po.setReceived(true);
            po.setTheCost(cost);
            System.out.println("total cost " + po.getTheCost());
            (new PurchaseOrderDAO()).update(po);
        }
    }

    @Override
    protected boolean hasCash() {
        return true;
    }

    @Override
    protected void onPayment() {
        PurchaseOrder po = getSelectedModel();

        if (po == null) {
            ApplicationView.showErrorMsg("Pour enregister le payement d'une commande, il faut la sélectionner !");

            return;
        }

        if (po.getTheInvoicedStatus().equalsIgnoreCase("oui")) {
            ApplicationView.showErrorMsg("Le payement de cette commande est déja enregistré !");

            return;
        }
        if (po.getTheDeliveredStatus().equalsIgnoreCase("non")) {
            int yesno = ApplicationView.showAskYesNo("Pour enregister  le payment,la validation de réception doit étre faite! "
                    + "Voulez vous valider la réception de cette commande ? ");
            if (Objects.equals(yesno, JOptionPane.YES_OPTION)) {
                onValidateReception();
            } else {
                ApplicationView.showErrorMsg("L'enregistrement de payment a echoué !");
                return;
            }
        }
        if (po.getTheDeliveredStatus().equalsIgnoreCase("oui")) {
            if (po.getThePaymentType().equalsIgnoreCase("espéces")) {
                Payment p = new Payment();
                int tva = 20;
                p.setTheVendor(po.getTheVendor());
                p.setTheCashingDate(new Date());
                p.setThePaymentType("achat");
                if (!po.getTheOrderedItems().isEmpty()) {
                    if (po.getTheOrderedItems().get(0) != null) {
                        tva = po.getTheOrderedItems().get(0).getTheOrderedItem().getTheTVA();
                    }
                }
                p.setTheHTaxAmount((-1) * po.getTheCost() / (1 + tva / 100.0));
                p.setTheTTCAmount((-1) * po.getTheCost());

                // p.setTheCustomer(new Customer());
                (new PaymentDAO()).create(p);

                ApplicationView.showErrorMsg("Enregistrement fait avec succés ! ");
            }

            if (po.getThePaymentType().equalsIgnoreCase("effets")) {

                CommercialPaper paper = new CommercialPaper();
                paper.setDate(new Date());
                paper.setDueDate(po.getTheEffetDueDate());
                paper.setPurchaseOrder(po);
                paper.setVendor(po.getTheVendor());
                paper.setState("non payé");
                paper.setAmmount(po.getTheCost());

                (new CommercialPaperDAO()).create(paper);
                ApplicationView.showErrorMsg("L'enregistrement d'effet est fait avec succés !");

            }
            po.setTheInvoicedStatus("oui");
            (new PurchaseOrderDAO()).update(po);
        }

    }
}
