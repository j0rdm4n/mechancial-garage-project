/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import java.awt.BorderLayout;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javax.swing.JPanel;
import ma.esolutions.projet.gestion.garage.model.User;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.STYLES;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSidePanel;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author E-solutions
 */
class UserSidePanel extends AbstractSidePanel<User> {

    public UserSidePanel() {
    }

  

    @Override
    public void getFieldsValues() {
    }

    @Override
    public String getTitle() {
            return "Détails sur l'utilisateur";
    }

    @Override
    public Node createCenterUI() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void handleRowSelected() {
           Node center = createUI();

        Platform.runLater(() -> {
            //getRoot().setTop(createDynamicBar());
            if (this.theEntity != null) {
                getRoot().setCenter(center);
            }
        });
    }
    public Node createUI() {
        GridPane grid = new GridPane();
        int row = 1;
        int col = 2;

        if (theEntity != null) {
            grid.setStyle(STYLES);
            grid.setAlignment(Pos.CENTER);
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(25, 25, 25, 25));
            Label firstName = new Label("Prénom");
            grid.add(firstName, 0, row);
            Label firstNamev = new Label(this.theEntity.getTheFirstname());
            grid.add(firstNamev, col, row++);

            Label lastName = new Label("Nom ");
            grid.add(lastName, 0, row);

            Label lastNamev = new Label(theEntity.getTheLastname());
            grid.add(lastNamev, col, row++);

            Label addr = new Label("Addresse ");
            grid.add(addr, 0, row);

            Label addrv = new Label(theEntity.getTheAddress());
            grid.add(addrv, col, row++);
}
        return grid;
    }
    
}
