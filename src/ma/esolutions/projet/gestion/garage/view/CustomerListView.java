/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import com.itextpdf.text.DocumentException;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Tuple;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.controller.commands.CustomerAddCommand;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.Customer;
import ma.esolutions.projet.gestion.garage.model.CustomerDAO;
import ma.esolutions.projet.gestion.garage.model.Payment;
import ma.esolutions.projet.gestion.garage.model.PaymentDAO;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS16;
import ma.esolutions.projet.gestion.garage.view.common.AbstractListView;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSearchPanel;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSidePanel;
import ma.esolutions.projet.gestion.garage.view.common.EntityTableColumn;
import ma.esolutions.projet.gestion.garage.view.common.IRowObserver;
import net.miginfocom.swing.MigLayout;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author Ahmed
 */
public class CustomerListView extends AbstractListView<Customer> {

    private List<Customer> theCustomersData;
    private CustomerSidePanel theCustomerSidePanel;

    public CustomerListView(List<Customer> customers) {

        super(null, customers);
        //data = customers;
        //theCustomersData = customers;
        //          System.out.println("start customers list sql query");

        /*data.forEach((c)->{
         System.out.println("Customer + " + c);
         });*/
    }

    public CustomerListView() {
        this(new ArrayList());
    }

    @Override
    public AbstractSearchPanel getSearchPanel() {
        if (theSearchPanel == null) {
            theSearchPanel = new CustomerSearchPanel();
            theSearchPanel.setParentListView(this);
        }
        return theSearchPanel;
    }

    @Override
    public void addTableColumns() {
        getTableModel().addColumn(new EntityTableColumn(
                "Prénom", "theFirstname", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "Nom", "theLastname", String.class, 300));

        getTableModel().addColumn(new EntityTableColumn(
                "Addresse", "theAddress", String.class, 500));
        getTableModel().addColumn(new EntityTableColumn(
                "Ville", "theCity", String.class, 500));

        getTableModel().addColumn(new EntityTableColumn(
                "Code Postal", "thePostalcode", String.class, 500));

        getTableModel().addColumn(new EntityTableColumn(
                "Tél", "thePhone", String.class, 500));

        getTableModel().addColumn(new EntityTableColumn(
                "Mobile", "theMobile", String.class, 500));

        getTableModel().addColumn(new EntityTableColumn(
                "Fax", "theFax", String.class, 500));
        getTableModel().addColumn(new EntityTableColumn(
                "Email", "theEmail", String.class, 500));

        getTableModel().addColumn(new EntityTableColumn(
                "Crédit", "theCredit", String.class, 500));

        getTableModel().addColumn(new EntityTableColumn(
                "Actif", "theActiveStatus", String.class, 500));

    }

    private JTable createTopNCustomers(int n) {
        JTable table = new JTable();
        DefaultTableModel dtm = new DefaultTableModel();
        table.setModel(dtm);
        dtm.addColumn("Client");
        //dtm.addColumn("Quantité");
        dtm.addColumn("Montant");
        //  dtm.addColumn("Pourcentage");
        List<Customer> l = (new CustomerDAO()).getTopNCustomers(n);

        for (Customer o : l) {

            dtm.addRow(new String[]{o.getTheFirstname() + " " + o.getTheLastname(), String.valueOf(o.getTheTotal())});
        }
        //  dtm.addRow(rowData);

        return table;

    }

    @Override
    protected void onReport() {

        List<String> header = new ArrayList();
        JTable table = new JTable();
        DefaultTableModel dtm = new DefaultTableModel();
        table.setModel(dtm);
        JPanel reportPanel = new JPanel(new MigLayout());

        JFrame frame = new JFrame("le rapport des clients");
        JPanel container = new JPanel(new BorderLayout());
        JButton pdfBtn = new JButton("générer le rapport en format pdf");
        frame.getContentPane().add(container);
        JTable topTable = createTopNCustomers(10);

        DefaultCategoryDataset customersData = new DefaultCategoryDataset();
        int col = 1;
        for (int row = 0; row < topTable.getRowCount(); row++) {

            customersData.setValue(Double.parseDouble(topTable.getValueAt(row, col).toString()), "Ventes", topTable.getValueAt(row, --col).toString());

            col = 1;
        }

        JFreeChart objChartBar = ChartFactory.createBarChart("Top clients", "Client", "Total", customersData, PlotOrientation.HORIZONTAL, true, true, false);
        reportPanel.add(new ChartPanel(objChartBar));
        pdfBtn.addActionListener((ActionEvent e) -> {
            String filename = "clients" + String.valueOf(year()) + String.valueOf(month()) + String.valueOf(day()) + ".pdf";
            createReportTable(filename, "liste des clients", createTopNCustomers(10));
            try {
                createPdf(filename);
            } catch (IOException ex) {
                Logger.getLogger(CustomerListView.class.getName()).log(Level.SEVERE, null, ex);
            } catch (DocumentException ex) {
                Logger.getLogger(CustomerListView.class.getName()).log(Level.SEVERE, null, ex);
            }
            int yesno = ApplicationView.showAskYesNo("Voulez vous visualiser le rapport en format pdf ? ");
            if (yesno == JOptionPane.YES_OPTION) {
                viewPdf(filename);
            }
        });
        container.add(new JLabel("Rapports des clients "), BorderLayout.NORTH);
        container.add(pdfBtn, BorderLayout.SOUTH);
        JScrollPane pane = new JScrollPane(reportPanel);
        pane.setPreferredSize(new Dimension(500, 500));
        container.add(pane, BorderLayout.CENTER);
        frame.pack();
        frame.setVisible(true);
    }

    @Override
    public String getTitle() {
        return "Gestion des clients ";
    }

    @Override
    public String getIconPath() {
        return ApplicationView.ICONS22 + "customer-icon.png";
    }

    @Override
    public AbstractSidePanel getSidePanel() {
        if (theCustomerSidePanel == null) {
            theCustomerSidePanel = new CustomerSidePanel();
        }
        this.setTheRowObserver(theCustomerSidePanel);

        return theCustomerSidePanel;
    }

    @Override
    public List<Customer> getTheTableData() {
        return theCustomersData;
    }

    @Override
    public void fireUpdateEvent(MouseEvent e) {

        Customer c = getSelectedModel();
        System.out.println("selceted model : ");
        System.out.println(c);
        if (c != null) {
            ApplicationController.getAppView().fireCustomerUpdateEvent(c);
        }
    }

    @Override
    public void showEntitySummary() {

    }

    @Override
    public void showEntityActions() {
    }

    @Override
    public void setTheRowObserver(IRowObserver theRowObserver) {
        this.theRowObserver = theCustomerSidePanel;
    }

    @Override
    public Customer getModel() {
        return new Customer();
    }

    @Override
    public void delete(Customer e) {
        (new CustomerDAO()).remove(e);
    }

    @Override
    public SecureCommand<Customer> getRelatedFormCommand(Customer e) {
        return new CustomerAddCommand(e);
    }

    @Override
    protected boolean hasDeactivateCustomer() {
        return true;
    }

    protected boolean hasBarChart() {
        return true;
    }

    @Override
    protected boolean hasCash() {
        return true;
    }

    @Override
    protected void onPayment() {

        Customer c = getSelectedModel();
        if (c == null) {
            ApplicationView.showErrorMsg("Vous devez sélectionnez un client pour pouvoir payer son crédit !");
            return;
        }
        float credit = (float) c.getTheCredit();
        
        if (credit == 0) {
            ApplicationView.showErrorMsg("Ce client a zero crédit!");
            return;

        }
        
        int askYesNo = ApplicationView.showAskYesNo("Voulez vous payer tout votre crédit(ou juste une partie) ?");
        float ammountToPay = 0;
        String val = "0.0";
        if (askYesNo == JOptionPane.NO_OPTION) {
            val = JOptionPane.showInputDialog("Entrez la valeur de crédit à payer", String.valueOf((float)c.getTheCredit()));
            System.out.println(val);
            try {
                if(val == null)
                {
                    ApplicationView.showErrorMsg("Opération annullée !");
                    return;
                }
                ammountToPay = Float.parseFloat(val);
                
            } catch (NumberFormatException e) {
                System.out.println("valeur invalide");
            }
        } else {
            ammountToPay = (float) c.getTheCredit();
        }
        

        // ammountToPay = credit - Float.parseFloat(val);
        if(ammountToPay > (float)c.getTheCredit())
        {
            ApplicationView.showErrorMsg("Erreur ,la valeur entrée dépasse le crédit du client !");
            return;
        }
        float tva = 20;
        Payment p = new Payment();
        p.setTheCashingDate(new Date());
        p.setTheCustomer(c);
        p.setThePartnerType("client");
        p.setTheHTaxAmount(ammountToPay / (1 + tva / 100));
        p.setTheTTCAmount(ammountToPay);
        p.setThePaymentType("credit");
        c.setTheCredit(credit - ammountToPay);
        (new PaymentDAO()).create(p);

        (new CustomerDAO()).update(c);
        ApplicationView.showErrorMsg("Crédit encaissé avec succés !");

    }

    @Override
    protected void onRefresh() {
        super.onRefresh(); //To change body of generated methods, choose Tools | Templates.
        if(theCustomerSidePanel != null)
            theCustomerSidePanel.update();
    }

    protected void onDeactivateCustomer() {
        Customer e = getSelectedModel();

        if (e == null) {
            ApplicationView.showErrorMsg("Pour marquer un client comme inactif ,sélectionnez le d'abord");
            return;
        } else {
            if (e.getTheActiveStatus() == false) {
                ApplicationView.showErrorMsg("le client CL" + e.getId() + " est déja inactif");
                return;
            }
            e.setTheActiveStatus(false);
            (new CustomerDAO()).update(e);
            ApplicationView.showErrorMsg("le client CL" + e.getId() + " est mis en etat inactif");
        }
    }

}
