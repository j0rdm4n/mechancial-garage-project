/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSearchPanel;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author E-solutions
 */
public class PlaceSearchPanel extends AbstractSearchPanel{
    private JTextField nameTxt;

    @Override
    public JPanel buildSearchForm() {
        JPanel searchPanel = new JPanel(new MigLayout());
         nameTxt = new JTextField();
        JLabel nameLbl = new JLabel("Nom d'emplacement ");
        searchPanel.add(nameLbl,"split ,left,width 300");
        searchPanel.add(nameTxt,"right,width 300,wrap");
        return searchPanel;
    }

    @Override
    public String buildSearchFilter() {
      
        StringBuilder filter = new StringBuilder();
        filter.append("theItemPlaceName");
        filter.append(":like:");
        filter.append(nameTxt.getText());
        filter.append(";");
        
        return filter.toString();
        
        
    }
    
}
