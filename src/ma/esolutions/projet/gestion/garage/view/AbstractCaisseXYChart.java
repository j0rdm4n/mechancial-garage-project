/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import ma.esolutions.projet.gestion.garage.view.common.AbstractXYChart;

/**
 *
 * @author E-solutions
 */
abstract public class AbstractCaisseXYChart extends AbstractXYChart{

   
    @Override
    protected String getYAxisLabel() {
        return "Total en caisse";
    }

    @Override
    public String getTitle() {
        return "Etat de caisse";
    }

    
}
