/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.awt.Component;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu.Separator;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.model.CommercialPaper;
import ma.esolutions.projet.gestion.garage.model.Vendor;
import ma.esolutions.projet.gestion.garage.model.VendorDAO;
import ma.esolutions.projet.gestion.garage.view.common.AbstractFormView;
import ma.esolutions.projet.gestion.garage.view.common.JTextFieldExtended;
import net.miginfocom.swing.MigLayout;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

/**
 *
 * @author E-solutions
 */
public class CommercialPaperFormView extends AbstractFormView<CommercialPaper> {

    private boolean isNewModel;
    private CommercialPaper commercialPaper;
    private Date date;
    private VendorDAO vendorDAO;
    private JComboBox vendorCmb;
    private JTextFieldExtended ammountTxt;
    private JTextFieldExtended dateTxt;
    private JTextFieldExtended dueTxt;
    private JDatePanelImpl dueDate;
    private JDatePickerImpl dueDatePicker;
    private JComboBox stateComb;

    public CommercialPaperFormView(CommercialPaper p) {
        super(ApplicationController.getAppView());

        if (p.getId() == null) {
            isNewModel = true;
        } else {
            isNewModel = false;
        }
        vendorDAO = new VendorDAO();
        this.commercialPaper = p;
    }

    @Override
    public void buildUI() {
        initComponents();
        
        this.addPageToForm("Effets ", buildPage());
        if (!isNewModel) {
            popFields();
        }
        pack();

    }
    @Override
    public void clearFields()
    {
        this.ammountTxt.setText("");
        this.dateTxt.setText("");
        
    }
    private void buildDueDateFields(JPanel panel) {

        UtilDateModel mm = new UtilDateModel();

        mm.setValue(new Date());
        mm.setSelected(true);
        JLabel dueLbl = new JLabel("Date de paiement");
        panel.add(dueLbl, "split,left,width 300");

        dueDate = new JDatePanelImpl(mm);
        dueDatePicker = new JDatePickerImpl(dueDate, new JFormattedTextField.AbstractFormatter() {
            private String theDatePattern = "dd-MM-yyyy";
            private SimpleDateFormat theSimpleDateFormatter = new SimpleDateFormat(theDatePattern);

            @Override
            public Object stringToValue(String text) throws ParseException {
                return theSimpleDateFormatter.parseObject(text);
            }

            @Override
            public String valueToString(Object value) throws ParseException {
                if (value != null) {
                    Calendar cal = (Calendar) value;
                    return theSimpleDateFormatter.format(cal.getTime());

                }

                return "";
            }
        });
        panel.add(dueDatePicker, "right,width 300,wrap");
        stateComb = new JComboBox();
        String[] states =new String[]{"pas encore payé","payé"};
        DefaultComboBoxModel stateModel = new DefaultComboBoxModel(states);
        stateComb.setModel(stateModel);
        panel.add(new JLabel("etat"),"split,left,width 300");
        panel.add(stateComb,"right,width 300,wrap");
        
    }

    private JPanel buildPage() {
        JPanel panel = new JPanel(new MigLayout());
        List<Vendor> vendors = vendorDAO.all();
        DefaultComboBoxModel vendorsModel = new DefaultComboBoxModel(vendors.toArray());

        vendorCmb = new JComboBox(vendorsModel);

        
        JLabel vendorLbl = new JLabel("Fournisseur ");
        
        panel.add(vendorLbl, "gaptop 40,split,left,width 300 ");
        panel.add(vendorCmb, "gaptop 40,right,width 300,wrap");
        
        JLabel ammountLbl = new JLabel("Montant à payer");
        ammountTxt = new JTextFieldExtended(90);

        panel.add(ammountLbl, "split,left,width 300");
        panel.add(ammountTxt, "right,width 300,wrap");

        dateTxt = new JTextFieldExtended(90);
        JLabel dateLbl = new JLabel("Date d'entrée ");
        panel.add(dateLbl, "split,left,width 300");
        panel.add(dateTxt, "right,width 300,wrap");
        date = new Date();
        dateTxt.setText(date.toString());
        this.buildDueDateFields(panel);

        return panel;
    }

    @Override
    public CommercialPaper getEntity() {
        return commercialPaper;
    }

    @Override
    public String getFormIconPath() {
        return ApplicationView.ICONS16 + "customer.png";
    }

    @Override
    public String getFormTitle() {
        if (isNewModel) {
            return "Ajouter nouvel effet";
        } else {
            return "Mettre à jour un effet ";
        }
    }

    @Override
    public void onHelp() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void popFields() {

    }

    @Override
    public CommercialPaper pushFields() {

        commercialPaper.setDate(date);
        commercialPaper.setVendor((Vendor) this.vendorCmb.getSelectedItem());
        try
        { 
            commercialPaper.setAmmount(Integer.parseInt(this.ammountTxt.getText()));
        }catch(NumberFormatException e)
        {
            commercialPaper.setAmmount(0);
        }
        commercialPaper.setDueDate((Date) dueDate.getModel().getValue());
        //commercialPaper.setDueDate(duedate);
        commercialPaper.setState((String) stateComb.getSelectedItem());
        
        return commercialPaper;
    }

    @Override
    public String getIconPath() {
        return ApplicationView.ICONS16 + "customer.png";
    }

    @Override
    public Component asComponent() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
