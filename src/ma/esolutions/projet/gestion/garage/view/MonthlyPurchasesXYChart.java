/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.util.Calendar;
import java.util.Map;
import javafx.scene.chart.Chart;
import javafx.scene.chart.XYChart;
import ma.esolutions.projet.gestion.garage.controller.MainController;

/**
 *
 * @author born2code
 */
abstract public class MonthlyPurchasesXYChart extends AbstractPurchasesXYChart {
    private  int theYear;


    @Override
    protected String getXAxisLabel() {
        return "Mois";
    }

    @Override
    protected String getSeriesLabel() {
        return "Achats par mois";
    }

    public MonthlyPurchasesXYChart(MainController controller) {
        super(controller);
        theYear = getCurrentYear();
    }

    private int getCurrentYear() {
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.YEAR);
    }
    @Override
    protected XYChart.Series getData() {
        String[] months = {"Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre"};
        Map mm = theController.getPurchasesPerMonthForYear(theYear);
        theSeries = new XYChart.Series();

        for (String m : months) {
            theSeries.getData().add(new XYChart.Data(m, mm.get(m)));
        }

        return theSeries;
    }

}
