/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import java.util.Map;
import javafx.scene.chart.XYChart;
import ma.esolutions.projet.gestion.garage.controller.MainController;

/**
 *
 * @author born2code
 */
abstract public class DailyPurchasesXYChart extends  AbstractPurchasesXYChart{

    private int theMonth;
    public DailyPurchasesXYChart(MainController controller)
    {
        super(controller);
        theMonth = 1;
    }
    public void setTheMonth(int m)
    {
        theMonth = m;
        notifyChart();
    }
    @Override
    protected XYChart.Series getData() {
        theSeries = new XYChart.Series();

        Map mm = theController.getPurchasesPerDayForMonth(theMonth);
         
        for(int i = 0;i<mm.size();i++)
        {
            theSeries.getData().add(new XYChart.Data(""+(i+1),mm.get(""+(i+1)) ));
        }
        
        return theSeries;
        
    }
    
    @Override
    protected String getXAxisLabel() {
        return "Jours";
    }

    @Override
    protected String getSeriesLabel() {
        return "Achats par jour";
    }


    
}
