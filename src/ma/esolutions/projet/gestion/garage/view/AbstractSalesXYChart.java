/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import ma.esolutions.projet.gestion.garage.controller.MainController;
import ma.esolutions.projet.gestion.garage.view.common.AbstractXYChart;

/**
 *
 * @author Ahmed
 */
public abstract class AbstractSalesXYChart extends AbstractXYChart {

    public AbstractSalesXYChart(MainController controller) {
        super(controller);
    }

    @Override
    public String getTitle() {
        return "Analyses des ventes";
    }

    @Override
    protected String getYAxisLabel() {

        return "Ventes";
    }

}
