/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import java.awt.BorderLayout;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javax.swing.JPanel;
import ma.esolutions.projet.gestion.garage.model.SalesOrder;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.STYLES;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSidePanel;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author E-solutions
 */
class SalesOrderSidePanel extends AbstractSidePanel<SalesOrder> {

    public SalesOrderSidePanel() {
        super();
    }

 

    @Override
    public void getFieldsValues() {
    }

    @Override
    public String getTitle() {
        return "Détails de vente";
    }

    @Override
    public Node createCenterUI() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void handleRowSelected() {
          Node center = createUI();

        Platform.runLater(() -> {
            //getRoot().setTop(createDynamicBar());
            if (this.theEntity != null) {
                getRoot().setCenter(center);
            }
        });
    }

    public Node createUI() {
        GridPane grid = new GridPane();
        int row = 1;
        int col = 2;

        if (theEntity != null) {
            grid.setStyle(STYLES);
            grid.setAlignment(Pos.CENTER);
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(25, 25, 25, 25));
            Label customerName = new Label("Nom de client");
            grid.add(customerName, 0, row);
            Label customerNamev = new Label(this.theEntity.getTheCustomer().toString());
            grid.add(customerNamev, col, row++);
            
            Label totalTtcName = new Label("Prix total TTC");
            grid.add(totalTtcName, 0, row);
            Label totalTtcv = new Label(""+this.theEntity.getTheTotalTTCPrice());
            grid.add(totalTtcv, col, row++);
            
            Label startDate = new Label("Date d'ouverture");
            grid.add(startDate, 0, row);
            Label startDatev = new Label(""+this.theEntity.geTheStartDate().toString());
            grid.add(startDatev, col, row++);
            
          /*  Label endDate = new Label("Date de fermeture");
            grid.add(endDate, 0, row);
            Label endDatev = new Label(""+this.theEntity.getTheClosedDate().toString());
            grid.add(endDatev, col, row++);*/
           

        }
        return grid;
    }
}
