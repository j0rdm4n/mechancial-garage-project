/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import java.awt.BorderLayout;
import java.awt.Component;
import javax.swing.JPanel;
import ma.esolutions.projet.gestion.garage.controller.common.IPageController;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS16;
import ma.esolutions.projet.gestion.garage.view.common.IPageView;

/**
 *
 * @author E-solutions
 */
public class ProviderView implements IPageView{

    private IPageController theController;
    private JPanel thePageView;
    
    @Override
    public void init(IPageController controller) {
        theController = controller;
        initComponents();
    
    }

    @Override
    public IPageController getController() {
            return theController;
    }

    @Override
    public String getTitle() {
        return "Les Fournisseurs";
    }

    @Override
    public String getIconPath() {
            return ICONS16 + "provider.png";
    }

    @Override
    public Component asComponent() {
        return thePageView;
    }

    private void initComponents() {
            thePageView = new JPanel(new BorderLayout());
            thePageView.add(buildHeaderBar(),BorderLayout.NORTH);
            thePageView.add(buildCenterPanel(),BorderLayout.CENTER);
            thePageView.add(buildStatusBar(),BorderLayout.SOUTH);
    }

    private JPanel buildHeaderBar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private JPanel buildCenterPanel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private JPanel buildStatusBar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void showMainView() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void init() {
        init(null);
    }
    
}
