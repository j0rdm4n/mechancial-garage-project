/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import javafx.scene.chart.BarChart;
import javafx.scene.chart.Chart;
import ma.esolutions.projet.gestion.garage.controller.MainController;

/**
 *
 * @author born2code
 */
public class YearlyPurchasesBarChart extends YearlyPurchasesXYChart {

    public YearlyPurchasesBarChart(MainController controller) {
        super(controller);
    }

    @Override
    protected void notifyChart() {
    }

    @Override
    protected Chart createChart() {
        BarChart bar = new BarChart<>(xAxis,yAxis);
        bar.setTitle(getTitle());
        bar.setBarGap(1.0);
        bar.setHorizontalZeroLineVisible(true);
        
        getData();
                theSeries.setName(getSeriesLabel());

        System.out.println("theSeries is null ?" + theSeries);
        
        bar.getData().addAll(theSeries);
        return bar;
    
    }

    @Override
    public String getReports() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
