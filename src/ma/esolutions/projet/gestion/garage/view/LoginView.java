/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import ma.esolutions.projet.gestion.garage.app.Session;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.controller.LoginController;
import ma.esolutions.projet.gestion.garage.controller.MainController;
import ma.esolutions.projet.gestion.garage.controller.commands.LoginCommand;
import ma.esolutions.projet.gestion.garage.controller.common.IPageController;
import ma.esolutions.projet.gestion.garage.model.LoginCount;
import ma.esolutions.projet.gestion.garage.model.LoginCountDAO;
import ma.esolutions.projet.gestion.garage.model.Permission;
import ma.esolutions.projet.gestion.garage.model.PermissionDAO;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.model.Role;
import ma.esolutions.projet.gestion.garage.model.RoleDAO;
import ma.esolutions.projet.gestion.garage.model.SessionDAO;
import ma.esolutions.projet.gestion.garage.model.SessionEntity;
import ma.esolutions.projet.gestion.garage.model.User;
import ma.esolutions.projet.gestion.garage.model.UserDAO;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS12;
import ma.esolutions.projet.gestion.garage.view.common.IPageView;
import net.miginfocom.swing.MigLayout;
import org.jvnet.substance.SubstanceLookAndFeel;
import org.jvnet.substance.painter.decoration.DecorationAreaType;

/**
 *
 * @author Ahmed
 */
public class LoginView implements IPageView {

    private CustomJPanel theLoginView;
    private JTextField theUserField;
    private JLabel theUserLabel;
    private JLabel theInfoLabel = new JLabel();
    private JButton theConnectButton;
    private JPasswordField thePasswordField;
    private JLabel thePasswordLabel;
    private LoginController theController;
    private boolean isFirstTimeRun = true;
    private JTextField passwordTxt;
    private JTextField dbTxt;
    private JTextField userTxt;
    private JButton validateBtn;
    private JTextField hostTxt;
    private JTextField portTxt;
    private UserDAO userDAO;
    private CustomJPanel loginPage;
    private JLabel lblError;

    @Override
    public void init(IPageController controller) {
        theController = (LoginController) controller;
        System.out.println("init with controller " + theController.getControllerName());
        isFirstTimeRun = !theController.checkConfiguration("config.init");
        if (!isFirstTimeRun) {
            Map<String, String> m = theController.readConfiguration("config.init");
            ApplicationController.getAppController().setSettingsMap(m);
        }
        initComponents();

    }

    @Override
    public IPageController getController() {
        return theController;
    }

    @Override
    public String getTitle() {
        if (this.isFirstTimeRun) {
            return "Configuratin de la base de données ";
        }
        return "Page de Connection";
    }

    @Override
    public String getIconPath() {
        return ICONS12 + "login.png";
    }

    @Override
    public Component asComponent() {
        return theLoginView;
    }

    private void initComponents() {
        theLoginView = new CustomJPanel(new BorderLayout(),"l1.jpg");
       // theLoginView.add(buildHeaderBar(), BorderLayout.NORTH);
        theLoginView.add(buildCenterPanel(), BorderLayout.CENTER);

    }

    private void createRoles() {
        Role role1 = new Role();
        role1.setTheRoleName("CréerRoles");
        List<Permission> s = new ArrayList();
        Permission createRole = new Permission();
        createRole.setThePermissionCode(PermissionsCodes.PERMISSION_ADD_ROLE);
        createRole.setThePermissionName("créer les roles");
        s.add(createRole);
        Permission listRole = new Permission();
        listRole.setThePermissionCode(PermissionsCodes.PERMISSION_ADD_ROLE);
        listRole.setThePermissionName("afficher les roles");
        //from database get the permission by its code then add it to role

        role1.setTheRolePermissions(s);
        RoleDAO roleDAO = new RoleDAO();
        roleDAO.create(role1);
    }

    private void createUsers() {

    }

    private void createSession() {
        SessionDAO theSessionDAO = new SessionDAO();
        SessionEntity theSession;
        try {
            theSession = theSessionDAO.find((long) 1);
            theSession.setTheFirstTimeRun(false);
            theSessionDAO.update(theSession);
            Session.getSession().setTheFirstTimeRun(false);
            System.out.println("not the first time run");
        } catch (Exception e) {
            theSession = new SessionEntity();
            theSession.setTheFirstTimeRun(true);
            Session.getSession().setTheFirstTimeRun(true);
            theSessionDAO.create(theSession);
            System.out.println("this is the first time run");
        }
    }

    private void createSuperUser() {

        if (!Session.getSession().isTheFirstTimeRun()) {

            return;
        }
        List<Permission> ps = PermissionDAO.createPermissions();
        User user = new User();
        user.setTheUsername("admin");
        user.setThePassword("admin");
        Role r = new Role();
        r.setTheRoleName("SuperAdmin");
        r.setTheRolePermissions(ps);

        (new RoleDAO()).create(r);
        user.AddRole(r);
        (new UserDAO()).create(user);
    }

    private void resumeLoadingLoginForm() {
        System.out.println("resuming to login form");
        theLoginView.removeAll();
        //theLoginView.repaint();
        theLoginView.add(buildHeaderBar(), BorderLayout.NORTH);
        isFirstTimeRun = false;
        theLoginView.add(buildCenterPanel(), BorderLayout.CENTER);

        theLoginView.revalidate();
    }

    private JPanel buildHeaderBar() {
        JPanel headerBar = new JPanel(new MigLayout("insets 2 2 2 2"));
        JLabel lblTitle = new JLabel(getTitle());
        lblTitle.setIcon(new ImageIcon(getClass().getResource(getIconPath())));
        lblTitle.setFont(lblTitle.getFont().deriveFont(Font.BOLD, 14));

        headerBar.setPreferredSize(new Dimension(lblTitle.getWidth(), lblTitle.getHeight() + 28));
        headerBar.add(lblTitle, "dock center, gapleft 4");

        SubstanceLookAndFeel.setDecorationType(headerBar, DecorationAreaType.HEADER);

        return headerBar;

    }
    /*private Component buildCenterPanel2()
     {
     JPanel centerPanel = new JPanel(new MigLayout(""));
         
     }*/

    private Component buildCenterPanel() {
        CustomJPanel centerPanel = new CustomJPanel(new MigLayout("insets 200 200 200 200"),"l1.jpg");

        if (this.isFirstTimeRun) {
            centerPanel.add(buildSettingsPanel(), "width :400:,gapright 70");

        } else {
            centerPanel.add(buildLoginPanel(), "width :400:,gapright 70");

        }
        return centerPanel;
    }

    private Component buildLoginPanel() {
        loginPage = new CustomJPanel(new MigLayout(""),"l1.jpg");
        //loginPage.setLayout(null);
        lblError = new JLabel("Erreur :cet utilisateur n'existe pas !");
        theUserField = new JTextField();
        theUserLabel = new JLabel();
        thePasswordLabel = new JLabel();
        theUserField = new JTextField();
        thePasswordField = new JPasswordField();
        theConnectButton = new JButton();

        theInfoLabel.setBackground(new Color(153, 153, 153));
        theInfoLabel.setFont(new Font("Tahoma", 0, 20));
        theInfoLabel.setForeground(Color.blue);

        theInfoLabel.setText("Utilisez ce formulaire pour se connecter !");
        theUserLabel.setText("Username : ");
        thePasswordLabel.setText("Mot de Passe : ");

        loginPage.add(theInfoLabel, "span,wrap");
        loginPage.add(theUserLabel, "split,gaptop 60,left,width 200! ");

        loginPage.add(theUserField, "right,gaptop 30,width 200!,wrap");

        loginPage.add(thePasswordLabel, "split,gaptop 10,left,width 200!");
        loginPage.add(thePasswordField, "right,gaptop 10,width 200!,wrap");

        theConnectButton.setText("Se Connecter");
        loginPage.add(theConnectButton, "span,gaptop 20,width 405!");

        LoginCountDAO loginCountDAO = new LoginCountDAO();
        loginCountDAO.create(new LoginCount());
        theConnectButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                onConnect();

            }

        });

        System.out.println("Building the Login Page");

        return loginPage;
    }

    @Override
    public void showMainView() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void onConnect() {
        User u = new User();
        u.setTheUsername(theUserField.getText());
        u.setThePassword(String.copyValueOf(thePasswordField.getPassword()));
        //theController.onLogin(u);
        LoginCommand cmd = new LoginCommand(u.getTheUsername(), u.getThePassword());
        cmd.execute();
        System.out.println("SUCCESS_STATUS " + cmd.getStatus());
        if (LoginCommand.SUCCESS_STATUS == cmd.getStatus()) {
            System.out.println("success !!!");
            ApplicationController.getAppView().resumeUILoading();
            MainController theMainController = new MainController();
            ApplicationView.showListView(theMainController.getPageView());
            //ApplicationView.showPage(theMainController.getPageView());

        } else 
        {
            loginPage.add(lblError, "wrap");
            loginPage.revalidate();
        }

        theUserField.setText("");
        thePasswordField.setText("");
    }

    @Override
    public void init() {
        init(null);
    }

    private void createDB(String dbuser, String dbname, String dbpassword) throws SQLException {
        StringBuilder url = new StringBuilder();
        url.append("jdbc:mysql://");
        url.append(hostTxt.getText());
        url.append(":");
        url.append(portTxt.getText());
        url.append("/?user=");
        System.out.println("url " + url.toString());
        Connection con = DriverManager.getConnection(url.toString() + dbuser, dbuser, dbpassword);
        System.out.println("connection with server " + con);
        Statement stmt = con.createStatement();
        stmt.executeUpdate("create database if not exists " + dbname);
        //  dbName = dbname;
        // dbUser = dbuser;
        //dbPassword = dbpassword;
    }

    private void clearFields() {
        userTxt.setText("");
        passwordTxt.setText("");
        dbTxt.setText("");
        hostTxt.setText("");
        portTxt.setText("3306");
    }

    private Component buildSettingsPanel() {
        JPanel panel = new JPanel(new MigLayout());
        JLabel userLbl = new JLabel("Nom d'utilisateur");
        userTxt = new JTextField();

        JLabel passwordLbl = new JLabel("mot de passe ");
        passwordTxt = new JTextField();

        JLabel dbLbl = new JLabel("Nom de base de données ");
        dbTxt = new JTextField();

        panel.add(userLbl, "split,left,width 300");
        panel.add(userTxt, "right,width 300,wrap");

        panel.add(passwordLbl, "split,left,width 300");
        panel.add(passwordTxt, "right,width 300,wrap");

        panel.add(dbLbl, "split,left,width 300");
        panel.add(dbTxt, "right,width 300,wrap");
        panel.add(new JLabel("Hote"), "split,left,width 300");
        hostTxt = new JTextField();
        panel.add(hostTxt, "right,width 300,wrap");

        panel.add(new JLabel("Port"), "split,left,width 300");
        portTxt = new JTextField();
        portTxt.setText("3306");
        panel.add(portTxt, "right,width 300,wrap");

        validateBtn = new JButton("Valider");
        panel.add(validateBtn, "gaptop 30,span 2 ,width 600,wrap");
        validateBtn.addActionListener((e) -> {

            Map<String, String> m = new HashMap<>();
            m.put("user", userTxt.getText());
            m.put("password", passwordTxt.getText());
            m.put("database", dbTxt.getText());
            m.put("host", hostTxt.getText());
            m.put("port", portTxt.getText());
            ApplicationController.getAppController().setSettingsMap(m);
            theController.createConfiguration("config.init", m);
            try {
                createDB(userTxt.getText(), dbTxt.getText(), passwordTxt.getText());
                createSession();
                createSuperUser();
                resumeLoadingLoginForm();

            } catch (SQLException ex) {
                clearFields();
                //afficher un message
            }
        });
        return panel;
    }

}
