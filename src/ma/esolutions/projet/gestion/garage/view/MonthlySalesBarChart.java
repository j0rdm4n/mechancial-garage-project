/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import javafx.scene.chart.BarChart;
import javafx.scene.chart.Chart;
import ma.esolutions.projet.gestion.garage.controller.MainController;

/**
 *
 * @author born2code
 */
public class MonthlySalesBarChart extends MonthlySalesXYChart{

    public MonthlySalesBarChart(MainController controller) {
        super(controller);
    }

   
    
    @Override
    protected Chart createChart() {
        BarChart bar = new BarChart<>(xAxis,yAxis);
        bar.setTitle(getTitle());
        bar.setBarGap(1.0);
        bar.setHorizontalZeroLineVisible(true);
        
        theSeries.setName(getSeriesLabel());
        getData();
        bar.getData().addAll(theSeries);
        return bar;
    }

    @Override
    public String getReports() {
        return "Mois avec le maximum des ventes \n"+
               "Mois avec le minimum des ventes \n"+
               "Le moyen arithmétique des ventes\n";
    }
    
}
