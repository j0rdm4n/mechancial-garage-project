/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import java.awt.Component;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu.Separator;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.model.Permission;
import ma.esolutions.projet.gestion.garage.model.PermissionDAO;
import ma.esolutions.projet.gestion.garage.model.Role;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS16;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS22;
import ma.esolutions.projet.gestion.garage.view.common.AbstractFormView;
import ma.esolutions.projet.gestion.garage.view.common.JTextFieldExtended;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author E-solutions
 */
public class RoleFormView extends AbstractFormView<Role> {

    Role theRole;
    private final boolean isNewModel;
    private JTextFieldExtended theRoleNameTextfield;
    private JLabel theRoleNameLabel;
    private JPanel thePermissionPanel;
    private JLabel thePermissionPanelLabel;
    private List<Permission> allPermissions;
    private PermissionDAO thePermissionDAO;
    private List<Permission> theRolePermissions ;
   
    private ArrayList<JCheckBox> theChekBoxes;
    public RoleFormView(Role role) {
        super(ApplicationController.getAppView());
        theRole = role;
        thePermissionDAO = new PermissionDAO();
        theRolePermissions = new ArrayList<>();
        if(role.getId() == null)
        {
            isNewModel = true;
        }
        else
        {
            isNewModel = false;
        }
    }
    private JPanel buildPage()
    {
        JPanel panel = new JPanel(new MigLayout());
        theRoleNameLabel = new JLabel("Nom du role");
        theRoleNameTextfield = new JTextFieldExtended(90);
        thePermissionPanelLabel = new JLabel("Selectionnez les permissions pour ce role");
        
        thePermissionPanel = new JPanel(new MigLayout("wrap 3"));
     
        
        panel.add(theRoleNameLabel,"gaptop 30,split,left,width 100");
        panel.add(theRoleNameTextfield,"right,width 200,wrap");
        panel.add(new Separator(),"span,growx");
        panel.add(thePermissionPanelLabel,"width 600,wrap,gaptop 30");
        panel.add(new Separator(),"span,growx");
        
        createPermissionsPanel();
        panel.add(thePermissionPanel,"width 60,wrap");
       panel.add(new Separator(),"span,growx");
        return panel;
    }
    @Override
    public void buildUI() {
       initComponents();
       addPageToForm("Informations sur le client",
                buildPage());
      

        popFields();
        pack();
        
    
        
    }

    private void createPermissionsPanel() {
        allPermissions = thePermissionDAO.all();
      
        allPermissions.forEach((Permission p)->{
           
            System.out.println("permission " + p );
            //JLabel label = new JLabel();
            JCheckBox checkbox = new JCheckBox(p.toString());
         
           // thePermissionPanel.add(label,"gaptop 10,width 40,gapleft 10");
          
            checkbox.addItemListener((e)->{
            
                if(checkbox.isSelected())
                {
                    System.out.println( p + "selected");
                   if( ! theRolePermissions.contains(p))
                   {
                     theRolePermissions.add(p);
                    }  
                }
                else
                {
                    if(theRolePermissions.contains(p))
                   {
                     theRolePermissions.remove(p);
                    } 
                }
            });
            
                thePermissionPanel.add(checkbox,"gaptop 10,width 10,gapleft 10");
            
         
            
            
        });
        
    }
    
    @Override
    public Role getEntity() {
        return theRole;
    }

    @Override
    public String getFormIconPath() {
        return ICONS22+"role.png";
    }

    @Override
    public String getFormTitle() {
        return "Ajouter un role";
    }

    @Override
    public void onHelp() {
    }

    @Override
    public void popFields() {
        this.theRoleNameTextfield.setText(theRole.getTheRoleName());
        //this(theRole.getTheRolePermissions())
        //todo : load permissions
    }

    @Override
    public Role pushFields() {
        
        Role r = new Role();
        r.setTheRoleName(this.theRoleNameTextfield.getText());
        r.setTheRolePermissions(theRolePermissions);
        this.theRole = r;
        return r;
    }

    @Override
    public String getIconPath() {
        return ICONS16+"role.png";
    }

    @Override
    public Component asComponent() {
        return null;
    }

}
