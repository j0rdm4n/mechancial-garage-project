/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.awt.BorderLayout;
import java.awt.Component;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTabbedPane;
import ma.esolutions.projet.gestion.garage.app.Session;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.User;
import ma.esolutions.projet.gestion.garage.model.UserDAO;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS16;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.IMAGES;
import ma.esolutions.projet.gestion.garage.view.common.AbstractFormView;
import ma.esolutions.projet.gestion.garage.view.common.JTextFieldExtended;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author E-solutions
 */
public class SwitchAccountFormView extends AbstractFormView<User> {

    protected User theUser = null;
    protected JTextFieldExtended theUsernameField;
    protected JPasswordField thePasswordField;
    protected JButton theConnectField;
    protected JButton theCancelField;
    protected SecureCommand theExecCommand;

    public SwitchAccountFormView() {
        super(ApplicationController.getAppView());
        theUser = new User();
        System.out.println("switch account");
    }

    @Override
    public void initComponents() {
        setLayout(new BorderLayout());
        setIconImage(new ImageIcon(getClass().getResource(getFormIconPath())).getImage());
        setTitle(getFormTitle());

        getContentPane().add(buildStatusBar(), BorderLayout.SOUTH);

        if (isMultiPageForm()) {
            theTabbedPages = new JTabbedPane();
            theTabbedPages.setFocusable(false);
            getContentPane().add(theTabbedPages, BorderLayout.CENTER);
        }
    }

    @Override
    public void buildUI() {
        initComponents();
        this.addPageToForm("", buildLoginPage());
        pack();
        setSize(550, 400);

    }

    @Override
    public boolean isMultiPageForm() {
        return false;
    }

    public void setExecCommand(SecureCommand cmd) {
        this.theExecCommand = cmd;
    }

    private JPanel buildLoginPage() {
        theUsernameField = new JTextFieldExtended(100);
        thePasswordField = new JPasswordField();
        theConnectField = new JButton("Confirmer");
        theCancelField = new JButton("Annuller");
        theCancelField.addActionListener((e) -> {
            setVisible(false);
            dispose();
        });
        theConnectField.addActionListener((e) -> {
            theUser = pushFields();
            System.out.println("the cuurent useer " + theUser);
            setVisible(false);
            System.out.println("exec command " + theExecCommand);
            UserDAO theUserDAO = new UserDAO();
            User tmp;
            if ((tmp = theUserDAO.findByUsernameAndPassword(theUser.getTheUsername(),theUser.getThePassword())) != null) {
                System.out.println("changing current user with success");
                Session.getSession().setTheCurrentUser(tmp);

            }
            if(theExecCommand != null)
                theExecCommand.execute();
          //  theExecCommand.setExecutedAlready();

            dispose();
        });
        JPanel panel = new JPanel(new MigLayout("insets 20 10 10 10", "[][50:100,fill][fill,grow]", ""));

        panel.add(new JLabel("Utilizez ce formulaire pour vous connectez !"), "span,wrap");
        panel.add(new JLabel(new ImageIcon(getClass().getResource(IMAGES + "account-128.png"))), "center,gapleft 130,gaptop 10,wrap");

        panel.add(new JLabel("Username"), "split,gaptop 30,left,width 150!");

        panel.add(theUsernameField, "right,wrap,width 220!,span");

        panel.add(new JLabel("Mot de Passe"), "split,gaptop 15,left,width 150!");
        panel.add(thePasswordField, "right,width 220! ,span,wrap");
        panel.add(theConnectField, "gaptop 15,split,right,gapright 75");
        panel.add(theCancelField, "gaptop 20,split,left");
        return panel;
    }

    @Override
    public User getEntity() {
        return theUser;
    }

    @Override
    public String getFormIconPath() {
        return ICONS16 + "account.png";
    }

    @Override
    public String getFormTitle() {

        return "Changement de Compte";
    }

    @Override
    public void onHelp() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void popFields() {
        return;
    }

    @Override
    public User pushFields() {
        User user = new User();
        user.setTheUsername(theUsernameField.getText());
        user.setThePassword(String.copyValueOf(thePasswordField.getPassword()));
        return user;
    }

    @Override
    public String getIconPath() {
        return ICONS16 + "account.png";
    }

    @Override
    public Component asComponent() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @SuppressWarnings("empty-statement")
    public void blockUI() {
        while (true)
            ;
    }

}
