/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.model.Item;
import ma.esolutions.projet.gestion.garage.model.ItemDAO;
import ma.esolutions.projet.gestion.garage.model.ItemPlace;
import ma.esolutions.projet.gestion.garage.model.MinimalStockException;
import ma.esolutions.projet.gestion.garage.model.NegativeStockException;
import ma.esolutions.projet.gestion.garage.model.Vendor;
import ma.esolutions.projet.gestion.garage.model.VendorDAO;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS16;
import ma.esolutions.projet.gestion.garage.view.common.AbstractFormView;
import ma.esolutions.projet.gestion.garage.view.common.JTextFieldExtended;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author E-solutions
 */
public class InventoryFormView extends AbstractFormView<Item> {
    
    private Item theItem;
    private List<Item> theItems;
    private ArrayList<Item> theExistingItems;
    private boolean isNewModel = true;
    
    private JLabel theItemNameLabel;
    private JComboBox theItemNameComboBox;
    private JLabel theItemQuantiteLabel;
    private JTextFieldExtended theItemQuantiteTextfield;
    private JLabel theAlimentationDateLabel;
    private JTextFieldExtended theAlimentationDateTextfield;
    private JLabel theUnitPriceLabel;
    private JTextFieldExtended theUnitPriceTextfield;
    private JLabel theItemPlaceLabel;
    private ComboBoxModel theItemPlaces;
    private JComboBox theItemPlaceComboBox;
    private JButton theSaveBtn;
    private JButton theCloseBtn;
    private JButton theNewPlaceBtn;
    private JLabel theCaptionLabel;
    private Date theDate = new Date();
    private ArrayList<ItemPlace> thePlaces;
    private JTextFieldExtended theNewPlaceTextfield;
    private DefaultComboBoxModel theItemNameModel;
    private JLabel theTVALabel;
    private JTextFieldExtended theTVATxt;
    private JLabel theVendorLabel;
    private JComboBox theVendorCombo;
    private Component thePrecentLbl;
    private DefaultComboBoxModel theVendors;
    private VendorDAO theVendorDAO;
    private ItemDAO theItemDAO;
    private JTextField salesPriceTxt;
    private JLabel stockMinLbl;
    private JTextFieldExtended stockMinTxt;
    
    public InventoryFormView(Item item) {
        this(item, null, null);
    }
    
    public InventoryFormView(Item item, ArrayList<ItemPlace> places, ArrayList<Item> myItems) {
        super(ApplicationController.getAppView());
        theItem = item;
        thePlaces = places;
        theExistingItems = myItems;
        // theItemPlaces = new DefaultComboBoxModel( thePlaces);
        if (places != null) {
            theItemPlaces = new DefaultComboBoxModel(places.toArray());
        }
        if (item.getId() == null) {
            isNewModel = true;
        } else {
            isNewModel = false;
        }
        theItems = new ArrayList<>();
    }
    
    @Override
    public String getFormTitle() {
        if (isNewModel) {
            return "Alimentation du stock";
        } else {
            return "Mise à jour du stock";
        }
        
    }
    
    @Override
    public String getFormIconPath() {
        return ICONS16 + "inventory.png";
    }
    
    @Override
    public void buildUI() {
        initComponents();
        addPageToForm("Entrée d'article", buildPage());
        if (!isNewModel) {
            popFields();
        }
        
        pack();
        
    }
    
    private JPanel buildPage() {
        JPanel panel = new JPanel(new MigLayout("insets 20 10 10 10", "[][50:100,fill][fill,grow]", ""));
        
        theItemNameLabel = new JLabel("Nom de l'article");
        //theExistingItems = new ArrayList<Item>();

        theItemNameModel = new DefaultComboBoxModel(theExistingItems.toArray());
        
        if (theItemNameModel != null) {
            theItemNameComboBox = new JComboBox(theItemNameModel);
            theItemNameComboBox.setSelectedIndex(-1);
            theItemNameComboBox.setEditable(true);
        }
        
        theItemQuantiteLabel = new JLabel("Quantité");
        theItemQuantiteTextfield = new JTextFieldExtended(50);
        
        theAlimentationDateLabel = new JLabel("Date d'alimentation");
        theAlimentationDateTextfield = new JTextFieldExtended(50);
        
        theAlimentationDateTextfield.setText(theDate.toString());
        theAlimentationDateTextfield.setEnabled(false);
        theUnitPriceLabel = new JLabel("Prix Unitaire HT(Hors Tax)");
        theUnitPriceTextfield = new JTextFieldExtended(50);
        
        theItemPlaceLabel = new JLabel("Placement d'Article");
        theItemNameComboBox.setEnabled(isNewModel);
        
        if (theItemPlaces != null) {
            theItemPlaceComboBox = new JComboBox(theItemPlaces);
            
            theItemPlaceComboBox.setEnabled(isNewModel);
            
        }
        theNewPlaceTextfield = new JTextFieldExtended(30);
        
        theCaptionLabel = new JLabel("Utilisez ce formulaire pour saisir les informations sur les articles à stocker ");
        theCaptionLabel.setBackground(Color.BLUE);
        theCaptionLabel.setFont(new Font("Serif", Font.BOLD | Font.ITALIC, 20));
        theCaptionLabel.setForeground(Color.blue);
        theNewPlaceBtn = new JButton("Nouveau Placement ... ");
        theTVALabel = new JLabel("Pourcentage TVA");
        theTVATxt = new JTextFieldExtended(50);
        theTVATxt.setText("20");
        thePrecentLbl = new JLabel("%");
        theVendorLabel = new JLabel("Fournisseur");
        theVendorCombo = new JComboBox();
        theVendorDAO = new VendorDAO();
        theVendors = new DefaultComboBoxModel(theVendorDAO.all().toArray());
        theVendorCombo.setModel(theVendors);
        
        salesPriceTxt = new JTextField();
        stockMinLbl = new JLabel("stock minimum ");
        stockMinTxt = new JTextFieldExtended(90);
        stockMinTxt.setText("0");
        panel.add(theCaptionLabel, "span , width 600,gaptop 20,wrap");
        panel.add(this.theItemNameLabel, "split ,left, width 300,gaptop 60");
        if (theItemPlaceComboBox != null) {
            panel.add(this.theItemNameComboBox, "right,width 300,wrap,gaptop 60");
        }
        
        panel.add(this.theItemQuantiteLabel, "split ,left, width 300");
        panel.add(this.theItemQuantiteTextfield, "right,width 300,wrap");
        
        panel.add(this.stockMinLbl, "split,left,width 300");
        panel.add(this.stockMinTxt, "right,width 300,wrap");
        
        panel.add(this.theUnitPriceLabel, "split,left,width 300");
        panel.add(this.theUnitPriceTextfield, "right,width 300,wrap");
        
        panel.add(this.theTVALabel, "split 3,left,width 300");
        panel.add(this.theTVATxt, "right,width 200");
        panel.add(this.thePrecentLbl, "right,width 99,wrap");
        
        this.salesPriceTxt.setText("0.0");
        panel.add(new JLabel("Marge de gain(hors tax) "), "split,left,width 300");
        panel.add(this.salesPriceTxt, "right,width 300,wrap");
        
        panel.add(this.theAlimentationDateLabel, "split, left,width 300");
        panel.add(this.theAlimentationDateTextfield, "right, width 300,wrap");
        
        panel.add(this.theVendorLabel, "split,left,width 300");
        panel.add(this.theVendorCombo, "right,width 300,wrap");
        
        panel.add(this.theItemPlaceLabel, "split 2,left,width 300");
        if (theItemPlaces != null) {
            panel.add(this.theItemPlaceComboBox, "right,width 300");
        }
        /**
         * start validation code
         */
        this.theItemQuantiteTextfield.addFocusListener(new FocusListener() {
            
            @Override
            public void focusGained(FocusEvent e) {
                
            }
            
            @Override
            public void focusLost(FocusEvent e) {
                if (false == validateItemQuantity()) {
                    ApplicationView.showErrorMsg("Entrez une valeur valide pour la quantité");
                    theItemQuantiteTextfield.grabFocus();
                }
            }
            
        });
        
        this.theUnitPriceTextfield.addFocusListener(new FocusListener() {
            
            @Override
            public void focusGained(FocusEvent e) {
            }
            
            @Override
            public void focusLost(FocusEvent e) {
                if (false == validatePriceValue()) {
                    ApplicationView.showErrorMsg("Entrez une valeur valide pour le prix unitaire");
                    theUnitPriceTextfield.grabFocus();
                    
                }
            }
            
        });
        
        this.theTVATxt.addFocusListener(new FocusListener() {
            
            @Override
            public void focusGained(FocusEvent e) {
            }
            
            @Override
            public void focusLost(FocusEvent e) {
                if (false == validateTva()) {
                    ApplicationView.showErrorMsg("Entrez une valeur valide pour le pourcentage tva");
                    theTVATxt.grabFocus();
                    
                }
            }
            
        });
        /**
         *
         */
        //panel.add(theNewPlaceBtn, "right,width 140,wrap");
        /*theNewPlaceBtn.addActionListener(new AbstractAction() {
         private PlaceFormView thePlaceFormView;

         @Override
         public void actionPerformed(ActionEvent e) {
         System.out.println("add new place");
         thePlaceFormView = new PlaceFormView(new ItemPlace());
         thePlaceFormView.showDialog();
         panel.revalidate();
         validate();
         }
         });*/
        
        return panel;
    }
    
    private boolean validateTva() {
        
        try {
            Integer.parseInt(theTVATxt.getText());
        } catch (NumberFormatException e) {
            setStatusbarMsg("Entrez une valeur valide pour le pourcentage tva ");
            return false;
        }
        return true;
    }
    
    private boolean validatePriceValue() {
        try {
            Double.parseDouble(theUnitPriceTextfield.getText());
        } catch (NumberFormatException e) {
            setStatusbarMsg("Entrez une valeur valide pour le prix unitaire ");
            return false;
        }
        return true;
    }
    
    private boolean validateItemQuantity() {
        try {
            Integer.parseInt(this.theItemQuantiteTextfield.getText());
        } catch (NumberFormatException e) {
            setStatusbarMsg("Entrez une valeur valide pour la quantité ");
            return false;
        }
        return true;
        
    }
    
    @Override
    public boolean validateForm() {
        //Item e = (Item) theItemNameComboBox.getSelectedItem();
        //System.out.println("selected item" + theItemNameComboBox.getSelectedItem());
        theItemDAO = new ItemDAO();
        if (theItemNameComboBox.getSelectedIndex() != -1) {
            if (theItemDAO.isItemWithSameNameAlreadyExists((String) theItemNameComboBox.getSelectedItem())) {
                ApplicationView.showErrorMsg("Une fiche article avec le méme nom existe déja !");
                return false;
            }
        }
        if (theItemNameComboBox.getSelectedIndex() != -1) {
            this.setStatusbarMsg("Cet article existe déja");
            System.out.println("existe deja");
            
            return false;
        }
        if (validateItemQuantity() == false) {
            return false;
        }
        
        return true;
    }
    
    @Override
    public void popFields() {
        if (this.theItem != null) {
            theItemNameComboBox.setSelectedItem(theItem);
            theItemPlaceComboBox.setSelectedItem(theItem.getTheItemPlace());
            theItemQuantiteTextfield.setText("" + theItem.getTheItemQuantity());
            theAlimentationDateTextfield.setText(theItem.getTheItemAddedDate().toString());
            theUnitPriceTextfield.setText("" + theItem.getTheUnitPrice());
            theTVATxt.setText("" + theItem.getTheTVA());
            salesPriceTxt.setText(String.valueOf(theItem.getTheGain()));
            
        }
        
        return;
    }
    
    public void popSelectedFields(Item selected) {
        this.theItemQuantiteTextfield.setText("" + selected.getTheItemQuantity());
        this.theUnitPriceTextfield.setText("" + selected.getTheUnitPrice());
        this.theAlimentationDateTextfield.setText("" + selected.getTheItemAddedDate());
        this.theItemPlaceComboBox.setSelectedItem(selected.getTheItemPlace());
    }
    
    @Override
    public void clearFields() {
//        this.theItemNameComboBox.setSelectedIndex(0);
        //this.theItemPlaceComboBox.setSelectedIndex(-1);

        this.theItemNameComboBox.setSelectedItem(new Item());
        this.theItemQuantiteTextfield.setText("");
        this.theUnitPriceTextfield.setText("");
        this.theTVATxt.setText("");
        theDate = new Date();
        this.theAlimentationDateTextfield.setText(theDate.toString());
        theItemPlaceComboBox.setSelectedIndex(-1);
        
    }
    
    @Override
    public boolean isPrintable() {
        return false;
    }
    
    private boolean isWorkfloable() {
        return true;
    }
    
    @Override
    public Item pushFields() {
        
        Item e;
        if (!isNewModel) {
            e = theItem;
        } else {
            e = new Item();
        }
        if (theItemNameComboBox.getSelectedIndex() == -1) {
            
            try {
                String newName = "";
                if (theItemNameComboBox.getSelectedItem() instanceof Item) {
                    Item ee = (Item) theItemNameComboBox.getSelectedItem();
                    newName = ee.getTheItemName();
                }
                if (theItemNameComboBox.getSelectedItem() instanceof String) {
                    newName = (String) theItemNameComboBox.getSelectedItem();
                }
                if (this.theVendorCombo.getSelectedIndex() != -1) {
                    e.setTheVendor((Vendor) this.theVendorCombo.getSelectedItem());
                }
                System.out.println("nex itme name:" + newName);
                e.setTheItemName(newName);
                
                try {
                    e.setTheItemQuantity(Integer.parseInt(this.theItemQuantiteTextfield.getText()));
                } catch (MinimalStockException ex) {
                    Logger.getLogger(InventoryFormView.class.getName()).log(Level.SEVERE, null, ex);
                }
                e.setStockMin(Integer.parseInt(this.stockMinTxt.getText()));
                
                e.setTheItemAddedDate(theDate);
                e.setTheTVA(Integer.parseInt(this.theTVATxt.getText()));
                
                e.setTheUnitPrice(Float.parseFloat(theUnitPriceTextfield.getText()));
                
                //e.setTheSalesPriceTTC((1 + e.getTheTVA() / 100.0) * e.getTheUnitPrice());
                double prixVente = 0;
                double tva = (1 + e.getTheTVA() / 100.0);
                prixVente = tva * e.getTheUnitPrice() + Float.parseFloat(this.salesPriceTxt.getText()) * tva;
                e.setTheGain(Double.parseDouble(salesPriceTxt.getText()));
                e.setTheSalesPriceTTC(prixVente);
                
                /*if (isNewModel) {
                e.setTheSalesPriceTTC((1 + e.getTheTVA() / 100.0) * e.getTheUnitPrice());
                }*/
                if (!isNewModel) {
                    Item eee = theItemDAO.find(e.getId());
                    
                    double v = ((eee.getTheUnitPrice() * eee.getTheItemQuantity()) + (e.getTheUnitPrice() * e.getTheItemQuantity())) / eee.getTheItemQuantity() + e.getTheItemQuantity();
                    System.out.println("the actual sales price " + v);
                }
                
                System.out.println("pric de vente " + (1 + e.getTheTVA() / 100.0) * e.getTheUnitPrice());
                e.setTheItemPlace((ItemPlace) theItemPlaceComboBox.getSelectedItem());
                
                System.out.println("new item added ");
            } catch (NegativeStockException ex) {
                Logger.getLogger(InventoryFormView.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            //the item exists ,update it 
            Item selected = (Item) theItemNameComboBox.getSelectedItem();
            
            popSelectedFields(selected);
            e = new Item();
            e.setId(selected.getId());
            e.setTheItemName(selected.getTheItemName());
            e.setTheItemPlace((ItemPlace) theItemPlaceComboBox.getSelectedItem());
            if (selected.getTheItemQuantity() != Integer.parseInt(this.theItemQuantiteTextfield.getText())) {
                try {
                    try {
                        e.setTheItemQuantity(selected.getTheItemQuantity() + Integer.parseInt(this.theItemQuantiteTextfield.getText()));
                    } catch (MinimalStockException ex) {
                        Logger.getLogger(InventoryFormView.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } catch (NegativeStockException ex) {
                    Logger.getLogger(InventoryFormView.class.getName()).log(Level.SEVERE, null, ex);
                } 
            } else {
                try {
                    try {
                        e.setTheItemQuantity(selected.getTheItemQuantity());
                    } catch (MinimalStockException ex) {
                        Logger.getLogger(InventoryFormView.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    e.setStockMin(selected.getStockMin());
                    e.setTheUnitPrice(selected.getTheUnitPrice());
                    e.setTheItemAddedDate(new Date());
                    System.out.println("updating the Item");
                } catch (NegativeStockException ex) {
                    Logger.getLogger(InventoryFormView.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        theItem = e;
        
        return e;
    }
    
    @Override
    public Item getEntity() {
        return theItem;
    }
    
    public List<Item> getEntities() {
        return theItems;
    }
    
    @Override
    public void onHelp() {
    }
    
    @Override
    public String getIconPath() {
        return ICONS16 + "inventory.png";
        
    }
    
    @Override
    public Component asComponent() {
        return null;
    }
    
}
