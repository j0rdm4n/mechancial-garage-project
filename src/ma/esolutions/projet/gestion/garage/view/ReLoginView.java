/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import java.awt.BorderLayout;
import java.awt.Component;
import javax.swing.JPanel;
import ma.esolutions.projet.gestion.garage.controller.common.IPageController;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS16;
import ma.esolutions.projet.gestion.garage.view.common.IPageView;

/**
 *
 * @author E-solutions
 */
public class ReLoginView implements IPageView{
    private IPageController theController;
    private JPanel thePageView;

    @Override
    public void init(IPageController controller) {
        theController = controller;
        initComponents();
    }

    @Override
    public IPageController getController() {
        return theController;
    }

    @Override
    public void showMainView() {
    }

    @Override
    public String getTitle() {
        return "";
    }

    @Override
    public String getIconPath() {
        return ICONS16+"home.png";
    }

    @Override
    public Component asComponent() {
        return thePageView;
    }

     private void initComponents() {
        thePageView = new JPanel(new BorderLayout());
     //   thePageView.add(buildHeaderBar(), BorderLayout.NORTH);
       // thePageView.add(buildCenterPanel(),BorderLayout.CENTER);
    
    }

    @Override
    public void init() {
        init(null);
    }
    
}
