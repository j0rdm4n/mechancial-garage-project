/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.Chart;
import javafx.scene.chart.XYChart;
import ma.esolutions.projet.gestion.garage.controller.MainController;

/**
 *
 * @author Ahmed
 */
public abstract class YearlySalesXYChart extends AbstractSalesXYChart{
    
    private Chart theChart;
    
    private int theEndYear ;
    public YearlySalesXYChart(MainController controller,int end)
    {
        super(controller);
        theEndYear  = end;
        //theSeries = new XYChart.Series();
    }
    @Override
    protected XYChart.Series getData() {
        Map mm = theController.getSalesPerYears();
        int currentYear  = getCurrentYear();
                theSeries = new XYChart.Series();

        System.out.println("yearly series");
        
        mm.forEach((k,v)->{
            System.out.println("mm k" + k + "v "+ v );
        });
        for(int i = 0;i<mm.size();i++)
        {
            System.out.println("series " +mm.get(""+(i+2014)) );
            theSeries.getData().add(new XYChart.Data(""+(i+2014),mm.get(""+(i+2014)) ));
            
        }   
        return theSeries;
    }

    @Override
    protected String getXAxisLabel() {
        return "Années";
    }

    @Override
    protected String getSeriesLabel() {
        return "Ventes par années";
    }

    private int getCurrentYear() {
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.YEAR);
    }
    
}
