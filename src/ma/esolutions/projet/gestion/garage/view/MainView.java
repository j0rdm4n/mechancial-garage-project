/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.embed.swing.JFXPanel;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.effect.Blend;
import javafx.scene.effect.BlendMode;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javax.swing.GroupLayout.Group;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import ma.esolutions.projet.gestion.garage.controller.MainController;
import ma.esolutions.projet.gestion.garage.controller.common.IPageController;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.CHART_STYLE;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS16;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS22;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.IMAGES;
import ma.esolutions.projet.gestion.garage.view.common.IChart;
import ma.esolutions.projet.gestion.garage.view.common.IPageView;
import net.miginfocom.swing.MigLayout;
import org.jvnet.substance.SubstanceLookAndFeel;
import org.jvnet.substance.painter.decoration.DecorationAreaType;

/**
 *
 * @author Ahmed
 */
public class MainView implements IPageView {

    private static MonthlySalesBarChart theMonthlySalesBarChart;
    private static DailySalesBarChart theDailySalesBarChart;
    private static YearlySalesBarChart theYearlySalesBarChart;
    private static QuantityPerItemBarChart theQuantityPerItemBarChart;
    private static DailyPurchasesBarChart theDailyPurchasesBarChart;
    private static DailyPurchasesLineChart theDailyPurchasesLineChart;
    private static SalesPerCustomerBarChart theSalesPerCustomerBarChart;
    private static SalesPerCustomerLineChart theSalesPerCustomerLineChart;
    private static ComboBox filterBox;
    private static QuantityPerItemLineChart theQuantityPerItemLineChart;
    private static ItemPerStockPieChart theItemPerStockPieChart;
    private static DailySalesLineChart theDailySalesLineChart;
    private static MonthlySalesBarChart theMonthlySalesLineChart;
    private static SalesPerProductPieChart theSalesPerProductPieChart;

    private JPanel thePageView;
    static private MainController theController;
    private String theChartTitle;
    private JFXPanel theMainPanel;
    private JPanel theCenterPanel;

    private static GridPane theMiddleGrid;
    static private Map<String, String> state = new HashMap<>();
    static private ObservableMap<String, String> viewState;

    private static final String FILTER_MONTH = "month";

    private static final String FILTER_DAY = "day";
    private static final String FILTER_YEAR = "year";

    private static int theYear;
    private static int theMonth;
    private static int theDay;
    private static IChart theActiveChart;

    public MainView() {
        theYear = getCurrentYear();
        theMonth = getCurrentMonth();
        theDay = getCurrentDay();
    }

    private int getCurrentYear() {
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.YEAR);
    }

    private int getCurrentMonth() {
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.MONTH);
    }

    private int getCurrentDay() {
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    static private void setUpViewState() {
        viewState = FXCollections.observableMap(state);

        viewState.put("year", "" + theController.getCurrentYear());
        String month = theController.getCurrentMonth();
        viewState.put(FILTER_MONTH, month);
        viewState.put("chart", "bar");
        viewState.put("data", "stocks");
        viewState.put("filter", FILTER_MONTH);
        viewState.addListener(new MapChangeListener() {

            @Override
            public void onChanged(MapChangeListener.Change change) {
                clearTheChartMiddleGrid();
                dispatch(viewState.get("chart"), viewState.get("filter"), viewState.get("data"));
                /*    if (change.getKey() == FILTER_MONTH) {
                 monthStateChanged();
                 }
                 if (change.getKey() == "year") {
                 yearStateChanged();
                 }
                 if (change.getKey() == "data") {
                 dataStateChanged();
                 }
                 if (change.getKey() == "filter") {
                 filterStateChanged();

                 }*/
            }

        });

    }

    private static void dataStateChanged() {
        System.out.println("data changed");

    }

    private static void yearStateChanged() {
        theYear = Integer.parseInt(viewState.get("year"));

        System.out.println("year changed to " + theYear);

    }

    private static void monthStateChanged() {
        String[] months = {"Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre"};
        Map<String, Integer> mm = new HashMap();
        int i = 1;
        for (String m : months) {
            mm.put(m, i);
            i++;
        }
        theMonth = mm.get(viewState.get("month"));
        System.out.println("month changed to " + theMonth);

    }

    private static void filterStateChanged() {
        System.out.println("filter changed");
    }

    private static void onPieButtonClick() {

        viewState.put("chart", "pie");
        System.out.println("pie button clicked !");
        clearTheChartMiddleGrid();
        deactivateFilterBox();
        /*  SalesPerProductPieChart m = new SalesPerProductPieChart(theController);

         theMiddleGrid.add(m.getChart(), 1, 3);
         theMiddleGrid.add(new Label(m.getReports()), 3, 3);*/
        ItemPerStockPieChart m = new ItemPerStockPieChart(theController);
        theMiddleGrid.add(m.getChart(), 2, 3);
        //  theMiddleGrid.add(new Label(m.getReports()), 3, 3);
        theActiveChart = m;

    }

    static private void deactivateFilterBox() {
        if (filterBox.isDisabled() == false) {
            filterBox.setDisable(true);
        }
    }

    static private void activateFilterBox() {
        if (filterBox.isDisabled() == true) {
            filterBox.setDisable(false);
        }
    }

    private static void onBarButtonClick() {
        activateFilterBox();
        clearTheChartMiddleGrid();

        if (viewState.get("filter").equalsIgnoreCase(FILTER_DAY)) {
            if (theDailySalesBarChart == null) {
                theDailySalesBarChart = new DailySalesBarChart(theController);
            }
            theMiddleGrid.add(theDailySalesBarChart.getChart(), 1, 3);
            theMiddleGrid.add(new Label(theDailySalesBarChart.getReports()), 3, 3);
            /*  if (theQuantityPerItemBarChart == null) {
             theQuantityPerItemBarChart = new QuantityPerItemBarChart(theController);
             }
             theMiddleGrid.add(theQuantityPerItemBarChart.getChart(), 1, 3);
             theMiddleGrid.add(new Label(theQuantityPerItemBarChart.getReports()), 3, 3);  */
            /*if (theDailyPurchasesBarChart == null) {
             theDailyPurchasesBarChart = new DailyPurchasesBarChart(theController);
             }
             theMiddleGrid.add(theDailyPurchasesBarChart.getChart(), 1, 3);
             theMiddleGrid.add(new Label(theDailyPurchasesBarChart.getReports()), 3, 3);*/
            /*   if (theSalesPerCustomerBarChart == null) {
             theSalesPerCustomerBarChart = new SalesPerCustomerBarChart(theController);
             }
             theSalesPerCustomerBarChart.setTheYear(theYear);
             theSalesPerCustomerBarChart.setTheMonth(theMonth);
             theSalesPerCustomerBarChart.setTheDay(theDay);
             theMiddleGrid.add(theSalesPerCustomerBarChart.getChart(), 1, 3);
             theMiddleGrid.add(new Label(theSalesPerCustomerBarChart.getReports()), 3, 3);
             theActiveChart = theSalesPerCustomerBarChart;*/

        }

        if (viewState.get("filter").equalsIgnoreCase(FILTER_MONTH)) {
            if (theMonthlySalesBarChart == null) {
                theMonthlySalesBarChart = new MonthlySalesBarChart(theController);
            }
            theMiddleGrid.add(theMonthlySalesBarChart.getChart(), 1, 3);
            //  theMiddleGrid.add(new Label(theMonthlySalesBarChart.getReports()), 3, 3);
            /*  if (theMonthlyPurchasesBarChart == null) {
             theMonthlyPurchasesBarChart = new MonthlyPurchasesBarChart(theController);
             }
             theMiddleGrid.add(theMonthlySalesBarChart.getChart(), 1, 3);
             theMiddleGrid.add(new Label(theMonthlySalesBarChart.getReports()), 3, 3);*/
        }

        if (viewState.get("filter").equalsIgnoreCase(FILTER_YEAR)) {

            if (theYearlySalesBarChart == null) {
                theYearlySalesBarChart = new YearlySalesBarChart(theController, 2020);
            }
            theMiddleGrid.add(theYearlySalesBarChart.getChart(), 1, 3);
            theMiddleGrid.add(new Label(theYearlySalesBarChart.getReports()), 3, 3);

        }
        viewState.put("chart", "bar");
    }

    private static void clearTheChartMiddleGrid() {
        List<Node> l;
        l = new ArrayList();
        if (theMiddleGrid != null) {
            if (theMiddleGrid.getChildren().size() != 0) {

                theMiddleGrid.getChildren().forEach(e -> {
                    l.add(e);
                });
            }

            theMiddleGrid.getChildren().removeAll(l);
        }
    }

    static private void dispatch(String chart, String filter, String data) {

        if (data.equalsIgnoreCase("sales")) {
            System.out.println("selecting sales");
            //  dispatchChartForSales(chart, filter);
            // buildSalesCharts();
            buildSalesCharts();
        } else if (data.equalsIgnoreCase("purchases")) {
            System.out.println("selecting purchases");
            // dispatchChartForPurchases(chart, filter);
            //buildPurchasesCharts();
        } else if (data.equalsIgnoreCase("customers")) {
            System.out.println("selecting customers");
            // dispatchChartForCustomers(chart, filter);
        } else if (data.equalsIgnoreCase("stocks")) {
            System.out.println("selecting stocks");

            // dispatchChartForStocks(chart, filter);
        } else if (data.equalsIgnoreCase("caisses")) {
            System.out.println("selecting caisses");
            // dispatchChartForCaisses(chart, filter);
        } else if (data.equalsIgnoreCase("credits")) {
            System.out.println("selecting credits");
            // dispatchChartForCredits(chart, filter);
        } else {
            System.out.println(" invalid choice");
        }

    }

    private static void dispatchChartForSales(String chart, String filter) {
        if (chart.equalsIgnoreCase("pie")) {
            System.out.println("selecting pie");
            clearTheChartMiddleGrid();
            dispatchFilterForSalesPie(filter);

        } else if (chart.equalsIgnoreCase("bar")) {
            System.out.println("selecting bar");
            clearTheChartMiddleGrid();
//            dispatchFilterForSalesBar(filter);

        } else if (chart.equalsIgnoreCase("line")) {
            System.out.println("selecting line");
            clearTheChartMiddleGrid();
            dispatchFilterForSalesLine(filter);

        } else {
            System.out.println("invalid bar choice");
        }
    }

    private static void dispatchFilterForSalesPie(String filter) {
        /*if(filter.equalsIgnoreCase("day"))
         {
            
         }
         if(filter.equalsIgnoreCase("month"))
         {
            
         MonthlySalesLineChart m = new MonthlySalesLineChart(theController);

         theMiddleGrid.add(m.getChart(), 1, 3);
         theMiddleGrid.add(new Label(m.getReports()), 3, 3);
         }*/
    }

    private static void dispatchFilterForSalesBar(String filter) {
        if (filter.equalsIgnoreCase("day")) {
            DailySalesBarChart m = new DailySalesBarChart(theController);
            theMiddleGrid.add(m.getChart(), 1, 3);
            //  theMiddleGrid.add(new Label(m.getReports()), 3, 3);

        }
        if (filter.equalsIgnoreCase("month")) {

            MonthlySalesBarChart m = new MonthlySalesBarChart(theController);

            theMiddleGrid.add(m.getChart(), 1, 3);
            //theMiddleGrid.add(new Label(m.getReports()), 3, 3);
        }
    }

    private static void dispatchFilterForSalesLine(String filter) {

        if (filter.equalsIgnoreCase("day")) {
            DailySalesLineChart m = new DailySalesLineChart(theController);
            theMiddleGrid.add(m.getChart(), 1, 3);
            theMiddleGrid.add(new Label(m.getReports()), 3, 3);

        }
        if (filter.equalsIgnoreCase("month")) {

            MonthlySalesLineChart m = new MonthlySalesLineChart(theController);

            theMiddleGrid.add(m.getChart(), 1, 3);
            theMiddleGrid.add(new Label(m.getReports()), 3, 3);
        }
    }

    private static void dispatchChartForPurchases(String chart, String filter) {
        if (chart.equalsIgnoreCase("pie")) {
            System.out.println("selecting pie");
            clearTheChartMiddleGrid();
            dispatchFilterForPurchasesPie(filter);

        } else if (chart.equalsIgnoreCase("bar")) {
            System.out.println("selecting bar");
            clearTheChartMiddleGrid();
            dispatchFilterForPurchasesBar(filter);

        } else if (chart.equalsIgnoreCase("line")) {
            System.out.println("selecting line");
            clearTheChartMiddleGrid();
            dispatchFilterForPurchasesLine(filter);

        } else {
            System.out.println("invalid bar choice");
        }
    }

    private static void dispatchFilterForPurchasesPie(String filter) {
    }

    private static void dispatchFilterForPurchasesLine(String filter) {
        if (filter.equalsIgnoreCase("day")) {
            DailyPurchasesLineChart m = new DailyPurchasesLineChart(theController);
            theMiddleGrid.add(m.getChart(), 1, 3);
            theMiddleGrid.add(new Label(m.getReports()), 3, 3);

        }
        if (filter.equalsIgnoreCase("month")) {

            /*MonthlyPurchasesLineChart m = new MonthlyPurchasesLineChart(theController);

             theMiddleGrid.add(m.getChart(), 1, 3);
             theMiddleGrid.add(new Label(m.getReports()), 3, 3);*/
        }
    }

    private static void dispatchFilterForPurchasesBar(String filter) {
        if (filter.equalsIgnoreCase("day")) {
            DailyPurchasesBarChart m = new DailyPurchasesBarChart(theController);
            theMiddleGrid.add(m.getChart(), 1, 3);
            theMiddleGrid.add(new Label(m.getReports()), 3, 3);

        }
        if (filter.equalsIgnoreCase("month")) {

            /*MonthlyPurchasesBarChart m = new MonthlyPurchasesBarChart(theController);

             theMiddleGrid.add(m.getChart(), 1, 3);
             theMiddleGrid.add(new Label(m.getReports()), 3, 3);*/
        }
    }

    private static void dispatchChartForCustomers(String chart, String filter) {

        if (chart.equalsIgnoreCase("pie")) {
            System.out.println("selecting pie");
            clearTheChartMiddleGrid();
            dispatchFilterForCustomersPie(filter);

        } else if (chart.equalsIgnoreCase("bar")) {
            System.out.println("selecting bar");
            clearTheChartMiddleGrid();
            dispatchFilterForCustomersBar(filter);

        } else if (chart.equalsIgnoreCase("line")) {
            System.out.println("selecting line");
            clearTheChartMiddleGrid();
            dispatchFilterForCustomersLine(filter);

        } else {
            System.out.println("invalid bar choice");
        }
    }

    static private void dispatchFilterForCustomersPie(String filter) {
        SalesPerProductPieChart m = new SalesPerProductPieChart(theController);
        theMiddleGrid.add(m.getChart(), 1, 3);
        theMiddleGrid.add(new Label(m.getReports()), 3, 3);
    }

    static private void dispatchFilterForCustomersBar(String filter) {
        SalesPerCustomerLineChart m = new SalesPerCustomerLineChart(theController);
        theMiddleGrid.add(m.getChart(), 1, 3);
        theMiddleGrid.add(new Label(m.getReports()), 3, 3);

    }

    static private void dispatchFilterForCustomersLine(String filter) {
        SalesPerCustomerBarChart m = new SalesPerCustomerBarChart(theController);
        theMiddleGrid.add(m.getChart(), 1, 3);
        theMiddleGrid.add(new Label(m.getReports()), 3, 3);

    }

    private static void dispatchChartForStocks(String chart, String filter) {

        if (chart.equalsIgnoreCase("pie")) {
            System.out.println("selecting pie");
            clearTheChartMiddleGrid();
            dispatchFilterForStocksPie(filter);

        } else if (chart.equalsIgnoreCase("bar")) {
            System.out.println("selecting bar");
            clearTheChartMiddleGrid();
            dispatchFilterForStocksBar(filter);

        } else if (chart.equalsIgnoreCase("line")) {
            System.out.println("selecting line");
            clearTheChartMiddleGrid();
            dispatchFilterForStocksLine(filter);

        } else {
            System.out.println("invalid bar choice");
        }

    }

    private static void dispatchFilterForStocksPie(String filter) {
        ItemPerStockPieChart m = new ItemPerStockPieChart(theController);
        theMiddleGrid.add(m.getChart(), 1, 3);
        theMiddleGrid.add(new Label(m.getReports()), 3, 3);
    }

    private static void dispatchFilterForStocksLine(String filter) {
        QuantityPerItemLineChart m = new QuantityPerItemLineChart(theController);
        theMiddleGrid.add(m.getChart(), 1, 3);
        theMiddleGrid.add(new Label(m.getReports()), 3, 3);
    }

    private static void dispatchFilterForStocksBar(String filter) {
        /*   QuantityPerItemBarChart m = new QuantityPerItemBarChart(theController);
         theMiddleGrid.add(m.getChart(), 1, 3);
         theMiddleGrid.add(new Label(m.getReports()), 3, 3);*/
    }

    private static void dispatchChartForCaisses(String chart, String filter) {

    }

    private static void dispatchChartForCredits(String chart, String filter) {

    }
    /* private static void dispatchFilterForCaisses(String chart,String filter) {
     if (filter.equalsIgnoreCase("day")) {
     System.out.println("selecting day filter");
     } else if (filter.equalsIgnoreCase("month")) {
     System.out.println("selecting month filter");
     } else {
     System.out.println("invlaid filter");
     }
     }*/

    private static void onLineButtonClick() {
        viewState.put("chart", "line");
        clearTheChartMiddleGrid();
        /*
         MonthlySalesLineChart m = new MonthlySalesLineChart(theController);

         theMiddleGrid.add(m.getChart(), 1, 3);
         theMiddleGrid.add(new Label(m.getReports()), 3, 3);*/

        /*QuantityPerItemLineChart m = new QuantityPerItemLineChart(theController);

         theMiddleGrid.add(m.getChart(), 1, 3);
         theMiddleGrid.add(new Label(m.getReports()), 3, 3);*/
        /*if (theDailyPurchasesLineChart == null) {
         theDailyPurchasesLineChart = new DailyPurchasesLineChart(theController);
         }
         theMiddleGrid.add(theDailyPurchasesLineChart.getChart(), 1, 3);
         theMiddleGrid.add(new Label(theDailyPurchasesLineChart.getReports()), 3, 3);*/
        /*if (theSalesPerCustomerLineChart == null) {
         theSalesPerCustomerLineChart = new SalesPerCustomerLineChart(theController);
         }
         theSalesPerCustomerLineChart.setTheYear(2014);
         theSalesPerCustomerLineChart.setTheMonth(8);
         theSalesPerCustomerLineChart.setTheDay(22);
         theMiddleGrid.add(theSalesPerCustomerLineChart.getChart(), 1, 3);
         theMiddleGrid.add(new Label(theSalesPerCustomerLineChart.getReports()), 3, 3);
         theActiveChart = theSalesPerCustomerLineChart;*/
    }

    private static void onSalesBtn() {
        viewState.put("data", "sales");

    }

    private static void onPurchasesBtn() {
        viewState.put("data", "purchases");
    }

    private static void onCaisseBtn() {
        viewState.put("data", "caisses");

    }

    private static void onCreditBtn() {
        viewState.put("data", "credits");

    }

    private static void onCustomerBtn() {
        viewState.put("data", "customers");

    }

    private static void onStockBtn() {
        viewState.put("data", "stocks");

    }

    @Override
    public void init(IPageController controller) {
        theController = (MainController) controller;
        initComponents();
        // theController.getSalesPerMonthForYear();
    }

    @Override
    public IPageController getController() {

        return theController;
    }

    @Override
    public String getTitle() {
        return "Home";
    }

    @Override
    public String getIconPath() {
        return ICONS22 + "home-icon22.png";
    }

    @Override
    public Component asComponent() {
        return thePageView;
    }
    private void initComponents() {
        thePageView = new JPanel(new BorderLayout());
        // thePageView.add(buildHeaderBar(), BorderLayout.NORTH);
        thePageView.add(buildCenterPanel(), BorderLayout.CENTER);

    }

    private JPanel buildHeaderBar() {
        JPanel headerBar = new JPanel(new MigLayout("insets 2 2 2 2"));
        JLabel lblTitle = new JLabel(getTitle());
        lblTitle.setIcon(new ImageIcon(getClass().getResource(getIconPath())));
        // lblTitle.setFont(lblTitle.getFont().deriveFont(Font.BOLD, 14));

        headerBar.setPreferredSize(new Dimension(lblTitle.getWidth(), lblTitle.getHeight() + 28));
        headerBar.add(lblTitle, "dock center, gapleft 4");

        SubstanceLookAndFeel.setDecorationType(headerBar, DecorationAreaType.HEADER);

        return headerBar;

    }

    static public VBox addVBoxForLinks() {
        VBox vbox = new VBox();
        vbox.setStyle("-fx-background-color: DAE6F3;");
        vbox.setPadding(new Insets(10));
        vbox.setSpacing(8);

        //Text title = new Text("Données");
        //title.setFont(Font.font("Verdana", FontWeight.BOLD, 30));
        //title.setFill(Color.BLUE);
        //createTextEffects(title);
        //Reflection r = new Reflection();
        // r.setFraction(0.3f);
        //title.setEffect(r);
        //vbox.getChildren().add(title);
        StackPane stack = makeBtn("Ventes");

        Button salesBtn = new Button();
        salesBtn.setPadding(Insets.EMPTY);

        salesBtn.setGraphic(stack);
        salesBtn.setOnAction((e) -> {
            onSalesBtn();
        });
        vbox.getChildren().add(salesBtn);
        StackPane stackPurchases = makeBtn("Achats");

        Button purchasesBtn = new Button();
        purchasesBtn.setPadding(Insets.EMPTY);

        purchasesBtn.setGraphic(stackPurchases);
        purchasesBtn.setOnAction((e) -> {
            onPurchasesBtn();
        });
        vbox.getChildren().add(purchasesBtn);

        StackPane stackRep = makeBtn("Réparations");

        Button repairBtn = new Button();
        repairBtn.setPadding(Insets.EMPTY);

        repairBtn.setGraphic(stackRep);
        repairBtn.setOnAction((e) -> {
            onCaisseBtn();
        });
        // vbox.getChildren().add(repairBtn);

        StackPane stackC = makeBtn("Caisse");

        Button cBtn = new Button();
        cBtn.setPadding(Insets.EMPTY);

        cBtn.setGraphic(stackC);
        cBtn.setOnAction((e) -> {
            onCreditBtn();
        });
        vbox.getChildren().add(cBtn);

        StackPane stackCl = makeBtn("Clients");

        Button clBtn = new Button();
        clBtn.setPadding(Insets.EMPTY);

        clBtn.setGraphic(stackCl);
        clBtn.setOnAction((e) -> {
            onCustomerBtn();
        });
        vbox.getChildren().add(clBtn);

        StackPane stackS = makeBtn("Stock");

        Button sBtn = new Button();
        sBtn.setPadding(Insets.EMPTY);

        sBtn.setGraphic(stackS);
        sBtn.setOnAction((e) -> {
            onStockBtn();
        });
        vbox.getChildren().add(sBtn);

        return vbox;
    }

    private static StackPane makeBtn(String title) {
        StackPane stack = new StackPane();
        Rectangle icon = new Rectangle(120.0, 45.0);

        icon.setFill(new LinearGradient(0, 0, 0, 0.1, true, CycleMethod.NO_CYCLE,
                new Stop[]{
                    new Stop(0, Color.web("#4977A3")),
                    new Stop(0.5, Color.web("#B0C6DA")),
                    new Stop(1, Color.web("#9CB6CF")),}));
        icon.setStroke(Color.web("#D0E6FA"));
        icon.setArcHeight(3.5);
        icon.setArcWidth(3.5);
        Text text = new Text(title);
        createTextEffects(text, Color.web("#0055ff"));
        text.setFont(Font.font("Verdana", FontWeight.BOLD, 16));
        text.setFill(Color.WHITE);
        text.setStroke(Color.web("#7080A0"));
        stack.getChildren().addAll(icon, text);
        StackPane.setMargin(text, new Insets(0, 0, 0, 0));
        StackPane.setMargin(icon, new Insets(0, 0, 0, 0));
        return stack;
    }

    private static void createTextEffects(Text title, Color c) {
        Blend blend = new Blend();
        blend.setMode(BlendMode.MULTIPLY);
        DropShadow ds1 = new DropShadow();

        ds1.setColor(c);
        ds1.setRadius(20);
        ds1.setSpread(0.2);

        InnerShadow is = new InnerShadow();
        is.setOffsetX(8.0f);
        is.setOffsetY(8.0f);
        blend.setBottomInput(is);
        blend.setBottomInput(ds1);
        title.setEffect(blend);
    }

    static public HBox addHBoxForFilters() {
        HBox hbox = new HBox();
        hbox.setPadding(new Insets(15, 12, 15, 12));
        hbox.setSpacing(10);
        hbox.setStyle("-fx-background-color: #336699;");

        setMonthsBtns(hbox);
        setYearsBtns(hbox, 2014, 2021);
        setFilterComboBox(hbox);
        return hbox;
    }

    private static void setFilterComboBox(HBox hbox) {
        VBox vb = new VBox();
        List<String> l = new ArrayList<>();
        l.add("Par jour");
        l.add("Par mois");
        // l.add("Par année");
        ObservableList items = FXCollections.observableList(l);
        filterBox = new ComboBox();
        filterBox.setItems(items);
        filterBox.setValue(l.get(0));
        viewState.put("filter", "day");
        filterBox.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String t, String t1) {
                if (t1.equals(l.get(0))) {
                    viewState.put("filter", "day");
                }
                if (t1.equals(l.get(1))) {
                    viewState.put("filter", FILTER_MONTH);
                }
                /* if (t1.equals(l.get(2))) {
                 viewState.put("filter", "year");
                 }*/
                System.out.println(t1);
            }
        });

        vb.getChildren().addAll(new Text("Choisir un critére "), filterBox);
        hbox.getChildren().add(vb);
    }

    private static void setYearsBtns(HBox hbox, int start, int end) {
        int j = start;
        GridPane grid = new GridPane();
        grid.add(new Label("Années"), 0, 0, 3, 1);

        int row = 1;
        int col = 1;

        for (int i = 1; i <= end - start; i++) {

            Button btn = new Button("" + j++);
            btn.setPrefSize(50, 12);
            String style = "-fx-font:  italic 7pt \"Verdana\";"
                    + "-fx-text-fill: rgb(0, 0, 100);\n"
                    + "-fx-effect: dropshadow( one-pass-box , blue , 8 , 0.0 , 2 , 0 );\n"
                    + "-fx-padding: 0 0 0 0\n"
                    + "-fx-margin : 0 0 0 0\n";
            btn.setStyle(style);
            btn.setOnAction((e) -> {
                viewState.put("year", btn.getText());
            });
            grid.add(btn, col, row);
            row++;
            if (i % 3 == 0) {
                col++;
                row = 1;
            }

        }
        hbox.getChildren().add(grid);
    }

    private static void setMonthsBtns(HBox hbox) {
        String[] months = {"Janvier",
            "Février",
            "Mars",
            "Avril",
            "Mai",
            "Juin",
            "Juillet",
            "Aout",
            "Septembre",
            "Octobre",
            "Novembre",
            "Décembre"};
        GridPane grid = new GridPane();
        grid.add(new Label("Mois"), 0, 0, 3, 1);

        int row = 1, col = 1;
        for (int i = 1; i <= 12; i++) {
            Button btn = new Button(months[i - 1]);
            btn.setPrefSize(50, 12);
            String style = "-fx-font:  italic 7pt \"Verdana\";"
                    + "-fx-text-fill: rgb(0, 0, 100);\n"
                    + "-fx-effect: dropshadow( one-pass-box , blue , 8 , 0.0 , 2 , 0 );\n"
                    + "-fx-padding: 0 0 0 0\n"
                    + "-fx-margin : 0 0 0 0\n";
            btn.setStyle(style);
            btn.setOnAction((e) -> {
                viewState.put(FILTER_MONTH, btn.getText());
            });
            grid.add(btn, col, row);
            row++;
            if (i % 3 == 0) {
                col++;
                row = 1;
            }
        }
        hbox.getChildren().add(grid);
    }

    static public void addStackPane(HBox hb) {
        StackPane stack = new StackPane();
        Rectangle helpIcon = new Rectangle(30.0, 25.0);
        helpIcon.setFill(new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE,
                new Stop[]{
                    new Stop(0, Color.web("#4977A3")),
                    new Stop(0.5, Color.web("#B0C6DA")),
                    new Stop(1, Color.web("#9CB6CF")),}));
        helpIcon.setStroke(Color.web("#D0E6FA"));
        helpIcon.setArcHeight(3.5);
        helpIcon.setArcWidth(3.5);

        Text helpText = new Text("?");
        helpText.setFont(Font.font("Verdana", FontWeight.BOLD, 18));
        helpText.setFill(Color.WHITE);
        helpText.setStroke(Color.web("#7080A0"));

        stack.getChildren().addAll(helpIcon, helpText);
        stack.setAlignment(Pos.CENTER_RIGHT);
        StackPane.setMargin(helpText, new Insets(0, 10, 0, 0));

        hb.getChildren().add(stack);
        HBox.setHgrow(stack, Priority.ALWAYS);
    }

    static public FlowPane addFlowPaneForChartsBtns() {
        FlowPane flow = new FlowPane();
        flow.setPrefWidth(66);
        flow.setPadding(new Insets(5, 0, 5, 0));
        flow.setVgap(10);
        flow.setHgap(4);
        flow.setPrefWrapLength(170);
        flow.setStyle("-fx-background-color: DAE6F3;");

        ImageView pie = new ImageView(new Image(MainView.class.getResourceAsStream(IMAGES + "pie_chart.png")));
        pie.setScaleX(0.9);
        pie.setScaleY(0.9);
        Button pieb = new Button();
        pieb.setGraphic(pie);
        pieb.setOnAction((e) -> {
            onPieButtonClick();
        });
        flow.getChildren().add(pieb);
        ImageView bar = new ImageView(new Image(MainView.class.getResourceAsStream(IMAGES + "bar_chart.png")));
        bar.setScaleX(0.9);
        bar.setScaleY(0.9);
        Button barb = new Button();
        barb.setGraphic(bar);
        barb.setOnAction((e) -> {
            onBarButtonClick();
        });
        flow.getChildren().add(barb);

        ImageView line = new ImageView(new Image(MainView.class.getResourceAsStream(IMAGES + "line_chart.png")));
        line.setScaleX(0.9);
        line.setScaleY(0.9);
        Button lineb = new Button();
        lineb.setGraphic(line);
        lineb.setOnAction((e) -> {
            onLineButtonClick();
        });

        flow.getChildren().add(lineb);

        /*ImageView pages[] = new ImageView[8];
         for (int i=0; i<8; i++) {
         pages[i] = new ImageView(
         new Image(getClass().getResourceAsStream(
         "graphics/chart_"+(i+1)+".png")));
         flow.getChildren().add(pages[i]);
         }*/
        return flow;
    }

    private static GridPane addGridPaneForCharts(String title, String filter, String subtitle, String image) {
        theMiddleGrid = new GridPane();
        theMiddleGrid.setHgap(5);
        theMiddleGrid.setVgap(10);
        theMiddleGrid.setPadding(new Insets(0, 10, 0, 10));
        Text category = new Text(title);
        category.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        //grid.add(category, 1, 0); 
        Text chartTitle = new Text(filter);
        chartTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        // theMiddleGrid.add(chartTitle, 2, 0);

        Text chartSubtile = new Text(subtitle);
        chartSubtile.setFont(Font.font("Verdana", FontWeight.BOLD, 15));

        ImageView lefticon = new ImageView(new Image(MainView.class.getResourceAsStream(image)));
        theMiddleGrid.setVgap(5);
        theMiddleGrid.setHgap(5);

        buildStockCharts();

        // theMiddleGrid.add(m.getChart(), 1, 3);
        // theMiddleGrid.add(new Label(m.getReports()), 3, 3);
        return theMiddleGrid;
    }

    private static void buildStockCharts() {
        // LineChart lineChart =genBarCharSalesPerMonths();
        // MonthlySalesBarChart m = new MonthlySalesBarChart(theController);
        //  MonthlySalesLineChart m = new MonthlySalesLineChart(theController);
        if (viewState.get("data").equalsIgnoreCase("stocks")) {
            if (theQuantityPerItemBarChart == null) {
                theQuantityPerItemBarChart = new QuantityPerItemBarChart(theController);
            }
            theMiddleGrid.add(theQuantityPerItemBarChart.getChart(), 1, 3);
            if (theItemPerStockPieChart == null) {
                theItemPerStockPieChart = new ItemPerStockPieChart(theController);
            }
            theMiddleGrid.add(theItemPerStockPieChart.getChart(), 2, 3);
            /* if (theQuantityPerItemLineChart == null) {
             theQuantityPerItemLineChart = new QuantityPerItemLineChart(theController);
             }
             theMiddleGrid.add(theQuantityPerItemLineChart.getChart(), 1, 4);*/
//             theMiddleGrid.add(theMonthlySalesLineChart.getChart(), 2, 4);
         /*   if(theSalesPerCustomerBarChart == null)
             {
             theSalesPerCustomerBarChart = new SalesPerCustomerBarChart(theController);
             }
             theMiddleGrid.add(theSalesPerCustomerBarChart.getChart(), 1, 4);*/

            if (theSalesPerProductPieChart == null) {
                theSalesPerProductPieChart = new SalesPerProductPieChart(theController);
            }
            theMiddleGrid.add(theSalesPerProductPieChart.getChart(), 1, 4);
            if (theDailySalesLineChart == null) {
                theDailySalesLineChart = new DailySalesLineChart(theController);
            }
            theDailySalesLineChart.setTheMonth(9);

            theMiddleGrid.add(theDailySalesLineChart.getChart(), 2, 4);

        }

        //   SalesPerProductPieChart m = new SalesPerProductPieChart(theController);
    }

    private static void buildSalesCharts() {
        if (viewState.get("data").equalsIgnoreCase("sales")) {
            if (theDailySalesBarChart == null) {
                theDailySalesBarChart = new DailySalesBarChart(theController);
            }
            theMiddleGrid.add(theDailySalesBarChart.getChart(), 1, 3);
            if (theMonthlySalesBarChart == null) {
                theMonthlySalesBarChart = new MonthlySalesBarChart(theController);
            }
            theMiddleGrid.add(theDailySalesBarChart.getChart(), 2, 3);

            /*  if (theDailySalesLineChart == null) {
             theDailySalesLineChart = new DailySalesLineChart(theController);
             }
             theMiddleGrid.add(theDailySalesLineChart.getChart(), 1, 4);*/
            if (theMonthlySalesLineChart == null) {
                theMonthlySalesLineChart = new MonthlySalesBarChart(theController);
            }
            theMiddleGrid.add(theMonthlySalesLineChart.getChart(), 2, 4);
            if (theSalesPerCustomerBarChart == null) {
                theSalesPerCustomerBarChart = new SalesPerCustomerBarChart(theController);
            }
            theMiddleGrid.add(theSalesPerCustomerBarChart.getChart(), 1, 4);
        }

    }

    static private Scene createScene() {

        BorderPane root = new BorderPane();
        root.setStyle(" -fx-font-smoothing-type: lcd;");
        setUpViewState();

   //     HBox hbox = addHBoxForFilters();
        //   root.setTop(hbox);
      //  root.setLeft(addVBoxForLinks());
     //   addStackPane(hbox);

        root.setCenter(addGridPaneForCharts("Ventes : ", "par jour", "tous les produits", IMAGES + "pie_chart.png"));
        // root.setRight(addFlowPaneForChartsBtns());
        
        Scene scene = new Scene(root, Color.ALICEBLUE);
        scene.getStylesheets().add(CHART_STYLE);

        return (scene);
    }

    public void initFX() {
        System.out.println("initfx");
        Scene scene = createScene();
        theMainPanel.setScene(scene);

    }

    public Component buildCenterPanel() {
        System.out.println("building center panel");
        theCenterPanel = new JPanel(new BorderLayout());
        theMainPanel = new JFXPanel();
        //theMainPanel.setPreferredSize(new Dimension(200,200));
        theCenterPanel.add(theMainPanel, BorderLayout.CENTER);
        if (Platform.isFxApplicationThread()) {
            System.out.println("javafx starting");
            initFX();
        } else {
            Platform.runLater(() -> {
                System.out.println("javafx starting");
                initFX();
            });
        }

        return theCenterPanel;
    }

    @Override
    public void showMainView() {

        return;
    }

    @Override
    public void init() {
        init(null);
    }

}
