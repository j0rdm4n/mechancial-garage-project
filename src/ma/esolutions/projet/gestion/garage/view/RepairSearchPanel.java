/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSearchPanel;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author E-solutions
 */
public class RepairSearchPanel extends AbstractSearchPanel{

    private JTextField clientfName;
    private JTextField clientlName;
    @Override
    public JPanel buildSearchForm() {
             JPanel searchPanel = new JPanel(new MigLayout());
        clientfName = new JTextField();
        clientlName = new JTextField();
        searchPanel.add(new JLabel("Prénom du client "), "split ,left ,width 300");
        searchPanel.add(clientfName, "right,width 300,wrap");
        searchPanel.add(new JLabel("Nom du client "), "split ,left,width 300");
        searchPanel.add(clientlName, "right,width 300,wrap");
        return searchPanel;
    }

    @Override
    public String buildSearchFilter() {
        StringBuilder filter = new StringBuilder();
        filter.append("theCustomer");
        filter.append(":like:");
        filter.append(clientfName.getText());
        filter.append(" ");
        filter.append(clientlName.getText());
        filter.append(";");
        return filter.toString();
    }
    
}
