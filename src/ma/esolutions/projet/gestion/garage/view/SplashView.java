/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;

/**
 *
 * @author Ahmed
 */
public class SplashView extends JFrame{
    private final String theTitle;
    private final String theIconPath;
    private final String theImagePath;
    
    public SplashView(String title,String iconPath,String imagePath)
    {
        this.theTitle = title;
        this.theIconPath = iconPath;
        this.theImagePath = imagePath;
        initComponents();
    }
    private void initComponents() {
        System.out.println("before creating database !!!!");
        //createDB("myfirstdb");
        this.setTitle(theTitle);
        this.setIconImage(new ImageIcon(getClass().getResource(theIconPath)).getImage());
        
        this.setAlwaysOnTop(true);
        this.setResizable(false);
        this.setUndecorated(true);
        JLabel splashImage = new JLabel(new ImageIcon(getClass().getResource(theImagePath)));   
        
        JProgressBar progressBar = new JProgressBar(0, 100);
        progressBar.setIndeterminate(true);
        progressBar.setPreferredSize(new Dimension(20, 7));
        progressBar.setBackground(new Color(0, 0, 0));
        progressBar.setForeground(new Color(0, 0, 255));
        
        getContentPane().add(splashImage,BorderLayout.CENTER);
        getContentPane().add(progressBar,BorderLayout.SOUTH);
        pack();
        setLocationRelativeTo(null);

    }
    
    
}
