/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.model.Role;
import ma.esolutions.projet.gestion.garage.model.RoleDAO;
import ma.esolutions.projet.gestion.garage.model.User;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS16;
import ma.esolutions.projet.gestion.garage.view.common.AbstractFormView;
import ma.esolutions.projet.gestion.garage.view.common.JTextFieldExtended;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author E-solutions
 */
public class UserFormView extends AbstractFormView<User> {

    private User theUser;
    private JPanel thePanel;
    private JLabel theUsernameLabel;
    private JTextFieldExtended theUsernameTextfield;
    private JLabel theUserNameLabel;
    private JTextFieldExtended theUserNameTextfield;
    private JLabel theFirstNameLabel;
    private JTextFieldExtended theFirstNameTextfield;
    private JLabel theLastNameLabel;
    private JTextFieldExtended theLastNameTextfield;
    private JLabel thePasswordLabel;
    private JTextFieldExtended thePasswordTextfield;
    private JLabel theEmailLabel;
    private JTextFieldExtended theEmailTextfield;
    private JLabel thePhoneLabel;
    private JTextFieldExtended thePhoneTextfield;
    private JButton theSaveBtn;
    private JButton theCloseBtn;
    private boolean isNewModel;
    private JComboBox theAttributedRolesCombo;
    private JLabel theAttributedRolesLabel;
    private JButton theAssignRoleBtn;
    private ArrayList<Role> theAttributedRoles;
    private List<User> theUsers;
    private List<Role> theExistingRoles;
    private RoleDAO theRoleDAO;

    public UserFormView(User user) {
        super(ApplicationController.getAppView());

        theUser = user;
        theUsers = new ArrayList<User>();
        theRoleDAO = new RoleDAO();
        theExistingRoles = theRoleDAO.all();
        if (user.getId() == null) {
            isNewModel = true;
        } else {
            isNewModel = false;
        }

    }

    @Override
    public void buildUI() {

        initComponents();
        addPageToForm("Informations sur l'utilisateur", buildPage());

        pack();
        setSize(700, 600);
        if (!isNewModel) {
            popFields();
        }
    }

    private JPanel buildPage() {
        thePanel = new JPanel(new MigLayout());
        theUserNameLabel = new JLabel("Nom d'utilisateur");
        theUserNameTextfield = new JTextFieldExtended(90);
        theFirstNameLabel = new JLabel("Prénom");
        theFirstNameTextfield = new JTextFieldExtended(90);
        theLastNameLabel = new JLabel("Nom");
        theLastNameTextfield = new JTextFieldExtended(90);

        thePasswordLabel = new JLabel("Mot de Passe");
        thePasswordTextfield = new JTextFieldExtended(90);

        theUsernameLabel = new JLabel("Username");
        theUsernameTextfield = new JTextFieldExtended(90);
        theEmailLabel = new JLabel("Email");
        theEmailTextfield = new JTextFieldExtended(90);

        thePhoneLabel = new JLabel("Tél");
        thePhoneTextfield = new JTextFieldExtended(90);

        theAttributedRoles = new ArrayList<Role>();
        theAttributedRolesCombo = new JComboBox(new DefaultComboBoxModel(theExistingRoles.toArray()));
        theAttributedRolesLabel = new JLabel("Roles ");
        theAssignRoleBtn = new JButton("Attribuer le Role");

        theAssignRoleBtn.addActionListener((ActionEvent e) -> {
            Role r = null;
            if (theAttributedRolesCombo.getSelectedIndex() != -1) {
                r = (Role) theAttributedRolesCombo.getSelectedItem();

            } else {
                setStatusbarMsg("Veuillez selectionner un role !");
            }
            if (r != null && !theAttributedRoles.contains(r)) {
                theAttributedRoles.add(r);
                List<User> users = r.getTheUsers();
                users.add(theUser);
                r.setTheUsers(users);
                setStatusbarMsg("Le role : " + r + " est ajouté avec succés ! ");
            }
            theAttributedRolesCombo.setSelectedIndex(-1);
        });
        thePanel.add(theFirstNameLabel, "split ,left,width 300,gaptop 60");
        thePanel.add(theFirstNameTextfield, "right,width 300,gaptop 60,wrap");

        thePanel.add(theLastNameLabel, "split , left,width 300");
        thePanel.add(theLastNameTextfield, "right,width 300,wrap");

        thePanel.add(theUsernameLabel, "split,left ,width 300");
        thePanel.add(theUserNameTextfield, "right,width 300,wrap");

        thePanel.add(thePasswordLabel, "split,left,width 300");
        thePanel.add(thePasswordTextfield, "right,width 300,wrap");
        thePanel.add(theEmailLabel, "split,left,width 300");
        thePanel.add(theEmailTextfield, "right,width 300,wrap");

        thePanel.add(thePhoneLabel, "split,left,width 300");
        thePanel.add(thePhoneTextfield, "right,width 300,wrap");
        thePanel.add(theAttributedRolesLabel, "split,left,width 300");
        thePanel.add(theAttributedRolesCombo, "right,width 150");
        thePanel.add(theAssignRoleBtn, "width 140,wrap");

        return thePanel;
    }

    @Override
    public User getEntity() {
        return theUser;
    }

    @Override
    public String getFormIconPath() {
        return ICONS16 + "account.png";
    }

    @Override
    public String getFormTitle() {
        if (isNewModel) {
            return "Créer un utilisateur";
        } else {
            return "Mettre à jour un utilisateur";
        }
    }

    @Override
    public void onHelp() {
    }

    @Override
    public void popFields() {
        this.theFirstNameTextfield.setText(theUser.getTheFirstname());
        this.theLastNameTextfield.setText(theUser.getTheLastname());
        this.theUserNameTextfield.setText(theUser.getTheUsername());
        this.thePasswordTextfield.setText(theUser.getThePassword());
        this.theEmailTextfield.setText(theUser.getTheEmail());
        this.thePhoneTextfield.setText(theUser.getThePhone());

    }

    @Override
    public User pushFields() {
        User user;
        if(isNewModel)
        {
            user = new User();
        }
        else
        {
            user = theUser;
            System.out.println("updating user with id" + user.getId());
        }
        user.setTheFirstname(this.theFirstNameTextfield.getText());
        user.setTheLastname(this.theLastNameTextfield.getText());
        user.setTheUsername(this.theUserNameTextfield.getText());
        System.out.println("the username " + this.theUserNameTextfield.getText());
        user.setTheEmail(this.theEmailTextfield.getText());
        user.setThePassword(this.thePasswordTextfield.getText());

        user.setThePhone(this.thePhoneTextfield.getText());

        user.setTheAttributedRoles((List<Role>) theAttributedRoles);
//        Role r = theRoleDAO.find(((Role)this.theAttributedRolesCombo.getSelectedItem()).getId());

        this.theUser = user;
        return user;
        //theUser.setTheAttributedRoles(null);
    }

    @Override
    public void clearFields() {
        theFirstNameTextfield.setText("");
        theLastNameTextfield.setText("");
        theUserNameTextfield.setText("");
        theEmailTextfield.setText("");
        thePasswordTextfield.setText("");
        thePhoneTextfield.setText("");
        theAttributedRolesCombo.setSelectedIndex(-1);
        this.theAttributedRoles.clear();

    }

    @Override
    public String getIconPath() {
        return ICONS16 + "account.png";
    }

    @Override
    public Component asComponent() {
        return thePanel;
    }

}
