/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import ma.esolutions.projet.gestion.garage.model.Item;
import ma.esolutions.projet.gestion.garage.model.ItemDAO;
import ma.esolutions.projet.gestion.garage.model.ItemPlace;
import ma.esolutions.projet.gestion.garage.model.ItemPlaceDAO;
import ma.esolutions.projet.gestion.garage.model.Vendor;
import ma.esolutions.projet.gestion.garage.model.VendorDAO;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSearchPanel;
import net.miginfocom.swing.MigLayout;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

/**
 *
 * @author E-solutions
 */
public class InventorySearchPanel extends AbstractSearchPanel {
   
    @Override
    public JPanel buildSearchForm() {
        JPanel searchPnl = new JPanel(new MigLayout());
         itemName = new JTextField();
         itemPrice = new JTextField();
        itemQuantity = new JTextField();
        
        itemQuantity.setText("0");
        /* begin date */
        
        UtilDateModel mm = new UtilDateModel();
        // m.setDay(d.getDay());
        // m.setMonth(d.getMonth());
        // m.setYear(d.getYear());
        mm.setValue(new Date());
        mm.setSelected(true);
        itemDate = new JDatePanelImpl(mm);
        datePicker = new JDatePickerImpl(itemDate, new JFormattedTextField.AbstractFormatter() {
            private String theDatePattern = "dd-MM-yyyy";
            private SimpleDateFormat theSimpleDateFormatter = new SimpleDateFormat(theDatePattern);

            @Override
            public Object stringToValue(String text) throws ParseException {
                return theSimpleDateFormatter.parseObject(text);
            }

            @Override
            public String valueToString(Object value) throws ParseException {
                if (value != null) {
                    Calendar cal = (Calendar) value;
                    return theSimpleDateFormatter.format(cal.getTime());

                }

                return "";
            }
        });
        /*end date*/
        ItemPlaceDAO theItemPlaceDAO = new ItemPlaceDAO();
        List<ItemPlace> alli = theItemPlaceDAO.all();
        ItemPlace i = new ItemPlace(); i.setTheItemPlaceName("tous");alli.add(i);
        itemPlace = new JComboBox(new DefaultComboBoxModel(alli.toArray()));
        VendorDAO theVendorDAO = new VendorDAO();
        Vendor v = new Vendor();v.setTheVendorName("tous");
        List<Vendor> allv = theVendorDAO.all();allv.add(v);
         itemVendor = new JComboBox(new DefaultComboBoxModel(allv.toArray()));
        searchPnl.add(new JLabel("Nom d'article: "), "split 4,left,width 150");
        searchPnl.add(itemName, "right,width 300,wrap");
        //searchPnl.add(new JLabel("Prix : "), "left,width 150");
      //  searchPnl.add(itemPrice, "right,width 150,wrap");
        String[] ops = {">", "=", "<"};
        String[] ranges = {"avant","aprés","à"};
        searchPnl.add(new JLabel("Quantité : "), "split 3,left,width 150");
        quantityFilter = new JComboBox(new DefaultComboBoxModel(ops));
        searchPnl.add(quantityFilter,"left,width 150");
        searchPnl.add(itemQuantity, "right,width 300,wrap");
      //  searchPnl.add(new JLabel("Date "),"split 3,left,width 150");
      //  dateFilter = new JComboBox(new DefaultComboBoxModel(ops));
       // searchPnl.add(dateFilter,"left,width 150");
       // searchPnl.add(datePicker,"right,width 150,wrap");
        searchPnl.add(new JLabel("Fournisseur"),"split 4 ,left ,width 150");
        searchPnl.add(itemVendor,"left,width 150");
        searchPnl.add(new JLabel("Emplacement"),"split 4 ,right ,width 150");
        searchPnl.add(itemPlace,"right,width 150,wrap");
        
        

        return searchPnl;

    }
 private JTextField itemName;
    private JTextField itemPrice;
    private JTextField itemQuantity;
    private JDatePanelImpl itemDate;
    private JDatePickerImpl datePicker;
    private JComboBox itemVendor;
    private JComboBox itemPlace;
    private JComboBox quantityFilter;
    private JComboBox dateFilter;
    @Override
    public String buildSearchFilter() {
        StringBuilder filter=new StringBuilder();
        filter.append("theItemName");
        filter.append(":like:");
        filter.append(itemName.getText());
        filter.append(";");
        
     /*   filter.append("price=");
        filter.append(itemPrice.getText());
        filter.append(";");*/
        
        
        filter.append("theItemQuantity:");
        filter.append(quantityFilter.getSelectedItem()+":");
        filter.append(itemQuantity.getText());
        filter.append(";");
        
      //  filter.append("theItemAddedDate:");
//        filter.append(dateFilter.getSelectedItem()+"");
       // filter.append(itemDate.getModel().getYear()+" ");
       // filter.append(itemDate.getModel().getMonth()+" ");
       // filter.append(itemDate.getModel().getDay()+" ");
        
        filter.append(";");
        
        filter.append("theItemPlace");
        filter.append(":=:");
        filter.append(itemPlace.getSelectedItem());
        filter.append(";");
        
        filter.append("theVendor");
        filter.append(":=:");
        filter.append(itemVendor.getSelectedItem());
        filter.append(";");
        
        filter.append("theVendor");
        filter.append(":=:");
        filter.append(itemVendor.getSelectedItem());
        filter.append(";");
        
        return filter.toString();
        
        
        
    }

}
