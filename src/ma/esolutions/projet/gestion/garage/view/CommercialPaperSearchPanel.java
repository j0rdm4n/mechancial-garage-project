/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import ma.esolutions.projet.gestion.garage.model.CommercialPaper;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSearchPanel;
import net.miginfocom.swing.MigLayout;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

/**
 *
 * @author E-solutions
 */
public class CommercialPaperSearchPanel extends AbstractSearchPanel<CommercialPaper> {
    private JDatePanelImpl dueDate;
    private JDatePickerImpl dueDatePicker;
    private JTextField vendorTxt;
    @Override
    public JPanel buildSearchForm() {
        JPanel searchPanel = new JPanel(new MigLayout());
        vendorTxt = new JTextField();
        JLabel vendorLbl = new JLabel("Fournisseur");
        
        searchPanel.add(vendorLbl,"split,left,width 300");
        searchPanel.add(vendorTxt,"right,width 300,wrap");
        
        buildDueDateFields(searchPanel);
        
        return searchPanel;
        
    }

    private void buildDueDateFields(JPanel panel) {

        UtilDateModel mm = new UtilDateModel();

        mm.setValue(new Date());
        mm.setSelected(true);
        JLabel dueLbl = new JLabel("Date de paiement");
        panel.add(dueLbl, "split,left,width 300");

        dueDate = new JDatePanelImpl(mm);
        dueDatePicker = new JDatePickerImpl(dueDate, new JFormattedTextField.AbstractFormatter() {
            private String theDatePattern = "dd-MM-yyyy";
            private SimpleDateFormat theSimpleDateFormatter = new SimpleDateFormat(theDatePattern);

            @Override
            public Object stringToValue(String text) throws ParseException {
                return theSimpleDateFormatter.parseObject(text);
            }

            @Override
            public String valueToString(Object value) throws ParseException {
                if (value != null) {
                    Calendar cal = (Calendar) value;
                    return theSimpleDateFormatter.format(cal.getTime());

                }

                return "";
            }
        });
        panel.add(dueDatePicker, "right,width 300,wrap");
    }
    @Override
    public String buildSearchFilter() {
        StringBuilder filter = new StringBuilder();
        filter.append("vendor");
        filter.append(":like:");
        filter.append(vendorTxt.getText());
        filter.append(";");
        
        filter.append("dueDateDay");
        filter.append(":=:");
        filter.append(dueDatePicker.getModel().getDay());
        filter.append(";");
        
        filter.append("dueDateMonth");
        filter.append(":=:");
        filter.append(dueDatePicker.getModel().getMonth());
        filter.append(";");
        
        filter.append("dueDateYear");
        filter.append(":=:");
        filter.append(dueDatePicker.getModel().getYear());
        filter.append(";");
        
        return filter.toString();
    }
    
}
