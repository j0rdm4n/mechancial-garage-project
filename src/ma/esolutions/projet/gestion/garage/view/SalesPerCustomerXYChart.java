/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javafx.scene.chart.Chart;
import javafx.scene.chart.XYChart;
import ma.esolutions.projet.gestion.garage.controller.MainController;
import ma.esolutions.projet.gestion.garage.view.common.AbstractXYChart;

/**
 *
 * @author E-solutions
 */
abstract public class SalesPerCustomerXYChart extends AbstractXYChart {

    protected int theYear;
    protected int theMonth;
    protected int theDay;
    public SalesPerCustomerXYChart(MainController controller)
    {
        theController = controller;
    }
    @Override
    protected String getXAxisLabel() {
        return "Clients";
    }

    @Override
    protected String getYAxisLabel() {
        return "Ventes";
    }

    public int getTheYear() {
        return theYear;
    }

    public void setTheYear(int theYear) {
        this.theYear = theYear;
    }

    public int getTheMonth() {
        return theMonth;
    }

    public void setTheMonth(int theMonth) {
        this.theMonth = theMonth;
    }

    public int getTheDay() {
        return theDay;
    }

    public void setTheDay(int theDay) {
        this.theDay = theDay;
    }

    @Override
    protected String getSeriesLabel() {
        return "Ventes réalisées par chaque client durant année : " + this.getTheYear() + " mois : " + this.getTheMonth() + " jour : " + this.getTheDay();
    }

    @Override
    protected XYChart.Series getData() {
        Map mm = theController.getSalesPerCustomer(theYear,theMonth,theDay);
        theSeries = new XYChart.Series();

      /*  for (String m : months) {
            theSeries.getData().add(new XYChart.Data(m, mm.get(m)));
        }*/
        Set<String> customers = mm.keySet();
        for(String s : customers)
        {
            theSeries.getData().add(new XYChart.Data(s, mm.get(s) ));
        }
        return theSeries;
    }

    @Override
    public String getTitle() {
        return "Ventes par clients";
    }

    

    private int getCurrentYear() {
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.YEAR);
    }

    private int getCurrentMonth() {
         Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.MONTH);
    }

    private int getCurrentDay() {
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.DAY_OF_MONTH);
    }
 @Override
    public String getReports() {
        return "Le client avec la plus grande quantité de ventes : " + theController.getCustomerWithBestSales() +
                "\n Le client avec la plus petite quantité de ventes " + theController.getCustomerWithWorstSales();
    }
}
