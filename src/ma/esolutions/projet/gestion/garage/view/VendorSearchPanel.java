/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSearchPanel;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author E-solutions
 */
public class VendorSearchPanel extends AbstractSearchPanel{
    private JTextField nameTxt;
    private JTextField emailTxt;
    private JTextField villeTxt;

    @Override
    public JPanel buildSearchForm() {
        JPanel searchPanel = new JPanel(new MigLayout());
         nameTxt = new JTextField();
         emailTxt = new JTextField();
         villeTxt = new JTextField();
        searchPanel.add(new JLabel("Nom de fournisseur"),"split ,left,width 300");
        searchPanel.add(nameTxt,"right,width 300,wrap");
        
        searchPanel.add(new JLabel("Email"),"split,left,width 300");
        searchPanel.add(emailTxt,"right,width 300,wrap");
        
        searchPanel.add(new JLabel("Ville"),"split,left,width 300");
        searchPanel.add(villeTxt,"right,width 300,wrap");
        return searchPanel;
    }

    @Override
    public String buildSearchFilter() {
        StringBuilder filter = new StringBuilder();
        filter.append("theVendorName");
        filter.append(":like:");
        filter.append(nameTxt.getText());
        filter.append(";");
       
        filter.append("theVendorEmail");
        filter.append(":like:");
        filter.append(emailTxt.getText());
        filter.append(";");
       
        filter.append("theVendorCity") ;
        filter.append(":like:");
        filter.append(villeTxt.getText());
        filter.append(";");
        
        return filter.toString();
    }
    
}
