/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import ma.esolutions.projet.gestion.garage.model.ItemPlace;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.STYLES;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSidePanel;

/**
 *
 * @author E-solutions
 */
public class PlaceSidePanel extends AbstractSidePanel<ItemPlace> {

    public PlaceSidePanel() {
        super();
    }

    @Override
    public Node createCenterUI() {
        return new HBox();
    }

    @Override
    public void handleRowSelected() {
        Node center = createUI();

        Platform.runLater(() -> {
            //getRoot().setTop(createDynamicBar());
            if (this.theEntity != null) {
                getRoot().setCenter(center);
            }
        });
    }

    public Node createUI() {
        GridPane grid = new GridPane();
        int row = 1;
        int col = 2;

        if (theEntity != null) {
            grid.setStyle(STYLES);
            grid.setAlignment(Pos.CENTER);
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(25, 25, 25, 25));
            Label placeName = new Label("Nom d'emplacement ");
            grid.add(placeName, 0, row);
            Label placeNamev = new Label(this.theEntity.getTheItemPlaceName());
            grid.add(placeNamev, col, row++);

        }
        return grid;
    }

    @Override
    public void getFieldsValues() {
    }

    @Override
    public String getTitle() {
        return "Détails sur l'emplacement";
    }

}
