/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 *
 * @author E-solutions
 */
class CustomJPanel extends JPanel {
    private String theImage;

    public CustomJPanel(LayoutManager layout, boolean isDoubleBuffered) {
        super(layout, isDoubleBuffered);
    }

    public CustomJPanel(LayoutManager layout,String image) {
        super(layout);
        theImage = image;
    }

    public CustomJPanel(boolean isDoubleBuffered) {
        super(isDoubleBuffered);
    }

    public CustomJPanel() {
    }

    @Override
    protected void paintComponent(Graphics g) {

        ImageIcon imageicon = new ImageIcon(getClass().getResource(ApplicationView.IMAGES + theImage));
        Image image = imageicon.getImage();

        super.paintComponent(g);

        if (image != null) {
            g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
        }
    }
}


