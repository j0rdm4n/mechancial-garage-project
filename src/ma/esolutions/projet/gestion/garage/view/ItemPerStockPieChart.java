/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import java.util.Map;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.PieChart.Data;
import ma.esolutions.projet.gestion.garage.controller.MainController;
import ma.esolutions.projet.gestion.garage.view.common.AbstractPieChart;

/**
 *
 * @author E-solutions
 */
public class ItemPerStockPieChart extends AbstractPieChart{

    public ItemPerStockPieChart(MainController controller) {
        super(controller);
    }

    @Override
    public ObservableList<PieChart.Data> getData() {
          Map<String,Number> m= theController.getStockPercentPerItem();
          ObservableList<PieChart.Data> pieChartData =
                FXCollections.observableArrayList();
          m.forEach((k,v)->{
              Data e = new Data(k,(Double)v);
              pieChartData.add(e);
          });
          return pieChartData;
    
    }

    @Override
    public String getTitle() {
        
        return "Produits par stock";
    }

    @Override
    public String getReports() {
        return "";
    }
    
}
