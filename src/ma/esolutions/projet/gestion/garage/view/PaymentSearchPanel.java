/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSearchPanel;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author E-solutions
 */
public class PaymentSearchPanel extends AbstractSearchPanel {

    private JTextField customerTxt;
    private JComboBox objetComb;

    @Override
    public JPanel buildSearchForm() {
        JPanel searchPanel = new JPanel(new MigLayout());
        customerTxt = new JTextField();
        String[] objets = new String[]{"vente", "achat", "retour", "depense", "alimentation", "tous"};
        DefaultComboBoxModel model = new DefaultComboBoxModel(objets);
        objetComb = new JComboBox(model);

        searchPanel.add(new JLabel("Nom de client"), "split,left,width 300 ");
        searchPanel.add(customerTxt, "right,width 300,wrap");

        searchPanel.add(new JLabel("Objet "), "split,left,width 300");
        searchPanel.add(objetComb, "right,width 300,wrap");

        return searchPanel;
    }

    @Override
    public String buildSearchFilter() {

        StringBuilder filter = new StringBuilder();
        filter.append("theCustomer");
        filter.append(":like:");
        filter.append(customerTxt.getText());
        filter.append(";");

        filter.append("thePaymentType");
        filter.append(":like:");

        filter.append(objetComb.getModel().getSelectedItem());

        filter.append(";");

        return filter.toString();

    }

}
