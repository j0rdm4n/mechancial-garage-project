/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.controller.commands.InventoryAddCommand;
import ma.esolutions.projet.gestion.garage.controller.commands.InventoryListCommand;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.Item;
import ma.esolutions.projet.gestion.garage.model.ItemDAO;
import ma.esolutions.projet.gestion.garage.view.common.AbstractListView;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSidePanel;
import ma.esolutions.projet.gestion.garage.view.common.EntityTableColumn;
import net.miginfocom.swing.MigLayout;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author E-solutions
 */
public class InventoryListView extends AbstractListView<Item> {

    private List<Item> theInventoryData;
    private InventorySidePanel theInventorySidePanel;
    protected InventoryListCommand theCommand;

    /**
     *
     * @param items
     */
    public InventoryListView(List<Item> items) {
        super(null, items);
    }

    public void setTheCommand(InventoryListCommand theCommand) {
        this.theCommand = theCommand;
    }

    @Override
    protected void onRefresh() {
        super.onRefresh();
        if(theInventorySidePanel != null)
        {
           theInventorySidePanel.update();
        }
    }

    @Override
    public String getIconPath() {
        return ApplicationView.ICONS22 + "list-stock-icon.png";
    }

    @Override
    public void addTableColumns() {
        getTableModel().addColumn(new EntityTableColumn(
                "Nom d'Article", "theItemName", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "Prix Unitaire(Achat)", "theUnitPrice", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn("Prix de vente(TTC)", "theSalesPriceTTC", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "%TVA", "theTVA", String.class, 300));

        getTableModel().addColumn(new EntityTableColumn(
                "Quantité", "theItemQuantity", String.class, 300));

        getTableModel().addColumn(new EntityTableColumn(
                "Quantité minimale", "stockMin", String.class, 300));

        getTableModel().addColumn(new EntityTableColumn(
                "Date Ajouté", "theItemAddedDate", String.class, 300));

        getTableModel().addColumn(new EntityTableColumn(
                "Emplacement", "theItemPlace", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn("Fournisseur", "theVendor", String.class, 300));

    }

    protected void onReport() {
        ItemDAO itemDAO = new ItemDAO();
        
        JFrame container = new JFrame();
        JPanel panel = new JPanel(new BorderLayout());
        JPanel catalog = new JPanel(new MigLayout());
        JScrollPane catalogPane = new JScrollPane(catalog);
        catalogPane.setPreferredSize(new Dimension(700,600));
        JButton printPdf = new JButton("Imprimer en format pdf");

        panel.add(new JLabel("Les rapports de stock"), BorderLayout.NORTH);
        panel.add(catalogPane, BorderLayout.CENTER);
        panel.add(printPdf, BorderLayout.SOUTH);

        Map<String, Number> stockPercent = itemDAO.getStockPercentPerItem();
        DefaultPieDataset dataset = new DefaultPieDataset();
        stockPercent.forEach((k, v) -> {
            dataset.setValue(k, v);
        });
        JFreeChart piechart = ChartFactory.createPieChart3D("Articles par stock", dataset);
        
        PiePlot plot = (PiePlot) piechart.getPlot();
        plot.setLabelFont(new Font("SansSerif", Font.PLAIN, 16));
        plot.setNoDataMessage("les données ne sont pas disponible !");
        plot.setCircular(false);
        plot.setLabelGap(0.01);

        
        Map<String,Number> sp = itemDAO.getStockQuantityPerItem();
        DefaultCategoryDataset ds = new DefaultCategoryDataset();
        sp.forEach((k,v)->{  
            ds.setValue(v, "", k);
        });
        JFreeChart barchart = ChartFactory.createBarChart("","","", ds, PlotOrientation.HORIZONTAL, true, true, true);
        catalog.add(new JScrollPane(new ChartPanel(piechart)), "wrap");
        catalog.add(new JScrollPane(new ChartPanel(barchart)), "wrap");
        container.add(new JScrollPane(panel));
        container.setResizable(false);
        container.pack();
        container.setVisible(true);

       // ApplicationView.showAskYesNo("Voulez vous visualisez le rapport d'inventaire en format pdf ? ");
    }

    @Override
    protected void onNew() {
        super.onNew(); //To change body of generated methods, choose Tools | Templates.
        if(theInventorySidePanel != null)
        {
           theInventorySidePanel.update();
        }
    }

    @Override
    public void showEntitySummary() {
    }

    @Override
    public void showEntityActions() {
    }

    @Override
    protected void onStockLimitWarning() {
        this.theSearchFilter = buildStockWarningFilter();
        super.onStockLimitWarning();
        
        
    }
 
    public String buildStockWarningFilter() {
       
       return "limit_warning"; 
        
        
    }

    @Override
    protected boolean hasStockLimitWarning() {
        return true;
    }

    @Override
    public void fireUpdateEvent(MouseEvent e) {

        Item item;
        item = this.getSelectedModel();

        if (item != null) {
            ApplicationController.getAppView().fireInventoryUpdateEvent(item);
        }
    }

    @Override
    public AbstractSidePanel getSidePanel() {
        if (theInventorySidePanel == null) {
            theInventorySidePanel = new InventorySidePanel();
        }
        this.setTheRowObserver(theInventorySidePanel);

        return theInventorySidePanel;
    }

    @Override
    public List<Item> getTheTableData() {
        return theInventoryData;
    }

    @Override
    public String getTitle() {
        return "Gestion de Stock";
    }

    @Override
    public Item getModel() {
        return new Item();
    }

    @Override
    public void delete(Item e) {
        (new ItemDAO()).remove(e);
    }

    @Override
    public SecureCommand<Item> getRelatedFormCommand(Item e) {
        return new InventoryAddCommand(e);
    }

}
