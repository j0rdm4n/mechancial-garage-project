/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import java.util.Map;
import java.util.Set;
import javafx.scene.chart.Chart;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import ma.esolutions.projet.gestion.garage.controller.MainController;

/**
 *
 * @author E-solutions
 */
public class SalesPerCustomerLineChart extends SalesPerCustomerXYChart{

    public SalesPerCustomerLineChart(MainController controller) {
        super(controller);
    }

    @Override
    protected void notifyChart() {
        System.out.println("notifying chart");
    }

  
    @Override
    protected Chart createChart() {

        LineChart lineChart = new LineChart(xAxis, yAxis);
//        theSeries.setName(getSeriesLabel());
        getData();
        lineChart.getData().addAll(theSeries);
        return lineChart;
    }
   @Override
    protected XYChart.Series getData() {
        Map mm = theController.getSalesPerCustomer(theYear,theMonth,theDay);
        theSeries = new XYChart.Series();

      /*  for (String m : months) {
            theSeries.getData().add(new XYChart.Data(m, mm.get(m)));
        }*/
        Set<String> customers = mm.keySet();
        for(String s : customers)
        {
            theSeries.getData().add(new XYChart.Data(s, mm.get(s) ));
        }
        return theSeries;
    }
 
    
    
}
