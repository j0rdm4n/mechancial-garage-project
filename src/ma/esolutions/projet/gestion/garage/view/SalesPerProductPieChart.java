/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import java.util.Map;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.PieChart.Data;
import ma.esolutions.projet.gestion.garage.controller.MainController;
import ma.esolutions.projet.gestion.garage.model.Item;
import ma.esolutions.projet.gestion.garage.view.common.AbstractPieChart;

/**
 *
 * @author E-solutions
 */
public class SalesPerProductPieChart extends AbstractPieChart {

    public SalesPerProductPieChart(MainController controller) {
        super(controller);
    }

    @Override
    public ObservableList<PieChart.Data> getData() {
        Map<String,Number> m= theController.getTotalSalesPerItem();
          ObservableList<PieChart.Data> pieChartData =
                FXCollections.observableArrayList();
          m.forEach((k,v)->{
              Data e = new Data(k,(Double)v);
              pieChartData.add(e);
          });
          return pieChartData;
    }

    @Override
    public String getTitle() {
        return "Total ventes par produit";
    }

    @Override
    public String getReports() {
        Item maxProduct = theController.getMaxSalesProduct();
        double maxProductCA = 0.0;
        double minCA = 0.0;
        Item minProduct = theController.getMinSalesProduct();
        
        return "Produit avec top ventes "+maxProduct + "avec C.A réalisé de "+maxProductCA+" DH"+
               "\n Produit avec minimum des ventes " +minProduct + "avec C.A réalisé de "+minCA +" DH";
    }
    
    
    
}
