package ma.esolutions.projet.gestion.garage.view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu.Separator;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import ma.esolutions.projet.gestion.garage.app.Session;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.model.Customer;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS16;
import ma.esolutions.projet.gestion.garage.view.common.AbstractFormView;
import ma.esolutions.projet.gestion.garage.view.common.JTextFieldExtended;
import net.miginfocom.swing.MigLayout;

public class CustomerFormView extends AbstractFormView<Customer> {

    private Customer theCustomer = null;
    private List<Customer> theCustomers;
    private boolean isNewModel = true;

    
    private JTextFieldExtended theFirstname;
    private JTextFieldExtended theLastname;
    private JCheckBox theActiveStatus;
    
    
    private JTextPane theAddress;
    private JTextFieldExtended theCity;
    private JTextFieldExtended thePostalcode;
    
    
    private JTextFieldExtended thePhone;
    private JTextFieldExtended theMobile;
    private JTextFieldExtended theFax;
    private JTextFieldExtended theEmail;
    private JButton theSaveButton ;
    private JButton theCloseButton;
    private JTextPane theNotes;
    private String theValue;
    
    
    public CustomerFormView(Customer customer) {
        super(ApplicationController.getAppView());
        this.theCustomers = new ArrayList<Customer>();
        this.theCustomer = customer;
        if (customer.getId() != null) {
            isNewModel = false;
        }
        
    }


    @Override
    public void buildUI() {
        initComponents();
        // add pages
        addPageToForm("Informations sur le client",
                buildPage());
      

        popFields();
        pack();
      //  setSize(700, 530);
    }
    @Override
    public boolean isPrintable()
    {
        return false;
    }
    private JPanel buildPage() {
        JPanel panel = new JPanel(new MigLayout("insets 20 10 10 10", "[][50:100,fill][fill,grow]", ""));
        theFirstname = new JTextFieldExtended(30);
        theLastname = new JTextFieldExtended(30);
        theActiveStatus = new JCheckBox("Actif");
        theAddress = new JTextPane();
        theAddress.setPreferredSize(new Dimension(200,120));
        thePhone = new JTextFieldExtended(50);
        theMobile = new JTextFieldExtended(50);
        theFax = new JTextFieldExtended(50);
        theEmail = new JTextFieldExtended(50);
        theCity = new JTextFieldExtended(30);
        thePostalcode = new JTextFieldExtended(30);
        
        
        panel.add(new JLabel("Nom"),"gap para,split 2,width 200");
        panel.add(theFirstname,"width 300,wrap");
          
        panel.add(new JLabel("Prénom"),"gap para,split 2,width 200");
        panel.add(theLastname,"width 300,wrap");

        panel.add(createBoldTitledSeperator(
                "Titre"), "span,growx");

        panel.add(new JLabel("Status "),"gap para,split 2,width 200");
        panel.add(theActiveStatus,"width 300,wrap");
        panel.add(createBoldTitledSeperator("Titre"),"span,growx,wrap");

        
        panel.add(new JLabel("Addresse"),"gap para ,split 2,width 200");
        panel.add(new JScrollPane(theAddress),"width 300 ,wrap");

        panel.add(new JLabel("Ville"),"gap para,split 2,width 200");
        panel.add(theCity,"width 300,wrap");
        panel.add(new JLabel("Code Postal"),"gap para,split 2,width 200");
        panel.add(thePostalcode,"width 300,wrap");
        
        panel.add(createBoldTitledSeperator(
                "Titre"), "span,growx");
        
        panel.add(new JLabel("Télephone"),"gap para ,split 2,width 200");
        panel.add(thePhone,"width 300,wrap");

        panel.add(new JLabel("Mobile"),"gap para ,split 2,width 200");
        panel.add(theMobile, "width 300,wrap");

        panel.add(new JLabel("Fax"), "gap para ,split 2,width 200");
        panel.add(theFax, "width 300,wrap");

        panel.add(new JLabel("Email"), "gap para ,split 2,width 200");
        panel.add(theEmail, "width 300,wrap");

        panel.add(createBoldTitledSeperator("Titre"),"span,growx");
        
   
        return panel;
    }

    private JPanel buildAddressPage() {
        theAddress = new JTextPane();
        theAddress.setPreferredSize(new Dimension(20, 80));
        theAddress.setMargin(new Insets(0, 0, 0, 0));
        theCity = new JTextFieldExtended(50);
        thePostalcode = new JTextFieldExtended(50);
        JPanel panel = new JPanel(new MigLayout("insets 20 10 10 10", "[][fill,grow]"));
        panel.add(new JLabel("Addresse"), "gap para");
        panel.add(new JScrollPane(theAddress), "span");

        panel.add(new JLabel("Ville"), "gap para");
        panel.add(theCity, "span");
        panel.add(new JLabel("code postal"), "gap para");
        panel.add(thePostalcode, "span");


        return panel;
    }

    private JPanel buildCommunicationPage() {
        thePhone = new JTextFieldExtended(50);
        theMobile = new JTextFieldExtended(50);
        theFax = new JTextFieldExtended(50);
        theEmail = new JTextFieldExtended(50);
        JPanel panel = new JPanel(new MigLayout("insets 20 10 10 10", "[][fill,grow]"));
        panel.add(new JLabel("Tél"), "gap para");
        panel.add(thePhone, "span");

        panel.add(new JLabel("Mobile"), "gap para");
        panel.add(theMobile, "span");

        panel.add(new JLabel("Fax"), "gap para");
        panel.add(theFax, "span");

        panel.add(new JLabel("Email"), "gap para");
        panel.add(theEmail, "span");


        return panel;
    }
    private JPanel buildNotesPage() {
        theNotes = new JTextPane();
        theNotes.setPreferredSize(new Dimension(50, 200));
        theNotes.setMargin(new Insets(0, 0, 0, 0));

        JPanel panel = new JPanel(new MigLayout("insets 20 10 10 10", "[][fill,grow]"));

        panel.add(new JLabel("Les Notes"), "gap para");
        panel.add(new JScrollPane(theNotes), "span");

        return panel;
    }

    @Override
    public Customer getEntity() {
        return theCustomer;
    }

    @Override
    public void clearFields() {
        theFirstname.setText("");
        theLastname.setText("");
        theActiveStatus.setSelected(false);
        theAddress.setText("");
        theCity.setText("");
        thePostalcode.setText("");
        thePhone.setText("");
        theMobile.setText("");
        theFax.setText("");
        theEmail.setText("");
       
    }
    @Override
    public void popFields() {
        theFirstname.setText(theCustomer.getTheFirstname());
        theLastname.setText(theCustomer.getTheLastname());
        theActiveStatus.setSelected(theCustomer.getTheActiveStatus());
        theAddress.setText(theCustomer.getAddress());
        theCity.setText(theCustomer.getCity());
        thePostalcode.setText(theCustomer.getPostalcode());
        thePhone.setText(theCustomer.getThePhone());
        theMobile.setText(theCustomer.getTheMobile());
        theFax.setText(theCustomer.getTheFax());
        theEmail.setText(theCustomer.getEmail());
    }

    @Override
    public Customer pushFields() {
     
            
            //Customer newCustomer = new Customer();
        String name = Session.getSession().getTheCurrentUser().getTheFirstname();
        
        if(isNewModel == false)
        {
            theCustomer.setTheFirstname(theFirstname.getText());
            theCustomer.setTheLastname(theLastname.getText());
            theCustomer.setTheActiveStatus(theActiveStatus.isSelected());
     
            theCustomer.setAddress(theAddress.getText());
            theCustomer.setCity(theCity.getText());
      
            theCustomer.setPostalcode(thePostalcode.getText());
   
    
            theCustomer.setThePhone(thePhone.getText());
            theCustomer.setTheMobile(theMobile.getText());
            theCustomer.setTheFax(theFax.getText());
            theCustomer.setEmail(theEmail.getText());
          //  theCustomer.setUpdatedBy(name);
            return theCustomer;
        }
        else
        {
            Customer newCustomer = new Customer();
            //    newCustomer.setCreatedBy(name);
            newCustomer.setTheFirstname(theFirstname.getText());
            newCustomer.setTheLastname(theLastname.getText());
            newCustomer.setTheActiveStatus(theActiveStatus.isSelected());
     
            newCustomer.setAddress(theAddress.getText());
            newCustomer.setCity(theCity.getText());
      
            newCustomer.setPostalcode(thePostalcode.getText());
   
    
            newCustomer.setThePhone(thePhone.getText());
            newCustomer.setTheMobile(theMobile.getText());
            newCustomer.setTheFax(theFax.getText());
            newCustomer.setEmail(theEmail.getText());
           // newCustomer.setUpdatedBy(name);
                theCustomer = newCustomer;
            return newCustomer;
        }
       
       
    }

   
    @Override
    public boolean validateForm() {
 
       

        if (theFirstname.getText().equals("")) {
            //MessageBox.showWarning("Erreur");
            theFirstname.requestFocus();
            return false;
        }

        if (theLastname.getText().equals("")) {
            //MessageBox.showWarning("Erreur");
            theLastname.requestFocus();
            return false;
        }
        if(this.theAddress.getText().equalsIgnoreCase(""))
        {
            theAddress.requestFocus();
            return false;
        }
        if(this.theCity.getText().equalsIgnoreCase(""))
        {
            theCity.requestFocus();
            return false;
        }
        
        if(this.thePostalcode.getText().equalsIgnoreCase(""))
        {
            thePostalcode.requestFocus();
            return false;
        }

        return true;
    }

    @Override
    public String getFormIconPath() {
        return ICONS16 + "customer.png";
    }

    @Override
    public String getFormTitle() {
        if (isNewModel) {
            return "Nouveau Client";
        } else {
            return "Modifier Client";
        }
    }
    
    @Override
    public boolean isMultiPageForm() {
        return true;
    }

    
    @Override
    public void onPrintPreview() {
       
    }

    @Override
    public void onPrint() {
      
    }

    @Override
    public void onHelp() {
       
    }

    private Component createBoldTitledSeperator(String titre) {
        return new Separator();
    }

    @Override
    public String getIconPath() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Component asComponent() {
        return null;
    }

    public String getValue() {
        return theValue;
    }

   

}
