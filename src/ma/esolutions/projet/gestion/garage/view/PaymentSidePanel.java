/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javax.swing.GroupLayout.Group;
import ma.esolutions.projet.gestion.garage.model.Payment;
import ma.esolutions.projet.gestion.garage.model.PaymentDAO;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSidePanel;

/**
 *
 * @author E-solutions
 */
final class PaymentSidePanel extends AbstractSidePanel<Payment> {

    private PaymentDAO paymentDAO;

    @Override
    public Node createCenterUI() {
        paymentDAO = new PaymentDAO();

        Label cashin = new Label("Total vente  " + paymentDAO.getDailyInTotal());
        Label cashout = new Label("Total achat " + paymentDAO.getDailyOutTotal());
        Label cashalim = new Label("Total alimentation " + paymentDAO.getDailyAlimentationTotal());
        Label cashdep = new Label("Total dépenses " + paymentDAO.getDailyDepensesTotal());
        Label total = new Label("Total caisse " + paymentDAO.getDailyTotal());

        CategoryAxis xAxis = new CategoryAxis();
        xAxis.setAutoRanging(true);
        xAxis.setTickLength(1);
        NumberAxis yAxis = new NumberAxis();
        yAxis.setTickLength(1);
        BarChart<String, Number> bc
                = new BarChart<String, Number>(xAxis, yAxis);
       // bc.setTitle("Caisse");
        xAxis.setLabel("Caisse");
        yAxis.setLabel("Valeur");
        bc.setBarGap(3);
        bc.setCategoryGap(20);
        xAxis.setTickLabelRotation(90);
        XYChart.Series series1 = new XYChart.Series();
       // series1.setName("Caisse");
        series1.getData().add(new XYChart.Data("Ventes et retour", Math.abs(paymentDAO.getDailyInTotal())));
        series1.getData().add(new XYChart.Data("Achat", Math.abs(paymentDAO.getDailyOutTotal())));
        series1.getData().add(new XYChart.Data("Alimentation",Math.abs(paymentDAO.getDailyAlimentationTotal())));
        series1.getData().add(new XYChart.Data("Dépenses", Math.abs(paymentDAO.getDailyDepensesTotal())));
        
        bc.getData().add(series1);

        /* ObservableList<PieChart.Data> pieChartData
         = FXCollections.observableArrayList(
         new PieChart.Data("Ventes", Math.abs(paymentDAO.getDailyInTotal()) / paymentDAO.getDailyTotal()),
         new PieChart.Data("Achats", Math.abs(paymentDAO.getDailyOutTotal()) / paymentDAO.getDailyTotal()),
         new PieChart.Data("Alimentation", Math.abs(paymentDAO.getDailyAlimentationTotal()) / paymentDAO.getDailyTotal()),
         new PieChart.Data("Dépenses", Math.abs(paymentDAO.getDailyDepensesTotal()) / paymentDAO.getDailyTotal()));
         final PieChart chart = new PieChart(pieChartData);
         chart.setTitle("Caisse");
         chart.setLabelLineLength(10);
         chart.setLegendSide(Side.LEFT);
        
       
         final Label caption = new Label("");
         caption.setTextFill(Color.DARKORANGE);
         caption.setStyle("-fx-font: 24 arial;");
        

         for (final PieChart.Data data : chart.getData()) {
         data.getNode().addEventHandler(MouseEvent.MOUSE_PRESSED,
         new EventHandler<MouseEvent>() {
         @Override
         public void handle(MouseEvent e) {
         caption.setTranslateX(e.getSceneX());
         caption.setTranslateY(e.getSceneY());
         caption.setText(String.valueOf(data.getPieValue()) + "%");
         }
         });
         }*/
        GridPane grid = new GridPane();
        int row = 2;
        int col = 0;
        grid.add(new Label("Totals journaliers"), col, row);
        row++;
        grid.add(cashin, col, row);
        row++;

        grid.add(cashout, col, row);
        row++;
        grid.add(cashalim, col, row);
        row++;
        grid.add(cashdep, col, row);
        row++;
        grid.add(total, col, row);
        row++;
        grid.add(bc, col, row);
        row++;

        return grid;
    }

    public void update() {
        Node center = createCenterUI();

        Platform.runLater(() -> {
            //getRoot().setTop(createDynamicBar());
            // if (this.theEntity != null) {
            getRoot().setCenter(center);
            //}
        });
    }

    public PaymentSidePanel() {
        Node center = createCenterUI();

        Platform.runLater(() -> {
            //getRoot().setTop(createDynamicBar());
            // if (this.theEntity != null) {
            getRoot().setCenter(center);
            //}
        });
    }

    @Override
    public void handleRowSelected() {
        System.out.println("row selected ");
        Node center = createCenterUI();

        Platform.runLater(() -> {
            //getRoot().setTop(createDynamicBar());
            if (this.theEntity != null) {
                getRoot().setCenter(center);
            }
        });
    }

    @Override
    public void getFieldsValues() {
    }

    @Override
    public String getTitle() {
        return "Détails paiement";
    }

}
