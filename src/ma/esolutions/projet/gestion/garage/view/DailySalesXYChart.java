/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import java.util.Calendar;
import java.util.Map;
import javafx.scene.chart.Chart;
import javafx.scene.chart.XYChart;
import ma.esolutions.projet.gestion.garage.controller.MainController;

/**
 *
 * @author Ahmed
 */
public abstract class DailySalesXYChart extends AbstractSalesXYChart{

    private int theMonth;
    public DailySalesXYChart(MainController controller)
    {
        super(controller);
        theMonth = getCurrentMonth();
    }
    private int getCurrentMonth()
    {
        return Calendar.getInstance().get(Calendar.MONTH);
    }
    public void setTheMonth(int m)
    {
        theMonth = m;
        notifyChart();
    }
    @Override
    protected XYChart.Series getData() {
        theSeries = new XYChart.Series();

        Map mm = theController.getSalesPerDayForMonth(theMonth);
         
        for(int i = 0;i<mm.size();i++)
        {
            theSeries.getData().add(new XYChart.Data(""+(i+1),mm.get(""+(i+1)) ));
        }
        
        return theSeries;
        
    }
    
    

    @Override
    protected String getXAxisLabel() {
        return "Jours";
    }

    @Override
    protected String getSeriesLabel() {
        return "Ventes par jour";
    }
    
}
