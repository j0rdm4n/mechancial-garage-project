/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu.Separator;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.model.Customer;
import ma.esolutions.projet.gestion.garage.model.CustomerDAO;
import ma.esolutions.projet.gestion.garage.model.Vehicle;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS16;
import ma.esolutions.projet.gestion.garage.view.common.AbstractFormView;
import ma.esolutions.projet.gestion.garage.view.common.JTextFieldExtended;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author E-solutions
 */
public class VehicleFormView extends AbstractFormView<Vehicle> {

    private Vehicle theVehicle;
    private final boolean isNewModel;
    private JTextFieldExtended theVinTextfield;
    private JComboBox theCustomerComboBox;
    private List<Customer> theCustomers;
    private CustomerDAO theCustomerDAO;
    private JLabel theVinLabel;
    private JLabel theCustomersLabel;
    private JLabel theInfoLabel;

    public VehicleFormView(Vehicle vehicle) {
        super(ApplicationController.getAppView());
        if (vehicle.getId() == null) {
            isNewModel = true;
        } else {
            isNewModel = false;
        }
        theCustomerDAO = new CustomerDAO();
        theVehicle = vehicle;
    }

    @Override
    public void buildUI() {
        initComponents();
        addPageToForm("Information sur le véhicule", buildPage());
        pack();
        if(!isNewModel)
        {
            popFields();
        }
        setSize(700, 500);

    }

    private JPanel buildPage() {
        JPanel panel = new JPanel(new MigLayout());
        theVinLabel = new JLabel("Numéro du chassis");
        theVinTextfield = new JTextFieldExtended(90);
        theCustomers = theCustomerDAO.all();
        theCustomersLabel = new JLabel("Client");
        theCustomerComboBox = new JComboBox(new DefaultComboBoxModel(theCustomers.toArray()));
        theInfoLabel = new JLabel("Ajouter des véhicules ,un client peut avoir "
                + "plusieurs véhicules");
        theInfoLabel.setForeground(Color.blue);
        panel.add(new Separator(), "span,growx");
        panel.add(theInfoLabel, "gaptop 60,width 600,wrap");
        panel.add(new Separator(), "span,growx");
        panel.add(theVinLabel, "gaptop 30,split ,left, width 300");
        panel.add(theVinTextfield, "gaptop 30,right,width 300,wrap");
        panel.add(theCustomersLabel, "split,gaptop 40,left,width 300");
        panel.add(theCustomerComboBox, "right,width 300,wrap");
        panel.add(new Separator(), "span,growx");

        return panel;
    }

    @Override
    public Vehicle getEntity() {
        return theVehicle;
    }

    @Override
    public String getFormIconPath() {
        return ICONS16 + "vehicle.png";
    }

    @Override
    public String getFormTitle() {
        if (isNewModel) {
            return "Ajouter un véhicule";
        } else {
            return "Mettre à jour un véhicule";
        }
    }

    @Override
    public void onHelp() {
    }

    @Override
    public void popFields() {
        theVinTextfield.setText(theVehicle.getTheVehicleIdentificationNumber());
    }

    @Override
    public void clearFields()
    {
        theVinTextfield.setText("");
        theCustomerComboBox.setSelectedIndex(-1);
    }
    @Override
    public Vehicle pushFields() {
        Vehicle vehicle = new Vehicle();
        vehicle.setTheVehicleIdentificationNumber(theVinTextfield.getText());
        if (theCustomerComboBox.getSelectedIndex() != - 1) {
            vehicle.setTheVehicleOwner((Customer) theCustomerComboBox.getSelectedItem());
        }
        this.theVehicle = vehicle;
        return vehicle;
    }

    @Override
    public String getIconPath() {
        return ICONS16 + "vehicle.png";
    }

    @Override
    public Component asComponent() {
        return null;
    }

}
