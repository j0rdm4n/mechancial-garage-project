/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu.Separator;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.model.Vendor;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS16;
import ma.esolutions.projet.gestion.garage.view.common.AbstractFormView;
import ma.esolutions.projet.gestion.garage.view.common.JTextFieldExtended;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author E-solutions
 */
public class VendorFormView extends AbstractFormView<Vendor> {

    private Vendor theVendor;
    private final boolean isNewModel;
    private JPanel thePanel;
    private JTextFieldExtended theName;
    private JTextPane theAddress;
    private JTextFieldExtended thePhone;
    private JTextFieldExtended theEmail;
    private JTextFieldExtended theSinceDate;
    private JLabel theNameLabel;
    private JLabel thePhoneLabel;
    private JLabel theEmailLabel;
    private JLabel theAddrLabel;
    private JLabel theSinceLabel;
    private JLabel theCityLabel;
    private JTextFieldExtended theCity;
    private JTextFieldExtended thePostalcode;
    
    private JLabel thePostalCodeLabel;
    private List<Vendor> theVendors;
    private Date theDate;

    public VendorFormView(Vendor vendor) {
        super(ApplicationController.getAppView());
        theVendor = vendor;
        theVendors = new ArrayList<Vendor>();
        if (vendor.getId() == null) {
            isNewModel = true;
        } else {
            isNewModel = false;
        }
    }

    @Override
    public void buildUI() {
        initComponents();
        addPageToForm("Informations sur le client",
                buildPage());
        popFields();

        pack();

    }

    JPanel buildPage() {
        thePanel = new JPanel(new MigLayout());
        theNameLabel = new JLabel("Nom");
        theName = new JTextFieldExtended(30);

        thePhoneLabel = new JLabel("Tél");
        thePhone = new JTextFieldExtended(30);
        theEmailLabel = new JLabel("Email");
        theEmail = new JTextFieldExtended(30);

        theAddrLabel = new JLabel("Addresse");
        theAddress = new JTextPane();
        theAddress.setPreferredSize(new Dimension(200, 120));

        theSinceLabel = new JLabel("Date d'ajout");
        theSinceDate = new JTextFieldExtended(30);

        theCityLabel = new JLabel("Ville");
        theCity = new JTextFieldExtended(30);
        thePostalCodeLabel = new JLabel("Code Postal");
        thePostalcode = new JTextFieldExtended(30);
        thePanel.add(theNameLabel, "split,left,width 300,gaptop 30");
        thePanel.add(theName, "right,width 300,gaptop 30,wrap");
        thePanel.add(thePhoneLabel, "split,left,width 300");
        thePanel.add(thePhone, "right,width 300,wrap");
        thePanel.add(theEmailLabel, "split,left,width 300");
        thePanel.add(theEmail, "right,width 300,wrap");
        thePanel.add(new Separator(),"span,growx");
        thePanel.add(theAddrLabel, "split,left,width 300");
        thePanel.add(new JScrollPane(theAddress), "right,width 300,wrap");
        thePanel.add(new Separator(),"span,growx");
        thePanel.add(thePostalCodeLabel, "split,left,width 300");
        thePanel.add(thePostalcode, "right,width 300,wrap");
        thePanel.add(theCityLabel, "split,left,width 300");
        thePanel.add(theCity, "right,width 300,wrap");
        thePanel.add(theSinceLabel, "split,left,width 300");
        thePanel.add(theSinceDate, "right,width 300,wrap ");

        return thePanel;
    }

    @Override
    public Vendor getEntity() {
        return theVendor;
    }

   

    @Override
    public String getFormIconPath() {
        return ICONS16 + "vendor.png";
    }

    @Override
    public String getFormTitle() {
        return "Ajouter un Fournisseur";
    }

    @Override
    public void onHelp() {

    }

    @Override
    public void popFields() {
        theDate = new Date();
        this.theSinceDate.setText(theDate.toString());
        this.theSinceDate.setEnabled(false);
        if (isNewModel) {
            return;
        }
        this.theName.setText(theVendor.getTheVendorName());
        this.theEmail.setText(theVendor.getTheVendorPhone());
        this.thePhone.setText(theVendor.getTheVendorPhone());
        this.theAddress.setText(theVendor.getTheVendorAddress());
        this.theCity.setText(theVendor.getTheVendorCity());
        this.theSinceDate.setText(this.theDate.toString());
        this.thePostalcode.setText("" + theVendor.getTheVendorPostalCode());
    }
    
    
    public void clearFileds()
    {
        
        this.theName.setText("");
        this.theEmail.setText("");
        this.thePhone.setText("");
        this.theAddress.setText("");
        this.theCity.setText("");
        //this.theSinceDate.setText("");
        this.thePostalcode.setText("");
    }
    @Override
    public Vendor pushFields() {
        Vendor v;
        if (isNewModel) {
            v = new Vendor();
        } else {
            v = theVendor;
        }
        v.setTheVendorName(this.theName.getText());
        v.setTheVendorPhone(this.thePhone.getText());
        v.setTheVendorEmail(this.theEmail.getText());
        v.setTheVendorAddress(this.theAddress.getText());
        try
        {
            v.setTheVendorPostalCode(Long.parseLong(this.thePostalcode.getText()));
        
        }catch(NumberFormatException e)
        {
            v.setTheVendorPostalCode((long)0x0);
        
        }
        v.setTheVendorCity(this.theCity.getText());

        v.setTheVendorSinceDate(theDate);
        this.theVendor = v;
        return v;

    }

    @Override
    public boolean validateForm() {
        if(this.theName.getText().equalsIgnoreCase(""))
        {
            return false;
        }
        if(this.theAddress.getText().equalsIgnoreCase(""))
        {
            return false;
        }
        if(this.theCity.getText().equalsIgnoreCase(""))
        {
            return false;
        }
        if(this.thePostalcode.getText().equalsIgnoreCase(""))
        {
            return false;
        }
        
        return true;
    }

    @Override
    public String getIconPath() {
        return ICONS16 + "vendor.png";
    }

    @Override
    public Component asComponent() {
        return null;
    }

  
    private void onCloseForm() {
        setVisible(false);
        dispose();
    }

    @Override
    public void clearFields() {
        this.theName.setText("");
        this.thePhone.setText("");
        this.theEmail.setText("");
        this.theAddress.setText("");
        this.thePostalcode.setText("");
        this.theCity.setText("");
        theDate = new Date();
        this.theSinceDate.setText(theDate.toString());
    }

}
