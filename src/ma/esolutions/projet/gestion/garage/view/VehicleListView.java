/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.controller.commands.VehicleAddCommand;
import ma.esolutions.projet.gestion.garage.controller.common.IDataPageController;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.Vehicle;
import ma.esolutions.projet.gestion.garage.model.VehicleDAO;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS22;
import ma.esolutions.projet.gestion.garage.view.common.AbstractListView;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSearchPanel;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSidePanel;
import ma.esolutions.projet.gestion.garage.view.common.EntityTableColumn;

/**
 *
 * @author E-solutions
 */
public class VehicleListView  extends AbstractListView<Vehicle>{
    private VehicleSidePanel theVehicleSidePanel;

    public VehicleListView(List<Vehicle> data) {
        super(null, data);
    }

    public VehicleListView() {
        super(null,new ArrayList());
    }
    @Override
    public AbstractSearchPanel getSearchPanel()
    {
        if(theSearchPanel == null)
        {
            theSearchPanel = new VehicleSearchPanel();
            theSearchPanel.setParentListView(this);
        }
        return theSearchPanel;
    }
    @Override
    public void addTableColumns() {
        
         getTableModel().addColumn(new EntityTableColumn(
                "Numéro de chassis", "theVehicleIdentificationNumber", String.class, 300));
         getTableModel().addColumn(new EntityTableColumn(
                "Client", "theVehicleOwner", String.class, 300));
    
    }

    @Override
    public void showEntitySummary() {
    }

    @Override
    public void showEntityActions() {
    }

    @Override
    public void fireUpdateEvent(MouseEvent e) {
          Vehicle v = getSelectedModel();
        if (v != null) {
            ApplicationController.getAppView().fireVehicleUpdateEvent(v);
        }
    }

    @Override
    public AbstractSidePanel getSidePanel() {
        if(theVehicleSidePanel == null)
            theVehicleSidePanel = new VehicleSidePanel();
        this.setTheRowObserver(theVehicleSidePanel);
        return theVehicleSidePanel;
    }

    @Override
    public List<Vehicle> getTheTableData() {
        return null;
    }

    @Override
    public String getTitle() {
        return "Gestion des véhicules";
    }

    @Override
    public String getIconPath() {
        return ICONS22+ "list-vehicle-icon.png";
    }

    @Override
    public Vehicle getModel() {
        return new Vehicle();
    }

    @Override
    public void delete(Vehicle e) {
        (new VehicleDAO()).remove(e);
    }

    @Override
    public SecureCommand<Vehicle> getRelatedFormCommand(Vehicle e) {
        return new VehicleAddCommand(e);
    }
    
}
