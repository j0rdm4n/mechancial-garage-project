/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view.common;

/**
 *
 * @author born2code
 */
public interface IDataObserver<T> {
    void added(T e);
}
