/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view.common;

import com.itextpdf.awt.PdfGraphics2D;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import ma.esolutions.projet.gestion.garage.controller.common.IDataPageController;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.Customer;
import ma.esolutions.projet.gestion.garage.model.SalesOrderLine;
import ma.esolutions.projet.gestion.garage.view.ApplicationView;
import ma.esolutions.projet.gestion.garage.view.InventorySearchPanel;
import ma.esolutions.projet.gestion.garage.view.SalesOrderFormView;
import net.miginfocom.swing.MigLayout;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.JXTableHeader;
import org.jdesktop.swingx.decorator.HighlighterFactory;
import org.jfree.chart.JFreeChart;
import org.jvnet.substance.SubstanceLookAndFeel;
import org.jvnet.substance.painter.decoration.DecorationAreaType;

/**
 *
 * @author Ahmed
 * @param <T>
 */
public abstract class AbstractListView<T> implements IListView<T>, IDataObserver<T> {

    protected IDataPageController<T> theController;
    protected SecureCommand<T> theInvokingCommand;
    protected JComponent dpvComponent = null;
    protected JSplitPane theHZSplitePane;
    protected JSearchField theSearchField;
    protected  AbstractSidePanel theSidePanel;
    //private JPanel theSidePanel;
    //les actions 
    private Action theAddNewAction;
    private Action theEditAction;
    private Action theDeleteAction;
    private Action theRefreshAction;
    private Action theSearchAction;

    protected JXTable theTable;
    protected JXTableHeader theTableHeader;
    protected EntityTableModel theEntityTableModel;
    protected List<T> theTableData;

    private Action theFirstPageAction;
    private Action theLastPageAction;
    private Action thePreviousPageAction;
    private Action theNextPageAction;

    private JLabel theCurrentPageLabel;
    private JLabel theFromLabel;
    private JLabel thePageCountLabel;
    private JLabel theRecordsFoundLabel;
    private int theCurrentPage = 1;
    private int theRowCount = 0;
    private int thePageCount = 0;
    private final int thePageSize = 3;

    protected String theSearchFilter = "";
    protected IRowObserver theRowObserver;
    protected IFormView theFormView;
    protected SecureCommand<T> theCommand;
    protected AbstractSearchPanel theSearchPanel;
    private JSplitPane theVSplitePane;
    private JToolBar thePagerToolbar;
    private JToolBar theInfoToolbar;
    private JToolBar tbPager;
    private AbstractAction theXlsExportAction;
    protected SecureCommand<T> theRelatedFormCommand;
    private AbstractAction theCloseAction;
    private AbstractAction theReportAction;
    private AbstractAction theCloseRepairAction;
    private AbstractAction theDeactivateCustomerAction;
    private AbstractAction theInvoiceAction;
    private AbstractAction theBillingAction;
    private AbstractAction theInvoicePrintAction;
    private AbstractAction theCashAction;

    private static com.itextpdf.text.Font catFont = new com.itextpdf.text.Font(com.itextpdf.text.Font.FontFamily.TIMES_ROMAN, 18,
            com.itextpdf.text.Font.BOLD);
    private static com.itextpdf.text.Font redFont = new com.itextpdf.text.Font(com.itextpdf.text.Font.FontFamily.TIMES_ROMAN, 12,
            com.itextpdf.text.Font.NORMAL, BaseColor.RED);
    private static com.itextpdf.text.Font subFont = new com.itextpdf.text.Font(com.itextpdf.text.Font.FontFamily.TIMES_ROMAN, 16,
            com.itextpdf.text.Font.BOLD);
    private static com.itextpdf.text.Font smallBold = new com.itextpdf.text.Font(com.itextpdf.text.Font.FontFamily.TIMES_ROMAN, 12,
            com.itextpdf.text.Font.BOLD);
    private static String PDF = "D:\\ProjetsJava\\gestgarage\\src\\ma\\esolutions\\projet\\gestion\\garage\\app\\resources\\";
    private AbstractAction theReturnAction;

    protected JFreeChart lineChart;
    protected JFreeChart barChart;
    protected JFreeChart pieChart;
    private AbstractAction theValidateReceptionAction;
    private AbstractAction theRverseTaxAction;
    private AbstractAction theWarningAction;

    enum Ops {

        add,
        edit,
        delete,
        other
    }
    private Ops lastOp = Ops.other;
    private String excelFile = "sample.xsl";

    //si la page a un deuxieme table pour lister les donnees
    @Override
    public boolean hasDetails() {
        return false;
    }

    //pour obtenir le model
    abstract public T getModel();

    //pour supprimer
    abstract public void delete(T e);

    abstract public SecureCommand<T> getRelatedFormCommand(T e);

    private void setRelatedFormCommand() {
        T e = getModel();
        if (lastOp == Ops.edit) {
            e = getSelectedModel();
        }
        /*  if(lastOp == Ops.add)
         e = getModel();*/
        theRelatedFormCommand = getRelatedFormCommand(e);
    }

    public void setTheInvokingCommand(SecureCommand<T> theInvokingCommand) {
        this.theInvokingCommand = theInvokingCommand;
    }

    public void setTheCommand(SecureCommand<T> theCommand) {
        System.out.println("setting the command " + theCommand);
        this.theCommand = theCommand;
    }

    public void setTheFormView(IFormView view) {
        theFormView = view;
    }

    public IFormView getTheFormView() {
        return theFormView;
    }

    public void setTheRowObserver(IRowObserver theRowObserver) {
        this.theRowObserver = theRowObserver;
    }

    public AbstractListView(IDataPageController<T> controller, List<T> data) {
        //  this.theTableData = data;
        lineChart = null;
        barChart = null;
        pieChart = null;

        init(controller);
        // this.setTheRowObserver(theRowObserver);

    }

    @Override
    public void added(T e) {
    }

    @Override
    public void init() {
        init(null);
    }

    @Override
    public void init(IDataPageController<T> controller) {
        this.theController = controller;
        setRelatedFormCommand();
        initComponents();
    }

    public abstract void addTableColumns();

    @Override
    public IDataPageController<T> getController() {
        return theController;
    }

    @Override
    public T getSelectedModel() {
        if (theTable.getSelectedRow() > -1) {
            return (T) theEntityTableModel.getRowAt(theTable.convertRowIndexToModel(
                    theTable.getSelectedRow()));
        } else {
            return null;
        }

    }

    @Override
    public void refreshData(List<T> data) {

        theEntityTableModel.setData(data);
        theTable.repaint();

    }

    @Override
    public Component asComponent() {
        return dpvComponent;
    }

    private void initComponents() {

        JPanel panel = new JPanel(new BorderLayout());
        //panel.add(buildHeaderBar(), BorderLayout.NORTH);
        panel.add(buildCenterPanel(), BorderLayout.CENTER);
        panel.add(buildHeaderBar(), BorderLayout.SOUTH);
        this.dpvComponent = panel;

        setCurrentPage(1);
    }

    private JPanel buildHeaderBar() {
        JPanel headerBar = new JPanel(new MigLayout("insets 2 2 2 2"));
        JLabel lblTitle = new JLabel(getTitle());
        // lblTitle.setIcon(new ImageIcon(getClass().getResource(getIconPath())));
        lblTitle.setFont(lblTitle.getFont().deriveFont(Font.BOLD, 14));

        // toolbar
        JToolBar tbHeader = new JToolBar();
        tbHeader.setFloatable(false);
        tbHeader.setRollover(true);
        tbHeader.setFocusable(false);
        //buildHeaderBarActions();
        theRefreshAction = new AbstractAction("", new ImageIcon(getClass().getResource(ApplicationView.ICONS16 + "refresh.png"))) {

            @Override
            public void actionPerformed(ActionEvent e) {
                onRefresh();
            }

        };
        theCloseAction = new AbstractAction("", new ImageIcon(getClass().getResource(ApplicationView.ICONS16 + "close.png"))) {

            @Override
            public void actionPerformed(ActionEvent e) {
                onClose();
            }
        };
        theEditAction = new AbstractAction("", new ImageIcon(getClass().getResource(ApplicationView.ICONS16 + "edit.png"))) {

            @Override
            public void actionPerformed(ActionEvent e) {
                onEdit();
            }

        };
        theDeleteAction = new AbstractAction("", new ImageIcon(getClass().getResource(ApplicationView.ICONS16 + "delete.png"))) {

            @Override
            public void actionPerformed(ActionEvent e) {
                onDelete();
            }

        };
        theAddNewAction = new AbstractAction("", new ImageIcon(getClass().getResource(ApplicationView.ICONS16 + "add.png"))) {

            @Override
            public void actionPerformed(ActionEvent e) {
                onNew();
            }

        };
        if (hasReport()) {
            theReportAction = new AbstractAction("", new ImageIcon(getClass().getResource(ApplicationView.ICONS16 + "report2.png"))) {

                @Override
                public void actionPerformed(ActionEvent e) {

                    onReport();
                }
            };

        }
        if (hasCloseRepair()) {
            theCloseRepairAction = new AbstractAction("", new ImageIcon(getClass().getResource(ApplicationView.ICONS16 + "repair_close.png"))) {

                @Override
                public void actionPerformed(ActionEvent e) {
                    onCloseRepair();
                }

            };
        }
        if(hasStockLimitWarning())
        {
            theWarningAction = new AbstractAction("",new ImageIcon(getClass().getResource(ApplicationView.ICONS16+"stock_warning.png"))) {

                @Override
                public void actionPerformed(ActionEvent e) {
                        onStockLimitWarning();
                }
            };
        }
        if (hasInvoice()) {
            theInvoiceAction = new AbstractAction("", new ImageIcon(getClass().getResource(ApplicationView.ICONS16 + "invoice.png"))) {

                @Override
                public void actionPerformed(ActionEvent e) {
                    onInvoice();
                }

            };
        }
        if (hasInvoicePrint()) {
            theInvoicePrintAction = new AbstractAction("", new ImageIcon(getClass().getResource(ApplicationView.ICONS16 + "print_invoice.png"))) {

                @Override
                public void actionPerformed(ActionEvent e) {
                    onPrintInvoice();
                }

            };
        }
        if (hasCash()) {
            theCashAction = new AbstractAction("", new ImageIcon(getClass().getResource(ApplicationView.ICONS16 + "cash.png"))) {

                @Override
                public void actionPerformed(ActionEvent e) {
                    onPayment();
                }

            };
        }

        if (hasBilling()) {
            theBillingAction = new AbstractAction("", new ImageIcon(getClass().getResource(ApplicationView.ICONS16 + "bill.png"))) {

                @Override
                public void actionPerformed(ActionEvent e) {
                    onBilling();
                }

            };
        }
        if (hasReturn()) {
            theReturnAction = new AbstractAction("", new ImageIcon(getClass().getResource(ApplicationView.ICONS16 + "return.png"))) {

                @Override
                public void actionPerformed(ActionEvent e) {
                    onReturn();
                }
            };
        }
        if (hasDeactivateCustomer()) {
            theDeactivateCustomerAction = new AbstractAction("", new ImageIcon(getClass().getResource(ApplicationView.ICONS16 + "deactivate_customer.png"))) {

                @Override
                public void actionPerformed(ActionEvent e) {
                    onDeactivateCustomer();
                }

            };
        }
        if (hasReverseTax()) {
            theRverseTaxAction = new AbstractAction("", new ImageIcon(getClass().getResource(ApplicationView.ICONS16 + "reverse_tax.png"))) {

                @Override
                public void actionPerformed(ActionEvent e) {
                    onReverseTax();
                }

            };

        }
        if (hasValidateReception()) {
            theValidateReceptionAction = new AbstractAction("", new ImageIcon(getClass().getResource(ApplicationView.ICONS16 + "validate_reception.png"))) {

                @Override
                public void actionPerformed(ActionEvent e) {
                    onValidateReception();
                }

            };
        }
        /*  tbHeader.addSeparator();
         tbHeader.addSeparator();
         tbHeader.addSeparator();
         tbHeader.addSeparator();
         tbHeader.addSeparator();*/
        tbHeader.addSeparator();
        if (hasReturn()) {
            tbHeader.add(createToolButton(theReturnAction, true, true));

        }
        tbHeader.addSeparator();
        if (hasBilling()) {
            tbHeader.add(createToolButton(theBillingAction, true, true));

        }

        tbHeader.addSeparator();
        if (hasValidateReception()) {
            tbHeader.add(createToolButton(theValidateReceptionAction, true, true));

        }

        tbHeader.addSeparator();
        if (hasInvoice()) {
            tbHeader.add(createToolButton(theInvoiceAction, true, true));

        }
        tbHeader.addSeparator();
        if (hasInvoicePrint()) {
            tbHeader.add(createToolButton(theInvoicePrintAction, true, true));

        }
        tbHeader.addSeparator();
        if (hasCash()) {
            tbHeader.add(createToolButton(theCashAction, true, true));

        }
        tbHeader.addSeparator();
        if(hasReverseTax())
        {
            tbHeader.add(createToolButton(theRverseTaxAction,true,true));
        }
        tbHeader.addSeparator();
        if (hasReport()) {
            tbHeader.add(createToolButton(theReportAction, true, true));
        }
        if(hasStockLimitWarning())
        {
            tbHeader.add(createToolButton(theWarningAction,true,true));
        }
        tbHeader.addSeparator();
        if (hasDeactivateCustomer()) {
            tbHeader.add(createToolButton(theDeactivateCustomerAction, true, true));
        }
        //  headerBar.add(lblTitle, "dock center, gapleft 4");
        headerBar.add(tbHeader, "dock east, gapleft 300, width 500!, height pref!");

        tbHeader.addSeparator();
        if (hasCloseRepair()) {
            tbHeader.add(createToolButton(theCloseRepairAction, true, true));
        }
        tbHeader.addSeparator();
        tbHeader.add(createToolButton(theAddNewAction, true, true));
        tbHeader.addSeparator();
        /* tbHeader.addSeparator();
         tbHeader.addSeparator();
         tbHeader.addSeparator();*/

        tbHeader.add(createToolButton(theEditAction, true, true));
        tbHeader.addSeparator();
        /* tbHeader.addSeparator();
         tbHeader.addSeparator();
         tbHeader.addSeparator();*/

        tbHeader.add(createToolButton(theDeleteAction, true, true));
        tbHeader.addSeparator();
        /* tbHeader.addSeparator();
         tbHeader.addSeparator();
         tbHeader.addSeparator();*/
        tbHeader.add(createToolButton(theRefreshAction, true, true));
        tbHeader.addSeparator();
        tbHeader.addSeparator();
        tbHeader.addSeparator();
        /*  tbHeader.addSeparator();
         tbHeader.addSeparator();
         tbHeader.addSeparator();
         tbHeader.addSeparator();
         tbHeader.addSeparator();
         tbHeader.addSeparator();
         tbHeader.addSeparator();
         tbHeader.addSeparator();*/
        tbHeader.add(createToolButton(theCloseAction, true, true));
        tbHeader.addSeparator();
        
        
        SubstanceLookAndFeel.setDecorationType(headerBar, DecorationAreaType.HEADER);
        SubstanceLookAndFeel.setDecorationType(tbHeader, DecorationAreaType.HEADER);

        return headerBar;

    }

    public EntityTableModel getTableModel() {
        return theEntityTableModel;
    }

    private void notifySidePanel() {

        showEntitySummary();
        showEntityActions();
        if (theRowObserver != null) {
            //  System.out.println(this.getTableModel());
            theRowObserver.tableRowSelected(this.getSelectedModel());
        }

    }

    public abstract void showEntitySummary();

    public abstract void showEntityActions();

    public void setTableFilter(String filter) {
        System.out.println("the search filter is " + filter);

        this.theSearchFilter = filter;
    }

    public abstract void fireUpdateEvent(MouseEvent e);

    public abstract AbstractSidePanel getSidePanel();

    public AbstractSearchPanel getSearchPanel() {

        if (theSearchPanel == null) {
            theSearchPanel = new InventorySearchPanel();
            theSearchPanel.setParentListView(this);
        }
        return theSearchPanel;
    }

    private AbstractSearchPanel createSearchPanel() {
        AbstractSearchPanel panel = getSearchPanel();

        return panel;
    }

    /*  private void exportToExcel() {
     //  setTableFilter(filter);
     //  refreshData();
     JTable t = new JTable();
     t.setModel(theTable.getModel());
     EntityTableModel m = new EntityTableModel();
     JTableHeader th = t.getTableHeader();
     t.setTableHeader(th);
     int sz = theInvokingCommand.getDataSize("");
     List<T> data = theInvokingCommand.getData("", 0, sz);
     m.setData(data);

     try {
     //System.out.println("table row count" + theInvokingCommand.getData("", 0,sz));
     ExcelExporter.export("excel.xls", t);

     //ExcelExporter.export(ApplicationView.IMAGES + "ee.xls", theTable,data);
     } catch (IOException ex) {
     Logger.getLogger(AbstractListView.class.getName()).log(Level.SEVERE, null, ex);
     }
     }*/
    private void exportToExcel() {
        excelFile = this.getTitle() + ".xls";
        //System.out.println(theTable.getRowCount());
        Workbook wb = new HSSFWorkbook();
        CreationHelper helper = wb.getCreationHelper();
        Sheet sheet = wb.createSheet("données");
        Row row = null;
        Cell cell = null;
        EntityTableModel dtm;
        dtm = (EntityTableModel) theTable.getModel();
        List<TableColumn> columns = theTable.getColumns();
        row = sheet.createRow(0);
        for (int i = 0; i < columns.size(); i++) {

            cell = row.createCell(i);
            cell.setCellValue(columns.get(i).getHeaderValue().toString());

        }
        for (int i = 1; i <= dtm.getRowCount(); i++) {
            row = sheet.createRow(i);
            for (int j = 0; j < dtm.getColumnCount(); j++) {

                cell = row.createCell(j);
                cell.setCellValue(dtm.getValueAt(i - 1, j).toString());
            }
        }
        try {
            FileOutputStream output = new FileOutputStream(new File(excelFile));
            wb.write(output);
            output.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AbstractListView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AbstractListView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private JSplitPane buildCenterPanel() {
        JPanel tablePanel = new JPanel(new BorderLayout());
        theSidePanel = getSidePanel();
        theSearchPanel = createSearchPanel();
        thePagerToolbar = new JCustomToolBar();
        thePagerToolbar.setFloatable(false);
        thePagerToolbar.setFocusable(false);
        thePagerToolbar.setRollover(true);
        theInfoToolbar = new JToolBar();
        theInfoToolbar.setFloatable(false);
        theInfoToolbar.setFocusable(false);
        theInfoToolbar.setRollover(true);
        theFirstPageAction = new AbstractAction("firstPage", new ImageIcon(getClass().getResource(ApplicationView.ICONS16 + "first.png"))) {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("get the first page");
                firstPage();

            }
        };
        theLastPageAction = new AbstractAction("lastPage", new ImageIcon(getClass().getResource(ApplicationView.ICONS16 + "last.png"))) {

            @Override
            public void actionPerformed(ActionEvent e) {
                //System.out.println("get the first page");
                lastPage();

            }
        };
        theNextPageAction = new AbstractAction("nextPage", new ImageIcon(getClass().getResource(ApplicationView.ICONS16 + "next.png"))) {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("get the next page");
                nextPage();

            }
        };
        thePreviousPageAction = new AbstractAction("prevPage", new ImageIcon(getClass().getResource(ApplicationView.ICONS16 + "prev1.png"))) {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("get the previous page");
                previousPage();
            }
        };
        theXlsExportAction = new AbstractAction("exportExcel", new ImageIcon(getClass().getResource(ApplicationView.ICONS16 + "excel.png"))) {

            @Override
            public void actionPerformed(ActionEvent e) {
//                System.out.println(" exporting to excel" + getTheTableData());
                try {
                    exportToExcel();
                } catch (Exception eee) {
                    ApplicationView.showErrorMsg("Une erreur s'est produite !");
                }
                if (Desktop.isDesktopSupported()) {
                    try {
                        Desktop.getDesktop().open(new File(excelFile));
                    } catch (IOException ex) {
                        ApplicationView.showErrorMsg("Erreur lors de l'ouverture du fichier excel");
                    } catch (IllegalArgumentException ex) {
                        ApplicationView.showErrorMsg("Erreur :le fichier excel n'est pas géneré !");
                    }
                }
            }

        };

        thePagerToolbar.addSeparator();
        thePagerToolbar.add(createToolButton(theFirstPageAction, false, false));
        thePagerToolbar.addSeparator();
        thePagerToolbar.add(createToolButton(thePreviousPageAction, false, false));
        thePagerToolbar.addSeparator();
        thePagerToolbar.add(createToolButton(theNextPageAction, false, false));
        thePagerToolbar.addSeparator();
        thePagerToolbar.add(createToolButton(theLastPageAction, false, false));
        thePagerToolbar.addSeparator();
        SubstanceLookAndFeel.setDecorationType(thePagerToolbar, DecorationAreaType.GENERAL);
        thePagerToolbar.addSeparator();
        thePagerToolbar.addSeparator();
        thePagerToolbar.addSeparator();
        thePagerToolbar.addSeparator();
        thePagerToolbar.addSeparator();
        thePagerToolbar.addSeparator();
        thePagerToolbar.add(createToolButton(theXlsExportAction, false, false));
        thePagerToolbar.addSeparator();
        thePagerToolbar.addSeparator();

        // thePagerToolbar.add(createToolButton(theEditAction, false, false));
        // JLabel labelFrom = new JLabel("De:");
        // thePagerToolbar.add(labelFrom);
        tablePanel.add(thePagerToolbar, BorderLayout.NORTH);
        //tablePanel.add(theInfoToolbar,BorderLayout.SOUTH);
        theHZSplitePane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        theVSplitePane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        theHZSplitePane.setDividerSize(5);
        theVSplitePane.setDividerSize(5);
        //theSplitePane.setResizeWeight(0.7);
        theVSplitePane.setTopComponent(theSearchPanel);
        theVSplitePane.setBottomComponent(tablePanel);
        theHZSplitePane.setLeftComponent(theSidePanel);
        theHZSplitePane.setRightComponent(theVSplitePane);

        theHZSplitePane.setDividerLocation(0.50);
        theVSplitePane.setDividerLocation(0.8);

        tablePanel.setMinimumSize(new Dimension(300, 300));
        theSidePanel.setMinimumSize(new Dimension(250, 220));
        theSidePanel.setEnabled(false);
        theTable = new JXTable();
        theTable.setRowHeight(19);
        theTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                notifySidePanel();

            }

        });
        theTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                //System.out.println(getSelectedModel());
                if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 1) {

                }

                if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() > 1) {

                    fireUpdateEvent(e);

                }
            }

        });

        theTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        theTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting()) {
                    return;
                }
                if (theSidePanel != null) {
                    notifySidePanel();
                }
            }
        });
        theTable.setColumnControlVisible(true);
        theTable.setHighlighters(HighlighterFactory.createSimpleStriping(
                HighlighterFactory.BEIGE));
        theTable.getTableHeader().setPreferredSize(
                new Dimension(theTable.getTableHeader().getPreferredSize().width, 19));

        System.out.println("---------------------------------");
        System.out.println("setting the table model");
        System.out.println(theTableData);

        theEntityTableModel = new EntityTableModel(theTableData);

        addTableColumns();
        //theEntityTableModel.setData(getData());
        //  theTable.repaint();
        //     theTable.setModel(theEntityTableModel);

        theTable.setAutoResizeMode(getJXTableAutoResizeMode());
        // toolbar
        tbPager = new JToolBar();
        tbPager.setFloatable(false);
        tbPager.setRollover(true);
        tbPager.setFocusable(false);
        SubstanceLookAndFeel.setDecorationType(tbPager, DecorationAreaType.GENERAL);

        //  buildPagerActions();
        theCurrentPageLabel = new JLabel("");
        //   theFromLabel = new JLabel("  Pages affichées de : 0  à  0   ");
        thePageCountLabel = new JLabel("");
        theRecordsFoundLabel = new JLabel("");
        setNavigationRecordCount();
       // this.theCurrentPageLabel.setText("Page actuelle : " + this.theCurrentPage);

        //this.thePageCountLabel.setText("Nombre de pages :  " + String.valueOf(thePageCount));
        // this.theRecordsFoundLabel.setText(String.valueOf(theRowCount));
        tbPager.add(theCurrentPageLabel);
        tbPager.addSeparator();
        tbPager.add(theRecordsFoundLabel);
        tbPager.addSeparator();
        tbPager.add(thePageCountLabel);
        tbPager.addSeparator();
        /*tbPager.add(createToolButton(theFirstPageAction, false, false));
         tbPager.add(createToolButton(thePreviousPageAction, false, false));
         tbPager.add(new JLabel(" "));
         tbPager.add(theCurrentPageLabel);
         tbPager.add(theFromLabel);
         tbPager.add(thePageCountLabel);
         tbPager.add(new JLabel(" "));
         tbPager.add(createToolButton(theNextPageAction, false, false));
         tbPager.add(createToolButton(theLastPageAction, false, false));
         tbPager.addSeparator();
         tbPager.add(Box.createGlue());
         tbPager.add(theRecordsFoundLabel);*/
        tablePanel.add(new JScrollPane(theTable), BorderLayout.CENTER);
        tablePanel.add(tbPager, BorderLayout.SOUTH);

        setUpColumns();

        return theHZSplitePane;

    }

    protected boolean hasReverseTax() {
        return false;
    }

    public void onReverseTax() {
            System.out.println("reversing tax");
    }

    public List<T> getTheTableData() {
        List<T> l = theInvokingCommand.getData("", 0, theRowCount - 1);
        return l;
    }

    /* private void setCurrentPage(int i) {
        
     }*/
    /*private void buildHeaderBarActions() {
     theAddNewAction = new AbstractAction("Nouveau", 
     new ImageIcon(getClass().getResource(ViewHelpers.ICONS16 + "addnew.png"))) {
     @Override
     public void actionPerformed(ActionEvent e) {
     getController().onAddNew();
     }
     };
     acAddNew.putValue(Action.SHORT_DESCRIPTION, I18n.COMMON.getString("Action.Hint.AddNew"));
        
     acEdit = new AbstractAction(I18n.COMMON.getString("Action.Edit"), 
     new ImageIcon(getClass().getResource(ViewHelpers.ICONS16 + "edit.png"))) {
     @Override
     public void actionPerformed(ActionEvent e) {
     getController().onEdit();
     }
     };
     acEdit.putValue(Action.SHORT_DESCRIPTION, I18n.COMMON.getString("Action.Hint.Edit"));
        
     acDelete = new AbstractAction(I18n.COMMON.getString("Action.Delete"), 
     new ImageIcon(getClass().getResource(ViewHelpers.ICONS16 + "delete.png"))) {
     @Override
     public void actionPerformed(ActionEvent e) {
     getController().onDelete();
     }
     };
     acDelete.putValue(Action.SHORT_DESCRIPTION, I18n.COMMON.getString("Action.Hint.Delete"));
        
     acRefresh = new AbstractAction(I18n.COMMON.getString("Action.Refresh"), 
     new ImageIcon(getClass().getResource(ViewHelpers.ICONS16 + "refresh.png"))) {
     @Override
     public void actionPerformed(ActionEvent e) {
     refreshData();
     }
     };
     acRefresh.putValue(Action.SHORT_DESCRIPTION, I18n.COMMON.getString("Action.Hint.Refresh"));
     }*/
    public void refreshData() {
        int start = ((thePageSize * getCurrentPage()) - thePageSize);
        int end = start + thePageSize;
        System.out.println(" refreshData for current page " + getCurrentPage() + " start: " + start + "end : " + end);
        System.out.println("the command" + theCommand);
        System.out.println(" setting data " + theInvokingCommand.getData(theSearchFilter, start, end));
        System.out.println(" the search filter is " + theSearchFilter);
        List<T> l = theInvokingCommand.getData(theSearchFilter, start, end);
        
        System.out.println("data result for refresh " + l);
        theRowCount = l.size();
        theEntityTableModel.setData(l);
        theEntityTableModel.fireTableDataChanged();
        theTable.setModel(theEntityTableModel);
        theTable.repaint();
        setPageCount();
        setNavigationRecordCount();

        /*  if (theEntityTableModel.getRowCount() > 0) {
         theTable.setRowSelectionInterval(0, 0);
         }*/
    }
    /*    private void setTableFilter() {
     theSearchFilter = theSearchField.getSearchTextField().getText();
     setCurrentPage(1);
     refreshData();
     }*/

    public void setPageCount() {
        theRowCount = (int) Math.ceil((double) (theInvokingCommand.getDataSize(theSearchFilter)));
        thePageCount = (int) Math.ceil((double) (theRowCount / thePageSize));
        double remainder = (theRowCount % thePageSize);

        if (remainder > 0) {
            thePageCount += 1;
        }

    }

    public void previousPage() {
        theSearchFilter = "";
        if (getCurrentPage() > 1) {
            setCurrentPage(getCurrentPage() - 1);
            refreshData();
        }
    }

    public void nextPage() {
        theSearchFilter = "";
        if (getCurrentPage() < getPageCount()) {
            setCurrentPage(getCurrentPage() + 1);
            refreshData();
        }
    }

    public void firstPage() {
        theSearchFilter = "";
        // if (getCurrentPage() < getPageCount()) {
        setCurrentPage(1);
        System.out.println("first page");
        refreshData();
        //}
    }

    public void lastPage() {
        theSearchFilter = "";
        if (getCurrentPage() < getPageCount()) {
            setCurrentPage(getPageCount());
            refreshData();
        }
    }

    public static AbstractButton createToolButton(Action action, boolean showTitle,
            boolean titlePositionBottom) {

        AbstractButton btn = new JButton(action);
        btn.setFocusPainted(false);
        btn.setMargin(new Insets(0, 0, 0, 0));

        if (!showTitle) {
            btn.setText("");
        }

        if (titlePositionBottom) {
            btn.setHorizontalTextPosition(SwingConstants.CENTER);
            btn.setVerticalTextPosition(SwingConstants.BOTTOM);
        }

        if (btn.getPreferredSize().width < 50) {
            btn.setMaximumSize(new Dimension(50, 41));
            btn.setMaximumSize(new Dimension(50, 41));
        }

        return btn;
    }

    private void buildPagerActions() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private int getJXTableAutoResizeMode() {
        return 1;
    }

    private void setUpColumns() {
        /*  for (int i = 0; i < theEntityTableModel.getColumnCount(); i++) {
         theTable.getColumnExt(theEntityTableModel.getColumnName(i)).setPreferredWidth(
         theEntityTableModel.getColumn(i).getWidth());
         theTable.getColumnExt(theEntityTableModel.getColumnName(i)).setVisible(
         theEntityTableModel.getColumn(i).isVisible());

         if (theTable.getColumnExt(theEntityTableModel.getColumnName(i)).isVisible()) {
         theTable.moveColumn(theTable.convertColumnIndexToView(i), i);
         }
         }*/
    }

    private int getCurrentPage() {
        return theCurrentPage;
    }

    private int getPageCount() {
        return thePageCount;
    }

    private void setCurrentPage(int pageCount) {
        this.theCurrentPage = pageCount;
    }

    private void setNavigationRecordCount() {
        this.theCurrentPageLabel.setText("Page actuelle : " + this.theCurrentPage);

        this.thePageCountLabel.setText("Nombre de pages :  " + String.valueOf(thePageCount));
        this.theRecordsFoundLabel.setText("Nombre de lignes  : " + String.valueOf(theTable.getModel().getRowCount()));
        this.tbPager.repaint();
    }

    private void onEdit() {
        System.out.println("on edit");
        lastOp = Ops.edit;
        if (getSelectedModel() == null) {
            ApplicationView.showErrorMsg("Aucune ligne n'est selectionnée");
        }
        fireUpdateEvent(null);
    }

    private void onClose() {
        System.out.println("on close");
        ApplicationView.closeTab(getTitle());

    }

    protected void onDelete() {
        System.out.println("on delete");
        lastOp = Ops.delete;
        T e;

        e = getSelectedModel();
        if (e != null) {
            try {
                delete(e);
            } catch (Exception ee) {
                ApplicationView.showErrorMsg("Impossible d'exécuter cette action ");
            }

            refreshData();

        } else {
            ApplicationView.showErrorMsg("Aucune ligne n'est selectionnée");
        }

    }

    protected void onNew() {
        System.out.println("new ");
        lastOp = Ops.add;
        setRelatedFormCommand();
        if (theRelatedFormCommand != null) {
            theRelatedFormCommand.execute();
        } else {
            ApplicationView.showErrorMsg("Impossible d'effectuer cette opération ! ");
            return;
        }

        this.refreshData();
    }

    protected void onReport() {
        ApplicationView.showErrorMsg("Impossible d'exécuter cette opération !");

    }

    protected void onCloseRepair() {
    }

    protected void onRefresh() {

        refreshData();
        if (this.theTable != null) {
            theTable.revalidate();
        }

    }

    protected boolean hasDeactivateCustomer() {
        return false;
    }

    protected boolean hasReturn() {
        return false;
    }

    protected void onReturn() {
        return;
    }

    protected void onDeactivateCustomer() {

    }

    protected boolean hasCloseRepair() {
        return false;
    }

    protected boolean hasInvoice() {
        return false;
    }

    protected boolean hasInvoicePrint() {
        return false;
    }

    protected boolean hasCash() {
        return false;
    }

    protected void onPayment() {
    }

    protected void onInvoice() {
    }

    protected void onPrintInvoice() {
        System.out.println("printing invoice !");
    }

    protected boolean hasBilling() {
        return false;
    }

    protected void onBilling() {

    }
    protected boolean hasStockLimitWarning()
    {
        return false;
    }
    protected void onStockLimitWarning()
    {
        System.out.println("stock warnings !");
        System.out.println(""+this.theSearchFilter);
        refreshData();
    }
    protected void onValidateReception() {
        ApplicationView.showErrorMsg("Impossible d'exécuter cette opération !");
    }

    protected boolean hasReport() {
        return true;
    }

    protected boolean hasValidateReception() {
        return false;
    }

    protected JFreeChart getLineChart() {

        return null;
    }

    protected JFreeChart getBarChart() {
        return null;
    }

    protected JFreeChart getPieChart() {
        return null;
    }

    protected String getDocumentName() {
        return "sample";
    }

    protected String getDocumentSubject() {
        return "Facturation";
    }

    protected String getDocumentTitle() {
        return "Facture";
    }

    protected String getDocumentPreface() {
        return "Facture N°";
    }

    protected List<SalesOrderLine> getOrderLines() {
        return new ArrayList();
    }

    protected String getDocumentPaymentType() {
        return "";
    }

    protected Customer getDocumentCustomer() {
        return new Customer();
    }

    protected double getDocumentTotalTTC() {
        return 0;
    }

    protected double getDocumentTotalHT() {
        return 0;
    }

    private void setupPdf() {

        Document document = new Document();
        try {
            try {
                PdfWriter.getInstance(document, new FileOutputStream(PDF + getDocumentName() + ".pdf"));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(SalesOrderFormView.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (DocumentException ex) {
            Logger.getLogger(SalesOrderFormView.class.getName()).log(Level.SEVERE, null, ex);
        }
        document.open();
        addMetaData(document);
        try {
            addTitlePage(document);
        } catch (DocumentException ex) {
            Logger.getLogger(SalesOrderFormView.class.getName()).log(Level.SEVERE, null, ex);
        }

        document.close();
    }

    private void addMetaData(Document document) {
        document.addTitle(getDocumentTitle());
        document.addSubject(getDocumentSubject());
        document.addAuthor("Bouchefra Ahmed");
        document.addCreator("Bouchefra Ahmed");
    }

    private void addTitlePage(Document document)
            throws DocumentException {
        Paragraph preface = new Paragraph();
        addEmptyLine(preface, 1);

        preface.add(new Paragraph(getDocumentPreface(), catFont));
        addEmptyLine(preface, 1);

        addEmptyLine(preface, 3);
        preface.add(new Paragraph("Nom du client :" + getDocumentCustomer(),
                smallBold));
        preface.add(new Paragraph("Prix total TTC à payer :" + getDocumentTotalTTC() + " DH",
                smallBold));
        preface.add(new Paragraph("Prix total HT à payer :" + getDocumentTotalHT() + " DH",
                smallBold));
        if (getDocumentPaymentType().equalsIgnoreCase("espéces")) {
            preface.add(new Paragraph("Payement par  : espéces",
                    smallBold));
        }
        if (getDocumentPaymentType().equals("crédit")) {
            preface.add(new Paragraph("Payement par  : crédit",
                    smallBold));
        }

        addEmptyLine(preface, 4);
        preface.add(new Paragraph("", redFont));
        createTable(preface);

        document.add(preface);
        //  document.newPage();
    }

    /* private void addContent(Document document) throws DocumentException {
     Anchor anchor = new Anchor("First Chapter", catFont);
     anchor.setName("First Chapter");

     // Second parameter is the number of the chapter
     Chapter catPart = new Chapter(new Paragraph(anchor), 1);

     Paragraph subPara = new Paragraph("Subcategory 1", subFont);
     Section subCatPart = catPart.addSection(subPara);
     subCatPart.add(new Paragraph("Hello"));

     subPara = new Paragraph("Subcategory 2", subFont);
     subCatPart = catPart.addSection(subPara);
     subCatPart.add(new Paragraph("Paragraph 1"));
     subCatPart.add(new Paragraph("Paragraph 2"));
     subCatPart.add(new Paragraph("Paragraph 3"));

     // add a list
     Paragraph paragraph = new Paragraph();
     addEmptyLine(paragraph, 5);
     subCatPart.add(paragraph);

     // add a table
     // createTable(subCatPart);
     // now add all this to the document
     document.add(catPart);

     // Next section
     anchor = new Anchor("Second Chapter", catFont);
     anchor.setName("Second Chapter");

     // Second parameter is the number of the chapter
     catPart = new Chapter(new Paragraph(anchor), 1);

     subPara = new Paragraph("Subcategory", subFont);
     subCatPart = catPart.addSection(subPara);
     subCatPart.add(new Paragraph("This is a very important message"));

     // now add all this to the document
     document.add(catPart);

     }
     */
    protected void createReportTable(String filename, String title, JTable table) {
        //PageSize.A4.rotate()
        Document doc = new Document();
        PdfWriter writer;
        try {

            try {

                writer = PdfWriter.getInstance(doc, new FileOutputStream(filename));
                doc.open();
                Paragraph page = new Paragraph();

                page.add(new Paragraph("Rapport : " + title));
                this.addEmptyLine(page, 5);

                PdfPTable pdfTable = new PdfPTable(table.getColumnCount());
                for (int col = 0; col < table.getColumnCount(); col++) {
                    System.out.println("cell ::");
                    PdfPCell cl = new PdfPCell(new Phrase(table.getColumnName(col)));
                    System.out.println(cl + " -- " + table.getColumnName(col));
                    pdfTable.addCell(cl);
                }
                // pdfTable.setHeaderRows(1);

                for (int row = 0; row < table.getRowCount(); row++) {
                    for (int col = 0; col < table.getColumnCount(); col++) {
                        System.out.println("cell data " + table.getValueAt(row, col));
                        PdfPCell cl = new PdfPCell(new Phrase(String.valueOf(table.getValueAt(row, col))));
                        pdfTable.addCell(cl);
                    }
                }
                page.add(pdfTable);
                doc.add(page);
            } catch (DocumentException ex) {
                ApplicationView.showErrorMsg("Erreur lors de l'ouverture du fichier !");
            }

        } catch (FileNotFoundException ex) {
            ApplicationView.showErrorMsg("Erreur lors de l'ouverture du fichier !");
            return;

        }
        doc.close();
    }

    private void createTable(Paragraph subCatPart)
            throws BadElementException {
        System.out.println("nbr of cells" + getOrderLines().size());
        PdfPTable table = new PdfPTable(3);

        PdfPCell c1 = new PdfPCell(new Phrase("Article"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Prix unitaire"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Quantité"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);
        table.setHeaderRows(1);
        List<SalesOrderLine> ol = getOrderLines();
        ol.forEach((o) -> {

            table.addCell(o.getTheOrderedItem().toString());
            table.addCell("" + o.getTheOrderedItem().getTheSalesPriceTTC() + " DH");
            table.addCell("" + o.getTheQuantity());
        });

        subCatPart.add(table);

    }

    private void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

    protected boolean hasBarChart() {
        return false;
    }

    protected boolean hasLineChart() {
        return false;
    }

    protected boolean hasPieChart() {
        return false;
    }

    protected int year() {
        return Calendar.getInstance().get(Calendar.YEAR);
    }

    protected int month() {
        return Calendar.getInstance().get(Calendar.MONTH);
    }

    protected int day() {
        return Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
    }

    public void viewPdf(String filename) {
        if (Desktop.isDesktopSupported()) {
            File file = new File(filename);
            try {
                Desktop.getDesktop().open(file);
            } catch (IOException ex) {
                Logger.getLogger(AbstractListView.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void createPdf(String filename) throws IOException, DocumentException {
        // step 1
        Document document = new Document(PageSize.A4);
        // step 2
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filename));
        // step 3
        document.open();
        // step 4
        PdfContentByte cb = writer.getDirectContent();
        float width = PageSize.A4.getWidth();
        float height = PageSize.A4.getHeight() / 2;
        // Pie chart
        //   PdfTemplate pie = cb.createTemplate(width, height);
        //Graphics2D g2d1 = new PdfGraphics2D(pie, width, height);
        //   Rectangle2D r2d1 = new Rectangle2D.Double(0, 0, width, height);
        //getPieChart().draw(g2d1, r2d1);
        // g2d1.dispose();
        // cb.addTemplate(pie, 0, height);
        // Bar chart
        if (hasBarChart()) {
            PdfTemplate bar = cb.createTemplate(width, height);
            Graphics2D g2d2 = new PdfGraphics2D(bar, width, height);
            Rectangle2D r2d2 = new Rectangle2D.Double(0, 0, width, height);

            getBarChart().draw(g2d2, r2d2);
            g2d2.dispose();
            cb.addTemplate(bar, 0, 0);
            // step 5
        }

        if (hasLineChart()) {
            PdfTemplate line = cb.createTemplate(width, height);
            Graphics2D g2d1 = new PdfGraphics2D(line, width, height);
            Rectangle2D r2d1 = new Rectangle2D.Double(0, 0, width, height);

            getLineChart().draw(g2d1, r2d1);
            g2d1.dispose();
            cb.addTemplate(line, 0, height);
            // step 5
        }
        if (hasPieChart()) {
            PdfTemplate pie = cb.createTemplate(width, height);
            Graphics2D g2d3 = new PdfGraphics2D(pie, width, height);
            Rectangle2D r2d3 = new Rectangle2D.Double(0, 0, width, height);

            getBarChart().draw(g2d3, r2d3);
            g2d3.dispose();
            cb.addTemplate(pie, 0, 0);
            // step 5

        }
        document.close();
    }

}
