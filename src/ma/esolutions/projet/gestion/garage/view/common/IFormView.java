/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view.common;

import ma.esolutions.projet.gestion.garage.model.BaseEntity;
import ma.esolutions.projet.gestion.garage.model.Customer;

/**
 *
 * @author born2code
 */
public interface IFormView<T> extends IView {

    T getEntity();

    String getFormIconPath();

    String getFormTitle();

    void closeForm();

    void onHelp();

    void onPrint();

    void onPrintPreview();

    boolean Save();

    void popFields();

    T pushFields();
    
    void clearFields();

    void showDialog();
    
}
