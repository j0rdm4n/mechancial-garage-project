/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view.common;

import ma.esolutions.projet.gestion.garage.controller.common.IPageController;

/**
 *
 * @author Ahmed
 */
public interface IPageView extends IView {
    void init(IPageController controller);
    void init();
    IPageController getController();

    public void showMainView();
    
}
