/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view.common;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.border.Border;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.IMAGES;

/**
 *
 * @author E-solutions
 */
public abstract class AbstractSearchPanel<T> extends JPanel {
    
    protected T entity;
    private JButton searchBtn;
    protected AbstractListView parentListView;
    
    public AbstractSearchPanel()
    {
        setLayout(new BorderLayout());
        initComponents();
    }

    public AbstractListView getParentListView() {
        return parentListView;
    }

    public void setParentListView(AbstractListView parentListView) {
        this.parentListView = parentListView;
    }
    public void setEntity(T e)
    {
        this.entity = e;
    }
    public T getEntity()
    {
        return entity;
    }
    private void initComponents() {
        
        searchBtn = new JButton("");
        searchBtn.setMargin(new Insets(5,5,15,15));
        searchBtn.setIcon(new ImageIcon(getClass().getResource(IMAGES+"search.png")));
        add(searchBtn,BorderLayout.EAST);
        setBorder(BorderFactory.createBevelBorder(1, Color.lightGray, Color.lightGray, Color.BLACK, Color.blue));
        add(buildSearchForm(),BorderLayout.CENTER);
       searchBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String filter = buildSearchFilter();
                parentListView.setTableFilter(filter);
                parentListView.refreshData();
                        
            }
        });
    }

    abstract public JPanel buildSearchForm();
    abstract public String buildSearchFilter();
    public JPanel getComponent()
    {
        return this;
    }
    
}
