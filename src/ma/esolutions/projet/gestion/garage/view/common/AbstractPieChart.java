/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view.common;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.Chart;
import javafx.scene.chart.PieChart;
import ma.esolutions.projet.gestion.garage.controller.MainController;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.CHART_STYLE;

/**
 *
 * @author E-solutions
 */
public abstract class AbstractPieChart implements IChart {
    
    private ObservableList<PieChart.Data> theChartData;
    protected MainController theController;
    public AbstractPieChart(MainController controller)
    {
        theChartData = FXCollections.observableArrayList();
        theController = controller;
                
    }
    public void addElement(PieChart.Data el)
    {
        theChartData.add(el);
    }
    public abstract ObservableList<PieChart.Data> getData();
    private Chart createChart()
    {
        theChartData = getData();
        PieChart chart = new PieChart(theChartData); 
        chart.setTitle(getTitle());
        return chart;
    }
    @Override
    public Chart getChart()
    {
        Chart chart = createChart();
       
        return chart;
    }
}
