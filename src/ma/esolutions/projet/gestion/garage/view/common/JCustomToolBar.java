/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view.common;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JToolBar;
import ma.esolutions.projet.gestion.garage.view.ApplicationView;

/**
 *
 * @author E-solutions
 */
class JCustomToolBar extends JToolBar {

    public JCustomToolBar() {
        super();
    }
    
    @Override
    protected void paintComponent(Graphics g) {

        ImageIcon imageicon = new ImageIcon(getClass().getResource(ApplicationView.IMAGES + "list.jpg"));
        Image image = imageicon.getImage();

        super.paintComponent(g);

        if (image != null) {
            g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
        }
    }
    
}
