package ma.esolutions.projet.gestion.garage.view.common;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.List;
import javax.persistence.PersistenceException;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import ma.esolutions.projet.gestion.garage.app.Application;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.controller.common.IFormCommand;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.view.ApplicationView;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXStatusBar;

/**
 *
 *
 * @param <T> entity
 *
 *
 */
public abstract class AbstractFormView<T> extends JDialog implements IFormView<T>, IDataObservable {

    protected ApplicationController theController;
    protected IFormCommand theControllerCommand;
    protected JMenuBar theMenuBar;
    protected JToolBar theToolBar;

    public IFormCommand getTheControllerCommand() {
        return theControllerCommand;
    }

    public void setTheControllerCommand(IFormCommand theControllerCommand) {
        this.theControllerCommand = theControllerCommand;
    }
    protected JTabbedPane theTabbedPages;
    protected JXStatusBar theStatusBar;
    protected IDataObserver theDataObserver;

    protected Action theSaveAction;
    protected Action thePrintPreviewAction;
    protected Action thePrintAction;
    protected Action theCloseAction;
    protected Action theHelpAction;
    protected JLabel theStatusBarLabel;
    private JScrollPane theScrollPane;
    protected IListView<T> theListView;

    public void setTheListView(IListView<T> view) {
        theListView = view;
    }

    public IListView<T> getTheListView() {
        return theListView;
    }

    @Override
    public void registerObserver(IDataObserver o) {
        theDataObserver = o;
    }

    @Override
    public void unregisterObserver(IDataObserver o) {
        theDataObserver = null;
    }

    @Override
    public void notifyObserver() {
        if (theDataObserver != null) {
            theDataObserver.added(this.getEntity());
        }
    }

    public void setStatusbarMsg(String status) {
        theStatusBarLabel.setText(status);
        theStatusBarLabel.repaint();
        theStatusBar.repaint();
    }

    public AbstractFormView(JFrame parent) {
        super(parent);
        this.theController = Application.getAppController();

    }

    public void initComponents() {
        setLayout(new BorderLayout());
        setIconImage(new ImageIcon(getClass().getResource(getFormIconPath())).getImage());
        setTitle(getFormTitle());
        this.setResizable(false);
        buildFormActions();
        // setJMenuBar(buildMenuBar());
        getContentPane().add(buildToolBar(), BorderLayout.WEST);
        getContentPane().add(buildStatusBar(), BorderLayout.SOUTH);

        if (isMultiPageForm()) {
            theTabbedPages = new JTabbedPane();
            theTabbedPages.setFocusable(true);
            theTabbedPages.setBackground(Color.DARK_GRAY);
            getContentPane().add(theTabbedPages, BorderLayout.CENTER);
        }
    }

    public abstract void buildUI();

    @Override
    public boolean Save() {
        System.out.println("saving entity");
        if (!validateForm()) {
            ApplicationView.showErrorMsg("Erreur avec les informations entrées ,création échouée !!");
            return false;
        }
        pushFields();
        notifyObserver();
      //  System.out.println("command controller" + theControllerCommand);
      /*  if(theControllerCommand != null)
         theControllerCommand.save(this.getEntity());*/

        return true;
    }

    public boolean validateForm() {
        return true;
    }

    public void addPageToForm(String title, JPanel page) {
        if (isMultiPageForm()) {
            theScrollPane = new JScrollPane(page);
            theTabbedPages.add(title, theScrollPane);
        } else {
            getContentPane().add(page, BorderLayout.CENTER);
        }
    }

    public boolean isMultiPageForm() {
        return true;
    }

    public boolean isPrintable() {
        return false;
    }

    @Override
    public void onPrintPreview() {
    }

    @Override
    public void onPrint() {
    }

    @Override
    public void showDialog() {
        buildUI();
        setLocationRelativeTo(ApplicationController.get().getAppView());
        setModalityType(ModalityType.APPLICATION_MODAL);
        setVisible(true);
    }

    @Override
    public void closeForm() {
        setVisible(false);
        dispose();
    }

    private JMenuBar buildMenuBar() {
        theMenuBar = new JMenuBar();

        // File menu
        JMenu mnuFile = new JMenu("Fichier");

        mnuFile.add(theSaveAction);
        mnuFile.add(new JSeparator());
        if (isPrintable()) {
            mnuFile.add(thePrintPreviewAction);
            mnuFile.add(thePrintAction);
            mnuFile.add(new JSeparator());
        }
        mnuFile.add(theCloseAction);
        theMenuBar.add(mnuFile);

        return theMenuBar;
    }

    private JToolBar buildToolBar() {
        theToolBar = new JToolBar();
        theToolBar.setRollover(true);
        theToolBar.setFloatable(false);
        theToolBar.setOrientation(SwingConstants.VERTICAL);
        theToolBar.setBorder(new Border() {

            @Override
            public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            }

            @Override
            public Insets getBorderInsets(Component c) {

                return new Insets(0, 0, 0, 0);
            }

            @Override
            public boolean isBorderOpaque() {
                return true;
            }
        });

        theToolBar.setOpaque(true);
        theToolBar.setLayout(new MigLayout());
        theToolBar.setMargin(new Insets(5, 20, 20, 20));
        theToolBar.setBackground(Color.blue);

        theToolBar.add(createToolButton(theSaveAction, true, true), "gaptop 90,wrap");

        theToolBar.add(createToolButton(thePrintPreviewAction, true, true), "gaptop 8 ,wrap");
        theToolBar.add(createToolButton(thePrintAction, true, true), "gaptop 8,wrap");

        // theToolBar.add(createToolButton(theHelpAction, true, true),"gaptop 8 ,wrap");
        theToolBar.add(createToolButton(theCloseAction, true, true), "gaptop 8,wrap");

        return theToolBar;
    }

    @Override
    public void clearFields() {

    }

    private List<T> getData() {
        System.out.println("form getting data");
//        System.out.println(((SecureCommand)theControllerCommand).getData("",1,1000));
        return ((SecureCommand) theControllerCommand).getData("", 1, 1000);
    }

    private void buildFormActions() {

        theSaveAction = new AbstractAction("",
                new ImageIcon(getClass().getResource(ApplicationView.ICONS48 + "saveg.png"))) {
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        if (Save()) {
                            try {
                                if (theControllerCommand != null) {
                                    theControllerCommand.save(getEntity());
                                }
                                if (theListView != null) {
                                    System.out.println("refreshing the list data");
                                    theListView.refreshData(getData());
                                //TODO do something

                                }
                                clearFields();
                            } catch (PersistenceException e) {
                                System.out.println(e);
                                ApplicationView.showErrorMsg("Une erreur de persistence s'est produite ! ");
                                throw new UnsupportedOperationException("remove this later ");
                            }

                        }

                    }

                };
        theSaveAction.putValue(Action.SHORT_DESCRIPTION, "Enregistrer ");
        theSaveAction.putValue(Action.MNEMONIC_KEY, new Integer(KeyEvent.VK_S));
        theSaveAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));

        thePrintPreviewAction = new AbstractAction("",
                new ImageIcon(getClass().getResource(ApplicationView.ICONS48 + "preview.png"))) {
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        onPrintPreview();
                    }
                };
        thePrintPreviewAction.setEnabled(this.isPrintable());
        thePrintPreviewAction.putValue(Action.SHORT_DESCRIPTION, "Imprimer");
        thePrintPreviewAction.putValue(Action.MNEMONIC_KEY, new Integer(KeyEvent.VK_V));
        thePrintPreviewAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_MASK));

        thePrintAction = new AbstractAction("",
                new ImageIcon(getClass().getResource(ApplicationView.ICONS48 + "print.png"))) {
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        onPrint();
                    }
                };
        thePrintAction.putValue(Action.SHORT_DESCRIPTION, "Imprimer");
        thePrintAction.putValue(Action.MNEMONIC_KEY, new Integer(KeyEvent.VK_D));
        thePrintAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK));
        thePrintAction.setEnabled(this.isPrintable());
        theHelpAction = new AbstractAction("",
                new ImageIcon(getClass().getResource(ApplicationView.ICONS48 + "print.png"))) {
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        onHelp();
                    }
                };
        theHelpAction.putValue(Action.SHORT_DESCRIPTION, "Aide");
        theHelpAction.putValue(Action.MNEMONIC_KEY, new Integer(KeyEvent.VK_F1));
        theHelpAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));

        theCloseAction = new AbstractAction("",
                new ImageIcon(getClass().getResource(ApplicationView.ICONS48 + "close.png"))) {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        //theController.onRefresh();
                        closeForm();
                    }
                };
        theCloseAction.putValue(Action.SHORT_DESCRIPTION, "Fermer");
        theCloseAction.putValue(Action.MNEMONIC_KEY, new Integer(KeyEvent.VK_X));
        theCloseAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.ALT_MASK));
    }

    protected JXStatusBar buildStatusBar() {
        theStatusBar = new JXStatusBar();
        theStatusBarLabel = new JLabel();
        theStatusBar.setPreferredSize(new Dimension(15, 20));
        theStatusBar.add(theStatusBarLabel);
        return theStatusBar;
    }

    public static AbstractButton createToolButton(Action action) {
        return createToolButton(action, false, false);
    }

    public static AbstractButton createToolButton(Action action, boolean showTitle) {
        return createToolButton(action, showTitle, false);
    }

    public static AbstractButton createToolButton(Action action, boolean showTitle,
            boolean titlePositionBottom) {

        AbstractButton btn = new JButton(action);
        btn.setFocusPainted(false);
        btn.setMargin(new Insets(0, 0, 0, 0));

        if (!showTitle) {
            btn.setText("");
        }

        if (titlePositionBottom) {
            btn.setHorizontalTextPosition(SwingConstants.CENTER);
            btn.setVerticalTextPosition(SwingConstants.BOTTOM);
        }

        if (btn.getPreferredSize().width < 50) {
            btn.setMaximumSize(new Dimension(50, 41));
            btn.setMaximumSize(new Dimension(50, 41));
        }

        return btn;
    }

}
