/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view.common;

/**
 *
 * @author Ahmed
 */
interface IDataObservable {
    void registerObserver(IDataObserver o);
    void unregisterObserver(IDataObserver o);
    void notifyObserver();
}
