/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view.common;

import java.awt.Component;
import java.awt.event.ActionListener;

/**
 *
 * @author Ahmed
 */
public interface IView {
    String getTitle();
    String getIconPath();

    /**
     *
     * @return the view as a component
     */
    Component asComponent();
    
}
