
package ma.esolutions.projet.gestion.garage.view.common;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS12;
import org.jdesktop.swingx.prompt.PromptSupport;



public class JSearchField extends JPanel implements KeyListener {

    private JTextField tfSearch;
    private final Action acSearch;
    private String prompt;

   
    public JSearchField(Action acSearch) {
        this(acSearch, "Search");
    }

    public JSearchField(Action acSearch, String prompt) {
        super(new BorderLayout());
        this.acSearch = acSearch;
        this.prompt = prompt;
        initComponents();
    }

  
    private void initComponents() {
        ImageIcon icoSearch = new ImageIcon(getClass().getResource(
                ICONS12 + "search.png"));
        JLabel lblSearch = new JLabel();
        lblSearch.setIcon(icoSearch);
        lblSearch.setPreferredSize(new Dimension(22, 20));

        tfSearch = new JTextField(50);
        PromptSupport.setPrompt(prompt, tfSearch);
        tfSearch.addKeyListener(this);
        setBorder(tfSearch.getBorder());
        tfSearch.setBorder(null);

        add(lblSearch, BorderLayout.WEST);
        add(tfSearch, BorderLayout.CENTER);

        setPreferredSize(new Dimension(
                tfSearch.getPreferredSize().width,
                tfSearch.getPreferredSize().height + 6));
    }

  
    public final JTextField getSearchTextField() {
        return tfSearch;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            if (acSearch != null) {
                acSearch.actionPerformed(null);
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (acSearch != null) {
            acSearch.actionPerformed(null);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
