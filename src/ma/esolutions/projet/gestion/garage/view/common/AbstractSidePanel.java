/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view.common;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.LabelBuilder;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import ma.esolutions.projet.gestion.garage.model.BaseEntity;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS16;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.IMAGES;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.STYLES;
import static ma.esolutions.projet.gestion.garage.view.MainView.addFlowPaneForChartsBtns;
import static ma.esolutions.projet.gestion.garage.view.MainView.addHBoxForFilters;
import static ma.esolutions.projet.gestion.garage.view.MainView.addStackPane;
import static ma.esolutions.projet.gestion.garage.view.MainView.addVBoxForLinks;
import org.jvnet.substance.SubstanceLookAndFeel;
import org.jvnet.substance.api.ColorSchemeAssociationKind;
import org.jvnet.substance.api.ComponentState;
import org.jvnet.substance.api.SubstanceColorScheme;
import org.jvnet.substance.painter.decoration.DecorationAreaType;
import org.jvnet.substance.utils.SubstanceColorSchemeUtilities;

/**
 *
 * @author Ahmed
 * @param <T>
 */
public abstract class AbstractSidePanel<T> extends JPanel implements IRowObserver<T> {

    private static HBox createHBox() {
    return null;
    }

    private static Node createCenter() {
return null;
    }
    
    protected T theEntity;
    private JSearchField theSearchField;
    private Action theSearchAction;
    private JFXPanel thePanel;
    private BorderPane theRoot;
    public AbstractSidePanel(){
       //
        setLayout(new BorderLayout());
        thePanel = new JFXPanel();
         initComponents();
       
    }
    
    public BorderPane getRoot()
    {
        return theRoot;
    }
    protected void initFX()
    {
        thePanel.setScene(createScene());
       
    }
     private Node createSearchBar()
    {
        HBox box = new HBox(0);
        
        TextField field = new TextField();
        field.setScaleX(0.5);
        field.setScaleY(0.6);
        
      
       
       // Button b = new Button("Rechercher");
        box.getChildren().addAll(field);
        return box;
    }
    abstract  public Node createCenterUI();
     private Scene createScene()
    {
    
         theRoot = new BorderPane();
        theRoot.setStyle(" -fx-font-smoothing-type: lcd;");
    //    HBox hbox = createHBox();
      //  theRoot.setTop(hbox);
        //root.setLeft(addVBoxForLinks());
        //addStackPane(hbox);         
      //  theRoot.setCenter(createCenter());
       // theRoot.setRight(addFlowPaneForChartsBtns());
       // theRoot.setBottom(createSearchBar());
        
        
        
      //  theRoot.setCenter(createCenterUI());
        
        
      //  Button b = new Button("click");
     //   b.getStyleClass().add("button1");
      //  b.setId("font-button");
       // theRoot.setCenter(b);
        Scene  scene  =  new  Scene(theRoot, Color.BLACK);
        scene.getStylesheets().add(STYLES);
                
        
        return (scene);
    }
  public Node getPanel(Stage stage) {
        GridPane grid = new GridPane();
        grid.setVgap(10);
        grid.setHgap(10);
        grid.setPadding(new Insets(30, 30, 0, 30));
        
        int row = 0;
        
        Label textFieldLabel = new Label("TextField");
       // textFieldLabel.setFont(Font.font(24));
        GridPane.setHalignment(textFieldLabel, HPos.CENTER);
        Label passwordFieldLabel = new Label("PasswordField");
        //passwordFieldLabel.setFont(Font.font(24));
        GridPane.setHalignment(passwordFieldLabel, HPos.CENTER);
        grid.add(textFieldLabel, 1, row);
        grid.add(passwordFieldLabel, 2, row);
        row++;

        grid.add(new Label("Normal TextField / PasswordField: "), 0, row);
        grid.add(new TextField(), 1, row);
        grid.add(new PasswordField(), 2, row++);
        
        
        grid.add(new Label("Custom*Field (no additional nodes): "), 0, row);
        grid.add(new TextField(), 1, row);
        grid.add(new PasswordField(), 2, row++);
        
        grid.add(new Label("Custom*Field (w/ right node): "), 0, row);
        TextField customTextField1 = new TextField();
        //customTextField1.setRight(new ImageView(image));
        grid.add(customTextField1, 1, row);
        
        PasswordField customPasswordField1 = new PasswordField();
        //customPasswordField1.setRight(new ImageView(image));
        grid.add(customPasswordField1, 2, row++);
        
        // Custom*Field (w/ left node)
        grid.add(new Label("Custom*Field (w/ left node): "), 0, row);
        TextField customTextField2 = new TextField();
        //customTextField2.setLeft(new ImageView(image));
        grid.add(customTextField2, 1, row);
        
        PasswordField customPasswordField2 = new PasswordField();
        //customPasswordField2.setLeft(new ImageView(image));
        grid.add(customPasswordField2, 2, row++);
        
        grid.add(new Label("Custom*Field (w/ left + right node): "), 0, row);
        TextField customTextField3 = new TextField();
        //customTextField3.setLeft(new ImageView(image));
        //customTextField3.setRight(new ImageView(image));
        grid.add(customTextField3, 1, row);
        
        PasswordField customPasswordField3 = new PasswordField();
        //customPasswordField3.setLeft(new ImageView(image));
        //customPasswordField3.setRight(new ImageView(image));
        grid.add(customPasswordField3, 2, row++);
        
        return grid;
    }
    @Override
    public void tableRowSelected(T e)
    {
        this.theEntity = e;
        handleRowSelected();
        
    }
   abstract public  void handleRowSelected();
    
    public void setTheEntity(T entity)
    {
        this.theEntity = entity;
        getFieldsValues();
    }
    public T getTheEntity()
    {
        return this.theEntity;
    }
    public abstract void getFieldsValues() ;
    public abstract String getTitle() ;
    public void addCenterComponent(JComponent component)
    {
        add(new JScrollPane(component),BorderLayout.CENTER);
        setPreferredSize(component.getPreferredSize());
        
    }
    
   private void setTableFilter() {
   }
    private void initComponents() {
        setLayout(new BorderLayout());
        setFocusable(false);
        SubstanceColorScheme borderColorScheme = SubstanceColorSchemeUtilities.getColorScheme(
                this, ColorSchemeAssociationKind.BORDER, ComponentState.DEFAULT);
        setBorder(BorderFactory.createMatteBorder(1, 1, 0, 1,
                borderColorScheme.getDarkColor()));
        JToolBar theHeaderbar = new JToolBar();
        JToolBar theSecondHeaderbar = new JToolBar();
        theHeaderbar.setPreferredSize(new Dimension(200,30));
        theSecondHeaderbar.setPreferredSize(new Dimension(200,30));
        JLabel theCaption = new JLabel(getTitle()); 
        
        theCaption.setFont(theCaption.getFont().deriveFont(Font.BOLD,14));
        theCaption.setIcon(new ImageIcon(getClass().getResource(ICONS16+"preview.png")));
                
        theSearchAction = new AbstractAction("search") {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent e) {
                setTableFilter();
                System.out.println("serach");
            }


        };
        theSearchField = new JSearchField(theSearchAction);
        JPanel theTopHeader = new JPanel(new BorderLayout());
        /*headerBar.setPreferredSize(new Dimension(theSearchField.getWidth(),
                theSearchField.getHeight() + 28));*/
        theHeaderbar.add(theSearchField, "dock east, gapright 4, width 250!, height pref!");
               SubstanceLookAndFeel.setDecorationType(theSearchField, DecorationAreaType.GENERAL);

        theHeaderbar.addSeparator();
        //theHeaderbar.add(theCaption);
        
        theHeaderbar.setFloatable(false);
        theHeaderbar.setFocusable(false);
        
        theSecondHeaderbar.setFloatable(false);
        
        theSecondHeaderbar.setFocusable(false);
        
       // theTopHeader.add(theHeaderbar,BorderLayout.NORTH);
      //  theTopHeader.add(theSecondHeaderbar,BorderLayout.SOUTH);
        add(theTopHeader,BorderLayout.NORTH);
        
          Platform.runLater(()->{
            System.out.println("initfx inside side panel");
            initFX();
        });
        add(thePanel,BorderLayout.CENTER);
    }

}
