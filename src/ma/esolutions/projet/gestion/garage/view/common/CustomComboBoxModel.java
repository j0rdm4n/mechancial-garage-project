/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import javax.swing.MutableComboBoxModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import ma.esolutions.projet.gestion.garage.model.BaseEntity;

/**
 *
 * @author E-solutions
 */
public class CustomComboBoxModel extends AbstractListModel implements ComboBoxModel{

    private ArrayList theDataList;
    private Object theSelectedObject;
    
    public CustomComboBoxModel()
    {
        this(new ArrayList());
    }
    public CustomComboBoxModel(ArrayList data)
    {
       if(getSize() > 0)
       {
           theSelectedObject = getElementAt(0);
       }
       theDataList = data;
    }
    @Override
    public int getSize() {
        if(this.theDataList == null)
            return 0;
        return theDataList.size();
    }

    @Override
    public Object getElementAt(int index) {
        return theDataList.get(index);
    }

   
    @Override
    public void setSelectedItem(Object anItem) {
        this.theSelectedObject = anItem;
    }

    @Override
    public Object getSelectedItem() {
        return this.theSelectedObject;
    }

    
    


}
