/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view.common;

import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.Chart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import ma.esolutions.projet.gestion.garage.controller.MainController;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.CHART_STYLE;

/**
 *
 * @author Ahmed
 */
public abstract class AbstractXYChart implements IChart{
    
    //abstract protected X getXAxis();
    //abstract protected Y getYAxis();
    
    abstract protected String getXAxisLabel();
    abstract protected String getYAxisLabel();
    
    abstract protected String getSeriesLabel();
    abstract protected XYChart.Series getData();
    protected CategoryAxis xAxis;
    protected NumberAxis yAxis;
    
    protected MainController theController;
    protected XYChart.Series theSeries;
   
    
    public AbstractXYChart(MainController controller)
    {
       
        theController = controller;
        theSeries = new XYChart.Series();
        init();
        
    }
    public AbstractXYChart()
    {
        init();
    }
    protected NumberAxis getYAxis() {
        NumberAxis yaxis = new NumberAxis();
        yaxis.setLabel(getYAxisLabel());
        return yaxis;
    }
    protected CategoryAxis getXAxis() {
        CategoryAxis xaxis = new CategoryAxis();
        xaxis.setLabel(getXAxisLabel());
        
        return xaxis;
        
    }
    private void init()
    {
        xAxis = getXAxis();
        
        yAxis = getYAxis();
    }
    abstract protected void notifyChart();
    abstract protected Chart createChart();
    @Override
    public Chart getChart() {
        Chart theChart;
        
            theChart = createChart();
            
            return theChart;
    }
}
