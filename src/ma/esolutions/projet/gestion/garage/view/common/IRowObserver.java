/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view.common;

import java.io.Serializable;

/**
 *
 * @author Ahmed
 */
public interface IRowObserver<T> {
    
    void tableRowSelected(T e);
}
