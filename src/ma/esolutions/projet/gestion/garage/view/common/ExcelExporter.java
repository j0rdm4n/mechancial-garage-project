/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view.common;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.table.TableModel;

/**
 *
 * @author E-solutions
 */
public class ExcelExporter {

    private static TableModel model;
    private static FileWriter fileWriter;

    static public void export(String filename, JTable table) throws IOException {

        File file = new File(filename);
        model = table.getModel();
        System.out.println("export " + file.getAbsoluteFile());
        file.createNewFile();
        fileWriter = new FileWriter(file);
        int cols = model.getColumnCount();
        int rows = model.getRowCount();

        for (int j = 0; j < cols; j++) {
            fileWriter.write(model.getColumnName(j) + "\t");
        }
        
        fileWriter.write("\n");
        
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                fileWriter.write(String.valueOf(model.getValueAt(i, j)) + "\t");
            }
            fileWriter.write("\n");
        }
    }

}
