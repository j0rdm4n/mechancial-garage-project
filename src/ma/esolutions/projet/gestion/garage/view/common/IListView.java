/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view.common;

import java.util.List;
import ma.esolutions.projet.gestion.garage.controller.common.IDataPageController;
import ma.esolutions.projet.gestion.garage.model.BaseEntity;
/**
 *
 * @author Ahmed
 */
public interface IListView<T> extends IView {
    
    void init(IDataPageController<T> controller);
    void init();
    IDataPageController<T> getController();
    T getSelectedModel();
    void refreshData(List<T> data);
    boolean hasDetails();
    
}
