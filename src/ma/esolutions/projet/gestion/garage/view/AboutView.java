/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.MatteBorder;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.controller.common.IPageController;
import ma.esolutions.projet.gestion.garage.view.common.IFormView;
import ma.esolutions.projet.gestion.garage.view.common.IPageView;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXHyperlink;

/**
 *
 * @author E-solutions
 */
public class AboutView extends JDialog {

    private JPanel aboutPanel;
    private JXHeader xheader;
    private Action okAction;
    private Action siteAction;
    private JButton btnOk;

    public AboutView(JFrame parent) {
        super(parent);
        initComponents();
        this.setLocationRelativeTo(null);

    }

    public void init() {
        this.initComponents();
    }

    private void initComponents() {
        setTitle("A Propos ");
        setIconImage(new ImageIcon(getClass()
                .getResource(ApplicationView.ICONS16 + "app.png")).getImage());
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        xheader = new JXHeader();
        xheader.setTitle("<html><body><b>"
                + ApplicationView.APPNAME
                + "</b></body></html>");
        xheader.setDescription(ApplicationView.APPDESCRIPTION);
        xheader.setFont(new Font("Tahoma", 0, 12));
        xheader.setIcon(new ImageIcon(getClass().getResource(
                ApplicationView.ICONS22 + "about-icon.png")));
        xheader.setBorder(new MatteBorder(0, 0, 1, 0, Color.LIGHT_GRAY));

        okAction = new AbstractAction("Ok",
                new ImageIcon(getClass().getResource(ApplicationView.ICONS16 + "ok.png"))) {
                    @Override
                    public void actionPerformed(java.awt.event.ActionEvent e) {
                        setVisible(false);
                        dispose();
                    }
                };
        btnOk = new JButton(okAction);

        JPanel buttonPanel = new JPanel(new MigLayout("nogrid, fillx, aligny 100%, gapy unrel"));
        buttonPanel.add(btnOk, "align center");

        JPanel buttonBar = new JPanel(new BorderLayout());
        buttonBar.add(buttonPanel, BorderLayout.CENTER);

        getContentPane().add(xheader, BorderLayout.NORTH);
        getContentPane().add(new JScrollPane(buildCenterPanel()), BorderLayout.CENTER);
        getContentPane().add(buttonBar, BorderLayout.SOUTH);

        pack();
        setSize(620, 350);
        setModal(true);
        btnOk.requestFocus();
    }

    private JPanel buildCenterPanel() {
        buildActions();
        JPanel centerPanel = new JPanel(new MigLayout("insets 20 10 10 10"));
        JLabel lblAppName = new JLabel(
                ApplicationView.APPNAME +" "
                + ApplicationView.APPVERSION);
        JLabel lblCopyright = new JLabel(ApplicationView.COPYRIGHT);
        JLabel lblCopyrightLink = new JLabel("www.esolutions.ma");

        JLabel lblDetails = new JLabel(ApplicationView.APPDESCRIPTION);
        JXHyperlink xlinkSite = new JXHyperlink(siteAction);

        centerPanel.add(lblAppName, "wrap");
        centerPanel.add(lblCopyright, "wrap");
        centerPanel.add(lblCopyrightLink, "wrap");
        centerPanel.add(new JLabel(""), "wrap");
        centerPanel.add(lblDetails, "wrap");
        centerPanel.add(xlinkSite, "wrap");

        centerPanel.add(new JLabel(""), "wrap");

        return centerPanel;
    }

    private void buildActions() {
        siteAction = new AbstractAction("http://www.esolutions.ma") {
            @Override
            public void actionPerformed(ActionEvent e) {
                ApplicationView.browseUrl("http://www.esolutions.ma");
            }
        };

    }
  
    public static void showDialog() {
        AboutView aboutView = new AboutView(ApplicationController.getAppView());
        aboutView.setVisible(true);
    }
}
