/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import ma.esolutions.projet.gestion.garage.app.Session;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.Payment;
import ma.esolutions.projet.gestion.garage.model.PaymentDAO;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS16;
import ma.esolutions.projet.gestion.garage.view.common.AbstractListView;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSearchPanel;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSidePanel;
import ma.esolutions.projet.gestion.garage.view.common.EntityTableColumn;
import ma.esolutions.projet.gestion.garage.view.common.IRowObserver;
import net.miginfocom.swing.MigLayout;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author E-solutions
 */
public class PaymentsListView extends AbstractListView<Payment> {

    private PaymentSidePanel thePaymentSidePanel;
    private PaymentDAO paymentDAO;

    public PaymentsListView(List<Payment> data) {
        super(null, data);
    }

    public PaymentsListView() {

        super(null, new ArrayList());
    }

    @Override
    public AbstractSearchPanel getSearchPanel() {
        if (theSearchPanel == null) {
            theSearchPanel = new PaymentSearchPanel();
            theSearchPanel.setParentListView(this);
        }
        return theSearchPanel;
    }

    @Override
    public void addTableColumns() {
        getTableModel().addColumn(new EntityTableColumn(
                "Par", "theBy", String.class, 300));

        getTableModel().addColumn(new EntityTableColumn(
                "Type", "thePartnerType", String.class, 300));

        getTableModel().addColumn(new EntityTableColumn(
                "Total TTC", "theHTAmount", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "Total HT", "theTTCAmount", String.class, 300));

        getTableModel().addColumn(new EntityTableColumn(
                "Date", "theCashingDate", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "Objet", "thePaymentType", String.class, 300));

    }

    private JTable createCashInPerMonth() {
        JTable table = new JTable();
        DefaultTableModel dtm = new DefaultTableModel();
        table.setModel(dtm);
        dtm.addColumn("Mois");
        //dtm.addColumn("Quantité");
        dtm.addColumn("Montant");
        //  dtm.addColumn("Pourcentage");
        Map<Integer, Double> l = (new PaymentDAO()).getCashInAmmountPerMonth();

        l.keySet().stream().forEach((k) -> {
            dtm.addRow(new Object[]{k, l.get(k)});
        });

        return table;

    }

    private JTable createCashOutPerMonth() {
        JTable table = new JTable();
        DefaultTableModel dtm = new DefaultTableModel();
        table.setModel(dtm);
        dtm.addColumn("Mois");
        //dtm.addColumn("Quantité");
        dtm.addColumn("Montant");
        //  dtm.addColumn("Pourcentage");
        Map<Integer, Double> l = (new PaymentDAO()).getCashOutAmmountPerMonth();

        for (int k : l.keySet()) {

            dtm.addRow(new Object[]{k, l.get(k)});
        }
        //  dtm.addRow(rowData);

        return table;

    }

    @Override
    protected boolean hasBarChart() {
        return true;
    }

    @Override
    protected JFreeChart getBarChart() {
        DefaultCategoryDataset data = new DefaultCategoryDataset();
        Map<Integer, Double> l = (new PaymentDAO()).getCashInAmmountPerMonth();

        for (int k : l.keySet()) {

            data.setValue(l.get(k), "Entrées en caisse", String.valueOf(k));
        }

        JFreeChart objChartBar = ChartFactory.createBarChart("Ventes et réparations", "Mois", "Total", data, PlotOrientation.HORIZONTAL, true, true, false);

        barChart = objChartBar;

        return objChartBar;
    }

    protected void onNew() {
        System.out.println("on new ");
        PaymentInFormView form = new PaymentInFormView(new Payment());
        form.showDialog();
        if (this.thePaymentSidePanel != null) {
            thePaymentSidePanel.update();
        }

    }

    @Override
    protected void onDelete() {
        PaymentOutFormView o = new PaymentOutFormView(new Payment());
        o.showDialog();
    }

    protected void onReport() {
        //ApplicationView.showAskYesNo("Voulez vous visualiser les le rapport des encaissements en format pdf ?");
        paymentDAO = new PaymentDAO();
        JFrame frame = new JFrame("le rapports des encaissements");

        JPanel container = new JPanel(new BorderLayout());
        JButton pdfBtn = new JButton("générer le rapport en format pdf");
        JScrollPane containerPane = new JScrollPane(container);
        containerPane.setPreferredSize(new Dimension(900, 700));
        frame.getContentPane().add(containerPane);
        JPanel reportPanel = new JPanel(new MigLayout());
        DefaultCategoryDataset data = new DefaultCategoryDataset();
        Map<Integer, Double> l = (new PaymentDAO()).getCashInAmmountPerMonth();

        for (int k : l.keySet()) {

            data.setValue(l.get(k), "", String.valueOf(k));
        }

        JFreeChart objChartBar = ChartFactory.createBarChart("Ventes et réparations", "Mois", "Total", data, PlotOrientation.HORIZONTAL, true, true, false);
        reportPanel.add(new ChartPanel(objChartBar));
        pdfBtn.addActionListener((ActionEvent e) -> {
            Document doc = new Document(PageSize.A4.rotate());
            String filename = "caisse" + String.valueOf(year()) + String.valueOf(month()) + String.valueOf(day()) + ".pdf";
            // createReportTable(filename, "Ventes et réparations", createCashInPerMonth());

            try {
                createPdf(filename);
            } catch (IOException ex) {
                Logger.getLogger(CustomerListView.class.getName()).log(Level.SEVERE, null, ex);
            } catch (DocumentException ex) {
                Logger.getLogger(CustomerListView.class.getName()).log(Level.SEVERE, null, ex);
            }
            int yesno = ApplicationView.showAskYesNo("Voulez vous visualiser le rapport en format pdf ? ");
            if (yesno == JOptionPane.YES_OPTION) {
                viewPdf(filename);
            }
        });
        JTable table = createCashInPerMonth();
        JTable outTable = createCashOutPerMonth();
        JPanel middle = new JPanel(new MigLayout());
        JScrollPane pane = new JScrollPane(reportPanel);
        JScrollPane tablePane = new JScrollPane(table);
        JScrollPane outTablePane = new JScrollPane(outTable);

        middle.add(new JLabel("Total journalier  des entrées : " + paymentDAO.getDailyInTotal()), "wrap");
        middle.add(new JLabel("Total journalier  des sorties : " + paymentDAO.getDailyOutTotal()), "wrap");
        middle.add(new JLabel("Total journalier  de caisse   : " + paymentDAO.getDailyTotal()), "wrap");
        middle.add(outTablePane, "wrap");

        container.add(new JLabel("Rapports des encaissements "), BorderLayout.NORTH);
        container.add(pdfBtn, BorderLayout.SOUTH);

        middle.add(new JLabel("Les entrées en caisse"), "wrap");
        middle.add(tablePane, "wrap");
        middle.add(new JLabel("Les sorties de caisse"), "wrap");

        middle.add(pane, "wrap");
        pane.setPreferredSize(new Dimension(700, 500));
        tablePane.setPreferredSize(new Dimension(700, 200));
        outTablePane.setPreferredSize(new Dimension(700, 200));

        container.add(middle, BorderLayout.CENTER);
        frame.setResizable(false);
        frame.pack();
        frame.setVisible(true);

    }

    @Override
    public void showEntitySummary() {
    }

    @Override
    public void showEntityActions() {
    }

    @Override
    public void onReverseTax() {
        super.onReverseTax();
        PaymentDAO paymentDAO = new PaymentDAO();
        Payment selected = getSelectedModel();

      /*  if (selected != null) {
            int ask = ApplicationView.showAskYesNo("Voulez vous reverser la taxe correspondante à ce payement ?");
            if (ask == JOptionPane.NO_OPTION) {
                return;
            }

            if(selected.getTheReversedState().equalsIgnoreCase("oui"))
            {
                ApplicationView.showErrorMsg("La taxe correspondante à ce payement est déja reversé !");
                return;
            }
            float ttax = (float) (selected.getTheAmountTTC() - selected.getTheTTCAmount());
            Payment taxed = new Payment();
            taxed.setTheCashingDate(new Date());
            taxed.setThePaymentType("taxe");
            taxed.setThePartnerType("utilisateur");
            taxed.setTheBy(Session.getSession().getTheCurrentUser().toString());
            taxed.setTheTTCAmount((-1) * ttax);
            taxed.setTheHTaxAmount((-1) * ttax);
            taxed.setTheReversedState("oui");
            paymentDAO.create(taxed);
            
            selected.setTheReversedState("oui");
            paymentDAO.update(selected);
            ApplicationView.showErrorMsg("Une valeur de " + ttax +" a été reversé avec succés !");
            return;
        }*/
        int askYesNo = ApplicationView.showAskYesNo("Cette opération va décaisser le montant de taxe  à reverser à l'état ! ,continuer ?");
        if (askYesNo == JOptionPane.NO_OPTION) {
            return;
        }

        List<Payment> l = paymentDAO.getNonTaxedPayments();
        if (l.isEmpty()) {
            ApplicationView.showErrorMsg("toutes les taxes sont déja reversés !");
            return;
        }
        float ttax = Math.abs((float) l.stream().filter(e-> e.getThePaymentType().equalsIgnoreCase("vente") || e.getThePaymentType().equalsIgnoreCase("réparation et vente")).mapToDouble(e -> e.getTheAmountTTC() - e.getTheTTCAmount()).sum());
        float atax = Math.abs((float) l.stream().filter(e-> e.getThePaymentType().equalsIgnoreCase("achat")).mapToDouble(e -> e.getTheAmountTTC() - e.getTheTTCAmount()).sum());

        System.out.println("ventes tax " + ttax);
        System.out.println("achats tax" + atax);
        
        if ((ttax - atax) == 0) {
            ApplicationView.showErrorMsg("Vous avez un montant 0 de taxe  à reverser !");
            return;
        }

        l.forEach(p -> {
            p.setTheReversedState("oui");
            paymentDAO.update(p);
        });
        System.out.println(" total tax equals " + ttax);
        Payment taxed = new Payment();
        taxed.setTheCashingDate(new Date());
        taxed.setThePaymentType("taxe");
        taxed.setThePartnerType("utilisateur");
        taxed.setTheBy(Session.getSession().getTheCurrentUser().toString());
        taxed.setTheTTCAmount((-1) * Math.abs(ttax - atax));
        taxed.setTheHTaxAmount((-1) *Math.abs(ttax - atax));
        taxed.setTheReversedState("oui");
        paymentDAO.create(taxed);

    }

    @Override
    protected boolean hasReverseTax() {
        return true;
    }

    @Override
    public void fireUpdateEvent(MouseEvent e) {
        System.out.println("update event");
        if (thePaymentSidePanel != null) {
            thePaymentSidePanel.update();
        }
    }

    @Override
    public AbstractSidePanel getSidePanel() {
        if (thePaymentSidePanel == null) {
            thePaymentSidePanel = new PaymentSidePanel();
            this.setTheRowObserver(thePaymentSidePanel);
        }
        return thePaymentSidePanel;
    }

    @Override
    protected void onRefresh() {
        super.onRefresh();
        if (thePaymentSidePanel != null) {
            thePaymentSidePanel.update();
        }
    }

    @Override
    public List<Payment> getTheTableData() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getTitle() {
        return "Paiements ";
    }

    @Override
    public String getIconPath() {
        return ApplicationView.ICONS22 + "caisse-icon.png";
    }

    @Override
    public Payment getModel() {
        return new Payment();
    }

    @Override
    public void delete(Payment e) {
        return;
    }

    @Override
    public SecureCommand<Payment> getRelatedFormCommand(Payment e) {
        return null;
    }

}
