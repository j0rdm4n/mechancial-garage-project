/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.controller.commands.RoleAddCommand;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.Role;
import ma.esolutions.projet.gestion.garage.model.RoleDAO;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS22;
import ma.esolutions.projet.gestion.garage.view.common.AbstractListView;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSearchPanel;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSidePanel;
import ma.esolutions.projet.gestion.garage.view.common.EntityTableColumn;
import ma.esolutions.projet.gestion.garage.view.common.IRowObserver;

/**
 *
 * @author E-solutions
 */
public class RoleListView extends AbstractListView<Role> {

    private RoleSidePanel theRoleSidePanel;

    public RoleListView(List<Role> data) {
        super(null, data);
    }

    public RoleListView() {
        super(null,new ArrayList());
    }
    @Override
    public AbstractSearchPanel getSearchPanel()
    {
        if(theSearchPanel == null)
        {
            theSearchPanel = new RoleSearchPanel();
            theSearchPanel.setParentListView(this);
        }
        return theSearchPanel;
    }
    @Override
    public void addTableColumns() {
        getTableModel().addColumn(new EntityTableColumn(
                "Nom du role", "theRoleName", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "Permissions", "theRolePermissions", String.class, 300));
        /*    getTableModel().addColumn(new EntityTableColumn(
         "Utilisateurs", "theUsers", String.class, 300));*/

    }

    @Override
    public void showEntitySummary() {
    }

    @Override
    public void showEntityActions() {
    }

    @Override
    public void fireUpdateEvent(MouseEvent e) {
        Role r = (Role) getSelectedModel();
        if (r != null) {
            ApplicationController.getAppView().fireRoleUpdateEvent(r);
        }
    }

    @Override
    public AbstractSidePanel getSidePanel() {
        if (theRoleSidePanel == null) {
            theRoleSidePanel = new RoleSidePanel();
        }

        this.setTheRowObserver(theRoleSidePanel);
        return theRoleSidePanel;
    }

    @Override
    public List<Role> getTheTableData() {
        return null;
    }

    @Override
    public String getTitle() {
        return "Gestion des roles";
    }

    @Override
    public String getIconPath() {
        return ICONS22 + "users-icon.png";
    }

    @Override
    public void setTheRowObserver(IRowObserver theRowObserver) {
        this.theRowObserver = theRowObserver;
    }

    @Override
    public Role getModel() {
        return new Role();
    }

    @Override
    public void delete(Role e) {
        (new RoleDAO()).remove(e);
    }

    @Override
    public SecureCommand<Role> getRelatedFormCommand(Role e) {
        return new RoleAddCommand(e);
    }

}
