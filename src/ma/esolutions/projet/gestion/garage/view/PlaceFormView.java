/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import java.awt.Component;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.model.ItemPlace;
import ma.esolutions.projet.gestion.garage.model.ItemPlaceDAO;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS16;
import ma.esolutions.projet.gestion.garage.view.common.AbstractFormView;
import ma.esolutions.projet.gestion.garage.view.common.JTextFieldExtended;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author E-solutions
 */
public class PlaceFormView extends AbstractFormView<ItemPlace>{
    private boolean isNewModel;
    private JPanel thePanel;
    private JLabel theNewPlaceItem;
    private JLabel theNewPlaceLabel;
    private JTextFieldExtended theNewPlaceTextfield;
    private JButton theNewPlacementBtn;
    private JButton theCloseBtn;
    private JButton theSaveBtn;
    private ItemPlace thePlace;
    private final ItemPlaceDAO theItemPlaceDAO;

    public PlaceFormView(ItemPlace place) {
        super(ApplicationController.getAppView());
        if(place.getId() == null)
        {
            isNewModel = true;
        }
        else
        {
            isNewModel = false;
        }
        thePlace = place;
        theItemPlaceDAO = new ItemPlaceDAO();
    }

    @Override
    public void buildUI() {
        initComponents();
        addPageToForm("Nouveau Placement",buildPage());
        pack();
        if( ! isNewModel )
            popFields();
       
        
    }
    private JPanel buildPage()
    {
       thePanel = new JPanel(new MigLayout());
       theNewPlaceLabel = new JLabel("Nom du Placement");
       theNewPlaceTextfield = new JTextFieldExtended(90);
       
     //  theCloseBtn = new JButton("Fermer");
      // theSaveBtn = new JButton("Enregistrer");
       
       thePanel.add(theNewPlaceLabel,"split, gaptop 60,left,width 300");
       thePanel.add(theNewPlaceTextfield,"right ,gaptop 60,right,width 300,wrap");
       
     //  thePanel.add(theCloseBtn,"split,gaptop 30,left,width 300");
   //    thePanel.add(theSaveBtn,"right,gaptop 30,right,width 300");
       return thePanel;
    }
    @Override
    public void clearFields()
    {
        this.theNewPlaceTextfield.setText("");
    }
    @Override
    public void popFields()
    {
     
        if(thePlace != null)
        {
            this.theNewPlaceTextfield.setText(thePlace.getTheItemPlaceName());
        }
    }
    @Override
    public boolean validateForm()
    {
       if(theNewPlaceTextfield.getText().equalsIgnoreCase(""))
       {
           setStatusbarMsg("Le nom d'emplacement est obligatoire ! ");
           return false;
       }
       else if(theItemPlaceDAO.checkIfPlaceExistsWithName(theNewPlaceTextfield.getText()))
       {
           if(! isNewModel )
           {
                setStatusbarMsg("Un nom d'emplacement avec ce nom existe déja !");
                return false;
           }
            
       }
        return true;
    }
    @Override
    public ItemPlace getEntity() {
        return thePlace;
    }

    @Override
    public String getFormIconPath() {
      return   ICONS16 + "inventory.png";
    }

    @Override
    public String getFormTitle() {
        if(isNewModel)
        {
            return "Ajouter Nouveau placement";
        }
        else
        {
            return "Mettre à jour Placement";
        }
    }

    @Override
    public void onHelp() {
    }


    @Override
    public ItemPlace pushFields() {
        System.out.println("pushing fields");
        ItemPlace p1 ;
        if(isNewModel)
        p1 = new ItemPlace();
        else
            p1 = thePlace;
        p1.setTheItemPlaceName(this.theNewPlaceTextfield.getText());
        thePlace = p1;
        return  p1;
    }

    @Override
    public String getIconPath() {
            return   ICONS16 + "inventory.png";
    }

    @Override
    public Component asComponent() {
            return thePanel;
    }
    
}
