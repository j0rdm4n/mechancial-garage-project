/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import com.alee.utils.SwingUtils;
import java.awt.BorderLayout;
import java.util.Map;
import java.util.stream.Collectors;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javax.swing.JButton;
import javax.swing.JPanel;
import ma.esolutions.projet.gestion.garage.model.Item;
import ma.esolutions.projet.gestion.garage.model.ItemDAO;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.STYLES;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSidePanel;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author E-solutions
 */
final class InventorySidePanel extends AbstractSidePanel<Item> {

    private ItemDAO itemDAO;

    public InventorySidePanel() {
        super();
        Node center = createCenterUI();

        Platform.runLater(() -> {
            //getRoot().setTop(createDynamicBar());

            getRoot().setCenter(center);

        });
    }

    @Override
    public void getFieldsValues() {
    }

    @Override
    public String getTitle() {
        return "";
    }

    @Override
    public void handleRowSelected() {
        Node center = createUI();

        Platform.runLater(() -> {
            //getRoot().setTop(createDynamicBar());
            if (this.theEntity != null) {
                getRoot().setCenter(center);
            }
        });
    }

    public void update() {
        Node center = createCenterUI();

        Platform.runLater(() -> {
            //getRoot().setTop(createDynamicBar());

            getRoot().setCenter(center);

        });
    }

    public Node createUI() {
        GridPane grid = new GridPane();
        int row = 1;
        int col = 2;

        if (theEntity != null) {
            grid.setStyle(STYLES);
            grid.setAlignment(Pos.CENTER);
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(25, 25, 25, 25));
            Label itemName = new Label("Nom d'article");
            grid.add(itemName, 0, row);
            Label itemNamev = new Label(this.theEntity.getTheItemName());
            grid.add(itemNamev, col, row++);

            Label place = new Label("Emplacement ");
            grid.add(place, 0, row);

            Label placev = new Label(theEntity.getTheItemPlace().toString());
            grid.add(placev, col, row++);

            Label q = new Label("Quantité ");
            grid.add(q, 0, row);

            Label qv = new Label("" + theEntity.getTheItemQuantity());
            grid.add(qv, col, row++);

            Label prix = new Label("Prix unitaire TTC");
            grid.add(prix, 0, row);
            Label prixv = new Label("" + theEntity.getTheUnitPrice());
            grid.add(prixv, col, row++);

        }
        return grid;
    }

    @Override
    public Node createCenterUI() {
        itemDAO = new ItemDAO();

        Map<String, Number> d = itemDAO.getStockQuantityPerItem();
        ObservableList<PieChart.Data> pieChartData
                = FXCollections.observableArrayList();
     
        float sum = (float) d.keySet().stream().mapToDouble(k -> d.get(k).doubleValue()).sum();
        d.forEach((k, v) -> {
            pieChartData.add(new PieChart.Data(k  , v.doubleValue()));
        });

        final PieChart chart = new PieChart(pieChartData);
        chart.setTitle("Stock ");
        chart.setLabelLineLength(10);
        chart.setLegendSide(Side.TOP);
        
     
         GridPane gp = new GridPane();
        int row = 0;
        int col = 0;
        gp.add(chart, col, ++row);
     
        
        return gp;

    }

}
