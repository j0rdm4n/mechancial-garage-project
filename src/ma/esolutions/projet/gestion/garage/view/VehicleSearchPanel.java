/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSearchPanel;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author E-solutions
 */
public class VehicleSearchPanel extends AbstractSearchPanel {

    private JTextField clientTxt;
    private JTextField chassisTxt;

    @Override
    public JPanel buildSearchForm() {
        JPanel searchPanel = new JPanel(new MigLayout());
        clientTxt = new JTextField();
        chassisTxt = new JTextField();
        searchPanel.add(new JLabel("Nom de client"), "split,left,width 300");
        searchPanel.add(clientTxt,"right,width 300,wrap");
        searchPanel.add(new JLabel("Numéro du chassis "), "split,left,width 300");
        searchPanel.add(chassisTxt,"right,width 300,wrap");
        return searchPanel;
    }

    @Override
    public String buildSearchFilter() {
        StringBuilder filter = new StringBuilder();
        filter.append("theVehicleOwner");
        filter.append(":like:");
        filter.append(clientTxt.getText());
        filter.append(";");
        
        filter.append("theVehicleIdentificationNumber");
        filter.append(":like:");
        filter.append(chassisTxt.getText());
        filter.append(";");
        
        return filter.toString();
    }

}
