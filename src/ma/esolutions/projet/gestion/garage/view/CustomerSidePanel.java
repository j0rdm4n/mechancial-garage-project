/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.util.List;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import ma.esolutions.projet.gestion.garage.model.Customer;
import ma.esolutions.projet.gestion.garage.model.CustomerDAO;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.IMAGES;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.STYLES;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSidePanel;

/**
 *
 * @author Ahmed
 */
class CustomerSidePanel extends AbstractSidePanel<Customer> {

    private JFXPanel thePanel;

    public CustomerSidePanel() {
        super();
           Node center = createCenterUI();

        Platform.runLater(() -> {
            //getRoot().setTop(createDynamicBar());
            // if (this.theEntity != null) {
            getRoot().setCenter(center);
            //}
        });
    }

    public void update() {
        Node center = createCenterUI();

        Platform.runLater(() -> {
            //getRoot().setTop(createDynamicBar());
            // if (this.theEntity != null) {
            getRoot().setCenter(center);
            //}
        });
    }
    @Override
    public void getFieldsValues() {
    }

    @Override
    public String getTitle() {
        return "";
    }

    @Override
    public void handleRowSelected() {
     //   System.out.println(this.theEntity);
//        Class entityClass = theEntity.getClass();
        //   for(Field f:entityClass.getFields())
        //   System.out.println("filed name " + f.getName());

        Node center = createUI();

        Platform.runLater(() -> {
            //getRoot().setTop(createDynamicBar());
            if (this.theEntity != null) {
                getRoot().setCenter(center);
            }
        });
    }

    private Node createDynamicBar() {
        GridPane grid = new GridPane();
        grid.setId("topGrid");
        grid.setVgap(0);
        grid.setHgap(0);

        int col = 0;
        int row = 0;
        Hyperlink delete = new Hyperlink();
        delete.setGraphic(new ImageView(IMAGES + "delete_26.png"));
        grid.add(delete, col++, row);
        Hyperlink edit = new Hyperlink();
        edit.setGraphic(new ImageView(IMAGES + "delete_26.png"));
        grid.add(edit, col++, row);
        Hyperlink exporter = new Hyperlink();

        exporter.setGraphic(new ImageView(IMAGES + "delete_26.png"));
        grid.add(exporter, col++, row);
        return grid;
    }

    private Node createCustomBar() {
        GridPane grid = new GridPane();
        grid.setVgap(0);
        grid.setHgap(0);
        //grid.setGridLinesVisible(true);
        grid.setPadding(new Insets(0, 0, 0, 0));
        int col = 0;
        int row = 0;

        Button btnEdit = new Button("Modifier");

        btnEdit.setScaleX(0.7);
        btnEdit.setScaleY(0.7);
        btnEdit.setGraphic(new ImageView(IMAGES + "delete_26.png"));
        grid.add(btnEdit, col++, row);

        Button btnDelete = new Button("Supprimer");

        btnDelete.setScaleY(0.7);
        btnDelete.setScaleX(0.7);
        btnDelete.setGraphic(new ImageView(IMAGES + "delete_26.png"));
        grid.add(btnDelete, col++, row);

        Button btnExcel = new Button("Exporter");
        btnExcel.setGraphic(new ImageView(IMAGES + "delete_26.png"));
        btnExcel.setScaleY(0.7);
        btnExcel.setScaleX(0.7);
        grid.add(btnExcel, col++, row);

        return grid;
    }

    private void buildFxUI() {

    }

    @Override
    public Node createCenterUI() {
        CategoryAxis xAxis = new CategoryAxis();
        xAxis.setAutoRanging(true);
         int nbr = 10;
        xAxis.setTickLength(1);
        NumberAxis yAxis = new NumberAxis();
        yAxis.setTickLength(1);
        BarChart<String, Number> bc
                = new BarChart<>(xAxis, yAxis);
       // bc.setTitle("Caisse");
        xAxis.setLabel("Clients");
        yAxis.setLabel("total achats");
        bc.setBarGap(3);
        bc.setCategoryGap(5);
        xAxis.setTickLabelRotation(90);
        XYChart.Series series1 = new XYChart.Series();
       // series1.setName("Caisse");
        CustomerDAO customerDAO = new CustomerDAO();
       
        List<Customer> l = customerDAO.getTopNCustomers(nbr);
        l.forEach(c->{  
            series1.getData().add(new XYChart.Data(c.toString(), Math.abs(c.getTheTotal())));
        });
        
       
        
        bc.getData().add(series1);
        GridPane gp = new GridPane();
        int row = 0;int col = 0;
        
        gp.add(new Label("Top " + nbr + " clients"),col,row);
        row++;
        gp.add(bc,col,row);
        return gp;
        
    }

    public Node createUI() {
        GridPane grid = new GridPane();
        int row = 1;
        int col = 2;

        if (theEntity != null) {
            grid.setStyle(STYLES);
            grid.setAlignment(Pos.CENTER);
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(25, 25, 25, 25));
            Label firstName = new Label("Prénom");
            grid.add(firstName, 0, row);
            Label firstNamev = new Label(this.theEntity.getTheFirstname());
            grid.add(firstNamev, col, row++);

            Label lastName = new Label("Nom ");
            grid.add(lastName, 0, row);

            Label lastNamev = new Label(theEntity.getTheLastname());
            grid.add(lastNamev, col, row++);

            Label addr = new Label("Addresse ");
            grid.add(addr, 0, row);

            Label addrv = new Label(theEntity.getTheAddress());
            grid.add(addrv, col, row++);

            Label ville = new Label("Ville");
            grid.add(ville, 0, row);
            Label villev = new Label(theEntity.getCity());
            grid.add(villev, col, row++);

            Label code = new Label("Code postal");
            grid.add(code, 0, row);
            Label codev = new Label(theEntity.getPostalcode());
            grid.add(codev, col, row++);

            Label tel = new Label("Télphone");
            grid.add(tel, 0, row);

            Label telv = new Label(theEntity.getThePhone());
            grid.add(telv, col, row++);

            Label mobile = new Label("Mobile");
            grid.add(mobile, 0, row);

            Label mobilev = new Label(theEntity.getTheMobile());
            grid.add(mobilev, col, row++);

            Label fax = new Label("Fax");
            grid.add(fax, 0, row);

            Label faxv = new Label(theEntity.getTheFax());
            grid.add(faxv, col, row++);

            Label email = new Label("Email");
            grid.add(email, 0, row);

            Label emailv = new Label(theEntity.getTheEmail());
            grid.add(emailv, col, row++);

            Label status = new Label("status");
            grid.add(status, 0, row);


           
        }
        return grid;
    }

}
