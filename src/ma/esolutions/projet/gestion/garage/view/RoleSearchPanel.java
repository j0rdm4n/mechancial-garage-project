/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSearchPanel;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author E-solutions
 */
public class RoleSearchPanel extends AbstractSearchPanel{
    private JTextField roleTxt;

    @Override
    public JPanel buildSearchForm() {
        JPanel searchPanel = new JPanel(new MigLayout());
         roleTxt = new JTextField();
        
        searchPanel.add(new JLabel("nom de role"),"gaptop 10,split,left,width 300");
        searchPanel.add(roleTxt,"gaptop 10,right,width 300,wrap");
        return searchPanel;
    }

    @Override
    public String buildSearchFilter() {
        
        StringBuilder filter = new StringBuilder();
        filter.append("theRoleName");
        filter.append(":like:");
        filter.append(roleTxt.getText());
        filter.append(";");
        return filter.toString();
    }
    
}
