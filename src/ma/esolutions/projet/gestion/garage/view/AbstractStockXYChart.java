/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.util.Map;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import ma.esolutions.projet.gestion.garage.controller.MainController;
import ma.esolutions.projet.gestion.garage.view.common.AbstractXYChart;

/**
 *
 * @author born2code
 */
abstract public class AbstractStockXYChart extends AbstractXYChart {

    protected MainController theController;
    protected XYChart.Series theSeries;

    public AbstractStockXYChart(MainController controller) {
        super();
        theController = controller;
        theSeries = new XYChart.Series();

    }

    @Override
    protected CategoryAxis getXAxis() {
        CategoryAxis xaxis = new CategoryAxis();
        xaxis.setLabel(getXAxisLabel());

        return xaxis;
    }

    @Override
    protected String getXAxisLabel() {
        return "Articles";
    }

    @Override
    protected String getSeriesLabel() {
        return "Quantités en stock par article";
    }

    @Override
    protected NumberAxis getYAxis() {
        NumberAxis yaxis = new NumberAxis();
        yaxis.setLabel(getYAxisLabel());
        return yaxis;
    }

    @Override
    protected XYChart.Series getData() {
        Map<String, Number> mm = theController.getStockQuantityPerItem();
        theSeries = new XYChart.Series();

        for (Object m : mm.keySet().toArray()) {
            System.out.println(m.toString());
            theSeries.getData().add(new XYChart.Data(m.toString(), mm.get(m)));
        }
        /*mm.forEach((it)->{
         //System.out.println(it);
         //theSeries.getData().add(new XYChart.Data<>(it,mm.get(it)));
         });*/

        return theSeries;
    }

    @Override
    protected String getYAxisLabel() {
        return "Quantité en stock";
    }

    @Override
    public String getTitle() {
        return "Stock";
    }

}
