/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import ma.esolutions.projet.gestion.garage.controller.commands.RepairOrderOpenCommand;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.Customer;
import ma.esolutions.projet.gestion.garage.model.CustomerDAO;
import ma.esolutions.projet.gestion.garage.model.Item;
import ma.esolutions.projet.gestion.garage.model.ItemDAO;
import ma.esolutions.projet.gestion.garage.model.MinimalStockException;
import ma.esolutions.projet.gestion.garage.model.NegativeStockException;
import ma.esolutions.projet.gestion.garage.model.Payment;
import ma.esolutions.projet.gestion.garage.model.PaymentDAO;
import ma.esolutions.projet.gestion.garage.model.RepairInvoice;
import ma.esolutions.projet.gestion.garage.model.RepairInvoiceDAO;
import ma.esolutions.projet.gestion.garage.model.RepairOrder;
import ma.esolutions.projet.gestion.garage.model.RepairOrderDAO;
import ma.esolutions.projet.gestion.garage.model.SalesOrderLine;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS22;
import ma.esolutions.projet.gestion.garage.view.common.AbstractListView;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSearchPanel;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSidePanel;
import ma.esolutions.projet.gestion.garage.view.common.EntityTableColumn;
import ma.esolutions.projet.gestion.garage.view.common.IRowObserver;

/**
 *
 * @author E-solutions
 */


public class RepairOrderListView extends AbstractListView<RepairOrder> {

    private RepairOrderSidePanel theRepairOrderSidePanel;

    public RepairOrderListView(List<RepairOrder> data) {
        super(null, data);
    }

    @Override
    public AbstractSearchPanel getSearchPanel() {
        if (theSearchPanel == null) {
            theSearchPanel = new RepairSearchPanel();
            theSearchPanel.setParentListView(this);
        }
        return theSearchPanel;
    }

    @Override
    public void addTableColumns() {
        getTableModel().addColumn(new EntityTableColumn(
                "Date de début", "theStartDate", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "Date de fin", "theClosedDate", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "Client", "theCustomer", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "Prix d'Oeuvre", "theLaborCost", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "Status", "theStatus", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "Payement", "thePaymentType", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn("Facturé ?", "theInvoicedStatus", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn("Livré ?", "theDeliveredStatus", String.class, 300));

        getTableModel().addColumn(new EntityTableColumn("Encaissement de facture", "thePaymentStatus", String.class, 300));

    }

    @Override
    public void showEntitySummary() {
    }

    @Override
    public void showEntityActions() {
    }

    @Override
    public void fireUpdateEvent(MouseEvent e) {
    }

    @Override
    public AbstractSidePanel getSidePanel() {
        if (theRepairOrderSidePanel == null) {
            theRepairOrderSidePanel = new RepairOrderSidePanel();
        }
        this.setTheRowObserver(theRepairOrderSidePanel);
        return theRepairOrderSidePanel;
    }

    @Override
    public List<RepairOrder> getTheTableData() {
        return null;
    }

    @Override
    public String getTitle() {
        return "Gestion des Réparations";
    }

    @Override
    public String getIconPath() {
        return ICONS22 + "list-repair-icon.png";
    }

    @Override
    public void setTheRowObserver(IRowObserver theRowObserver) {
        this.theRowObserver = theRowObserver;
    }

    @Override
    public RepairOrder getModel() {
        return new RepairOrder();
    }

    @Override
    public void delete(RepairOrder e) {
        (new RepairOrderDAO()).remove(e);
    }

    @Override
    public SecureCommand<RepairOrder> getRelatedFormCommand(RepairOrder e) {
        return new RepairOrderOpenCommand(e);
    }

    @Override
    protected boolean hasInvoice() {
        return true;
    }

    @Override
    protected boolean hasInvoicePrint() {
        return true;
    }

    @Override
    protected boolean hasCash() {
        return true;
    }

    protected void onPayment() {
        RepairOrder ro = getSelectedModel();
        if (ro == null) {
            ApplicationView.showErrorMsg("Selectionnez une commande facturée pour l'encaisser ! ");
            return;
        }
        if (ro.getTheInvoicedStatus().equalsIgnoreCase("non")) {
            int yesno = ApplicationView.showAskYesNo("Cette commande n'est pas encore facturée.Voulez vous la facturer ?");
            if (yesno == JOptionPane.NO_OPTION) {
                return;
            }
            generateInvoice(ro);

        }
        if (ro.getTheInvoicedStatus().equalsIgnoreCase("oui")) {
            if (ro.getThePaymentStatus().equalsIgnoreCase("non")) {
                if (ro.getThePaymentType().equalsIgnoreCase("espéces")) {
                    notifyPayment(ro);

                }

                if (ro.getThePaymentType().equalsIgnoreCase("crédit")) {
                    notifyCredit(ro);

                }
                ro.setThePaymentStatus("oui");
                (new RepairOrderDAO()).update(ro);
                ApplicationView.showErrorMsg("Encaissement effectué avec succés !");
            } else {
                ApplicationView.showErrorMsg("Vous avez déja encaissé la facture de cette commande !");
            }

        }
    }

    protected void onPrintInvoice() {

        RepairOrder ro = getSelectedModel();

        if (ro == null) {
            ApplicationView.showErrorMsg("Selectionnez une commande pour imprimer sa facture !");
            return;
        }
        if (ro.getTheInvoicedStatus().equalsIgnoreCase("non")) {
            ApplicationView.showAskYesNo("Cette commande n'as pas de facture !");
            return;
        }
        if (ro.getTheInvoicedStatus().equalsIgnoreCase("oui")) {
            printInvoice();
        }
    }

    protected void onInvoice() {
        RepairOrder e = getSelectedModel();
        if (e == null) {
            ApplicationView.showErrorMsg("Selectionnez une commande pour la facturer !");
        } else {
            if (e.getTheInvoicedStatus().equalsIgnoreCase("oui")) {
                ApplicationView.showErrorMsg("Cette commande est déja facturé");
            } else {
                generateInvoice(e);
            }

        }
    }

    @Override
    protected boolean hasBilling() {
        return true;
    }

    public void notifyCredit(RepairOrder ro) {
        Customer c = ro.getTheCustomer();
        c.setTheCredit(c.getTheCredit() + ro.getTheTotalTTC());
        System.out.println("setting the client credit " + c.getTheCredit());
        (new CustomerDAO()).update(c);
    }

    @Override
    protected void onBilling() {

        RepairOrder ro = getSelectedModel();
        if (ro == null) {
            ApplicationView.showErrorMsg("Selectionnez une commande  pour procéder à la livraison ! ");
            return;
        }

        if (ro.getTheDeliveredStatus().equalsIgnoreCase("oui")) {
            int noyes = ApplicationView.showAskYesNo("Voulez vous imprimer le bon ?");
            if (noyes == JOptionPane.YES_OPTION) {
                printBill(ro);
            }
            return;

        }
        if (ro.getTheInvoicedStatus().equalsIgnoreCase("non")) {
            int yesno = ApplicationView.showAskYesNo("Cette commande n'est pas encore facturée.Voulez vous la facturer ?");
            if (yesno == JOptionPane.NO_OPTION) {
                return;
            }
            generateInvoice(ro);

            if (ro.getThePaymentStatus().equalsIgnoreCase("non")) {
                int yesno2 = ApplicationView.showAskYesNo("La facture de cette commande n'est pas encore encaissée.Voulez vous l'encaisser ?");
                if (yesno2 == JOptionPane.YES_OPTION) {
                    notifyPayment(ro);
                }
            }

        }
        if (ro.getTheInvoicedStatus().equalsIgnoreCase("oui")) {
            try {
                //if(ro.getEncaissementStatus())
                notifyStock(ro);
            } catch (NegativeStockException ex) {
                Logger.getLogger(RepairOrderListView.class.getName()).log(Level.SEVERE, null, ex);
            } catch (MinimalStockException ex) {
                Logger.getLogger(RepairOrderListView.class.getName()).log(Level.SEVERE, null, ex);
                int askYesNo = ApplicationView.showAskYesNo("Voulez vous créer une notification de commande d'achat ?");
                if (askYesNo == JOptionPane.YES_OPTION) {
                    ApplicationView.createAndShowPurchaseNotification();

                }

            }
            ro.setTheDeliveredStatus("oui");
            ApplicationView.showErrorMsg("Le bon de livraison est géneré avec succés !");
            int noyes = ApplicationView.showAskYesNo("Voulez vous imprimer le bon ?");
            if (noyes == JOptionPane.YES_OPTION) {
                printBill(ro);
            }
        }

    }

    private void printBill(RepairOrder ro) {

    }

    private void createBill(RepairOrder so) {

    }

    @Override
    protected boolean hasCloseRepair() {
        return true;
    }

    protected void onReport() {
        ApplicationView.showAskYesNo("Voulez vous visualiser le rapport des réparations en format pdf ? ");
        throw new UnsupportedOperationException("implement repartions pdf report");
    }

    @Override
    protected void onCloseRepair() {
        RepairOrder o = getSelectedModel();
        if (o == null) {

            ApplicationView.showErrorMsg("Pour fermer une commande ,selectionnez la ligne correspondante");

            return;
        }
        if (o.getTheStatus().equalsIgnoreCase("fermée")) {
            ApplicationView.showErrorMsg("Cette commande est déja fermée");

            return;
        }
        o.setTheStatus("fermée");
        o.setTheClosedDate(new Date());
        (new RepairOrderDAO()).update(o);
        refreshData();
        ApplicationView.showErrorMsg("La commande de réparation N° " + o.getId() + " a été fermée avec succés");
        generateInvoice(o);
    }

    private void generateInvoice(RepairOrder e) {
        if (e.getTheInvoicedStatus().equalsIgnoreCase("non")) {
            RepairInvoice oi = new RepairInvoice();
            oi.setTheCustomer(e.getTheCustomer());
            oi.setTheInvoiceDate(new Date());
            oi.setTheRepairOrder(e);
            oi.setThePayementType(e.getThePaymentType());
            oi.setTheTotal(e.getTheTotalTTC());

            oi.setTheLaborPrice(e.getTheLaborCost());
            (new RepairInvoiceDAO()).create(oi);
            e.setTheInvoicedStatus("oui");
            (new RepairOrderDAO()).update(e);
            //refreshData();
            int YesNo = ApplicationView.showAskYesNo("Facture generée avec succés ,voullez vous l'imprimer ? ");
            if (YesNo == JOptionPane.YES_OPTION) {
                printInvoice();
            }

        }
    }

    private void printInvoice() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private boolean isItemAvailable(Item item, int quantity) {
        if (item.getTheItemQuantity() < quantity) {
            return false;
        }
        return true;
    }

    public void notifyStock(RepairOrder ro) throws NegativeStockException, MinimalStockException {
        System.out.println("updating stock !!!");
        Item item;
        for (SalesOrderLine ol : ro.getTheOrderLines()) {
            item = ol.getTheOrderedItem();
            if (item != null) {

                item.setTheItemQuantity(item.getTheItemQuantity() - ol.getTheQuantity());

                (new ItemDAO()).update(item);
            }
        }
    }

    public void notifyPayment(RepairOrder ro) {
        System.out.println("notifying the payment");
        Item item = null;
        int quantity = 0;
        float price = 0;
        float sum = 0;
        float sumht = 0;
        for (SalesOrderLine ol : ro.getTheOrderLines()) {
            item = ol.getTheOrderedItem();
            quantity = ol.getTheQuantity();
            price = (float) item.getTheSalesPriceTTC();
            System.out.println("the total ttc quantity " + quantity * price);
            sumht += (item.getTheUnitPrice() * quantity);
            sum += price * quantity;
        }
        sum += ro.getTheLaborCost();
        sumht += (ro.getTheLaborCost() / (1 + 20.0 / 100.0));
        ro.setTheTotal((float) sum);

        System.out.println("the repair order sum price " + sum);
        if (ro.getThePaymentType().equalsIgnoreCase("crédit")) {
            Customer co = ro.getTheCustomer();
            co.setTheCredit(co.getTheCredit() + sum);
            (new CustomerDAO()).update(co);
            return;
        }
        Payment p = new Payment();
        p.setThePaymentType("réparation et vente");
        p.setTheHTaxAmount(sumht);
        p.setTheTTCAmount(sum);
        p.setTheCashingDate(new Date());
        System.out.println("customer " + ro.getTheCustomer());
        p.setTheCustomer(ro.getTheCustomer());
        (new PaymentDAO()).create(p);
    }

}
