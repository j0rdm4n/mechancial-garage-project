/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.YES_OPTION;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.controller.commands.SalesOrderCommand;
import ma.esolutions.projet.gestion.garage.controller.common.ICommand;
import ma.esolutions.projet.gestion.garage.model.Customer;
import ma.esolutions.projet.gestion.garage.model.CustomerDAO;
import ma.esolutions.projet.gestion.garage.model.Item;
import ma.esolutions.projet.gestion.garage.model.ItemDAO;
import ma.esolutions.projet.gestion.garage.model.MinimalStockException;
import ma.esolutions.projet.gestion.garage.model.NegativeStockException;
import ma.esolutions.projet.gestion.garage.model.Payment;
import ma.esolutions.projet.gestion.garage.model.PaymentDAO;
import ma.esolutions.projet.gestion.garage.model.SalesOrder;
import ma.esolutions.projet.gestion.garage.model.SalesOrderDelivery;
import ma.esolutions.projet.gestion.garage.model.SalesOrderDeliveryDAO;
import ma.esolutions.projet.gestion.garage.model.SalesOrderLine;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS16;
import ma.esolutions.projet.gestion.garage.view.common.AbstractFormView;
import ma.esolutions.projet.gestion.garage.view.common.JTextFieldExtended;
import net.miginfocom.swing.MigLayout;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

/**
 *
 * @author E-solutions
 */
public class SalesOrderFormView extends AbstractFormView<SalesOrder> {

    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
            Font.BOLD);
    private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.NORMAL, BaseColor.RED);
    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16,
            Font.BOLD);
    private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.BOLD);
    private static String PDF = "D:\\ProjetsJava\\gestgarage\\src\\ma\\esolutions\\projet\\gestion\\garage\\app\\resources\\";
    protected SalesOrder theSalesOrder;
    protected CustomerDAO theCustomerDAO;
    private boolean isNewModel = true;

    private JLabel theStartDateLabel;
    private JDatePanelImpl theStartDate;
    private JDatePickerImpl theStartDatePicker;

    private JLabel theCloseDateLabel;
    private JDatePanelImpl theCloseDate;
    private JDatePickerImpl theCloseDatePicker;

    private JLabel theStatusLabel;
    private JCheckBox theStatusCheckBox;

    private JLabel theCustomerLabel;
    private JComboBox theCustomerCombobox;

    private JLabel theItemsLabel;
    private JComboBox theItemsCombobox;
    private JTextFieldExtended theItemsCountTextfield;
    //  private JLabel theItemsLabel;

    private JLabel theOrderedItemsLabel;
    private JButton theAddItemButton;
    private JButton theCloseButton;
    private JButton theSaveButton;
    private JLabel theItemsQuantityLabel;
    private JPanel theItemsPanel;
    private JComboBox thePayementCombo;
    private JLabel thePayementLabel;
    private DefaultComboBoxModel theItemsModel;
    private ArrayList<Item> theItems;
    private String[] thePaymentTypes;
    private DefaultTableModel theItemsTableModel;
    private JScrollPane theItemsTableScrollPane;
    private ComboBoxModel theCustomersModel;
    private ItemDAO theItemDAO;
    private SalesOrderCommand theHandlerCommand;
    private final PaymentDAO thePaymentDAO;
    private double totalTTCPrice;
    private SalesOrderDelivery theBill;

    public SalesOrderFormView(SalesOrder order, List<Item> items) {
        super(ApplicationController.getAppView());
        theSalesOrder = order;
        theItemDAO = new ItemDAO();
        thePaymentDAO = new PaymentDAO();
        if (items == null) {
            theItems = new ArrayList<Item>();
        } else {
            theItems = (ArrayList<Item>) items;
        }
        if (order.getId() != null) {
            isNewModel = false;

        } else {
            isNewModel = true;
            theCustomerDAO = new CustomerDAO();
        }

    }

    @Override
    public void buildUI() {
        initComponents();
        addPageToForm("Commande de Vente", buildPage());
        popFields();
        pack();

    }

    private boolean isItemAvailable(Item item, int quantity) {
        if (item.getTheItemQuantity() < quantity) {
            return false;
        }
        return true;
    }

    private boolean isItemMinimal(Item item, int quantity) {
        if (Math.abs(item.getTheItemQuantity() - quantity) > item.getStockMin()) {
            return false;
        }
        return true;
    }

    private JPanel buildPage() {
        JPanel panel = new JPanel(new MigLayout());
        theStartDateLabel = new JLabel("Date de début ");
        theCloseDateLabel = new JLabel("Date de fin");
        SimpleDateFormat theDateFormatter = new SimpleDateFormat();

        UtilDateModel mm = new UtilDateModel();
        // m.setDay(d.getDay());
        // m.setMonth(d.getMonth());
        // m.setYear(d.getYear());
        mm.setValue(new Date());
        mm.setSelected(true);

        theStartDate = new JDatePanelImpl(mm);
        theStartDatePicker = new JDatePickerImpl(theStartDate, new AbstractFormatter() {
            private String theDatePattern = "dd-MM-yyyy";
            private SimpleDateFormat theSimpleDateFormatter = new SimpleDateFormat(theDatePattern);

            @Override
            public Object stringToValue(String text) throws ParseException {
                return theSimpleDateFormatter.parseObject(text);
            }

            @Override
            public String valueToString(Object value) throws ParseException {
                if (value != null) {
                    Calendar cal = (Calendar) value;
                    return theSimpleDateFormatter.format(cal.getTime());

                }

                return "";
            }
        });
        Date d = new Date();
        UtilDateModel m = new UtilDateModel();
        // m.setDay(d.getDay());
        // m.setMonth(d.getMonth());
        // m.setYear(d.getYear());
        m.setValue(new Date());
        m.setSelected(true);
        theCloseDate = new JDatePanelImpl(m);
        theCloseDatePicker = new JDatePickerImpl(theCloseDate, new AbstractFormatter() {
            private String theDatePattern = "dd-MM-yyyy";
            private SimpleDateFormat theSimpleDateFormatter = new SimpleDateFormat(theDatePattern);

            @Override
            public Object stringToValue(String text) throws ParseException {

                return theSimpleDateFormatter.parseObject(text);
            }

            @Override
            public String valueToString(Object value) throws ParseException {
                if (value != null) {
                    Calendar cal = (Calendar) value;
                    return theSimpleDateFormatter.format(cal.getTime());

                }

                return "";

            }
        });

        theStatusLabel = new JLabel("Fermé ");
        theStatusCheckBox = new JCheckBox();
        theCustomerLabel = new JLabel("Client");
        if (theCustomerDAO != null) {
            theCustomersModel = new DefaultComboBoxModel(theCustomerDAO.all().toArray());
        } else {
            theCustomersModel = new DefaultComboBoxModel();
        }

        theCustomerCombobox = new JComboBox(theCustomersModel);
        theCustomerCombobox.setEditable(true);
        theOrderedItemsLabel = new JLabel("Articles demandés ");
        theItemsCountTextfield = new JTextFieldExtended(50);

        theItemsLabel = new JLabel("Articles");
        theItemsQuantityLabel = new JLabel("Quantité ");
        //   theItemsModel = new DefaultComboBoxModel();
        // theItemsModel.
        //  theItems = new ArrayList<Item>();
        // theItems.add(new Item());
        System.out.println("comb items " + theItems);
        theItemsCombobox = new JComboBox(theItems.toArray());
        theItemsPanel = new JPanel(new MigLayout());
        thePayementLabel = new JLabel("Méthode de Payement ");
        thePaymentTypes = new String[]{"Espéces", "Crédit"};
        thePayementCombo = new JComboBox(thePaymentTypes);
        JTable theItemsTable = buildItemsTable();
        theAddItemButton = new JButton("Ajouter un Article ");

        theAddItemButton.addActionListener(new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                int quantity = 0;
                try {
                    quantity = Integer.parseInt(theItemsCountTextfield.getText());

                } catch (NumberFormatException ex) {
                    ApplicationView.showErrorMsg("Entrez une valeur valide pour la quantité");

                }
                Item item = (Item) theItemsCombobox.getSelectedItem();

                if (isItemAvailable(item, quantity)) {
                    String price = String.valueOf((item).getTheSalesPriceTTC());

                    if (isItemMinimal(item, quantity)) {

                        int askYesNo = ApplicationView.showAskYesNo("La réalisation de cette vente va rendre la quantité de cet article  minimal,continuer ?");
                        if (askYesNo == JOptionPane.YES_OPTION) {
                            theItemsTableModel.addRow(new String[]{item.toString(),
                                theItemsCountTextfield.getText(), price, String.valueOf(item.getTheTVA())});
                                
                        }
                    } else {
                        theItemsTableModel.addRow(new String[]{item.toString(),
                            theItemsCountTextfield.getText(), price, String.valueOf(item.getTheTVA())});

                    }
                 /*   int askYesNo = ApplicationView.showAskYesNo("Voulez vous créer une notification de commande d'achat ?");
                    if(askYesNo == JOptionPane.YES_OPTION)
                    {
                        ApplicationView.createAndShowPurchaseNotification();
                        
                    }
                    */
                } else {
                    // setStatusbarMsg("Alerte : la quantitée demandée n'est pas disponible en stock");
                    ApplicationView.showErrorMsg("la quantité demandée n'est pas disponible en stock");

                }
                theItemsCountTextfield.setText("");
            }

        });

        panel.add(theCustomerLabel, "gaptop 100,split,left,width 300");
        panel.add(theCustomerCombobox, "gaptop 100,right,width 300,wrap");
        panel.add(theStartDateLabel, "split,width 300,left");

        panel.add(theStartDatePicker, "right,width 300,wrap");
        //  panel.add(theCloseDateLabel, "split,width 300,left");
        // panel.add(theCloseDatePicker, "width 300,wrap");
        // panel.add(theStatusLabel, "split,left,width 300");
        //  panel.add(theStatusCheckBox, "right,width 300,wrap");

        panel.add(theItemsLabel, "split,left,width 300");
        panel.add(theItemsCombobox, "right,width 300,wrap");
        panel.add(theOrderedItemsLabel, "split,left,width 300,wrap");
        panel.add(theItemsQuantityLabel, "split ,left,width 300");
        panel.add(theItemsCountTextfield, "right,width 300,wrap");
        panel.add(theAddItemButton, "split,right,width 300,height 20,wrap");
        panel.add(thePayementLabel, "split,left,width 300");
        panel.add(thePayementCombo, "right,width 300,wrap");
        panel.add(theOrderedItemsLabel, "split,left,width 300,left,wrap");
        panel.add(theItemsPanel, "right,width 300,wrap");
        theItemsTableScrollPane = new JScrollPane(theItemsTable);
        // table.setPreferredSize(new Dimension(400,200));
        theItemsTableScrollPane.setSize(new Dimension(300, 20));
        JScrollBar sb = new JScrollBar(JScrollBar.VERTICAL);
        sb.setAutoscrolls(true);
        theItemsTableScrollPane.setVerticalScrollBar(sb);
        theItemsTableScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

        theItemsTableScrollPane.setVisible(true);
        theItemsTableScrollPane.revalidate();
        panel.add(theItemsTableScrollPane, "right,height 60,width 300,wrap");

        return panel;
    }

    private void buildAddItemsForm() {

        //JOptionPane.showMessageDialog(this, "Items");
        System.out.println("nouvel article");
        theItemsPanel.add(new JLabel("element ajouté"), "height 10,width 200,wrap");
    }

    @Override
    public SalesOrder getEntity() {
        return theSalesOrder;
    }

    @Override
    public String getFormIconPath() {
        return ICONS16 + "sales.png";
    }

    @Override
    public String getFormTitle() {
        if (isNewModel) {
            return "Créer une commande de vente";
        } else {
            return "Mettre à jour ...";
        }
    }

    @Override
    public void onHelp() {
    }

    @Override
    public void popFields() {

    }

    @Override
    public SalesOrder pushFields() {
        SalesOrder o = new SalesOrder();
        Double totalHorsTaxPrice = 0.0;
        totalTTCPrice = 0.0;
        o.setTheClosedDate((Date) this.theCloseDate.getModel().getValue());
        o.setTheStartDate((Date) this.theStartDate.getModel().getValue());
        if (this.theStatusCheckBox.isSelected()) {
            o.setTheStatus(1);

        }
        o.setTheCustomer((Customer) theCustomerCombobox.getSelectedItem());

        Set<SalesOrderLine> orderLines = new HashSet<>();
        totalHorsTaxPrice = setOrderLines(totalTTCPrice, totalHorsTaxPrice, orderLines);
        System.out.println(orderLines);

        o.setTheOrderLines((HashSet<SalesOrderLine>) orderLines);
        System.out.println("order total" + totalTTCPrice + ": " + o.getTheTotalHTPrice());
        o.setTheTotalTTCPrice(totalTTCPrice);
        o.setTheTotalHTPrice(totalHorsTaxPrice);
        System.out.println("payment type " + this.thePayementCombo.getSelectedItem().toString());
        if ("Espéces".equalsIgnoreCase(this.thePayementCombo.getSelectedItem().toString())) {
            System.out.println(" selecting " + this.thePayementCombo.getSelectedItem().toString());
            o.setThePaymentType("espéces");
        }
        if ("Crédit".equalsIgnoreCase(this.thePayementCombo.getSelectedItem().toString())) {
            System.out.println("selecting " + this.thePayementCombo.getSelectedItem().toString());
            o.setThePaymentType("crédit");
        }
        System.out.println("get the payment type" + o.getThePaymentType());
        this.theSalesOrder = o;

        return o;
    }

    @Override
    public void clearFields() {
        theCustomerCombobox.setSelectedIndex(-1);
        theStatusCheckBox.setSelected(false);
        theItemsCombobox.setSelectedIndex(-1);

        this.theItemsTableModel.setRowCount(0);
    }

    private Double setOrderLines(Double ttcp, Double htp, Set<SalesOrderLine> orderLines) throws NumberFormatException {
        for (Object e : theItemsTableModel.getDataVector()) {
            SalesOrderLine s = new SalesOrderLine();
            System.out.println(e);
            //System.out.println(e.getClass());
            //s.setTheOrderedItem(e);
            //s.setTheQuantity();
            Vector r = (Vector) e;
            System.out.println(r.get(0) + " " + r.get(1) + " " + r.get(2));
            Item myItem;
            s.setTheQuantity(Integer.parseInt((String) r.get(1)));
            htp = setTheItemsTotalPrice(htp, r);
            double val1 = Double.parseDouble(r.get(3).toString()) / 100;
            //double val = setTheItemsTotalPrice(total,r)/(1 + val1 );
            totalTTCPrice += setTheItemsTotalPrice(totalTTCPrice, r) * (1 + val1);

            System.out.println("order total ht price " + htp);
            System.out.println("order total ttc price " + ttcp);
            setTheOrderedItem(r, s);
            orderLines.add(s);
        }
        return htp;
    }

    private void setTheOrderedItem(Vector r, SalesOrderLine s) {
        theItems.forEach((Item it) -> {
            if (it.getTheItemName() == r.get(0)) {
                s.setTheOrderedItem(it);
            }
        });
    }

    private Double setTheItemsTotalPrice(Double totalItemsPrice, Vector r) throws NumberFormatException {
        totalItemsPrice += Integer.parseInt((String) r.get(1)) * Double.parseDouble(r.get(2).toString());
        System.out.println("ttc pour item " + Integer.parseInt((String) r.get(1)) * Double.parseDouble(r.get(2).toString()));
        return totalItemsPrice;
    }

    private Double setTheItemsTotalHTPrice(Double total, Vector r) throws NumberFormatException {
        double val1 = Double.parseDouble(r.get(3).toString()) / 100;
        //double val = setTheItemsTotalPrice(total,r)/(1 + val1 );
        total += setTheItemsTotalPrice(total, r) / (1 + val1);

        return total;
    }

    @Override
    public String getIconPath() {
        return ICONS16 + "sales.png";
    }
//Prix HT = Prix TTC / ( 1 + 19,6/100 )

    @Override
    public Component asComponent() {
        return null;
    }

    private JTable buildItemsTable() {
        String[] columnsNames = {"Article", "Quantité", "Prix de vente TTC", "Pourcentage TVA"};
        theItemsTableModel = new DefaultTableModel(columnsNames, 0);

        //   theItemsTableModel.setColumnIdentifiers(new String[]{"Article","Quatité"});
        //tableModel.setRowCount(4);
        JTable table = new JTable(theItemsTableModel);

        //  table.addColumn(new TableColumn());
        return table;
    }

    public void setCommand(ICommand cmd) {
        theHandlerCommand = (SalesOrderCommand) cmd;
    }

    public void notifyBill() {
        System.out.println("notifying the bil  system");

        theBill = new SalesOrderDelivery();
        theBill.setTheSalesOrder(theSalesOrder);
        theBill.setTheDeliveryDate(new Date());

        SalesOrderDeliveryDAO theSalesOrderDeliveryDAO = new SalesOrderDeliveryDAO();
        theSalesOrderDeliveryDAO.create(theBill);
        System.out.println(theBill);

    }

    public void notifyCredit() {
        Customer c = theSalesOrder.getTheCustomer();
        c.setTheCredit(theSalesOrder.getTheTotalTTCPrice());
        System.out.println("setting the client credit " + c.getTheCredit());
        CustomerDAO theCustomerDAO = new CustomerDAO();
        theCustomerDAO.update(c);
    }

    public void notifyStock() {
        Item item = new Item();
        for (SalesOrderLine ol : theSalesOrder.getTheOrderLines()) {
            item = ol.getTheOrderedItem();
            try {
                item.setTheItemQuantity(item.getTheItemQuantity() - ol.getTheQuantity());
            } catch (NegativeStockException ex) {
                Logger.getLogger(SalesOrderFormView.class.getName()).log(Level.SEVERE, null, ex);
            } catch (MinimalStockException ex) {
                Logger.getLogger(SalesOrderFormView.class.getName()).log(Level.SEVERE, null, ex);
            }
            theItemDAO.update(item);
        }
    }

    public void notifyPayment() {
        System.out.println("notifying the payment");
        Item item = null;
        double quantity = 0;
        double price = 0;
        double pricettc = 0;
        double sumht = 0.0;
        double sumttc = 0.0;
        for (SalesOrderLine ol : theSalesOrder.getTheOrderLines()) {
            item = ol.getTheOrderedItem();
            quantity = ol.getTheQuantity();
            price = item.getTheUnitPrice();
            pricettc = item.getTheSalesPriceTTC();
            System.out.println("tva" + item.getTheTVA() + " price ttc " + pricettc);
            System.out.println("the total ttc quantity " + quantity * price);
            sumht += price * quantity;
            sumttc += quantity * pricettc;
        }
        System.out.println("the order sum price " + sumht);
        Payment p = new Payment();
        p.setThePaymentType("vente");
        p.setTheHTaxAmount(sumttc);
        p.setTheTTCAmount(sumht);
        p.setTheCashingDate(new Date());
        System.out.println("customer " + theSalesOrder.getTheCustomer());
        p.setTheCustomer(theSalesOrder.getTheCustomer());

        this.thePaymentDAO.create(p);
        this.thePaymentDAO.getTodayTotal();
    }

    @Override
    public void onPrintPreview() {
        notifyBill();

        System.out.print("printing preview for " + this.theSalesOrder);

        setupPdf();
        if (Desktop.isDesktopSupported()) {
            File f = new File(PDF + "bill" + theBill.getId() + ".pdf");
            try {
                Desktop.getDesktop().open(f);
            } catch (IOException ex) {
                Logger.getLogger(SalesOrderFormView.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void prepareToPrintBill() {

        this.thePrintPreviewAction.setEnabled(true);

    }

    private void setupPdf() {

        Document document = new Document();
        try {
            try {
                PdfWriter.getInstance(document, new FileOutputStream(PDF + "bill" + theBill.getId() + ".pdf"));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(SalesOrderFormView.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (DocumentException ex) {
            Logger.getLogger(SalesOrderFormView.class.getName()).log(Level.SEVERE, null, ex);
        }
        document.open();
        addMetaData(document);
        try {
            addTitlePage(document);
        } catch (DocumentException ex) {
            Logger.getLogger(SalesOrderFormView.class.getName()).log(Level.SEVERE, null, ex);
        }

        document.close();
    }

    private void addMetaData(Document document) {
        document.addTitle("bon de livraison");
        document.addSubject("bon de livraison ");
        document.addAuthor("Bouchefra Ahmed");
        document.addCreator("Bouchefra Ahmed");
    }

    private void addTitlePage(Document document)
            throws DocumentException {
        Paragraph preface = new Paragraph();
        addEmptyLine(preface, 1);

        preface.add(new Paragraph("Bon de livraison N : " + "BL" + theBill.getId(), catFont));
        addEmptyLine(preface, 1);

        addEmptyLine(preface, 3);
        preface.add(new Paragraph("Nom du client :" + theBill.getTheSalesOrder().getTheCustomer(),
                smallBold));
        preface.add(new Paragraph("Prix total TTC à payer :" + theBill.getTheSalesOrder().getTheTotalTTCPrice() + " DH",
                smallBold));
        preface.add(new Paragraph("Prix total HT à payer :" + theBill.getTheSalesOrder().getTheTotalHTPrice() + " DH",
                smallBold));
        if (theBill.getTheSalesOrder().getThePaymentType().equalsIgnoreCase("espéces")) {
            preface.add(new Paragraph("Payement par  : espéces",
                    smallBold));
        }
        if (theBill.getTheSalesOrder().getThePaymentType().equals("crédit")) {
            preface.add(new Paragraph("Payement par  : crédit",
                    smallBold));
        }

        addEmptyLine(preface, 4);
        preface.add(new Paragraph("", redFont));
        createTable(preface);

        document.add(preface);
        //  document.newPage();
    }

    private void addContent(Document document) throws DocumentException {
        Anchor anchor = new Anchor("First Chapter", catFont);
        anchor.setName("First Chapter");

        // Second parameter is the number of the chapter
        Chapter catPart = new Chapter(new Paragraph(anchor), 1);

        Paragraph subPara = new Paragraph("Subcategory 1", subFont);
        Section subCatPart = catPart.addSection(subPara);
        subCatPart.add(new Paragraph("Hello"));

        subPara = new Paragraph("Subcategory 2", subFont);
        subCatPart = catPart.addSection(subPara);
        subCatPart.add(new Paragraph("Paragraph 1"));
        subCatPart.add(new Paragraph("Paragraph 2"));
        subCatPart.add(new Paragraph("Paragraph 3"));

        // add a list
        Paragraph paragraph = new Paragraph();
        addEmptyLine(paragraph, 5);
        subCatPart.add(paragraph);

        // add a table
        // createTable(subCatPart);
        // now add all this to the document
        document.add(catPart);

        // Next section
        anchor = new Anchor("Second Chapter", catFont);
        anchor.setName("Second Chapter");

        // Second parameter is the number of the chapter
        catPart = new Chapter(new Paragraph(anchor), 1);

        subPara = new Paragraph("Subcategory", subFont);
        subCatPart = catPart.addSection(subPara);
        subCatPart.add(new Paragraph("This is a very important message"));

        // now add all this to the document
        document.add(catPart);

    }

    private void createTable(Paragraph subCatPart)
            throws BadElementException {
        System.out.println("nbr of cells" + this.theBill.getTheSalesOrder().getTheOrderLines().size());
        PdfPTable table = new PdfPTable(3);

        PdfPCell c1 = new PdfPCell(new Phrase("Article"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Prix unitaire"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Quantité"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);
        table.setHeaderRows(1);
        Set<SalesOrderLine> ol = theBill.getTheSalesOrder().getTheOrderLines();
        ol.forEach((o) -> {

            table.addCell(o.getTheOrderedItem().toString());
            table.addCell("" + o.getTheOrderedItem().getTheUnitPrice() + " DH");
            table.addCell("" + o.getTheQuantity());
        });

        // table.addCell("2.1");
        // table.addCell("2.2");
        // table.addCell("2.3");
        subCatPart.add(table);

    }

    private void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

    private void openPurchaseCommand() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
