/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import java.util.Calendar;
import java.util.Map;
import javafx.scene.chart.Chart;
import javafx.scene.chart.XYChart;
import ma.esolutions.projet.gestion.garage.controller.MainController;

/**
 *
 * @author born2code
 */
abstract public class YearlyPurchasesXYChart extends AbstractPurchasesXYChart{

    public YearlyPurchasesXYChart(MainController controller) {
        super(controller);
    }

    @Override
    protected String getXAxisLabel() {
        return "Années";
    }

    @Override
    protected String getSeriesLabel() {
        return "Achats par année";
    }
     private int getCurrentYear() {
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.YEAR);
    }

     @Override
    protected XYChart.Series getData() {
        Map mm = theController.getPurchasesPerYearForPast10Years();
        int currentYear  = getCurrentYear();
                theSeries = new XYChart.Series();

        System.out.println("yearly series");
        
        mm.forEach((k,v)->{
            System.out.println("mm k" + k + "v "+ v );
        });
        for(int i = 0;i<mm.size();i++)
        {
            System.out.println("series " +mm.get(""+(i+2014)) );
            theSeries.getData().add(new XYChart.Data(""+(i+2014),mm.get(""+(i+2014)) ));
            
        }   
        return theSeries;
    }


}
