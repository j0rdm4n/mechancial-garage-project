/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import ma.esolutions.projet.gestion.garage.app.Application;
import ma.esolutions.projet.gestion.garage.model.Item;
import ma.esolutions.projet.gestion.garage.model.ItemDAO;
import ma.esolutions.projet.gestion.garage.model.MinimalStockException;
import ma.esolutions.projet.gestion.garage.model.NegativeStockException;
import ma.esolutions.projet.gestion.garage.model.PurchaseOrder;
import ma.esolutions.projet.gestion.garage.model.PurchaseOrderDAO;
import ma.esolutions.projet.gestion.garage.model.PurchaseOrderLine;
import ma.esolutions.projet.gestion.garage.model.Vendor;
import ma.esolutions.projet.gestion.garage.model.VendorDAO;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS16;
import ma.esolutions.projet.gestion.garage.view.common.AbstractFormView;
import ma.esolutions.projet.gestion.garage.view.common.JTextFieldExtended;
import net.miginfocom.swing.MigLayout;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

/**
 *
 * @author E-solutions
 */
public class PurchaseOrderFormView extends AbstractFormView<PurchaseOrder> {
    
    private PurchaseOrder thePurchaseOrder;
    private boolean isNewModel;
    private JLabel theStartDateLabel;
    private JLabel theCloseDateLabel;
    private JLabel theStatusLabel;
    private JCheckBox theStatusCheckBox;
    private JLabel theCustomerLabel;
    private JLabel theDateLabel;
    private JTextFieldExtended theDateTxt;
    private JLabel theItemLabel;
    private JComboBox theItemBox;
    private JLabel theQuantiteLabel;
    private JTextFieldExtended theQuantiteTxt;
    private JLabel theVendorLabel;
    private JComboBox theVendorBox;
    private ComboBoxModel theVendorsModel;
    private VendorDAO theVendorDAO;
    private Date theOrderDate;
    private ItemDAO theItemDAO;
    private DefaultComboBoxModel theItemsModel;
    private JButton addPurchaseLineBtn;
    private ArrayList thePurchaseOrderLines;
    private JLabel theLinesLabel;
    private JTable theLinesTable;
    private DefaultTableModel theLinesModel;
    private JButton validateBtn;
    private JLabel theIdLabel;
    private JTextFieldExtended theIdTxt;
    private JLabel receivedLabel;
    private JTextFieldExtended priceTxt;
    private JLabel priceLabel;
    private Map<String, JTextField> prices;
    private PurchaseOrderDAO thePurchaseOrderDAO;
    private JLabel paymentLbl;
    private JComboBox paymentBox;
    private DefaultComboBoxModel paymentModel;
    private JDatePanelImpl theEffetDueDate;
    private JDatePickerImpl theEffetDueDatePicker;
    private JLabel dueLabel;
    
    public PurchaseOrderFormView(PurchaseOrder order) {
        super(Application.getAppView());
        thePurchaseOrder = order;
        
        if (order.getId() == null) {
            isNewModel = true;
        } else {
            isNewModel = false;
        }
      }
    
    @Override
    public void buildUI() {
        initComponents();
        addPageToForm("Commande d'achat", buildPage());
        if (!isNewModel) {
            popFields();
        }
        pack();
        return;
    }
    
    private JPanel buildPage() {
        JPanel panel = new JPanel(new MigLayout());
        theVendorLabel = new JLabel("Fournisseur");
        theVendorBox = new JComboBox();
        theVendorDAO = new VendorDAO();
        List<Vendor> vendors = theVendorDAO.all();
        theVendorsModel = new DefaultComboBoxModel(vendors.toArray());
        theVendorBox.setModel(theVendorsModel);
        
        theDateLabel = new JLabel("Date de commande ");
        theDateTxt = new JTextFieldExtended(50);
        theOrderDate = new Date();
        theDateTxt.setText(theOrderDate.toString());
        
        theItemLabel = new JLabel("Article à commander");
        theItemDAO = new ItemDAO();
        List<Item> items = theItemDAO.all();
        theItemsModel = new DefaultComboBoxModel(items.toArray());
        theItemBox = new JComboBox();
        theItemBox.setModel(theItemsModel);
        
        theQuantiteLabel = new JLabel("Quantité");
        theQuantiteTxt = new JTextFieldExtended(50);
        theLinesLabel = new JLabel("Articles à commander");
           Date d = new Date();
        UtilDateModel m = new UtilDateModel();
        
        m.setValue(new Date());
        m.setSelected(true);
        theEffetDueDate = new JDatePanelImpl(m);
        theEffetDueDatePicker = new JDatePickerImpl(theEffetDueDate, new JFormattedTextField.AbstractFormatter() {
            private String theDatePattern = "dd-MM-yyyy";
            private SimpleDateFormat theSimpleDateFormatter = new SimpleDateFormat(theDatePattern);

            @Override
            public Object stringToValue(String text) throws ParseException {

                return theSimpleDateFormatter.parseObject(text);
            }

            @Override
            public String valueToString(Object value) throws ParseException {
                if (value != null) {
                    Calendar cal = (Calendar) value;
                    return theSimpleDateFormatter.format(cal.getTime());

                }

                return "";

            }
        });

    
        
        dueLabel = new JLabel("Date de payement");
        paymentLbl = new JLabel("Type de payement ");
        paymentModel = new DefaultComboBoxModel(new String[]{COMBO_CHOICE_ESPECES, COMBO_CHOICE_EFFETS});
        paymentBox = new JComboBox();
        paymentBox.setModel(paymentModel);
        paymentBox.addItemListener(new ItemListener() {
            
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(paymentBox.getSelectedItem().equals(COMBO_CHOICE_EFFETS))
                {
                    panel.add(dueLabel, "split,left,width 300");
                    panel.add(theEffetDueDatePicker,"right,width 300,wrap");
                    System.out.println("updating ui");
                   
                }
                else
                {
                    panel.remove(dueLabel);
                    panel.remove(theEffetDueDatePicker);
                }
                //    panel.repaint();
                    panel.revalidate();
                    
                
            }
        });
        String[] cols = new String[]{"Article", "Quantité", "Prix unitaire"};
        theLinesModel = new DefaultTableModel(cols, 0);
        //theLinesTable.setModel(theLinesModel);
        theLinesTable = new JTable(theLinesModel);
        addPurchaseLineBtn = new JButton("Ajouter à la commande");
        JScrollPane linesPane = new JScrollPane(theLinesTable);
        linesPane.setPreferredSize(new Dimension(300, 100));
        linesPane.setAutoscrolls(true);
        if (!isNewModel) {
            theIdLabel = new JLabel("Numéro de commande");
            panel.add(theIdLabel, "split,left,width 300");
            theIdTxt = new JTextFieldExtended(50);
            theIdTxt.setText("PO" + thePurchaseOrder.getId());
            theIdTxt.setEnabled(false);
            panel.add(theIdTxt, "right,width 300,wrap");
        }
        panel.add(theVendorLabel, "split ,left,width 300");
        panel.add(theVendorBox, "right,width 300,wrap");
        
        panel.add(theDateLabel, "split,left,width 300");
        panel.add(theDateTxt, "right,width 300,wrap");
        
        panel.add(theItemLabel, "split,left,width 300");
        panel.add(theItemBox, "right,width 150");
        panel.add(addPurchaseLineBtn, "right,width 150,wrap");
        
        panel.add(theQuantiteLabel, "split ,left,width 300");
        panel.add(theQuantiteTxt, "right,width 300,wrap");
        panel.add(paymentLbl, "split,left,width 300");
        panel.add(paymentBox, "right,width 150,wrap");
        panel.add(theLinesLabel, "split,left,width 300");
        panel.add(linesPane, "split,right,width 300,wrap");
        
        if (!isNewModel) {
            priceLabel = new JLabel("Entrez le prix TTC d'achat  ");
            priceTxt = new JTextFieldExtended(50);
            
            validateBtn = new JButton("Valider la réception");
            validateBtn.setBackground(Color.red);
            if (!thePurchaseOrder.isReceived()) {
                receivedLabel = new JLabel("Commande pas encore reçue !");
                receivedLabel.setBackground(Color.red);
                receivedLabel.setForeground(Color.red);
                
            } else {
                receivedLabel = new JLabel("Commande déja reçue !");
                validateBtn.setEnabled(false);
                receivedLabel.setBackground(Color.blue);
                receivedLabel.setForeground(Color.blue);
            }
            //  Timer theTimer  = new Timer();
           /*  theTimer.scheduleAtFixedRate(new TimerTask() {

             @Override
             public void run() {
             switchReceivedColor();
             System.out.println("rec");
             }
             }, 0, 1000);*/
            panel.add(receivedLabel, "gaptop 60,span 2,width 600,wrap");
            prices = new HashMap<>();
            
            panel.add(new JLabel("Entrez les prix des articles"), "split,left,width 600,wrap");
            
            thePurchaseOrder.getTheOrderedItems().forEach((ol) -> {
                
                JLabel l = new JLabel(ol.getTheOrderedItem().toString());
                
                JTextField t = new JTextField();
                prices.put(ol.getTheOrderedItem().toString(), t);
                
                panel.add(l, "split,left,width 300");
                panel.add(t, "right,width 300,wrap");
                
            });
            
            panel.add(validateBtn, "gaptop 40, span 2,width 600,wrap");
            
            validateBtn.addActionListener((ActionEvent e) -> {
                validateCommandReception();
                validateBtn.setEnabled(false);
            });
        }
        
        addPurchaseLineBtn.addActionListener((ActionEvent e) -> {
            onAddPurchaseLine();
        });
        return panel;
    }
    private static final String COMBO_CHOICE_ESPECES = "espéces";
    private static final String COMBO_CHOICE_EFFETS = "effets";
    
    private void switchReceivedColor() {
        if (receivedLabel.getForeground() == Color.red) {
            receivedLabel.setBackground(Color.blue);
        }
        if (receivedLabel.getForeground() == Color.blue) {
            receivedLabel.setBackground(Color.red);
        }
        receivedLabel.invalidate();
        receivedLabel.repaint();
        
    }
    
    private void validateCommandReception() {
        this.thePurchaseOrder.setReceived(true);
        PurchaseOrderDAO thePurchaseOrderDAO = new PurchaseOrderDAO();
        ItemDAO theItemDAO = new ItemDAO();
        thePurchaseOrderDAO.update(thePurchaseOrder);
        receivedLabel.setBackground(Color.red);
        receivedLabel.setText("Reception de la commande en stock est faite avec succés !");
        putInStock();
    }
    
    private void onAddPurchaseLine() {
        if (thePurchaseOrderLines == null) {
            thePurchaseOrderLines = new ArrayList();
        }
        if (theItemBox.getSelectedIndex() == -1) {
            return;
        }
        
        PurchaseOrderLine pol = new PurchaseOrderLine();
        pol.setTheOrderedItem((Item) this.theItemBox.getSelectedItem());
        try {
            pol.setTheQuantity(Integer.parseInt(this.theQuantiteTxt.getText()));
        } catch (NumberFormatException e) {
            return;
        }
        pol.setTheUnitPrice(((Item) this.theItemBox.getSelectedItem()).getTheUnitPrice());
        theLinesModel.addRow(new String[]{pol.getTheOrderedItem().toString(), pol.getTheQuantity() + "", pol.getTheUnitPrice() + ""});
        thePurchaseOrderLines.add(pol);
        this.theQuantiteTxt.setText("0");
    }
    
    @Override
    public PurchaseOrder getEntity() {
        return thePurchaseOrder;
    }
    
    @Override
    public String getFormIconPath() {
        return ICONS16 + "sales.png";
    }
    
    @Override
    public String getFormTitle() {
        if (isNewModel) {
            return "Créer nouvelle Commande d'achat";
        } else {
            return "Mettre à jour la commande d'achat";
        }
    }
    
    @Override
    public void onHelp() {
    }
    
    @Override
    public void clearFields() {
        theOrderDate = new Date();
        theDateTxt.setText(theOrderDate.toString());
        theLinesModel.setRowCount(0);
        thePurchaseOrderLines.clear();
    }
    
    @Override
    public void popFields() {
        
    }
    
    @Override
    public PurchaseOrder pushFields() {
        PurchaseOrder o = new PurchaseOrder();
        o.setTheOrderDate(theOrderDate);
        o.setTheVendor((Vendor) theVendorBox.getSelectedItem());
        o.setTheOrderedItems(thePurchaseOrderLines);
        
        if (paymentBox.getSelectedIndex() != -1) {
            o.setThePaymentType((String) this.paymentBox.getSelectedItem());
        }
        o.setTheEffetDueDate((Date) this.theEffetDueDate.getModel().getValue());
        thePurchaseOrder = o;
        return thePurchaseOrder;
    }
    
    @Override
    public String getIconPath() {
        return ICONS16 + "sales.png";
    }
    
    @Override
    public Component asComponent() {
        return null;
    }
    
    private void putInStock() {
        Item item = new Item();
        item.setTheItemAddedDate(new Date());
        List<PurchaseOrderLine> lines = thePurchaseOrder.getTheOrderedItems();
        thePurchaseOrder.setTheCost(0);
        lines.forEach((ol) -> {
            Item it = ol.getTheOrderedItem();
            System.out.println(it + " q : " + ol.getTheQuantity());
            try {
                it.setTheItemQuantity(it.getTheItemQuantity() + ol.getTheQuantity());
            } catch (NegativeStockException ex) {
                Logger.getLogger(PurchaseOrderFormView.class.getName()).log(Level.SEVERE, null, ex);
            } catch (MinimalStockException ex) {
                Logger.getLogger(PurchaseOrderFormView.class.getName()).log(Level.SEVERE, null, ex);
            }
            //  it.setTheUnitPrice(theItemPrice);
            System.out.println("price : " + prices.get(it.toString()).getText());
            it.setTheUnitPrice(Double.parseDouble(prices.get(it.toString()).getText()));
            it.setTheSalesPriceTTC((1 + it.getTheTVA() / 100) * it.getTheUnitPrice());
            thePurchaseOrder.setTheCost(thePurchaseOrder.getTheCost() + (ol.getTheQuantity() * ol.getTheUnitPrice()));
            theItemDAO.update(it);
        });
        thePurchaseOrderDAO = new PurchaseOrderDAO();
        thePurchaseOrderDAO.update(thePurchaseOrder);
        System.out.println("the cost of order " + thePurchaseOrder.getTheCost());
        
    }
    
}
