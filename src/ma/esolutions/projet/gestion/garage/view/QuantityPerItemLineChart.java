/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import javafx.scene.chart.Chart;
import javafx.scene.chart.LineChart;
import ma.esolutions.projet.gestion.garage.controller.MainController;

/**
 *
 * @author born2code
 */
 public class QuantityPerItemLineChart extends AbstractStockXYChart{

    public QuantityPerItemLineChart(MainController controller) {
        super(controller);
    }

    
  

    @Override
    protected void notifyChart() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

       @Override
    protected Chart createChart() {
        
         LineChart lineChart = new LineChart(this.getXAxis(),this.getYAxis());
         theSeries.setName(getSeriesLabel());
         getData();
         lineChart.getData().addAll(theSeries);
         return lineChart;
    }

    @Override
    public String getReports() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
