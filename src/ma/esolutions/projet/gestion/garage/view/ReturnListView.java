/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.view;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.controller.commands.ItemReturnCommand;
import ma.esolutions.projet.gestion.garage.controller.common.IDataPageController;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.Return;
import ma.esolutions.projet.gestion.garage.model.ReturnDAO;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS16;
import ma.esolutions.projet.gestion.garage.view.common.AbstractListView;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSearchPanel;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSidePanel;
import ma.esolutions.projet.gestion.garage.view.common.EntityTableColumn;

/**
 *
 * @author E-solutions
 */
public class ReturnListView extends AbstractListView<Return> {

    private ReturnSidePanel theReturnSidePanel;

    public ReturnListView(List<Return> data) {
        super(null, data);
    }

    public ReturnListView() {
        super(null,new ArrayList<>());
    }
    @Override
    public AbstractSearchPanel getSearchPanel()
    {
        if(theSearchPanel == null)
        {
            theSearchPanel = new ItemReturnSearchPanel();
            theSearchPanel.setParentListView(this);
        }
        return theSearchPanel;
        
    }
    @Override
    public void addTableColumns() {
        
          getTableModel().addColumn(new EntityTableColumn(
                "Client", "theCustomer", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "Article", "theItem", String.class, 300));
          getTableModel().addColumn(new EntityTableColumn(
                "Quantité", "theItemQuantity", String.class, 300));
        getTableModel().addColumn(new EntityTableColumn(
                "Prix total TTC", "theTotalPrice", String.class, 300));
    }

    @Override
    public void showEntitySummary() {
    }

    @Override
    public void showEntityActions() {
    }

    @Override
    public void fireUpdateEvent(MouseEvent e) {
             
        Return ret;
        ret = this.getSelectedModel();
        
        if(ret != null)
        {
            ApplicationController.getAppView().fireReturnUpdateEvent(ret);
        }
    }

    @Override
    public AbstractSidePanel getSidePanel() {
        if (theReturnSidePanel == null) {
            theReturnSidePanel = new ReturnSidePanel();
        }
        this.setTheRowObserver(theReturnSidePanel);
        return theReturnSidePanel;
    }

    @Override
    public List<Return> getTheTableData() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getTitle() {
        return "Gestion de retour";
    }

    @Override
    public String getIconPath() {
        return ApplicationView.ICONS22 + "list-return-icon.png";
    }

    @Override
    public Return getModel() {
        return new Return();
    }

    @Override
    public void delete(Return e) {
        (new ReturnDAO()).remove(e);
    }

    @Override
    public SecureCommand<Return> getRelatedFormCommand(Return e) {
        return new ItemReturnCommand(e);
    }

}
