/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.view;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import ma.esolutions.projet.gestion.garage.view.common.AbstractSearchPanel;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author E-solutions
 */
public class ItemReturnSearchPanel extends AbstractSearchPanel {
    private JTextField customerTxt;
    private JTextField itemTxt;

    @Override
    public JPanel buildSearchForm() {
        JPanel searchPanel = new JPanel(new MigLayout());
         itemTxt = new JTextField();
         customerTxt = new JTextField();
        searchPanel.add(new JLabel("Nom d'article "),"split ,left,width 300");
        searchPanel.add(itemTxt,"right,width 300,wrap");
        
        searchPanel.add(new JLabel("Nom de client "),"split ,left,width 300");
        searchPanel.add(customerTxt,"right,width 300,wrap");
        
        
        return searchPanel;
    }

    @Override
    public String buildSearchFilter() {
        StringBuilder filter = new StringBuilder();
        
        
        filter.append("theCustomer");
        filter.append(":like:");
        filter.append(customerTxt.getText());
        filter.append(";");
        
        filter.append("theItem");
        filter.append(":like:");
        filter.append(itemTxt.getText());
        filter.append(";");
        
        
        return filter.toString();
        
    }
    
}
