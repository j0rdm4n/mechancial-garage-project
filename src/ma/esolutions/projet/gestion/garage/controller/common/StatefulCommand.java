/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.common;

/**
 *
 * @author born2code
 */
public abstract class StatefulCommand implements ICommand {
    protected int theCmdStatus = Command.FAILURE_STATUS;

    
    public int getStatus() {
        return theCmdStatus;
    }
    
}
