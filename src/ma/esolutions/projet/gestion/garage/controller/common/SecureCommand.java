/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.controller.common;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.JOptionPane;
import ma.esolutions.projet.gestion.garage.app.Session;
import ma.esolutions.projet.gestion.garage.model.Permission;
import ma.esolutions.projet.gestion.garage.model.Role;
import ma.esolutions.projet.gestion.garage.model.User;
import ma.esolutions.projet.gestion.garage.model.UserDAO;
import ma.esolutions.projet.gestion.garage.view.ApplicationView;
import ma.esolutions.projet.gestion.garage.view.ReLoginView;
import ma.esolutions.projet.gestion.garage.view.SwitchAccountFormView;
import ma.esolutions.projet.gestion.garage.view.common.IListView;
import ma.esolutions.projet.gestion.garage.view.common.IPageView;

/**
 *
 * @author E-solutions
 */
public abstract class SecureCommand<T> extends Command<T> {

    protected int theRequiredPermission;
    public UserDAO theUserDAO;
    protected boolean showPermissionsDlg = true;
    private static final String theMessage = "Vous n'avez pas la permission d'exécuter cette opération \n "
            + "Voulez vous changez le compte ? ";
    private static final String theTitle = "Besoin de Permission ";
    private IPageView theConnectionView;
    protected boolean abort;
    private boolean delayExecution = false;

    abstract public List<T> getData(String filter, int start, int end);

    protected void showRequiredOptions() {
        if (showPermissionsDlg == true) {
            if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(null, theMessage, theTitle, JOptionPane.YES_NO_OPTION)) {
                
                SwitchAccountFormView accountView = new SwitchAccountFormView();
                accountView.setExecCommand(this);
                accountView.setVisible(true);
                accountView.showDialog();
                
                //  accountView.blockUI();
                delayExecution = true;
                abort = false;
            } else {
                abort = true;
            }

        }
    }

    protected Map<String, String[]> parseFilter(String filter) {
        Map<String, String[]> m = new HashMap<>();
        String[] exps = filter.split(";");
        int i = 0;
        for (String ex : exps) {
            String[] ops = ex.split(":");
            String[] mm = new String[2];
            try {
                mm[0] = ops[1];

            } catch (Exception e) {
                mm[0] = "";
            }
            try {
                mm[1] = ops[2];

            } catch (Exception e) {
                mm[1] = "";
            }

            m.put(ops[0], mm);
        }
        return m;
    }

    abstract public void execute_body();

    public int getDataSize(String filter) {
        return 0;
    }

    public void setExecutedAlready()
    {
        this.delayExecution = true;
    }
    @Override
    public void execute() {
        System.out.println("current user " + Session.getSession().getTheCurrentUser());
        System.out.println("roles " + Session.getSession().getTheCurrentUser().getTheAttributedRoles());
        Session.getSession().getTheCurrentUser().getTheAttributedRoles().forEach((e) -> {
            e.getTheRolePermissions().forEach((ee) -> {
                System.out.println("" + ee.getThePermissionCode());
            });

        });

        if (Session.getSession().isTheLoginStatus() == true) {
            before();
             
            if (!abort) {
                if( ! delayExecution )
                execute_body();
            }
        } else {
            theConnectionView = new ReLoginView();
            theConnectionView.init(null);
            ApplicationView.showListView((IListView) theConnectionView);
        }
        after();
    }

    @Override
    public void before() {
        setTheRequiredPermission();
        System.out.println("the required permission is " + theRequiredPermission);
        System.out.println("has permission " + checkIfUserHasPermission());
    /*    if (!checkIfUserHasPermission()) {
            showRequiredOptions();
        } else {
            System.out.println("u have the permission to continue");
        }*/

    }

    @Override
    public void after() {
    }

    private void showAccountSwitchDlg() {
        //SwitchAccountFormView userFormView = new SwitchAccountFormView();

        SwitchAccountFormView accountView = new SwitchAccountFormView();
        accountView.setVisible(true);
        accountView.setExecCommand(this);
        accountView.showDialog();

    }

    @Override
    public boolean checkIfUserHasPermission() {
        User theCurrentUser = Session.getSession().getTheCurrentUser();
        List<Role> roles = theCurrentUser.getTheAttributedRoles();
        Iterator<Role> it = roles.iterator();
        while (it.hasNext()) {
            Role r = it.next();
            List<Permission> permissions = r.getTheRolePermissions();
            Iterator<Permission> itr = permissions.iterator();
            while (itr.hasNext()) {
                int permission = itr.next().getThePermissionCode();
                if (permission == theRequiredPermission) {
                    return true;
                }
            }
        }
        return false;
    }

}
