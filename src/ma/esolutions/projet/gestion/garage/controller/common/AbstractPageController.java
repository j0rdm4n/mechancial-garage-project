/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.common;

import ma.esolutions.projet.gestion.garage.controller.common.IPageController;
import ma.esolutions.projet.gestion.garage.view.common.IPageView;

/**
 *
 * @author Ahmed
 */
public abstract class AbstractPageController implements IPageController {
    
    private IPageView thePageView;

    public AbstractPageController() 
    {
    }
    protected abstract IPageView createPageView();
      @Override
    public void setTheCommandHandler(ICommandHandler cmdHandler) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    @Override
    public IPageView getPageView() {
        if (thePageView == null) {
            thePageView = createPageView();
            thePageView.init(this);
        }
        
        return thePageView;
    }
}
