/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.common;

//import ma.esolutions.projet.gestion.garage.model.AbstractDAO;
import ma.esolutions.projet.gestion.garage.model.BaseEntity;
import ma.esolutions.projet.gestion.garage.view.common.IListView;

/**
 *
 * @author Ahmed
 * @param <T>
 */
public abstract class AbstractDataPageController<T extends BaseEntity> implements IDataPageController<T>{
    //private AbstractDAO dao;
    private IListView<T> view;
}
