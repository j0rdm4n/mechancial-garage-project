/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.common;

import ma.esolutions.projet.gestion.garage.model.BaseEntity;
import ma.esolutions.projet.gestion.garage.model.common.AbstractDAO;
import ma.esolutions.projet.gestion.garage.view.common.IFormView;

/**
 *
 * @author born2code
 */
interface IFormController<T extends BaseEntity> extends IController {
    
    AbstractDAO getDAO();
    IFormView<T> getTheFormView();
   
    void save(T e);
    
    
    
    
}
