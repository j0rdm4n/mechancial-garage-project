/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.common;

import java.util.List;
import ma.esolutions.projet.gestion.garage.model.BaseEntity;
//import ma.esolutions.projet.gestion.garage.model.AbstractDAO;

import ma.esolutions.projet.gestion.garage.view.common.IListView;

/**
 *
 * @author Ahmed
 */
public interface IDataPageController<T> extends IController {
    
    //AbstractDAO getDAO();
    IListView getDataPageView();
    void openFormView(T e);
    //actions
    
    void onAddNew();
    void onEdit();
    void onDelete();
    void onRefresh();
    void onSave(T e);
    List<T> getData(String filter, int start, int end);
    int getDataSize(String filter);
    String getNamedQuery();

    String getNamedQueryWithFilter();

            
    
    
    
    
    
}
