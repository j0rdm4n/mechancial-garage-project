/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.common;

import java.util.Map;
import ma.esolutions.projet.gestion.garage.view.common.IPageView;

/**
 *
 * @author Ahmed
 */
public interface IPageController extends IController{
    
   // protected PageView createPageView();
    IPageView getPageView();

    
    
    
    
}
