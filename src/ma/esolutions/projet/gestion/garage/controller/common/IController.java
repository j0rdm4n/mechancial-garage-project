/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.common;

import java.awt.event.ActionListener;

/**
 *
 * @author Ahmed
 */
public interface IController{

    /**
     *
     */
   ICommandHandler theCommandHandler = null; 
   
    /**
     *
     * @return
     */
    String getControllerName();
    void setTheCommandHandler(ICommandHandler cmdHandler);
    void on(Long action);
    
    
}
