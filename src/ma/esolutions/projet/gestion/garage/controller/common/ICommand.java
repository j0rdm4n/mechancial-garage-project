/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.common;

import java.awt.event.ActionListener;

/**
 *
 * @author Ahmed
 */
public interface ICommand  {
    
    //ICommand getCommand(Long action);
    public static final int SUCCESS_STATUS = 1;
    public static final int FAILURE_STATUS = 0;
    
    void execute();
    
    
    void undo();
    
    
}
