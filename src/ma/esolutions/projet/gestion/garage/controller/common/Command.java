/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.common;

/**
 *
 * @author born2code
 */
public abstract class Command<T> extends StatefulCommand {
    abstract public  void before();
    abstract public void after();
    public abstract void setTheRequiredPermission();
    public abstract boolean checkIfUserHasPermission();
    
    /**
     *
     * @param filter
     * @param start
     * @param end
     * @return
     */
    protected void setSuccessStatus() {
        theCmdStatus = Command.SUCCESS_STATUS;
    }

    protected void setFailureStatus() {
        theCmdStatus = Command.FAILURE_STATUS;
    }

    
}
