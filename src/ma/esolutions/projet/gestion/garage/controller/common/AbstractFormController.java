/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.common;

import ma.esolutions.projet.gestion.garage.model.BaseEntity;
import ma.esolutions.projet.gestion.garage.model.common.AbstractDAO;
import ma.esolutions.projet.gestion.garage.view.common.IFormView;

/**
 *
 * @author born2code
 */
public abstract class AbstractFormController<T extends BaseEntity> implements IFormController {
    
    private AbstractDAO<T> theDAO;
    private IFormView<T> theView;
    
    public AbstractFormController()
    {
        
    }
    protected abstract  AbstractDAO<T> createDAO();
    public AbstractDAO<T> getDAO()
    {
        if(theDAO == null)
        {
            theDAO = createDAO();
        }
        return theDAO;
    }
    protected abstract IFormView<T> createView();
    public IFormView<T> getView()
    {
        if(theView == null)
        {
            theView = createView();
        }
        return theView;
    }
}
