/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.controller;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import ma.esolutions.projet.gestion.garage.controller.commands.LoginCommand;
import ma.esolutions.projet.gestion.garage.controller.common.AbstractPageController;
import ma.esolutions.projet.gestion.garage.model.User;
import ma.esolutions.projet.gestion.garage.view.LoginView;
import ma.esolutions.projet.gestion.garage.view.common.IPageView;

/**
 *
 * @author Ahmed
 */
public class LoginController extends AbstractPageController {

    @Override
    protected IPageView createPageView() {
        /* EntityManagerFactory emf;
         emf = Persistence.createEntityManagerFactory("GestionGaragePU");
         EntityManager theEntityManager = emf.createEntityManager();
         EntityTransaction entityTransaction = null;
         entityTransaction = theEntityManager.getTransaction();
         // entityTransaction.begin();
         // theEntityManager.persist(null);
         //  entityTransaction.commit();*/
        return new LoginView();
    }

    @Override
    public String getControllerName() {
        return "LoginController";
    }

    public void onLogin(User u) {
        System.out.println("username : " + u.getTheUsername() + "password : " + u.getThePassword());
        LoginCommand cmd = new LoginCommand(u.getTheUsername(), u.getThePassword());
        cmd.execute();
        if (LoginCommand.SUCCESS_STATUS == cmd.getStatus()) {
            ApplicationController.getAppView().resumeUILoading();
            MainController theMainController = new MainController();
            theMainController.getPageView().showMainView();

        } else {

        }
        //theCommandHandler.execute(cmd);
    }

    @Override
    public void on(Long action) {

    }

    public boolean checkConfiguration(String filename) {

        File file = new File(filename);
        if (file.exists()) {
            System.out.println("config file exists not the first time run");
            return true;
        } else {
            return false;
        }
    }

    public Map<String, String> readConfiguration(String filename) {
        File file = new File(filename);
        Map<String,String> m =new HashMap();
        try {
            InputStreamReader is = new InputStreamReader(Files.newInputStream(file.toPath()));
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line ="";
            while((line=br.readLine()) != null)
            {
                String[] pairs = line.split(":");
                
                try
                {
                    m.put(pairs[0], pairs[1]);
                }catch(Exception e){}
                
                System.out.println(line);
            }
        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (file.exists()) {

        }
        return m;
    }

    public void createConfiguration(String filename, Map<String, String> c) {
        File file = new File(filename);

        if (file.exists()) {
            System.out.println("configuration file is found");

        } else {
            try {
                file.createNewFile();
                FileOutputStream os = new FileOutputStream(file);

                for (String k : c.keySet()) {
                    os.write(k.getBytes());
                    os.write(":".getBytes());
                    os.write(c.get(k).getBytes());
                    os.write("\n".getBytes());
                }

            } catch (IOException ex) {
                Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
