/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import ma.esolutions.projet.gestion.garage.controller.commands.CreateSuperUserCommand;
import ma.esolutions.projet.gestion.garage.view.ApplicationView;
import ma.esolutions.projet.gestion.garage.view.SplashView;
import org.jvnet.lafwidget.LafWidget;
import org.jvnet.substance.SubstanceLookAndFeel;
import org.jvnet.substance.api.SubstanceConstants.TabContentPaneBorderKind;
import org.jvnet.substance.skin.SubstanceOfficeSilver2007LookAndFeel;

/**
 *
 * @author Ahmed
 */

public abstract class ApplicationController {
    private static final Logger LOGGER = Logger.getLogger(ApplicationController.class.getName());
    
    protected static ApplicationController theAppController ;
    protected static ApplicationView theAppView; 
   // protected static ApplicationModel theAppModel ;
    private SplashView theSplashView;
    private Map<String, String> dbSettings;
    
    
 
    
    private void setView(ApplicationView view)
    {
        if(view != null && this.theAppView == null )
        {
            this.theAppView = view;
        }
    }

    public ApplicationController() {
        
    }
    //pour commencer l'application
    public static ApplicationController get()
    {
        return theAppController; 
    }
    private void createSuperUser()
    {
        CreateSuperUserCommand superUserCommand = new CreateSuperUserCommand("superuser","info4");
        superUserCommand.execute();
    }
    public void run()
    {
        
        System.out.println("The app is running ");
        JFrame.setDefaultLookAndFeelDecorated(true);
        JDialog.setDefaultLookAndFeelDecorated(true);
                try {
            UIManager.setLookAndFeel(new SubstanceOfficeSilver2007LookAndFeel());
        } catch (UnsupportedLookAndFeelException ex) {
            LOGGER.log(Level.SEVERE, "Substance Look and Feel Error", ex);
        }

        // TabbedPane border settings in substance look and feel. 
        UIManager.put(SubstanceLookAndFeel.TABBED_PANE_CONTENT_BORDER_KIND,
                TabContentPaneBorderKind.DOUBLE_PLACEMENT);

        // Cut, copy, paste menu in TextField with substance look and feel. 
        UIManager.put(LafWidget.TEXT_EDIT_CONTEXT_MENU, true);


        //CustomerDAO sr = new CustomerDAO();
        //EntityManager em = sr.getTheEntityManager();
        
       // EntityManagerFactory emf;
       // emf = Persistence.createEntityManagerFactory("GestionGaragePU");
       // EntityManager theEntityManager = emf.createEntityManager();
        //EntityTransaction entityTransaction = null;
        //entityTransaction = theEntityManager.getTransaction();
       // entityTransaction.begin();
       // theEntityManager.persist(null);
      //  entityTransaction.commit();
        
     //   SessionEntity session = HibernateUtil.getSessionFactory().openSession();
      //  Transaction t = session.getTransaction();
       // t.commit();
       // session.close();
        
        theAppView = new ApplicationView("Gestion de Garage v1.0");
      
       
       
       // JRibbonBand band1 = new JRibbonBand("Hello",  null);
     //  JRibbonBand band2 = new JRibbonBand("world!", null);
 
      //  band1.setResizePolicies((List) Arrays.asList(new IconRibbonBandResizePolicy(band1.getControlPanel())));
      //  band2.setResizePolicies((List) Arrays.asList(new IconRibbonBandResizePolicy(band1.getControlPanel())));
 
      //  RibbonTask task1 = new RibbonTask("One", band1);
     //   RibbonTask task2 = new RibbonTask("Two", band2);
    //    theAppView.getRibbon().addTask(task1);
      //  theAppView.getRibbon().addTask(task2);
        theAppView.pack();
        /*SessionDAO theSessionDAO = new SessionDAO();
        SessionEntity theSession;
        try
        {
            theSession = theSessionDAO.find((long)1);
            theSession.setTheFirstTimeRun(false);
            theSessionDAO.update(theSession);
            Session.getSession().setTheFirstTimeRun(false);
            System.out.println("not the first time run");
        }catch(Exception e){
            theSession = new SessionEntity();
            theSession.setTheFirstTimeRun(true);
            Session.getSession().setTheFirstTimeRun(true);
            theSessionDAO.create(theSession);
            System.out.println("this is the first time run");
        }*/
      
        //createSuperUser();
        if(theSplashView != null)
        {
            theSplashView.setVisible(false);
            theSplashView.dispose();
        }
        
        theAppView.setVisible(true);
    }
    public static ApplicationController getAppController()
    {
        return theAppController;
    }
    public  static ApplicationView getAppView()
    {
        return theAppView;
    }
    public void setIntroSplash()
    {
        //theIntroSplash.setVisible();
        //theIntroSplash.dispose();
    }
    public void exit()
    {
        //close database connection
        
        //exit 
        System.exit(0);
    }

    public void setSplashView(SplashView theSplashView) {
        this.theSplashView = theSplashView;
    }

    public void setSettingsMap(Map<String, String> m) {

        dbSettings = m;
    }
    
     public Map<String,String> getSettingsMap() {
       //  Map<String,String> m = new HashMap<>();
     //    m.put("host","localhost");
      //   m.put("port", "3306") ;
      //   return m;
        return dbSettings;
    }
    
}
