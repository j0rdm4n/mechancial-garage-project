/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.controller;

import java.util.Map;
import ma.esolutions.projet.gestion.garage.controller.common.AbstractPageController;
import ma.esolutions.projet.gestion.garage.model.Item;
import ma.esolutions.projet.gestion.garage.model.ItemDAO;
import ma.esolutions.projet.gestion.garage.model.PurchaseOrderDAO;
import ma.esolutions.projet.gestion.garage.model.PurchaseOrderLineDAO;
import ma.esolutions.projet.gestion.garage.model.SalesOrderDAO;
import ma.esolutions.projet.gestion.garage.model.SalesOrderLineDAO;
import ma.esolutions.projet.gestion.garage.view.MainView;
import ma.esolutions.projet.gestion.garage.view.common.IPageView;

/**
 *
 * @author Ahmed
 */
public class MainController extends AbstractPageController {

    private SalesOrderDAO theSalesOrderDAO;
    private SalesOrderLineDAO theSalesOrderLineDAO;
    private ItemDAO theItemDAO;
    private PurchaseOrderDAO thePurchaseOrderDAO;

    @Override
    protected IPageView createPageView() {
        MainView theMainView = new MainView();
        theSalesOrderDAO = new SalesOrderDAO();
        theSalesOrderLineDAO = new SalesOrderLineDAO();
        theItemDAO = new ItemDAO();
        theMainView.init(this);

        return theMainView;
    }

    @Override
    public String getControllerName() {
        return "MainController";
    }

    @Override
    public void on(Long action) {
        return;
    }

    public Map<Number, Double> getAvgSalesPerDay() {
        theSalesOrderDAO.getAvgSalesPerDate();
        return null;
    }

    public Map<String, Number> getSalesPerMonthForYear(int year) {
        return theSalesOrderDAO.getSalesPerMonthForYear(year);
    }

    public Map<String,Number> getSalesPerYears() {
        return theSalesOrderDAO.getSalesPerYears();
    }

    public Map getSalesPerDayForMonth(int month) {
            return theSalesOrderDAO.getSalesPerDayForMonth(month);
    
    }

    public Map<String, Number> getTotalSalesPerItem() {
        Map<String, Number> m = theSalesOrderLineDAO.getTotalSalesPerItem();

        return m;
    }

    public Item getMinSalesProduct() {
        return new Item();
    }

    public Item getMaxSalesProduct() {
        return new Item();
    }

    public String getCurrentMonth() {
        return "Juillet";
    }

    public int getCurrentYear() {
        return 2014;
    }

    public Map<String, Number> getStockPercentPerItem() {
        
        Map<String,Number> m  = theItemDAO.getStockPercentPerItem();
        return m;
    }

    public Map getStockQuantityPerItem() {
        Map<String,Number> m = theItemDAO.getStockQuantityPerItem();
        return m;
    }

    public Map getPurchasesPerDayForMonth(int theMonth) {
        
        if(thePurchaseOrderDAO == null)
            thePurchaseOrderDAO = new PurchaseOrderDAO();
        Map<String, Number> m = thePurchaseOrderDAO.getPurchasesPerDayForMonth(theMonth);
        
        return m;
    }

    public Map getPurchasesPerMonthForYear(int theYear) {
        if(thePurchaseOrderDAO == null)
            thePurchaseOrderDAO = new PurchaseOrderDAO();
        return this.thePurchaseOrderDAO.getPurchasesPerMonthForYear(theYear);
    }

    public Map getPurchasesPerYearForPast10Years() {
        
        if(thePurchaseOrderDAO == null)
            thePurchaseOrderDAO = new PurchaseOrderDAO();
        return this.thePurchaseOrderDAO.getPurchasesPerYearForPast10Years();
    }

    public Map getSalesPerCustomer(int theYear, int theMonth, int theDay) {
        if(theSalesOrderDAO == null)
            theSalesOrderDAO = new SalesOrderDAO();
        
        return this.theSalesOrderDAO.getSalesPerCustomer(theYear,theMonth,theDay);
    }

    public String getCustomerWithBestSales() {
        return "";
    }

    public String getCustomerWithWorstSales() {
            return "";
    }

}
