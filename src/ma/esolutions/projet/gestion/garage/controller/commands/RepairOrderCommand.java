/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.List;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.Customer;
import ma.esolutions.projet.gestion.garage.model.Item;
import ma.esolutions.projet.gestion.garage.model.ItemDAO;
import ma.esolutions.projet.gestion.garage.model.RepairOrder;
import ma.esolutions.projet.gestion.garage.model.RepairOrderDAO;

/**
 *
 * @author E-solutions
 */
public abstract class RepairOrderCommand extends SecureCommand<RepairOrder>{
    
    protected RepairOrder theRepairOrder;
    protected RepairOrderDAO theRepairOrderDAO;
    protected List<Item> theItems;
    protected Customer theCustomer;
    protected ItemDAO theItemDAO;
    
    public RepairOrderCommand(RepairOrder order)
    {
        theRepairOrder = order;
        theRepairOrderDAO = new RepairOrderDAO(); 
        theItemDAO = new ItemDAO();
        theItems = theItemDAO.all();
        
    }
    
}
