/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.ArrayList;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.Item;
import ma.esolutions.projet.gestion.garage.model.ItemDAO;
import ma.esolutions.projet.gestion.garage.model.ItemPlace;

/**
 *
 * @author E-solutions
 */
public abstract class InventoryCommand extends SecureCommand<Item> {

    protected Item theItem;
    protected ItemDAO theItemDAO;
    protected ArrayList<Item> theAddedItems;
    protected ArrayList<Item> theInventoryItems;
    protected ArrayList<ItemPlace> theInventoryPlaces;
    boolean showPermissionsDlg = true;
    
    protected boolean itemExists(Item it)
    {
         try {
            TypedQuery<Item> query = theItemDAO.getTheEntityManager().createQuery("SELECT it FROM Item it WHERE it.theItemName = :name ",Item.class);
            query.setParameter("name", it.getTheItemName()).getSingleResult();
            return true;
        } catch (NoResultException e) {
            return false;
        }
    }
    protected void setTheInventoryItems()
    {
        TypedQuery<Item> query = theItemDAO.getTheEntityManager().createQuery("select it from Item it",Item.class);
        
        theInventoryItems = (ArrayList<Item>) query.getResultList();
        System.out.println(theInventoryItems);
    }
    protected void setTheInventoryPlaces()
    {
        TypedQuery<ItemPlace> query = theItemDAO.getTheEntityManager().createQuery("select ip from ItemPlace ip", ItemPlace.class);
        
        theInventoryPlaces = (ArrayList<ItemPlace>) query.getResultList();
    }
    public InventoryCommand(Item item)
    {
        theItem = item;
        theItemDAO = new ItemDAO();
        
    }
   

   
    
}
