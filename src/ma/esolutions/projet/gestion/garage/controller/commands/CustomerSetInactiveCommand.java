/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.controller.common.ICommand;
import ma.esolutions.projet.gestion.garage.model.Customer;
import ma.esolutions.projet.gestion.garage.model.Permission;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.model.User;
import ma.esolutions.projet.gestion.garage.model.UserDAO;

/**
 *
 * @author E-solutions
 */
public class CustomerSetInactiveCommand extends CustomerCommand {

    private int theId;
    
    public CustomerSetInactiveCommand(Customer customer)
    {
        super(customer);
    }
   
     
    @Override
    public void execute_body() {
        // don't delet make as inactive
            theCustomerDAO.remove(theCustomer);
            //theCustomerDAO.find(Long.MIN_VALUE);
            setSuccessStatus();
    }

    @Override
    public void undo() {
            theCustomerDAO.create(theCustomer);
            setSuccessStatus();
    }

   


    @Override
    public void before() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void after() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setTheRequiredPermission() {
        theRequiredPermission = PermissionsCodes.PERMISSION_DELETE_CUSTOMERS;
    }

    @Override
    public void save(Customer e) {
    }
    
}
