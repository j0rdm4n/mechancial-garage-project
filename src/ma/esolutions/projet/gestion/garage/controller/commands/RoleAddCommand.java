/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.List;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.controller.common.SecureFormCommand;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.model.Role;
import ma.esolutions.projet.gestion.garage.model.RoleDAO;
import ma.esolutions.projet.gestion.garage.view.RoleFormView;

/**
 *
 * @author E-solutions
 */
public class RoleAddCommand extends SecureFormCommand<Role>{

    private Role theRole;
    private RoleDAO theRoleDAO;
    private RoleFormView theRoleFormView;
    public RoleAddCommand(Role role)
    {
       theRole = role;
       theRoleDAO = new RoleDAO();
       theRoleFormView = new RoleFormView(role);
    }
    @Override
    public void execute_body() {
        theRoleFormView.setTheControllerCommand(this);
        theRoleFormView.showDialog();
        //theRole = theRoleFormView.getEntity();
        //theRoleDAO.create(theRole);
        //System.out.println(theRole);
    }

    @Override
    public void setTheRequiredPermission() {
        theRequiredPermission = PermissionsCodes.PERMISSION_ADD_ROLE;
    }

    @Override
    public void undo() {
    }

    @Override
    public void save(Role e) {
        theRoleDAO.create(e);
        
    }

    @Override
      public List<Role> getData(String filter,int pageNumber,int pageSize)
    {
       /* if(pageNumber > lastPage(countPages(),pageSize))
        {
            return null;
        }*/
        if(filter.equals(""))
        {
            TypedQuery<Role> q = theRoleDAO.getTheEntityManager().createQuery("select r from Role r ", Role.class);
            q.setFirstResult((pageNumber -  1)*pageSize);
            q.setMaxResults(pageSize);
            List<Role> l = q.getResultList();
          
            
            return l;
        }
        return null;
    }
    
}
