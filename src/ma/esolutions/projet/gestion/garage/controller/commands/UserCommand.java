/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.List;
import java.util.Map;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.User;
import ma.esolutions.projet.gestion.garage.model.UserDAO;

/**
 *
 * @author E-solutions
 */
public abstract class UserCommand extends SecureCommand<User> {

    protected User theUser;
    protected UserDAO theUserDAO;

    public UserCommand() {
        this(new User());
    }

    public UserCommand(User user) {
        theUser = user;
        theUserDAO = new UserDAO();
    }
    @Override
    public int getDataSize(String filter)
    {
        return theUserDAO.all().size();
    }
    @Override
    public List<User> getData(String filter, int start, int end) {

        if (filter.equals("")) {
            TypedQuery<User> q = theUserDAO.getTheEntityManager().createQuery("select u from User u ", User.class);
            q.setFirstResult(start);
            q.setMaxResults(end);
            List<User> l = q.getResultList();

            return l;
        }
        else
        {
            Map<String,String[]> parsed = parseFilter(filter);
            String name = parsed.get("theName")[1];
            String email = parsed.get("theEmail")[1];
            String[] fl =name.split(" ") ;
            String firstname="";String lastname="";
            try
            {
                firstname = fl[0];lastname=fl[1];
            }catch(Exception e)
            {
                    
            }
            String sql = "select e from User e where e.theFirstname like :firstname and e.theLastname like :lastname and e.theEmail like :email ";
            TypedQuery q = this.theUserDAO.getTheEntityManager().createQuery(sql, User.class);
            q.setParameter("firstname","%"+firstname+"%");
            q.setParameter("lastname","%"+lastname+"%");
            q.setParameter("email", "%"+email+"%");
            
            return q.getResultList();
            
        }
    }

}
