/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.List;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.model.Vehicle;
import ma.esolutions.projet.gestion.garage.model.VehicleDAO;
import ma.esolutions.projet.gestion.garage.view.ApplicationView;
import ma.esolutions.projet.gestion.garage.view.VehicleListView;

/**
 *
 * @author E-solutions
 */
public class VehicleListCommand extends VehicleCommand{

    private VehicleListView theVehicleListView;
    private VehicleDAO theVehicleDAO = new VehicleDAO();
    public VehicleListCommand()
    {
        super();
        theVehicleListView = new VehicleListView();
    }
    
     
    @Override
    public void execute_body() {
        theVehicleListView.setTheInvokingCommand(this);
       theVehicleListView.refreshData();
        
        updateView();
        setSuccessStatus();
    }

    @Override
    public void setTheRequiredPermission() {
        theRequiredPermission = PermissionsCodes.PERMISSION_LIST_VEHICLES;
    }

    @Override
    public void undo() {
    }

    private void updateView() {
        ApplicationView.showListView(theVehicleListView);
    }
    
}
