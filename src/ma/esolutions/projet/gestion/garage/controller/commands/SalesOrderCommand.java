/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.Date;
import java.util.List;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.controller.common.SecureFormCommand;
import ma.esolutions.projet.gestion.garage.model.Customer;
import ma.esolutions.projet.gestion.garage.model.Item;
import ma.esolutions.projet.gestion.garage.model.ItemDAO;
import ma.esolutions.projet.gestion.garage.model.PaymentTypes;
import ma.esolutions.projet.gestion.garage.model.SalesInvoice;
import ma.esolutions.projet.gestion.garage.model.SalesInvoiceDAO;
import ma.esolutions.projet.gestion.garage.model.SalesInvoicePayment;
import ma.esolutions.projet.gestion.garage.model.SalesInvoicePaymentDAO;
import ma.esolutions.projet.gestion.garage.model.SalesOrder;
import ma.esolutions.projet.gestion.garage.model.SalesOrderDAO;
import ma.esolutions.projet.gestion.garage.model.SalesOrderDelivery;
import ma.esolutions.projet.gestion.garage.model.SalesOrderDeliveryDAO;
import ma.esolutions.projet.gestion.garage.model.SalesOrderLine;
import ma.esolutions.projet.gestion.garage.view.SalesOrderFormView;

/**
 *
 * @author E-solutions
 */
public abstract class SalesOrderCommand extends SecureFormCommand<SalesOrder> {

    protected SalesOrder theSalesOrder;
    protected SalesOrderDAO theSalesOrderDAO;
    protected List<Item> theItems;
    protected Customer theCustomer;
    protected ItemDAO theItemDAO;
    private final SalesInvoiceDAO theSalesInvoiceDAO;
    private final SalesInvoicePaymentDAO theInvoicePaymentDAO;
    private SalesInvoice theSalesInvoice;
    private SalesInvoicePayment theInvoicePayment;
    private SalesOrderDelivery theSalesOrderDelivery;
    private final SalesOrderDeliveryDAO theSalesOrderDeliveryDAO;
    private double theTTCAvance;
    private double theHTAvance;
    public SalesOrderCommand(SalesOrder order) {
        theSalesOrder = order;
        theSalesOrderDAO = new SalesOrderDAO();
        theSalesOrderDeliveryDAO = new SalesOrderDeliveryDAO();
        theSalesInvoiceDAO = new SalesInvoiceDAO();
        theInvoicePaymentDAO = new SalesInvoicePaymentDAO();
        theItemDAO = new ItemDAO();
        theItems = theItemDAO.all();

    }

    @Override
    public void save(SalesOrder order) {
        theSalesOrderDAO.create(order);
        theSalesOrder = order;
        if (theSalesOrder != null) {
            System.out.println("saving sales order");
         //   createOrderDelivery();
           // createOrderInvoice();
           /* if (theSalesOrder.getThePaymentType() == PaymentTypes.CASH) {
                createInvoicePayment(theSalesOrder.getTheTotalTTCPrice(),theSalesOrder.getTheTotalHTPrice());
                
            }
            if (theSalesOrder.getThePaymentType() == PaymentTypes.CREDIT) {
                createInvoicePayment(theTTCAvance,theHTAvance);
               // createCustomerCredit(theSalesOrder.getTheTotalTTCPrice()-theTTCAvance);
            }*/
            System.out.println("payment type "+ theSalesOrder.getThePaymentType());

        }
    }

    private void createCustomerCredit(double amount) {
        Customer c = theSalesOrder.getTheCustomer();
        c.setTheCredit(amount);
    }

    private void createOrderDelivery() {

        theSalesOrderDelivery = new SalesOrderDelivery();
        setOrderDelivery();
        theSalesOrderDeliveryDAO.create(theSalesOrderDelivery);
    }

    private void createOrderInvoice() {
        theSalesInvoice = new SalesInvoice();
        setOrderInvoice();
        theSalesInvoiceDAO.create(theSalesInvoice);
    }

    private void createInvoicePayment(double ttc,double ht) {
        theInvoicePayment = new SalesInvoicePayment();
        setInvoicePayment(ttc,ht);
        theInvoicePaymentDAO.create(theInvoicePayment);
    }

    private void setOrderInvoice() {
        if (theSalesInvoice != null) {
            theSalesInvoice.setTheInvoiceDate(new Date());
        //    theSalesInvoice.setThePaymentType(theSalesOrder.getThePaymentType());
            theSalesInvoice.setThePayementDate(theSalesOrder.getTheClosedDate());
            theSalesInvoice.setTheSalesOrder(theSalesOrder);
        }
    }

    @Override
    public List<SalesOrder> getData(String filter, int pageNumber, int pageSize) {
        if (filter.equals("")) {
            TypedQuery<SalesOrder> q = theSalesOrderDAO.getTheEntityManager().createQuery("select s from SalesOrder s ", SalesOrder.class);
            q.setFirstResult((pageNumber - 1) * pageSize);
            q.setMaxResults(pageSize);
            List<SalesOrder> l = q.getResultList();

            return l;
        }
        return null;
    }

    private void setInvoicePayment(double ttc,double ht) {
        if (theInvoicePayment != null) {
            theInvoicePayment.setTheSalesInvoice(theSalesInvoice);
            theInvoicePayment.setTheTTCAmount(ttc);
            theInvoicePayment.setTheHTAmount(ht);
            theInvoicePayment.setThePaymentDate(new Date());
        }

    }

    private void setOrderDelivery() {
        if (theSalesOrderDelivery != null) {
            theSalesOrderDelivery.setTheSalesOrder(theSalesOrder);
            theSalesOrderDelivery.setTheDeliveryDate(theSalesOrder.getTheClosedDate());
        }
    }

    private Double calAvanceHT() {
        return theHTAvance;
    }

}
