/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.model.Item;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.view.ApplicationView;
import ma.esolutions.projet.gestion.garage.view.InventoryListView;

/**
 *
 * @author E-solutions
 */
public class InventoryListCommand extends InventoryCommand {

    protected InventoryListView theInventoryListView;

    public InventoryListCommand(Item item) {
        super(item);

    }

    public InventoryListView getTheInventoryListView() {
        return theInventoryListView;
    }

    @Override
    public List<Item> getData(String filter, int start, int end) {
        if (filter.equals("")) {
            TypedQuery<Item> q = theItemDAO.getTheEntityManager().createQuery("select it from Item it ", Item.class);
            q.setFirstResult(start);
            q.setMaxResults(end);
            List<Item> l = q.getResultList();

            return l;
        } else if (filter.equalsIgnoreCase("limit_warning")) {
            TypedQuery<Item> q = theItemDAO.getTheEntityManager().createQuery("select it from Item it where it.theItemQuantity <= it.stockMin", Item.class);
            //q.setFirstResult(start);
            //q.setMaxResults(end);
            List<Item> l = q.getResultList();

            return l;
        } else {
            Map<String, String[]> parsed = parseFilter(filter);
            String sql = "";

            String quantityOp = parsed.get("theItemQuantity")[0];

            TypedQuery<Item> q;
            if (parsed.get("theVendor")[1].equals("tous") && parsed.get("theItemPlace")[1].equals("tous")) {
                System.out.println("inhere");
                sql = "select it from Item it where it.theItemName " + " like  :name" + " and it.theItemQuantity" + quantityOp + " :quantity ";

                q = theItemDAO.getTheEntityManager().createQuery(sql, Item.class);

                q.setParameter("name", "%" + parsed.get("theItemName")[1] + "%");
                q.setParameter("quantity", Long.parseLong(parsed.get("theItemQuantity")[1]));

            } else if (parsed.get("theVendor")[1].equals("tous")) {
                sql = "select it from Item it where it.theItemName " + " like  :name" + " and it.theItemQuantity" + quantityOp + " :quantity "
                        + "    and it.theItemPlace.theItemPlaceName = :place";

                q = theItemDAO.getTheEntityManager().createQuery(sql, Item.class);

                q.setParameter("name", "%" + parsed.get("theItemName")[1] + "%");
                q.setParameter("place", parsed.get("theItemPlace")[1]);
                q.setParameter("quantity", Long.parseLong(parsed.get("theItemQuantity")[1]));

            } else if (parsed.get("theItemPlace")[1].equals("tous")) {
                sql = "select it from Item it where it.theItemName " + " like  :name" + " and it.theItemQuantity" + quantityOp + " :quantity "
                        + "   and it.theVendor.theVendorName = :vendor";

                q = theItemDAO.getTheEntityManager().createQuery(sql, Item.class);
                q.setParameter("vendor", parsed.get("theVendor")[1]);

                q.setParameter("name", "%" + parsed.get("theItemName")[1] + "%");
                q.setParameter("quantity", Long.parseLong(parsed.get("theItemQuantity")[1]));

            } else {

                sql = "select it from Item it where it.theItemName " + " like  :name" + " and it.theItemQuantity" + quantityOp + " :quantity "
                        + "   and it.theVendor.theVendorName = :vendor and it.theItemPlace.theItemPlaceName = :place";

                q = theItemDAO.getTheEntityManager().createQuery(sql, Item.class);

                q.setParameter("name", "%" + parsed.get("theItemName")[1] + "%");
                q.setParameter("vendor", parsed.get("theVendor")[1]);
                q.setParameter("place", parsed.get("theItemPlace")[1]);
                q.setParameter("quantity", Long.parseLong(parsed.get("theItemQuantity")[1]));
            }

            //q.setParameter("nop", parsed.get("theItemName")[0]);
            // String[] dateParts = parsed.get("theItemAddedDate")[1].split(" ");
            //System.out.println("dates " +parsed.get("theItemAddedDate")[1] );
            // q.setParameter("dateyear", Integer.parseInt(dateParts[0]));
            //q.setParameter("datemonth", Integer.parseInt(dateParts[1]));
            // q.setParameter("dateday", Integer.parseInt(dateParts[2]));
            //   System.out.println("pasred name op " + parsed.get("theItemName")[0] + " oper " + parsed.get("theItemName")[1]);
            // q.setParameter("op",parsed.get("theItemQuantity")[0] );
            //   System.out.println("pasred quantity op " + parsed.get("theItemQuantity")[0] + " oper " + parsed.get("theItemQuantity")[1]);
            List<Item> frs = q.getResultList();
            System.out.println("filtered result set is " + frs);
            return frs;
        }

    }

    @Override
    public int getDataSize(String filter) {
        System.out.println("data size " + theItemDAO.all().size());
        return theItemDAO.all().size();
    }

    @Override
    public void execute_body() {

        theInventoryListView = null;
        // List<Item> l = getData("",1,1000);
        theInventoryListView = new InventoryListView(null);
        theInventoryListView.setTheInvokingCommand(this);
        // this.theInventoryListView.refreshData(this.getData("", 1, 1000));
        this.theInventoryListView.refreshData();
        updateView();
        setSuccessStatus();

    }

    @Override
    public void setTheRequiredPermission() {
        this.theRequiredPermission = PermissionsCodes.PERMISSION_LIST_INVENTORY;
    }

    @Override
    public void undo() {
    }

    private void updateView() {

        ApplicationView.showListView(theInventoryListView);
    }

}
