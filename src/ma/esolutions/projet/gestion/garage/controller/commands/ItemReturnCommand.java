/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.controller.common.SecureFormCommand;
import ma.esolutions.projet.gestion.garage.model.Item;
import ma.esolutions.projet.gestion.garage.model.ItemDAO;
import ma.esolutions.projet.gestion.garage.model.MinimalStockException;
import ma.esolutions.projet.gestion.garage.model.NegativeStockException;
import ma.esolutions.projet.gestion.garage.model.Payment;
import ma.esolutions.projet.gestion.garage.model.PaymentDAO;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.model.Return;
import ma.esolutions.projet.gestion.garage.model.ReturnDAO;
import ma.esolutions.projet.gestion.garage.model.SalesOrder;
import ma.esolutions.projet.gestion.garage.model.SalesOrderDAO;
import ma.esolutions.projet.gestion.garage.model.SalesOrderLine;
import ma.esolutions.projet.gestion.garage.view.ReturnFormView;

/**
 *
 * @author E-solutions
 */
public class ItemReturnCommand extends SecureFormCommand<Return> {

    private ReturnFormView theReturnFormView;
    private final ReturnDAO theReturnDAO;
    private SalesOrderDAO theSalesOrderDAO;
    private ItemDAO itemDAO;
    private PaymentDAO thePaymentDAO;
    private List<Item> returned;

    public ItemReturnCommand(Return ret) {
        this.theReturnFormView = new ReturnFormView(ret);
        theReturnDAO = new ReturnDAO();
        returned = new ArrayList<>();
    }

    @Override
    public void execute_body() {
        theReturnFormView.setTheControllerCommand(this);
        theReturnFormView.showDialog();

    }

    @Override
    public void setTheRequiredPermission() {
        this.theRequiredPermission = PermissionsCodes.PERMISSION_ADD_RETURN;
    }

    @Override
    public void undo() {
    }

    @Override
    public List<Return> getData(String filter, int pageNumber, int pageSize) {
        if (filter.equals("")) {
            TypedQuery<Return> q = theReturnDAO.getTheEntityManager().createQuery("select c from Return c ", Return.class);
            q.setFirstResult((pageNumber - 1) * pageSize);
            q.setMaxResults(pageSize);
            List<Return> l = q.getResultList();

            return l;
        }
        return null;
    }

    @Override
    public void save(Return e) {
        System.out.println("saving the return order");
        theReturnDAO.create(e);
        //SalesOrder o = loadCorrespondingSalesOrder(e);
       // returnToStock(e);
      //  updateCaisse(e);
        
        System.out.println("returned items are : " + returned);
        
    }

    private void returnToStock(Return e) {
        System.out.println("retrun to stock quntity " + e.getTheItemQuantity() + " of " + e.getTheItem().toString());

        itemDAO = new ItemDAO();
        Item ee = new Item();
        ee.setTheItemName(e.getTheItem().getTheItemName());
        try {
            ee.setTheItemQuantity(e.getTheItemQuantity());
        } catch (NegativeStockException ex) {
            Logger.getLogger(ItemReturnCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MinimalStockException ex) {
            Logger.getLogger(ItemReturnCommand.class.getName()).log(Level.SEVERE, null, ex);
        }

        itemDAO.returnItem(ee);
    }

    private SalesOrder loadCorrespondingSalesOrder(Return e) {

        theSalesOrderDAO = new SalesOrderDAO();
        SalesOrder o = theSalesOrderDAO.getCorrespondingSalesOrder(e);
        return o;
    }

    private void updateCaisse(Return e) {
        // e.setTheTotalPrice(theCmdStatus);
        System.out.println("total price to return to customer " + e.getTheTotalPrice());
        Payment p = new Payment();
        p.setThePaymentType("retour");
        /*  p.setTheHTaxAmount(sumttc);
         p.setTheTTCAmount(sumht);
         p.setTheCashingDate(new Date());
         System.out.println("customer " + theSalesOrder.getTheCustomer());
         p.setTheCustomer(theSalesOrder.getTheCustomer());
        
         thePaymentDAO.create(p);
         thePaymentDAO.getTodayTotal();
         */
        p.setTheCustomer(e.getTheCustomer());

        p.setTheHTaxAmount((-1) * e.getTheTotalPrice());
        p.setTheTTCAmount((-1) * (e.getTheTotalPrice() / (1 + e.getTheItem().getTheTVA() / 100)));
        System.out.println(" ht price to return to customer " + p.getTheAmountTTC());
        p.setTheCashingDate(new Date());
        thePaymentDAO = new PaymentDAO();
        thePaymentDAO.create(p);
        thePaymentDAO.getTodayTotal();

    }

    public void addToReturnList(Item e) {
        if (!returned.stream().map(ee -> ee.getId()).collect(Collectors.toList()).contains(e.getId())) {
            returned.add(e);

        }
    }

    public List<Item> getReturnedItems() {
        return returned;
    }

}
