/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.List;
import java.util.Map;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.model.PurchaseOrder;
import ma.esolutions.projet.gestion.garage.model.PurchaseOrderDAO;
import ma.esolutions.projet.gestion.garage.model.SalesOrder;
import ma.esolutions.projet.gestion.garage.view.ApplicationView;
import ma.esolutions.projet.gestion.garage.view.PurchaseOrderListView;

/**
 *
 * @author E-solutions
 */
public class PurchaseOrderListCommand extends SecureCommand<PurchaseOrder> {
    private PurchaseOrderDAO thePurchaseOrderDAO;
    private final PurchaseOrderListView thePurchaseOrderListView;

    public PurchaseOrderListCommand()
    {
        
        thePurchaseOrderListView = new PurchaseOrderListView(getData("",1,100));
    }
    @Override
    public List<PurchaseOrder> getData(String filter, int start, int end) {
        thePurchaseOrderDAO = new PurchaseOrderDAO();
      if(filter.equals(""))
        {
            TypedQuery<PurchaseOrder> q = thePurchaseOrderDAO.getTheEntityManager().createQuery("select p from PurchaseOrder p ", PurchaseOrder.class);
            q.setFirstResult(start);
            q.setMaxResults(end);
            List<PurchaseOrder> l = q.getResultList();
          
            
            return l;
        }
      else
      {
          Map<String,String[]> parsed = parseFilter(filter);
          String vendorName = parsed.get("theVendor")[1];
          System.out.println("vendor " + vendorName);
          String sql = "select p from PurchaseOrder p where p.theVendor.theVendorName like :name";
          TypedQuery<PurchaseOrder> q = thePurchaseOrderDAO.getTheEntityManager().createQuery(sql, PurchaseOrder.class);
          q.setParameter("name", "%"+vendorName+"%");
          
          return q.getResultList();
      }
        
    }
    @Override
    public int getDataSize(String filter)
    {
        return thePurchaseOrderDAO.all().size();
    }
    @Override
    public void execute_body() {
        
        thePurchaseOrderListView.setTheInvokingCommand(this);
        thePurchaseOrderListView.refreshData();
        updateView();
        setSuccessStatus();
        
    }
     private void updateView() {
            ApplicationView.showListView(thePurchaseOrderListView);
    }
    @Override
    public void setTheRequiredPermission() {
        this.theRequiredPermission = PermissionsCodes.PERMISSION_LIST_PURCHASES;
    }

    @Override
    public void undo() {
    }
    
}
