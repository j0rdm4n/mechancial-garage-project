/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.List;
import ma.esolutions.projet.gestion.garage.controller.common.SecureFormCommand;
import ma.esolutions.projet.gestion.garage.model.CommercialPaper;
import ma.esolutions.projet.gestion.garage.model.CommercialPaperDAO;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.view.CommercialPaperFormView;

/**
 *
 * @author E-solutions
 */
public class CommercialPaperAddCommand extends SecureFormCommand<CommercialPaper>{
    private CommercialPaper commercialPaper;
    private CommercialPaperFormView commercialFormView;
    private CommercialPaperDAO commercialPaperDAO;

    public CommercialPaperAddCommand(CommercialPaper paper)
    {
        commercialPaper = paper;
    }
    @Override
    public List<CommercialPaper> getData(String filter, int start, int end) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void execute_body() {
        commercialFormView = new CommercialPaperFormView(commercialPaper);
        commercialFormView.setTheControllerCommand(this);
        
        commercialFormView.showDialog();
    }

    @Override
    public void setTheRequiredPermission() {
        theRequiredPermission = PermissionsCodes.PERMISSION_ADD_COMMERCIALPAPER;
    }

    @Override
    public void undo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save(CommercialPaper e) {
        
        commercialPaperDAO = new CommercialPaperDAO();
        commercialPaperDAO.create(e);
    }
    
}
