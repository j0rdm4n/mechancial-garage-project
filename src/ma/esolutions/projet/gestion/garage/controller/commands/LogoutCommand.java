/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.List;
import ma.esolutions.projet.gestion.garage.app.Session;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.controller.common.ICommand;
import ma.esolutions.projet.gestion.garage.model.User;
import ma.esolutions.projet.gestion.garage.view.ApplicationView;

/**
 *
 * @author E-solutions
 */
public class LogoutCommand extends SecureCommand{

    private User theUser;
    public LogoutCommand(User u){
        theUser = u;
        
    }
 
    @Override
    public void execute_body() {
            Session.getSession().setTheLoginStatus(false);
            Session.getSession().setTheCurrentUser(null);
            theCmdStatus = LogoutCommand.SUCCESS_STATUS;
    }

    @Override
    public void undo() {
        //ApplicationView.showConnectionPage();
    }

  
    @Override
    public int getStatus() {
            return theCmdStatus;
    }

    @Override
    public void before() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void after() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setTheRequiredPermission() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List getData(String filter, int start, int end) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
