/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.List;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.controller.common.IFormCommand;
import ma.esolutions.projet.gestion.garage.model.Item;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.view.InventoryFormView;
import ma.esolutions.projet.gestion.garage.view.InventoryListView;

/**
 *
 * @author E-solutions
 */
public class InventoryAddCommand extends InventoryCommand implements IFormCommand<Item> {

    InventoryFormView theInventoryFormView ; 
    InventoryListView theInventoryListView ; 
    private  Item theModel;
    public InventoryAddCommand(Item item) {
        super(item);
        theModel = item;
        
    }
    
   
    @Override
    public void execute_body() {
        setTheInventoryPlaces();
        setTheInventoryItems();
      
      /*  if(this.theInventoryPlaces.isEmpty())
        {
            ItemPlace p1 = new ItemPlace();
            p1.setTheItemPlaceName("Place1");
            ItemPlace p2 = new ItemPlace();
            p2.setTheItemPlaceName("Place2");
            ItemPlaceDAO theItemPlaceDAO = new ItemPlaceDAO();
            
            theInventoryPlaces.add(p1);
            theItemPlaceDAO.create(p1);
            theInventoryPlaces.add(p2);
            theItemPlaceDAO.create(p2);
        }*/
        InventoryFormView theInventoryFormView = new InventoryFormView(theModel,this.theInventoryPlaces,this.theInventoryItems);
        theInventoryFormView.setTheControllerCommand(this);
        theInventoryListView = ApplicationController.getAppView().getInventoryListView();
        theInventoryFormView.setTheListView(theInventoryListView);
        theInventoryFormView.showDialog();
        
     
        
    }

    @Override
    public void setTheRequiredPermission() {
        this.theRequiredPermission = PermissionsCodes.PERMISSION_ADD_INVENTORY;
    }

    @Override
    public void undo() {
        theItemDAO.remove(theItem);
    }

    @Override
    public void save(Item e) {
        theItemDAO.create(e);
            
    }

    @Override
    public List<Item> getData(String filter,int pageNumber,int pageSize)
    {
       /* if(pageNumber > lastPage(countPages(),pageSize))
        {
            return null;
        }*/
        if(filter.equals(""))
        {
            TypedQuery<Item> q = theItemDAO.getTheEntityManager().createQuery("select c from Item c ", Item.class);
            q.setFirstResult((pageNumber -  1)*pageSize);
            q.setMaxResults(pageSize);
            List<Item> l = q.getResultList();
          
            return l;
        }
        return null;
    }
    
}
