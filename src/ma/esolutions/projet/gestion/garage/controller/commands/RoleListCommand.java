/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.List;
import java.util.Map;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.model.Role;
import ma.esolutions.projet.gestion.garage.model.RoleDAO;
import ma.esolutions.projet.gestion.garage.view.ApplicationView;
import ma.esolutions.projet.gestion.garage.view.RoleListView;

/**
 *
 * @author E-solutions
 */
public class RoleListCommand extends SecureCommand {

    
    RoleListView theRoleListView;
    RoleDAO theRoleDAO;
    public RoleListCommand()
    {
        theRoleDAO = new RoleDAO();
        theRoleListView = new RoleListView();
    }
    
    @Override
    public int getDataSize(String filter)
    {
        return this.theRoleDAO.all().size();
    }
    @Override
      public List<Role> getData(String filter,int start,int end)
    {
       /* if(pageNumber > lastPage(countPages(),pageSize))
        {
            return null;
        }*/
        if(filter.equals(""))
        {
            TypedQuery<Role> q = theRoleDAO.getTheEntityManager().createQuery("select r from Role r ", Role.class);
            q.setFirstResult(start);
            q.setMaxResults(end);
            List<Role> l = q.getResultList();
          
            
            return l;
        }
        else
        {
            Map<String,String[]> parsed = parseFilter(filter);
            String roleName = parsed.get("theRoleName")[1];
            TypedQuery<Role> q = theRoleDAO.getTheEntityManager().createQuery("select r from Role r where r.theRoleName like :name ", Role.class);
            q.setParameter("name", "%"+roleName+"%");
            
            return q.getResultList();
        }
        
    }
    @Override
    public void execute_body() {
        
            theRoleListView.setTheInvokingCommand(this);
            theRoleListView.refreshData();
            updateView();
            setSuccessStatus();
    }

    @Override
    public void setTheRequiredPermission() {
        theRequiredPermission = PermissionsCodes.PERMISSION_LIST_ROLES;
    }

    @Override
    public void undo() {
    }

    private void updateView() {
    ApplicationView.showListView(theRoleListView);
    }
    
}
