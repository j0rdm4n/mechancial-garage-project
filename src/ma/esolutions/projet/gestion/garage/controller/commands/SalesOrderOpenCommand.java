/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.ArrayList;
import java.util.List;
import ma.esolutions.projet.gestion.garage.controller.common.IFormCommand;
import ma.esolutions.projet.gestion.garage.model.Customer;
import ma.esolutions.projet.gestion.garage.model.CustomerDAO;
import ma.esolutions.projet.gestion.garage.model.PaymentTypes;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.model.SalesOrder;
import ma.esolutions.projet.gestion.garage.view.SalesOrderFormView;

/**
 *
 * @author E-solutions
 */
public class SalesOrderOpenCommand extends SalesOrderCommand{

    protected SalesOrderFormView theSalesOrderFormView;
    public SalesOrderOpenCommand(SalesOrder order) {
        super(order);
        theItems = theItemDAO.all();
        theSalesOrderFormView = new SalesOrderFormView(theSalesOrder,theItems);
        
    }

    
    @Override
    public void setTheRequiredPermission() {
            theRequiredPermission = PermissionsCodes.PERMISSION_ADD_SALES;
    }

    @Override
    public void save(SalesOrder order) {
        System.out.println("before persisting sales order");
        theSalesOrderDAO.create(order);
        
        theSalesOrder = order;
        if (theSalesOrder != null) {
           
            theSalesOrderFormView.prepareToPrintBill();
            Customer cr = order.getTheCustomer();
        cr.addTheSalesOrder(theSalesOrder);
        
        (new CustomerDAO()).update(cr);    
        
        }
    }
   
    @Override
    public void execute_body() {
            
            theSalesOrderFormView.setTheControllerCommand(this);
            theSalesOrderFormView.showDialog();
            SalesOrder order = theSalesOrderFormView.getEntity();
            
           // notifyStock();
            System.out.println(order);
            setSuccessStatus();
    }


    @Override
    public void undo() {
        theSalesOrderDAO.remove(theSalesOrder);
        setSuccessStatus();
    }

    @Override
    public List<SalesOrder> getData(String filter, int start, int end) {
        return new ArrayList();
    }


    
    
    
}
