/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.List;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.controller.common.IFormCommand;
import ma.esolutions.projet.gestion.garage.model.Customer;
import ma.esolutions.projet.gestion.garage.model.CustomerDAO;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.model.RepairOrder;
import ma.esolutions.projet.gestion.garage.view.RepairOrderFormView;

/**
 *
 * @author E-solutions
 */
public class RepairOrderOpenCommand extends RepairOrderCommand implements IFormCommand<RepairOrder> {

    protected RepairOrderFormView theRepairOrderFormView;

    public RepairOrderOpenCommand(RepairOrder order) {
        super(order);
        theItems = theItemDAO.all();
        theRepairOrderFormView = new RepairOrderFormView(theRepairOrder, theItems);
    }

    @Override
    public void execute_body() {
        theRepairOrderFormView.setTheControllerCommand(this);
        theRepairOrderFormView.showDialog();
        System.out.println(" save repair order " + theRepairOrderFormView.getEntity());
        //this.theRepairOrderDAO.create(theRepairOrderFormView.getEntity());

    }

    @Override
    public void setTheRequiredPermission() {
        this.theRequiredPermission = PermissionsCodes.PERMISSION_ADD_REPAIR;
    }

    @Override
    public void undo() {
    }

    @Override
    public void save(RepairOrder e) {
        theRepairOrderDAO.create(e);
        /* if(theRepairOrderFormView != null)
         {
         theRepairOrderFormView.notifyStock();
         theRepairOrderFormView.notifyPayment();
         }*/
        theRepairOrder = e;

        if (theRepairOrder != null) {

            Customer cr = e.getTheCustomer();
            cr.addTheRepairOrder(theRepairOrder);

            (new CustomerDAO()).update(cr);

        }

    }

    @Override
    public List<RepairOrder> getData(String filter, int pageNumber, int pageSize) {
        if (filter.equals("")) {
            TypedQuery<RepairOrder> q = theRepairOrderDAO.getTheEntityManager().createQuery("select r from RepairOrder r ", RepairOrder.class);
            q.setFirstResult((pageNumber - 1) * pageSize);
            q.setMaxResults(pageSize);
            List<RepairOrder> l = q.getResultList();

            return l;
        }
        return null;
    }

}
