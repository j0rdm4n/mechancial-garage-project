/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.controller.common.IFormCommand;
import ma.esolutions.projet.gestion.garage.model.Item;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.view.InventoryFormView;
import ma.esolutions.projet.gestion.garage.view.InventoryListView;

/**
 *
 * @author E-solutions
 */
public class InventoryUpdateCommand extends InventoryCommand implements IFormCommand<Item>{
    private InventoryFormView theInventoryFormView;
    private InventoryListView theInventoryListView;

    public InventoryUpdateCommand(Item item) {
        super(item);
    }

      @Override
    public List<Item> getData(String filter,int pageNumber,int pageSize)
    {
       /* if(pageNumber > lastPage(countPages(),pageSize))
        {
            return null;
        }*/
        if(filter.equals(""))
        {
            TypedQuery<Item> q = theItemDAO.getTheEntityManager().createQuery("select c from Item c ", Item.class);
            q.setFirstResult((pageNumber -  1)*pageSize);
            q.setMaxResults(pageSize);
            List<Item> l = q.getResultList();
          
            return l;
        }
        return null;
    }

    @Override
    public void execute_body() {
        System.out.println(" executing the inventory update command"); 
         setTheInventoryPlaces();
        setTheInventoryItems();
         theInventoryFormView = new InventoryFormView(theItem,this.theInventoryPlaces,this.theInventoryItems);
         theInventoryFormView.setTheControllerCommand(this);
         theInventoryListView = ApplicationController.getAppView().getInventoryListView();
        theInventoryFormView.setTheListView(theInventoryListView);
         theInventoryFormView.showDialog();
    }

    @Override
    public void setTheRequiredPermission() {
        this.theRequiredPermission = PermissionsCodes.PERMISSION_MODIFY_INVENTORY;
    }
    @Override
    public void save(Item e)
    {
        this.theItemDAO.update(e);
        System.out.println("updating the item " + e +"q : "+e.getTheItemQuantity());
        
    }
    @Override
    public void undo() {
    }
    
}
