/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.List;
import java.util.Map;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.Payment;
import ma.esolutions.projet.gestion.garage.model.PaymentDAO;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.view.ApplicationView;
import ma.esolutions.projet.gestion.garage.view.PaymentsListView;

/**
 *
 * @author E-solutions
 */
public class ListPaymentsCommand extends SecureCommand<Payment> {

    private PaymentsListView thePaymentsListView;
    private final PaymentDAO thePaymentDAO;
    private int dataSize;

    public ListPaymentsCommand() {
        thePaymentDAO = new PaymentDAO();
    }

    @Override
    public List<Payment> getData(String filter, int start, int end) {

        if (filter.equals("")) {
            System.out.println("getting data");
            TypedQuery<Payment> q = thePaymentDAO.getTheEntityManager().createQuery("select p from Payment p ", Payment.class);
            q.setFirstResult(start);
            q.setMaxResults(end);
            List<Payment> l = q.getResultList();
            /* l.forEach((c)->{
             System.out.println("customer  : " + c );
             });*/

            return l;
        } else {
            Map<String, String[]> parsed = parseFilter(filter);
            String customerTxt = parsed.get("theCustomer")[1];
            String objettxt = parsed.get("thePaymentType")[1];
            //String[] fl = customerTxt.split(" ");
            String by = customerTxt;
            System.out.println(" payement by " + by);
            String sql = "";
            TypedQuery<Payment> q;
            if (objettxt.equals("tous")) {
                sql = "select p from Payment p where p.theBy like :by ";
                q = this.thePaymentDAO.getTheEntityManager().createQuery(sql, Payment.class);
                // q.setParameter("firstname", "%" + firstname + "%");
                q.setParameter("by", "%" + by + "%");

            } else {
                System.out.println(" type of payments q " + objettxt);
                sql = "select p from Payment p where p.theBy like :by and p.thePaymentType like :type";
                q = this.thePaymentDAO.getTheEntityManager().createQuery(sql, Payment.class);
                q.setParameter("by", "%" + by + "%");

                q.setParameter("type", "%" + objettxt + "%");
            }
            List rl = q.getResultList();
            dataSize = rl.size();
            return q.getResultList();
        }
    }

    @Override
    public int getDataSize(String filter) {

        if (filter.equalsIgnoreCase("")) {
            return thePaymentDAO.all().size();
        }
        else
        {
            return dataSize;
        }
    }

    @Override
    public void execute_body() {
        thePaymentsListView = new PaymentsListView();
        thePaymentsListView.setTheInvokingCommand(this);
        thePaymentsListView.refreshData();
        updateView();
        setSuccessStatus();

    }

    private void updateView() {
        ApplicationView.showListView(thePaymentsListView);

    }

    @Override
    public void setTheRequiredPermission() {
        this.theRequiredPermission = PermissionsCodes.PERMISSION_LIST_PAYMENTS;
    }

    @Override
    public void undo() {
    }

}
