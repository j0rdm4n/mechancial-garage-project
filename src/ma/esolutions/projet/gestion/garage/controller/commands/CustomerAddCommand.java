/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.ArrayList;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.model.Customer;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.view.CustomerFormView;

/**
 *
 * @author E-solutions
 */
public class CustomerAddCommand extends CustomerCommand{
    
    private CustomerFormView theCustomerFormView;
    
    public CustomerAddCommand(Customer customer)
    {
        super(customer);
        theCustomersList = new ArrayList<Customer>();   
    }
     @Override
    public void setTheRequiredPermission() {
        theRequiredPermission = PermissionsCodes.PERMISSION_ADD_CUSTOMERS;
    }
    @Override
    public void execute_body() {
        
            theCustomerFormView = new CustomerFormView(new Customer());
            theCustomerFormView.setTheControllerCommand(this);
            theCustomersListView = ApplicationController.getAppView().getTheCustomersListView();
            theCustomerFormView.setTheListView(theCustomersListView);
            theCustomerFormView.showDialog();
            
            
            
            
        
        /*   theCustomersList = theCustomerFormView.getEntitities();
           Iterator<Customer> it = theCustomersList.iterator();
           System.out.println(theCustomersList);
           theCustomersList.forEach((customer)->{
               System.out.println("elemene : "+customer.getTheFirstname());
               if( ! customerExists(customer))
                   {
                       theCustomerDAO.create(customer);
                       System.out.println("le client " + customer + " est crée");
                   }
           });*/
           
          // ApplicationController.getAppView().refreshCurrentView();
          // if(theCustomersListView != null)
            //    theCustomersListView.refreshData(getData("",1,100));
           
       /*    while(it.hasNext())
           {
               Customer c = it.next();
               if( ! customerExists(c))
              {
                theCustomerDAO.create(c);
                
              }
               System.out.println("client "+it.next().getTheFirstname()+" crée");
           }*/
           /*if(theCustomer != null)
           {
              // JOptionPane.showConfirmDialog(null, "l'opération a été faite avec succés !");
              if( ! customerExists(theCustomer))
              {
                theCustomerDAO.create(theCustomer);
                System.out.println("client nn trouvé");
              }
              else
              {
                  System.out.println("client existant !");
              }
           }
           else
           {
               JOptionPane.showConfirmDialog(null, "Une erreur s'est produite ", "Erreur",JOptionPane.OK_OPTION);
           }*/
            setSuccessStatus();
            
    }

    @Override
    public void undo() {
            theCustomerDAO.remove(theCustomer);
            setSuccessStatus();    
    }


    @Override
    public void after() {
        return;
    }

    @Override
    public void save(Customer e) {
        theCustomerDAO.create(e);
        System.out.println("customer saved !");
    }

   

    



   

   


   
}
