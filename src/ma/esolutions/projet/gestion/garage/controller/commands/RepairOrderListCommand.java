/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.List;
import java.util.Map;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.model.Item;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.model.RepairOrder;
import ma.esolutions.projet.gestion.garage.view.ApplicationView;
import ma.esolutions.projet.gestion.garage.view.RepairOrderListView;

/**
 *
 * @author E-solutions
 */
public class RepairOrderListCommand extends RepairOrderCommand {

    RepairOrderListView theRepairOrderListView;

    public RepairOrderListCommand(RepairOrder order) {
        super(order);
        theRepairOrderListView = new RepairOrderListView(getData("", 1, 10));
    }
    public int getDataSize(String filter)
    {
        return theRepairOrderDAO.all().size();
    }
    @Override
    public List<RepairOrder> getData(String filter, int start, int end) {
        if (filter.equals("")) {
            TypedQuery<RepairOrder> q = theRepairOrderDAO.getTheEntityManager().createQuery("select r from RepairOrder r ", RepairOrder.class);
            q.setFirstResult(start);
            q.setMaxResults(end);
            List<RepairOrder> l = q.getResultList();
            /* l.forEach((i)->{
             System.out.println("item  : " + i );
             });*/

            return l;
        }
    else
        {
                      Map<String, String[]> parsed = parseFilter(filter);
            String sql = "select s from RepairOrder s where s.theCustomer.theFirstname like :firstname and theCustomer.theLastname like :lastname";
            TypedQuery<RepairOrder> q = theRepairOrderDAO.getTheEntityManager().createQuery(sql, RepairOrder.class);

            String[] firstlast = parsed.get("theCustomer")[1].split(" ");
            String firstname = "";
            String lastname = "";
            try {

                firstname = firstlast[0];
                lastname = firstlast[1];
            } catch (Exception e) {
            }

            q.setParameter("firstname", "%"+firstname+"%");
            q.setParameter("lastname", "%"+lastname+"%");
            System.out.println("serach for client firstname " + firstname);
            System.out.println("serach for client lastname " + lastname);
            System.out.println(" " + q.getResultList());
            return q.getResultList();
        }
    }

    @Override
    public void execute_body() {
        theRepairOrderListView.setTheInvokingCommand(this);
        this.theRepairOrderListView.refreshData();
        updateView();
        setSuccessStatus();
    }

    @Override
    public void setTheRequiredPermission() {
        this.theRequiredPermission = PermissionsCodes.PERMISSION_LIST_REPAIRS;
    }

    @Override
    public void undo() {
    }

    private void updateView() {
        ApplicationView.showListView(theRepairOrderListView);

    }

}
