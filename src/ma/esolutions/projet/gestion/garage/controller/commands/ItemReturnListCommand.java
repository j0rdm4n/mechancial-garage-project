/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.List;
import java.util.Map;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.model.Return;
import ma.esolutions.projet.gestion.garage.model.ReturnDAO;
import ma.esolutions.projet.gestion.garage.view.ApplicationView;
import ma.esolutions.projet.gestion.garage.view.ReturnListView;

/**
 *
 * @author E-solutions
 */
public class ItemReturnListCommand extends SecureCommand<Return> {

    private final ReturnDAO theReturnDAO;
    private ReturnListView theReturnListView;

    public ItemReturnListCommand() {
        super();
        theReturnDAO = new ReturnDAO();
    }

    @Override
    public List<Return> getData(String filter, int start, int end) {

        if (filter.equals("")) {
            System.out.println("getting data");
            TypedQuery<Return> q = theReturnDAO.getTheEntityManager().createQuery("select r from Return r ", Return.class);
            q.setFirstResult(start);
            q.setMaxResults(end);
            List<Return> l = q.getResultList();

            return l;
        } else {
            Map<String, String[]> parsed = parseFilter(filter);
            String customerTxt = "";
            String itemtxt = "";
            String firstname ="";
            String lastname="";
            try {

                customerTxt = parsed.get("theCustomer")[1];
                String[] fl = customerTxt.split(" ");
                firstname = fl[0];
                lastname = fl[1];
                itemtxt = parsed.get("theItem")[1];

            } catch (Exception e) {
            }
            String sql = "select r from Return r where r.theCustomer.theFirstname like :firstname and r.theCustomer.theLastname like :lastname and r.theItem.theItemName like :item ";
            TypedQuery<Return> q = this.theReturnDAO.getTheEntityManager().createQuery(sql, Return.class);
            System.out.println(" firstname " + firstname +"lastname " + lastname + " item " + itemtxt);
            q.setParameter("firstname", "%"+firstname+ "%");
            q.setParameter("lastname", "%"+lastname+"%");
            q.setParameter("item", "%"+itemtxt+"%");
            return q.getResultList();
        }

    }

    @Override
    public void execute_body() {
        theReturnListView = new ReturnListView();
        theReturnListView.setTheInvokingCommand(this);
        theReturnListView.refreshData();
        updateView();
        setSuccessStatus();
    }

    private void updateView() {
        ApplicationView.showListView(theReturnListView);

    }

    @Override
    public void setTheRequiredPermission() {
        this.theRequiredPermission = PermissionsCodes.PERMISSION_LIST_RETURNS;
    }

    @Override
    public void undo() {
    }

}
