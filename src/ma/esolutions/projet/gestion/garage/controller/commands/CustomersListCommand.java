/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.List;
import java.util.Map;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.swing.JComponent;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.Customer;
import ma.esolutions.projet.gestion.garage.model.CustomerDAO;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.view.ApplicationView;
import ma.esolutions.projet.gestion.garage.view.CustomerListView;

/**
 *
 * @author E-solutions
 */
public class CustomersListCommand extends SecureCommand<Customer>{
    
    private final Customer theCustomer;
    private final CustomerDAO theCustomerDAO;
    private final CustomerListView theCustomersListView;
    //private ApplicationView theApplicationView;
    private JComponent theMainPanel;
    public CustomersListCommand(Customer customer)
    {
        theCustomer = customer;
        theCustomerDAO = new CustomerDAO();
        theCustomersListView =  new CustomerListView();
        
        
        
       
    }

    public CustomerListView getTheCustomersListView() {
        return theCustomersListView;
    }
    private long countPages()
    {
        Query 
           q = theCustomerDAO.getTheEntityManager().createQuery("select count(c) from Customer c");
            return (long)q.getSingleResult();
    }
    private long lastPage(long count,int pageSize)
    {
        return (long) ((count / pageSize) + 1);
    }
    @Override
    public int getDataSize(String filter)
    {
        return this.theCustomerDAO.all().size();
    }
            
    @Override
    public List<Customer> getData(String filter,int start,int end)
    {
       /* if(pageNumber > lastPage(countPages(),pageSize))
        {
            return null;
        }*/
        if(filter.equals(""))
        {
            System.out.println("getting data");
            TypedQuery<Customer> q = theCustomerDAO.getTheEntityManager().createQuery("select c from Customer c ", Customer.class);
            q.setFirstResult(start);
            q.setMaxResults(end);
            List<Customer> l = q.getResultList();
           /* l.forEach((c)->{
                System.out.println("customer  : " + c );
            });*/
            
            return l;
        }
        else
        {
                 Map<String,String[]> parsed = parseFilter(filter);
            String customerName = parsed.get("theCustomerName")[1];
            String customerEmail = parsed.get("theCustomerEmail")[1];
            String customerCity = parsed.get("theCustomerCity")[1];
            String[] fl = customerName.split(" ");
            String firstname ="";String lastname="";
            try
            {
                firstname= fl[0];
                lastname = fl[1];
            }catch(Exception e)
            {
                
            }
            String sql = "select c from Customer c where c.theCity like :city and c.theFirstname like :firstname and c.theLastname like :lastname and c.theEmail like :email";
            TypedQuery<Customer> q = this.theCustomerDAO.getTheEntityManager().createQuery(sql, Customer.class);
            System.out.println("the vendor name " + customerName +" email " +customerEmail  +" city" + customerCity);
            q.setParameter("firstname", "%"+firstname+"%");
            q.setParameter("lastname", "%"+lastname+"%");
            q.setParameter("email","%"+customerEmail+"%");
            q.setParameter("city", "%"+customerCity+"%");
            
            return q.getResultList();
        }
        
    }
    @Override
    public void execute_body() 
    {
        
        theCustomersListView.setTheInvokingCommand(this);
        theCustomersListView.refreshData();
        //theCustomerDAO.create(theCustomer);
        updateView();
        setSuccessStatus();
    }   
    private void updateView()
    {
         ApplicationView.showListView(theCustomersListView);
        
    }
  
    @Override
    public void undo() {
        theCustomerDAO.remove(theCustomer);
        setSuccessStatus();
    }



    

    @Override
    public void after() {
        return;
    }

    @Override
    public void setTheRequiredPermission() {
            theRequiredPermission = PermissionsCodes.PERMISSION_LIST_CUSTOMERS;
    }

    @Override
    public void before() {
        return;
    }
    
}
