/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import javax.swing.ImageIcon;
import ma.esolutions.projet.gestion.garage.app.Session;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.controller.common.ICommand;
import ma.esolutions.projet.gestion.garage.model.User;
import ma.esolutions.projet.gestion.garage.model.UserDAO;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.ICONS22;
import static ma.esolutions.projet.gestion.garage.view.ApplicationView.IMAGES;
import ma.esolutions.projet.gestion.garage.view.SwitchAccountFormView;

/**
 *
 * @author E-solutions
 */
public class SwitchAccountCommand implements ICommand {

    private SwitchAccountFormView theSwitchAccountFormView ;
    private User theCurrentUser;
    private User theTargetUser;
    private  UserDAO theUserDAO;
    public SwitchAccountCommand()
    {
        theSwitchAccountFormView = new SwitchAccountFormView();
        theUserDAO = new UserDAO();
        theCurrentUser = Session.getSession().getTheCurrentUser();
    
    }
    
    @Override
    public void execute() {

        theSwitchAccountFormView.showDialog();
        theTargetUser = theSwitchAccountFormView.getEntity();
        String targetUsername = theTargetUser.getTheUsername();
        String targetPassword = theTargetUser.getThePassword();
        System.out.println("target username"+targetUsername + " password "+targetPassword);
   /*     if(theCurrentUser != null)
        {
             if(theCurrentUser.getTheUsername().equalsIgnoreCase(targetUsername))
             {
                if(theCurrentUser.getThePassword().equalsIgnoreCase(targetPassword))
                {
                    return ;
                }
              }
        }*/
        System.out.println("the target username"+ theTargetUser.getTheUsername() );
        System.out.println("the target password "+ theTargetUser.getThePassword());
        theTargetUser = theUserDAO.checkIfUserExists(theTargetUser.getTheUsername(), theTargetUser.getThePassword());
        System.out.println("the target user "+ theTargetUser);
        if(theTargetUser != null)
        {
                    Session.getSession().setTheCurrentUser(theTargetUser);
                    Session.getSession().setTheLoginStatus(true);
                    System.out.println("the current user is " + Session.getSession().getTheCurrentUser());
                    notifyStatusBar();
                    notifyStatusBar(ICONS22+"luser.png",ICONS22+"connected.png");
        }
        return;
    }
    @Override
    public void undo() {
    }

    private void notifyStatusBar() {
      ApplicationController.getAppView().notifyStatusBar("Connecté",Session.getSession().getTheCurrentUser().toString());
    }

    private void notifyStatusBar(String user, String status) {
      ApplicationController.getAppView().notifyStatusBar(new ImageIcon(getClass().getResource(user)),new ImageIcon(getClass().getResource(status)));
    
    }
    
}
