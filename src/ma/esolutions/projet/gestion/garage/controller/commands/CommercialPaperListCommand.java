/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.List;
import java.util.Map;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.CommercialPaper;
import ma.esolutions.projet.gestion.garage.model.CommercialPaperDAO;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.view.ApplicationView;
import ma.esolutions.projet.gestion.garage.view.CommercialPaperListView;

/**
 *
 * @author E-solutions
 */
public class CommercialPaperListCommand extends SecureCommand<CommercialPaper> {

    private CommercialPaperListView commercialPaperListView;
    private final CommercialPaperDAO commercailPaperDAO;
    private CommercialPaperDAO commercialPaperDAO;

    public CommercialPaperListCommand()
    {
        commercailPaperDAO  = new CommercialPaperDAO();
    }
    @Override
    public int getDataSize(String filter)
    {
        commercialPaperDAO = new CommercialPaperDAO();
        return commercialPaperDAO.all().size();
    }
    @Override
    public List<CommercialPaper> getData(String filter, int start, int end) {
        if(filter.equals(""))
        {
                String sql ="select p from CommercialPaper p";
                TypedQuery query = this.commercailPaperDAO.getTheEntityManager().createQuery(sql, CommercialPaper.class);
                query.setFirstResult(start);
                query.setMaxResults(end);
                return query.getResultList();
        }
        else
        {
            Map<String,String[]> parsed = parseFilter(filter);
            String vendor = parsed.get("vendor")[1];
            
            
            return null;
            
        
        }
        
            
    }

    @Override
    public void execute_body() {
      commercialPaperListView = new CommercialPaperListView();

        commercialPaperListView.setTheInvokingCommand(this);
        commercialPaperListView.refreshData();
        updateView();
    }

    private void updateView() {
        ApplicationView.showListView(commercialPaperListView);
    }

    @Override
    public void setTheRequiredPermission() {
        this.theRequiredPermission = PermissionsCodes.PERMISSION_LIST_COMMERCIALPAPER;
    }

    @Override
    public void undo() {
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
