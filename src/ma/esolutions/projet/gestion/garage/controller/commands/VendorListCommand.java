/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.model.Vendor;
import ma.esolutions.projet.gestion.garage.model.VendorDAO;
import ma.esolutions.projet.gestion.garage.view.ApplicationView;
import ma.esolutions.projet.gestion.garage.view.VendorListView;

/**
 *
 * @author E-solutions
 */
public class VendorListCommand extends VendorCommand{

    VendorListView theVendorListView;
    VendorDAO theVendorDAO;
    
    public VendorListCommand()
    {
        theVendorDAO = new VendorDAO();
        theVendorListView = new VendorListView(getData("",1,100));
    }
    @Override
    public void execute_body() {
        theVendorListView.setTheInvokingCommand(this);
        theVendorListView.refreshData();
        updateView();
        setSuccessStatus();
    }

    @Override
    public void undo() {
    }

    @Override
    public int getStatus() {
        return 0;
    }

    @Override
    public void setTheRequiredPermission() {
        theRequiredPermission = PermissionsCodes.PERMISSION_LIST_VENDORS;
    }

    private void updateView() {
        ApplicationView.showListView(theVendorListView);
    }

    @Override
    public void save(Vendor e) {
    }




    
}
