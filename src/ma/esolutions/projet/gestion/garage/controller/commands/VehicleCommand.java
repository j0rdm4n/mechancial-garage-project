/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.List;
import java.util.Map;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.Vehicle;
import ma.esolutions.projet.gestion.garage.model.VehicleDAO;
import ma.esolutions.projet.gestion.garage.model.Vendor;

/**
 *
 * @author Ahmed
 */
public abstract class VehicleCommand extends SecureCommand<Vehicle> {
    private final VehicleDAO theVehicleDAO;

    public VehicleCommand() {
        theVehicleDAO = new VehicleDAO();
    }
    @Override
    public int getDataSize(String filter)
    {
        return theVehicleDAO.all().size();
    }
    @Override
    public List<Vehicle> getData(String filter,int start,int end)
    {
       
        if(filter.equals(""))
        {
            TypedQuery<Vehicle> q = theVehicleDAO.getTheEntityManager().createQuery("select v from Vehicle v ", Vehicle.class);
            q.setFirstResult(start);
            q.setMaxResults(end);
            List<Vehicle> l = q.getResultList();
          
            
            return l;
        }
        else
        {
            Map<String,String[]> parsed = parseFilter(filter);
            String clientName = parsed.get("theVehicleOwner")[1];
            String chassisNumber = parsed.get("theVehicleIdentificationNumber")[1];
            String[] fl = clientName.split(" ");
            String firstname = "";String lastname = "";
            
            try
            {
                firstname = fl[0];
                lastname = fl[1];
            }catch(Exception e){}
            String sql ="select v from Vehicle v where v.theVehicleOwner.theFirstname like :ownerfirst and v.theVehicleOwner.theLastname like :ownerlast and v.theVehicleIdentificationNumber like :num";
            
            TypedQuery q = this.theVehicleDAO.getTheEntityManager().createQuery(sql,Vehicle.class);
            q.setParameter("ownerfirst","%"+firstname+"%" );
            q.setParameter("ownerlast", "%"+lastname+"%");
            q.setParameter("num", "%"+chassisNumber+"%");
            
            return q.getResultList();
        }
       
    }
    
}
