/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.List;
import ma.esolutions.projet.gestion.garage.controller.common.IFormCommand;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.controller.common.SecureFormCommand;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.model.Vehicle;
import ma.esolutions.projet.gestion.garage.model.VehicleDAO;
import ma.esolutions.projet.gestion.garage.view.VehicleFormView;

/**
 *
 * @author E-solutions
 */
public class VehicleAddCommand extends VehicleCommand implements IFormCommand<Vehicle> {

    private VehicleFormView theVehicleFormView ;
    private Vehicle theVehicle;
    private List<Vehicle> theVehicles;
    private VehicleDAO theVehicleDAO; 
    public VehicleAddCommand(Vehicle vehicle)
    {
        super();
        theVehicle = vehicle;
        theVehicleDAO = new VehicleDAO();
        theVehicleFormView = new VehicleFormView(theVehicle);
    }
    @Override
    public void execute_body() {
        
        theVehicleFormView.setTheControllerCommand(this);
        theVehicleFormView.setTheControllerCommand(this);
        
        theVehicleFormView.showDialog();
        
      /*  theVehicles = theVehicleFormView.getEntities();
        theVehicles.forEach((e)->{
            theVehicleDAO.create(e);
        });*/
        setSuccessStatus();
    }

    @Override
    public void setTheRequiredPermission() {
        theRequiredPermission = PermissionsCodes.PERMISSION_ADD_VEHICLE;
    }

    @Override
    public void undo() {
    }

   

    @Override
    public void save(Vehicle e) {
         theVehicleDAO.create(e);

    }

    
}
