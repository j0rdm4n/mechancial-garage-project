/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.List;
import java.util.Map;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.controller.common.SecureFormCommand;
import ma.esolutions.projet.gestion.garage.model.Vendor;
import ma.esolutions.projet.gestion.garage.model.VendorDAO;

/**
 *
 * @author born2code
 */
public abstract class VendorCommand extends SecureFormCommand<Vendor> {
    private final VendorDAO theVendorDAO;

    public VendorCommand() {
        theVendorDAO = new VendorDAO();
    }

    @Override
    public int getStatus() {
        return 0;
    }
    @Override
    public int getDataSize(String filter)
    {
        return this.theVendorDAO.all().size();
    }
    @Override
    public List<Vendor> getData(String filter, int start, int end) {
        if (filter.equals("")) {
            TypedQuery<Vendor> q = theVendorDAO.getTheEntityManager().createQuery("select v from Vendor v ", Vendor.class);
            q.setFirstResult(start);
            q.setMaxResults(end);
            List<Vendor> l = q.getResultList();
            return l;
        }
        else
        {
            Map<String,String[]> parsed = parseFilter(filter);
            String vendorName = parsed.get("theVendorName")[1];
            String vendorEmail = parsed.get("theVendorEmail")[1];
            String vendorCity = parsed.get("theVendorCity")[1];
            
            String sql = "select v from Vendor v where v.theVendorName like :name and v.theVendorCity like :city and v.theVendorEmail like :email";
            TypedQuery<Vendor> q = this.theVendorDAO.getTheEntityManager().createQuery(sql, Vendor.class);
            System.out.println("the vendor name " + vendorName +" email " +vendorEmail  +" city" + vendorCity);
            q.setParameter("name", "%"+vendorName+"%");
            q.setParameter("email","%"+vendorEmail+"%");
            q.setParameter("city", "%"+vendorCity+"%");
            
            return q.getResultList();
        }
    }
    
}
