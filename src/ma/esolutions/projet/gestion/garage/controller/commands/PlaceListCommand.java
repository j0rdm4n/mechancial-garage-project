/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.List;
import java.util.Map;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.Item;
import ma.esolutions.projet.gestion.garage.model.ItemPlace;
import ma.esolutions.projet.gestion.garage.model.ItemPlaceDAO;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.view.ApplicationView;
import ma.esolutions.projet.gestion.garage.view.PlaceListView;

/**
 *
 * @author E-solutions
 */
public class PlaceListCommand extends SecureCommand<ItemPlace> {

    private PlaceListView thePlaceListView;
    private ItemPlaceDAO theItemPlaceDAO;

    public PlaceListCommand() {
        super();
    }

    @Override
    public List<ItemPlace> getData(String filter, int start, int end) {
        if (filter.equals("")) {
            System.out.println("getting data");
            TypedQuery<ItemPlace> q = theItemPlaceDAO.getTheEntityManager().createQuery("select c from ItemPlace c ", ItemPlace.class);
            q.setFirstResult(start);
            q.setMaxResults(end);
            List<ItemPlace> l = q.getResultList();

            return l;
        }
        else
        {
             Map<String, String[]> parsed = parseFilter(filter);
            String nameTxt = parsed.get("theItemPlaceName")[1];
            String sql = "select p from ItemPlace p where p.theItemPlaceName like :name";
            
            TypedQuery<ItemPlace> q = this.theItemPlaceDAO.getTheEntityManager().createQuery(sql,ItemPlace.class);
            q.setParameter("name", "%"+nameTxt+"%");
            System.out.println("name text : " + nameTxt);
            return q.getResultList();
            
        }
        
    }

    @Override
    public void execute_body() {
        theItemPlaceDAO = new ItemPlaceDAO();
        thePlaceListView = new PlaceListView(getData("", 1, 1000));
        thePlaceListView.setTheInvokingCommand(this);
        thePlaceListView.refreshData();
        updateView();
    }
 @Override
    public int getDataSize(String filter) {
        
        System.out.println("data size " + theItemPlaceDAO.all().size());
        return theItemPlaceDAO.all().size();
    }
    private void updateView() {
        ApplicationView.showListView(thePlaceListView);

    }

    @Override
    public void setTheRequiredPermission() {
        this.theRequiredPermission = PermissionsCodes.PERMISSION_LIST_PLACES;
    }

    @Override
    public void undo() {
    }

    public PlaceListView getThePlaceListView() {
        return this.thePlaceListView;
    }

}
