/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.List;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.controller.common.SecureFormCommand;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.model.PurchaseOrder;
import ma.esolutions.projet.gestion.garage.model.PurchaseOrderDAO;
import ma.esolutions.projet.gestion.garage.view.PurchaseOrderFormView;

/**
 *
 * @author E-solutions
 */
public class PurchaseOrderOpenCommand extends SecureFormCommand<PurchaseOrder>{
    private PurchaseOrderFormView thePurchaseOrderFormView;
    private PurchaseOrderDAO thePurchaseOrderDAO;
    private final PurchaseOrder thePurchaseOrder;

    public PurchaseOrderOpenCommand(PurchaseOrder po) {
        thePurchaseOrder = po;
    }

    @Override
    public List<PurchaseOrder> getData(String filter, int pageNumber, int pageSize) {
        if (filter.equals("")) {
            TypedQuery<PurchaseOrder> q = thePurchaseOrderDAO.getTheEntityManager().createQuery("select s from PurchaseOrder s ", PurchaseOrder.class);
            q.setFirstResult((pageNumber - 1) * pageSize);
            q.setMaxResults(pageSize);
            List<PurchaseOrder> l = q.getResultList();

            return l;
        }
        return null;
    }


    @Override
    public void execute_body() {
        thePurchaseOrderFormView = new PurchaseOrderFormView(thePurchaseOrder);
        
        thePurchaseOrderFormView.setTheControllerCommand(this);
        thePurchaseOrderFormView.showDialog();
        
    }

    @Override
    public void setTheRequiredPermission() {
        this.theRequiredPermission = PermissionsCodes.PERMISSION_ADD_PURCHASES;
    }

    @Override
    public void undo() {
    }

   
    @Override
    public void save(PurchaseOrder e) {
        thePurchaseOrderDAO = new PurchaseOrderDAO();
        thePurchaseOrderDAO.create(e);
        
        thePurchaseOrderFormView.setStatusbarMsg("Commande N° PO"+e.getId()+" est ajouté avec succés");
    }
    
}
