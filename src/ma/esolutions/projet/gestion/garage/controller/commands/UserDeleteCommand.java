/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import ma.esolutions.projet.gestion.garage.controller.common.ICommand;
import ma.esolutions.projet.gestion.garage.model.Permission;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.model.User;

/**
 *
 * @author E-solutions
 */
public class UserDeleteCommand extends UserCommand{

    public UserDeleteCommand(User user) {
        super(user);
    }

    @Override
    public void setTheRequiredPermission() {
        theRequiredPermission = PermissionsCodes.PERMISSION_DELETE_CUSTOMERS;
    }

    @Override
    public void execute_body() {
        
        
    }


    @Override
    public void undo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
}
