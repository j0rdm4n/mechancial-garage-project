/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.model.Permission;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.model.SalesOrder;
import ma.esolutions.projet.gestion.garage.view.ApplicationView;
import ma.esolutions.projet.gestion.garage.view.SalesOrderListView;

/**
 *
 * @author E-solutions
 */
public class SalesOrderListCommand extends SalesOrderCommand {

    SalesOrderListView theSalesOrderListView;

    public SalesOrderListCommand() {
        super(null);
        theSalesOrderListView = new SalesOrderListView(getData("", 1, 100));
    }

    @Override
    public List<SalesOrder> getData(String filter, int start, int end) {
        if (filter.equals("")) {
            TypedQuery<SalesOrder> q = theSalesOrderDAO.getTheEntityManager().createQuery("select s from SalesOrder s ", SalesOrder.class);
            q.setFirstResult(start);
            q.setMaxResults(end);
            List<SalesOrder> l = q.getResultList();

            return l;
        } else {
            Map<String, String[]> parsed = parseFilter(filter);
            String sql = "select s from SalesOrder s where s.theCustomer.theFirstname like :firstname and theCustomer.theLastname like :lastname";
            TypedQuery<SalesOrder> q = theSalesOrderDAO.getTheEntityManager().createQuery(sql, SalesOrder.class);

            String[] firstlast = parsed.get("theCustomer")[1].split(" ");
            String firstname = "";
            String lastname = "";
            try {

                firstname = firstlast[0];
                lastname = firstlast[1];
            } catch (Exception e) {
            }

            q.setParameter("firstname", "%"+firstname+"%");
            q.setParameter("lastname", "%"+lastname+"%");
            System.out.println("serach for client firstname " + firstname);
            System.out.println("serach for client lastname " + lastname);
            System.out.println(" " + q.getResultList());
            return q.getResultList();
        }

    }

    @Override
    public void setTheRequiredPermission() {
        theRequiredPermission = PermissionsCodes.PERMISSION_LIST_SALES;
    }

    @Override
    public int getDataSize(String filter) {
        return theSalesOrderDAO.all().size();
    }

    @Override
    public void execute_body() {
        theSalesOrderListView.setTheInvokingCommand(this);
        theSalesOrderListView.refreshData();
        updateView();
        setSuccessStatus();
    }

    @Override
    public void undo() {
        return;
    }

    private void updateView() {
        ApplicationView.showListView(theSalesOrderListView);
    }

}
