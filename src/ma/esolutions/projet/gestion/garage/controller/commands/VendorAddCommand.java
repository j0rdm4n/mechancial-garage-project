/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.model.Vendor;
import ma.esolutions.projet.gestion.garage.model.VendorDAO;
import ma.esolutions.projet.gestion.garage.view.VendorFormView;

/**
 *
 * @author E-solutions
 */
public class VendorAddCommand extends VendorCommand{

    private Vendor theVendor;
    private VendorDAO theVendorDAO;
    private VendorFormView theVendorFormView;
    public VendorAddCommand(Vendor vendor)
    {
        theVendor = vendor;
        
        
    }
    @Override
    public void execute_body() {
        theVendorFormView = new VendorFormView(theVendor);
        theVendorFormView.setTheControllerCommand(this);
        theVendorDAO = new VendorDAO();
        theVendorFormView.showDialog();

    }

    @Override
    public void undo() {
    }


    @Override
    public void setTheRequiredPermission() {
        theRequiredPermission  = PermissionsCodes.PERMISSION_ADD_VENDORS;
    }

   
    @Override
    public void save(Vendor e) {
        theVendorDAO.create(e);
    }

    
}
