/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.List;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.controller.common.ICommand;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.Customer;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.model.User;
import ma.esolutions.projet.gestion.garage.model.UserDAO;
import ma.esolutions.projet.gestion.garage.view.ApplicationView;
import ma.esolutions.projet.gestion.garage.view.UserListView;

/**
 *
 * @author E-solutions
 */
public class UserListCommand extends UserCommand{

    UserListView theUserListView ;
    UserDAO theUserDAO;
    public UserListCommand()
    {
        super();
        theUserDAO = new UserDAO();
        theUserListView = new UserListView(getData("",1,100));
    }
    @Override
    public void execute_body() {
        theUserListView.setTheInvokingCommand(this);
        theUserListView.refreshData();
        updateView();
        setSuccessStatus();
    
    }

    @Override
    public void undo() {
    }
    @Override
    public int getStatus() {
        return 0;
    }

    @Override
    public void setTheRequiredPermission() {
        theRequiredPermission = PermissionsCodes.PERMISSION_LIST_USERS;
    }

    private void updateView() {
       // ApplicationController.getAppView().showListView(theUserListView);
        ApplicationView.showListView(theUserListView);
    }

    
}
