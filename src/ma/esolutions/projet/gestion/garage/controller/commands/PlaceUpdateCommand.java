/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.List;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.controller.common.IFormCommand;
import ma.esolutions.projet.gestion.garage.controller.common.SecureFormCommand;
import ma.esolutions.projet.gestion.garage.model.ItemPlace;
import ma.esolutions.projet.gestion.garage.model.ItemPlaceDAO;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.view.PlaceFormView;
import ma.esolutions.projet.gestion.garage.view.PlaceListView;

/**
 *
 * @author E-solutions
 */
public class PlaceUpdateCommand extends SecureFormCommand<ItemPlace> implements IFormCommand<ItemPlace>{
    private ItemPlaceDAO theItemPlaceDAO;
    private PlaceFormView thePlaceFormView;
    private PlaceListView thePlaceListView;
    private final ItemPlace thePlace;

    public PlaceUpdateCommand(ItemPlace place)
    {
        super();
            theItemPlaceDAO = new ItemPlaceDAO();
    
            thePlace = place;
    }
     @Override
    public List<ItemPlace> getData(String filter, int pageNumber, int pageSize) {
             if(filter.equals(""))
        {
            System.out.println("getting data");
            TypedQuery<ItemPlace> q = theItemPlaceDAO.getTheEntityManager().createQuery("select c from ItemPlace c ", ItemPlace.class);
            q.setFirstResult((pageNumber -  1)*pageSize);
            q.setMaxResults(pageSize);
            List<ItemPlace> l = q.getResultList();
           
            
            return l;
        }
        return null;
    }

    @Override
    public void execute_body() {
        System.out.println("executing place update command");
           thePlaceFormView = new PlaceFormView(thePlace);
        thePlaceFormView.setTheControllerCommand(this);
         thePlaceListView = ApplicationController.getAppView().getThePlaceListView();
        thePlaceFormView.setTheListView(thePlaceListView);
        thePlaceFormView.showDialog();
   
    }

    @Override
    public void setTheRequiredPermission() {
        this.theRequiredPermission = PermissionsCodes.PERMISSION_MODIFY_PLACES;
    }

    @Override
    public void undo() {
    }

    @Override
    public void save(ItemPlace e) {
        System.out.println("updating "+e+" : "+e.getTheItemPlaceName() );
        theItemPlaceDAO.update(e);
    }
    
}
