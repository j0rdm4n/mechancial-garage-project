/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.List;
import javax.persistence.TypedQuery;
import ma.esolutions.projet.gestion.garage.controller.common.ICommand;
import ma.esolutions.projet.gestion.garage.controller.common.IFormCommand;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.Permission;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.model.User;
import ma.esolutions.projet.gestion.garage.model.UserDAO;
import ma.esolutions.projet.gestion.garage.view.UserFormView;

/**
 *
 * @author E-solutions
 */
public class UserAddCommand extends UserCommand implements IFormCommand<User>{
    UserFormView theUserFormView ;
     
    public UserAddCommand(User user){
        super(user);
    }
    
    @Override
    public List<User> getData(String filter,int pageNumber,int pageSize)
    {
       
        if(filter.equals(""))
        {
            TypedQuery<User> q = theUserDAO.getTheEntityManager().createQuery("select u from User u ", User.class);
            q.setFirstResult((pageNumber -  1)*pageSize);
            q.setMaxResults(pageSize);
            List<User> l = q.getResultList();
          
            
            return l;
        }
        return null;
    }

    @Override
    public void execute_body() {
        theUserFormView = new UserFormView(theUser);
        theUserFormView.setTheControllerCommand(this);
        theUserFormView.showDialog();
       /* List<User> users =  theUserFormView.getEntities();
        users.forEach((u)->{
           
            System.out.println("user to create " + u.getTheAttributedRoles());
            theUserDAO.create(u);
            
        
        });
        setSuccessStatus();
        theUserDAO.all().forEach((u)->{
            System.out.println("user " + u.getTheFirstname() +" has roles " + u.getTheAttributedRoles());
        });*/
    }

    @Override
    public void undo() {
        theUserDAO.remove(theUser);
        setSuccessStatus();
    }

   
    @Override
    public void setTheRequiredPermission() {
        theRequiredPermission = PermissionsCodes.PERMISSION_CREATE_USERS;
    }

   

    @Override
    public void save(User e) {
        if(e.getId() == null)
            theUserDAO.create(e);
        else
            theUserDAO.update(e);

    }
    
}
