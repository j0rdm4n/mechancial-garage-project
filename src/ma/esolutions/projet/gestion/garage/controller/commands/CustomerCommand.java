/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.swing.JOptionPane;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.controller.common.SecureFormCommand;
import ma.esolutions.projet.gestion.garage.model.Customer;
import ma.esolutions.projet.gestion.garage.model.CustomerDAO;
import ma.esolutions.projet.gestion.garage.view.CustomerListView;

/**
 *
 * @author E-solutions
 */
public abstract class CustomerCommand extends SecureFormCommand<Customer> {
    protected Customer theCustomer;
    protected CustomerDAO theCustomerDAO;
    boolean showPermissionsDlg = true;
    protected List<Customer> theCustomersList;
    protected CustomerListView theCustomersListView;
    public CustomerCommand(Customer customer) {
        theCustomer = customer;
        theCustomerDAO = new CustomerDAO();
    }

    @Override
    public List<Customer> getData(String filter, int pageNumber, int pageSize) {
        /* if(pageNumber > lastPage(countPages(),pageSize))
        {
        return null;
        }*/
        if (filter.equals("")) {
            TypedQuery<Customer> q = theCustomerDAO.getTheEntityManager().createQuery("select c from Customer c ", Customer.class);
            q.setFirstResult((pageNumber - 1) * pageSize);
            q.setMaxResults(pageSize);
            List<Customer> l = q.getResultList();
            /* l.forEach((c)->{
            System.out.println("customer  : " + c );
            });*/
            return l;
        }
        return null;
    }

    protected boolean customerExists(Customer theCustomer) {
        try {
            TypedQuery<Customer> query = theCustomerDAO.getTheEntityManager().createQuery("SELECT c FROM Customer c WHERE c.theFirstname = :firstname and c.theLastname = :lastname", Customer.class);
            query.setParameter("firstname", theCustomer.getTheFirstname()).setParameter("lastname", theCustomer.getTheLastname()).getSingleResult();
            return true;
        } catch (NoResultException e) {
            return false;
        }
    }

   
    
}
