/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.esolutions.projet.gestion.garage.controller.commands;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import ma.esolutions.projet.gestion.garage.app.Session;
import ma.esolutions.projet.gestion.garage.controller.common.Command;
import ma.esolutions.projet.gestion.garage.model.ItemDAO;
import ma.esolutions.projet.gestion.garage.model.Permission;
import ma.esolutions.projet.gestion.garage.model.PermissionDAO;
import ma.esolutions.projet.gestion.garage.model.Role;
import ma.esolutions.projet.gestion.garage.model.RoleDAO;
import ma.esolutions.projet.gestion.garage.model.User;
import ma.esolutions.projet.gestion.garage.model.UserDAO;

/**
 *
 * @author E-solutions
 */
public class CreateSuperUserCommand extends Command {

    private UserDAO theUserDAO;
    private User theUser;
    RoleDAO theRoleDAO;
    Role theRole;
    private String theUsername;
    private String thePassword;

    public CreateSuperUserCommand(String username, String password) {

        theUserDAO = new UserDAO();
        theRoleDAO = new RoleDAO();
        theUser = new User();
        theRole = new Role();
        theUsername = username;
        thePassword = password;

    }

    @Override
    public void execute() {
        // System.out.println("executing the CreateSuperUserCommand");
        if (!Session.getSession().isTheFirstTimeRun()) {

            return;
        }
        PermissionDAO.createPermissions();

        theUser.setTheUsername(theUsername);
        theUser.setThePassword(thePassword);
        theRole.setTheRoleName("Admin");
        theRole.setTheRolePermissions(new ArrayList((new PermissionDAO()).all()));

        theRoleDAO.create(theRole);
        theUserDAO.create(theUser);

    }

    @Override
    public void undo() {
        theUserDAO.remove(theUser);
    }

    @Override
    public void before() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void after() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setTheRequiredPermission() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean checkIfUserHasPermission() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
