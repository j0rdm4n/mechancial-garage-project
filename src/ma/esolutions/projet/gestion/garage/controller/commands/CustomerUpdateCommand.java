/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.esolutions.projet.gestion.garage.controller.commands;

import ma.esolutions.projet.gestion.garage.controller.ApplicationController;
import ma.esolutions.projet.gestion.garage.controller.common.SecureCommand;
import ma.esolutions.projet.gestion.garage.model.Customer;
import ma.esolutions.projet.gestion.garage.model.PermissionsCodes;
import ma.esolutions.projet.gestion.garage.view.CustomerFormView;

/**
 *
 * @author E-solutions
 */
public class CustomerUpdateCommand extends CustomerCommand {
    
    public CustomerUpdateCommand(Customer customer) {
        super(customer);
        
    }

    @Override
    public void execute_body() {
        
        CustomerFormView theCustomerFormView = new CustomerFormView(theCustomer);
         theCustomerFormView.setTheControllerCommand(this);
            theCustomersListView = ApplicationController.getAppView().getTheCustomersListView();
            theCustomerFormView.setTheListView(theCustomersListView);
        theCustomerFormView.showDialog();
       // Customer newCustomer = theCustomerFormView.getEntity();
      //  theCustomerDAO.update(newCustomer);
        setSuccessStatus();
    }

    @Override
    public void setTheRequiredPermission() {
        this.theRequiredPermission = PermissionsCodes.PERMISSION_MODIFY_CUSTOMERS;
    }

    @Override
    public void undo() {
        theCustomerDAO.update(theCustomer);
        setSuccessStatus();
    }

    @Override
    public void save(Customer e) {
        theCustomerDAO.update(e);
    }
    
}
